package com.dcode.gacinventory.common;

import android.text.format.DateUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StagingValidation {
    public StagingValidation()
    {

    }

    public boolean _isCoMan(List<GACDynFuncs> gacFuncList , String RefCode, String CallBatchNo)
    {
        GACDynFuncs matchingfunc = gacFuncList.stream().
                filter(p -> p.getFUNCTION_ID().equals("XML_GENERATION_N_FILE_TRANSFER")
                        && p.getREF_CODE().equals(RefCode)).
                findAny().orElse(null);
        if (matchingfunc != null && CallBatchNo.equals("Y")) {
            return  true;
        }
        else
        {
            return  false;
        }

    }

    public ValidRespSet xxMfgDateDisable(List<GACDynFuncs> gacFuncList , String RefCode,  String Mandate,String Axis, String ProdTye)
    {
        ValidRespSet obret = new ValidRespSet();
        try
        {
                GACDynFuncs matchingfunc = gacFuncList.stream().
                        filter(p -> p.getFUNCTION_ID().equals("EXP_VALIDATE_PRIN_CODE")
                         && p.getREF_CODE().equals(RefCode)).
                        findAny().orElse(null);
                if (matchingfunc != null)
                {
                    //AXIS = “03” or AXIS = “PI”):
                    if (Axis.equals("03") || Axis.equals("PI"))
                    {
                        obret.setErrCode("S");
                    }
                    else if (Axis.equals("YPL2") || ProdTye.equals("YPL2"))
                    {
                        obret.setErrCode("S");
                    }
                }
        }
        catch (Exception ex)
        {
            obret.setErrCode("E");
            obret.setErrMSG(ex.getMessage());
        }


        return  obret;
    }

    public ValidRespSet xxExpDateDisable(List<GACDynFuncs> gacFuncList , String RefCode,  String Mandate,String Axis, String ProdTye)
    {
        ValidRespSet obret = new ValidRespSet();
        try
        {
            GACDynFuncs matchingfunc = gacFuncList.stream().
                    filter(p -> p.getFUNCTION_ID().equals("EXP_VALIDATE_PRIN_CODE")
                            && p.getREF_CODE().equals(RefCode)).
                    findAny().orElse(null);
            if (matchingfunc != null)
            {
                //AXIS = “03” or AXIS = “PI”):
                if (Axis.equals("YPL2") || ProdTye.equals("YPL2"))
                {
                    obret.setErrCode("S");
                }
            }
        }
        catch (Exception ex)
        {
            obret.setErrCode("E");
            obret.setErrMSG(ex.getMessage());
        }
        return  obret;
    }

    public ValidRespSet FormulaAndFillCodeDisable(List<GACDynFuncs> gacFuncList , String RefCode)
    {
        ValidRespSet obret = new ValidRespSet();
        try
        {
            GACDynFuncs matchingfunc = gacFuncList.stream().
                    filter(p -> p.getFUNCTION_ID().equals("XML_GENERATION_N_FILE_TRANSFER")
                            && p.getREF_CODE().equals(RefCode)).
                    findAny().orElse(null);
            if (matchingfunc != null)
            {
                obret.setErrCode("S");

            }
        }
        catch (Exception ex)
        {
            obret.setErrCode("E");
            obret.setErrMSG(ex.getMessage());
        }
        return  obret;
    }


    public ValidRespSet getExpDate(List<GACDynFuncs> gacFuncList , String RefCode,  String Mandate, int ShelfLife, String Datefromat , String ExpDate)
    {
        //has to pass collection of functins, Principle Code, ManDate , and Shelf life
        ValidRespSet obret = new ValidRespSet();
        Date dtmandate;
        Date expdate;
        if ((Mandate == null || Mandate.trim().length()<=0))
        {
            return  obret;
        }

        if (!(ExpDate == null || ExpDate.trim().length()<=0))
        {
            return  obret;
        }

        //obret.setErrCode("I");  //ignore no action to be done
        try {
            SimpleDateFormat dateFormater = new SimpleDateFormat(Datefromat);
            GACDynFuncs matchingfunc = gacFuncList.stream().
                    filter(p -> p.getFUNCTION_ID().equals("XML_GENERATION_N_FILE_TRANSFER")
                            && p.getREF_CODE().equals(RefCode)).
                    findAny().orElse(null);

            if (matchingfunc != null) {

                dtmandate = dateFormater.parse(Mandate);
                expdate = AddDays(dtmandate, ShelfLife);
                obret.setErrCode("R");
                obret.setTargetValue(dateFormater.format(expdate));
            }
        }
        catch (Exception ex)
        {
            obret.setErrCode("E");
            obret.setErrMSG(ex.getMessage());
        }
        return  obret;
    }

    public ValidRespSet getManfDate(List<GACDynFuncs> gacFuncList , String RefCode,  String ExpDate, int ShelfLife, String Datefromat , String ManfDate)
    {
        //has to pass collection of functins, Principle Code, ManDate , and Shelf life
        ValidRespSet obret = new ValidRespSet();
        Date dtExpDate;
        Date manfdate;

        if ((ExpDate == null || ExpDate.trim().length()<=0))
        {
            return  obret;
        }

        if (!(ManfDate == null || ManfDate.trim().length()<=0))
        {
            return  obret;
        }

        //obret.setErrCode("I");  //ignore no action to be done
        try {
            SimpleDateFormat dateFormater = new SimpleDateFormat(Datefromat);
            GACDynFuncs matchingfunc = gacFuncList.stream().
                    filter(p -> p.getFUNCTION_ID().equals("XML_GENERATION_N_FILE_TRANSFER")
                            && p.getREF_CODE().equals(RefCode)).
                    findAny().orElse(null);

            if (matchingfunc != null) {

                dtExpDate = dateFormater.parse(ExpDate);
                manfdate = AddDays(dtExpDate, 0-ShelfLife);
                obret.setErrCode("R");
                obret.setTargetValue(dateFormater.format(manfdate));
            }
        }
        catch (Exception ex)
        {
            obret.setErrCode("E");
            obret.setErrMSG(ex.getMessage());
        }
        return  obret;
    }

    public ValidRespSet ValidateLotNoOnEntry(List<GACDynFuncs> gacFuncList , String RefCode, String LotNo, String CallBatchNo)
    {
        ValidRespSet obret = new ValidRespSet();
        GACDynFuncs matchingfunc;
        Pattern regxpattern = null;
        Matcher matchr = null;
        String regx = "";

        try
        {
            matchingfunc = gacFuncList.stream().
                    filter(p -> p.getFUNCTION_ID().equals("LOTNOSTART10LEN12MARS_VAL")
                            && p.getREF_CODE().equals(RefCode)).
                    findAny().orElse(null);

            if (matchingfunc != null) //3
            {
                regx = "^(?!10)^\\w{12}$";    //If (Lot NOT start  with "10") and length = 12
                regxpattern = Pattern.compile(regx);
                matchr = regxpattern.matcher(LotNo);
                if (matchr.find())
                {
                    obret.setErrCode("E");
                    obret.setErrMSG("Invalid Lot no. Please scan again.");
                    return  obret;
                }
            }

            matchingfunc = gacFuncList.stream().
                    filter(p -> p.getFUNCTION_ID().equals("LOTLEN8NUMBDF_VAL")
                            && p.getREF_CODE().equals(RefCode)).
                    findAny().orElse(null);

            //[0-9]+
            if (matchingfunc != null)  ///4
            {
                regx = "(^[0-9]*$)|(^\\w{8}$)|(^\\w{1}([1-9a-zA-Z]{3})\\w*)";
                //Lot No must contain only numeric character or length of lot no must be 8
                // or second/third/fourth char of lot no must be NOT “0”.
                regxpattern = Pattern.compile(regx);
                matchr = regxpattern.matcher(LotNo);
                if (matchr.find())
                {
                    obret.setErrCode("F");
                    return  obret;
                }
                else
                {
                    obret.setErrCode("E");
                    obret.setErrMSG("Invalid Lot no. It should be 8 char length and should be numeric. Pls scan again.");
                    return  obret;
                    //Invalid Lot no. It should be 8 char length and should be numeric. Pls scan again.
                }
            }

            matchingfunc = gacFuncList.stream().
                    filter(p -> p.getFUNCTION_ID().equals("LOTLEN10MARS_VAL")
                            && p.getREF_CODE().equals(RefCode)).
                    findAny().orElse(null);
            if (matchingfunc != null) //6
            {
                    boolean b_isCoMan = _isCoMan(gacFuncList, RefCode, CallBatchNo);
                    if (!b_isCoMan && LotNo.length() <10)
                    {
                        obret.setErrCode("E");
                        obret.setErrMSG("Invalid Lot Number. Length must be 10 characters long.");
                        return  obret;
                    }
            }

            String lotnoleft2 = "";
            String lotnoleft1 = "";
            String LotnoParsed = "";
            if (LotNo.length() >2)
            {
                lotnoleft2 = LotNo.substring(0,2);
            }
            if (LotNo.length() >1)
            {
                lotnoleft1 = LotNo.substring(0,1);
            }
            matchingfunc = gacFuncList.stream().
                    filter(p -> p.getFUNCTION_ID().equals("LOT56789MARS_VAL")
                            && p.getREF_CODE().equals(RefCode)).
                    findAny().orElse(null);

            if (matchingfunc != null) //7
            {
                boolean b_isCoMan = _isCoMan(gacFuncList, RefCode, CallBatchNo);
                if (b_isCoMan )
                {
                    if (lotnoleft2.equals("10")   || lotnoleft2.equals("20") || lotnoleft2.equals("30") || lotnoleft2.equals("40"))
                    {
                        obret.setErrCode("R");
                        LotnoParsed = LotNo.substring(2);
                        obret.setTargetValue(LotnoParsed);
                        return  obret;
                    }
                   else  if (lotnoleft1.equals("1")   || lotnoleft1.equals("2") || lotnoleft1.equals("3") || lotnoleft1.equals("4"))
                    {
                        obret.setErrCode("R");
                        LotnoParsed = LotNo.substring(1);
                        obret.setTargetValue(LotnoParsed);
                        return  obret;
                    }
                        //9 or 5 o 6 or 7 or 8
                    else if (lotnoleft1.equals("9")   || lotnoleft1.equals("5") || lotnoleft1.equals("6") || lotnoleft1.equals("7") || lotnoleft1.equals("8"))
                    {
                        obret.setErrCode("R");
                        obret.setTargetValue(LotNo);
                        return  obret;
                    }
                    else
                    {
                        obret.setErrCode("E");
                        obret.setErrMSG("Invalid Lot Number. It must start with 5,6,7,8 or 9 and length would be 10 char long.");
                        return  obret;
                    }

                }
            }

        }


        catch (Exception ex)
        {
            obret.setErrCode("E");
            obret.setErrMSG(ex.getMessage());
        }
        return  obret;
    }

    public ValidRespSet ValidateLotNoOnSave(List<GACDynFuncs> gacFuncList , String RefCode, String LotNo, String CallBatchNo)
    {
        ValidRespSet obret = new ValidRespSet();
        GACDynFuncs matchingfunc;
        Pattern regxpattern = null;
        Matcher matchr = null;
        String regx = "";

        try
        {

            matchingfunc = gacFuncList.stream().
                    filter(p -> p.getFUNCTION_ID().equals("LOTLEN8NUMBDF_VAL")
                            && p.getREF_CODE().equals(RefCode)).
                    findAny().orElse(null);

            //[0-9]+
            if (matchingfunc != null)  ///2
            {
                regx = "(^[0-9]*$)|(^\\w{8}$)|(^\\w{1}([1-9a-zA-Z]{3})\\w*)";
                //Lot No must contain only numeric character or length of lot no must be 8
                // or second/third/fourth char of lot no must be NOT “0”.
                regxpattern = Pattern.compile(regx);
                matchr = regxpattern.matcher(LotNo);
                if (matchr.find())
                {
                    obret.setErrCode("S");
                    return  obret;
                }
                else
                {
                    obret.setErrCode("E");
                    obret.setErrMSG("Invalid Lot no. It should be 8 char length and should be numeric. Pls scan again.");
                    return  obret;
                    //Invalid Lot no. It should be 8 char length and should be numeric. Pls scan again.
                }
            }


            matchingfunc = gacFuncList.stream().
                    filter(p -> p.getFUNCTION_ID().equals("LOTLEN10MARS_VAL")
                            && p.getREF_CODE().equals(RefCode)).
                    findAny().orElse(null);
            if (matchingfunc != null) //4
            {
                boolean b_isCoMan = _isCoMan(gacFuncList, RefCode, CallBatchNo);
                if (!b_isCoMan && LotNo.length() <10)
                {
                    obret.setErrCode("E");
                    obret.setErrMSG("Invalid Lot Number. Length must be 10 characters long.");
                    return  obret;
                }
            }



            String lotnoleft1 = "";


            if (LotNo.length() >1)
            {
                lotnoleft1 = LotNo.substring(0,1);
            }
            matchingfunc = gacFuncList.stream().
                    filter(p -> p.getFUNCTION_ID().equals("LOT56789MARS_VAL")
                            && p.getREF_CODE().equals(RefCode)).
                    findAny().orElse(null);


            /*

            If NOT(_isCoMan) then check if Lot No is
            NOT started with char “9” or ”5” or ”6” or ”7” or ”8”
            then display message "Invalid Lot Number. It must start with 5,6,7,8 or 9."
            followed by return and set focus on Lot No and select the text.

             */
            if (matchingfunc != null) //5
            {
                boolean b_isCoMan = _isCoMan(gacFuncList, RefCode, CallBatchNo);
                if (!b_isCoMan )
                {
                    if (!(lotnoleft1.equals("9")   || lotnoleft1.equals("5") || lotnoleft1.equals("6") || lotnoleft1.equals("7") || lotnoleft1.equals("8")))
                    {
                        obret.setErrCode("R");
                        obret.setTargetValue(LotNo);
                        return  obret;
                    }
                    else
                    {
                        obret.setErrCode("E");
                        obret.setErrMSG("Invalid Lot Number. It must start with 5,6,7,8 or 9.");
                        return  obret;
                    }

                }
            }
        }


        catch (Exception ex)
        {
            obret.setErrCode("E");
            obret.setErrMSG(ex.getMessage());
        }
        return  obret;
    }

    public ValidRespSet ValidateLotNoEmptyOnSave(List<GACDynFuncs> gacFuncList , String RefCode, String LotNo, String CallBatchNo, String LotRequired)
    {
        ValidRespSet obret = new ValidRespSet();
        GACDynFuncs matchingfunc;
        Pattern regxpattern = null;
        Matcher matchr = null;
        String regx = "";

        try
        {

            if(LotRequired.length()>0 && LotRequired.equals("N")){
                return  obret;
            }


            if (LotRequired.equals("Y") && (LotNo == null || LotNo.isEmpty()) )
            {
                boolean b_isCoMan = _isCoMan(gacFuncList, RefCode, CallBatchNo);
                matchingfunc = gacFuncList.stream().
                        filter(p -> p.getFUNCTION_ID().equals("XML_GENERATION_N_FILE_TRANSFER")
                                && p.getREF_CODE().equals(RefCode)).
                        findAny().orElse(null);
                if ((!b_isCoMan) && matchingfunc != null)  ///6
                {
                    obret.setErrCode("E");
                    obret.setErrMSG("Please enter Lot No");
                    return  obret;

                }
                else
                {
                    obret.setErrCode("C");
                    obret.setErrMSG("Lot No is empty, do you wish to proceed?");
                    return  obret;
                }

            }

        }
        catch (Exception ex)
        {
            obret.setErrCode("E");
            obret.setErrMSG(ex.getMessage());
            ex.printStackTrace();
        }
        return  obret;
    }

    public ValidRespSet ValidateMfgDateEmptyOnSave(List<GACDynFuncs> gacFuncList , String RefCode, String MfgDate, String CallBatchNo, String ManfDateReuired)
    {
        ValidRespSet obret = new ValidRespSet();
        GACDynFuncs matchingfunc;
        Pattern regxpattern = null;
        Matcher matchr = null;
        String regx = "";

        try
        {
            if(ManfDateReuired.length()>0 && ManfDateReuired.equals("N")){
                return  obret;
            }

            if (ManfDateReuired.equals("Y") && (MfgDate == null || MfgDate.isEmpty()) )
            {
                boolean b_isCoMan = _isCoMan(gacFuncList, RefCode, CallBatchNo);
                if (!b_isCoMan)
                {
                    obret.setErrCode("E");
                    obret.setErrMSG("Please enter Manufacturing Date.");
                    return  obret;
                    //Invalid Lot no. It should be 8 char length and should be numeric. Pls scan again.
                }
                else
                {
                    obret.setErrCode("C");
                    obret.setErrMSG("Manufacturing Date is not set, do you wish to proceed?");
                    return  obret;
                }
            }
        }
        catch (Exception ex)
        {
            obret.setErrCode("E");
            obret.setErrMSG(ex.getMessage());
        }
        return  obret;
    }

    public ValidRespSet ValidateExpDateEmptyOnSave(List<GACDynFuncs> gacFuncList , String RefCode, String MfgDate, String CallBatchNo, String ExpDateReuired)
    {
        ValidRespSet obret = new ValidRespSet();
        GACDynFuncs matchingfunc;
        Pattern regxpattern = null;
        Matcher matchr = null;
        String regx = "";

        try
        {
            if(ExpDateReuired.length()>0 && ExpDateReuired.equals("N")){
                return  obret;
            }

            if(ExpDateReuired.equals("Y") && (MfgDate == null || MfgDate.isEmpty())){
                boolean b_isCoMan = _isCoMan(gacFuncList, RefCode, CallBatchNo);
                if(!b_isCoMan){
                    obret.setErrCode("E");
                    obret.setErrMSG("Please enter Expiry Date.");
                    return  obret;
                    //Invalid Lot no. It should be 8 char length and should be numeric. Pls scan again.
                }
                else
                {
                    obret.setErrCode("C");
                    obret.setErrMSG("Expirty Date is not set, do you wish to proceed?");
                    return  obret;
                }
            }
        }
        catch (Exception ex)
        {
            obret.setErrCode("E");
            obret.setErrMSG(ex.getMessage());
        }
        return  obret;
    }

    public ValidRespSet ValidateLotstatusOnSave(List<GACDynFuncs> gacFuncList , String RefCode,  String OriginalLotStat, String NewLotStatus)
    {
        ValidRespSet obret = new ValidRespSet();
        GACDynFuncs matchingfunc;
        Pattern regxpattern = null;
        Matcher matchr = null;
        String regx = "";

        try
        {
            matchingfunc = gacFuncList.stream().
                    filter(p -> p.getFUNCTION_ID().equals("LOTSTATTODAMAGEMARS_VAL")
                            && p.getREF_CODE().equals(RefCode)).
                    findAny().orElse(null);
            if (matchingfunc != null)
            {
                //If old Lot Status (LOT_STATUS)  "H" or "R" or "B" and new Lot Status <> “D”
                if ((OriginalLotStat.equals("H") || OriginalLotStat.equals("R") || OriginalLotStat.equals("B"))
                   && !(NewLotStatus.equals("D")))
                {
                    //You cannot change the Lot Status other than Damage.
                    obret.setErrCode("E");
                    obret.setErrMSG("You cannot change the Lot Status other than Damage.");
                    return  obret;
                }
            }

        }
        catch (Exception ex)
        {
            obret.setErrCode("E");
            obret.setErrMSG(ex.getMessage());
        }
        return  obret;
    }

    public ValidRespSet validatePutAwayConfirmation(List<GACDynFuncs> gacFuncList , String RefCode, String locStat, String loc,String siteType,String scanLoc)
    {
        ValidRespSet obret = new ValidRespSet();
        GACDynFuncs matchingfunc;
        Pattern regxpattern = null;
        Matcher matchr = null;
        String regx = "";

        try
        {
            matchingfunc = gacFuncList.stream().
                    filter(p -> p.getFUNCTION_ID().equals("DISABLE_MANUAL_ENTRY")
                            && (p.getREF_CODE().equals(RefCode))).
                    findAny().orElse(null);
            if (matchingfunc != null)
            {
                if(loc.equalsIgnoreCase("M") || locStat.equalsIgnoreCase("M")){
                    obret.setErrCode("M");
                    return  obret;
                }
                if(siteType.equalsIgnoreCase("GEN")){
                    if(loc!=null && loc.length()>0 ){
                        if(!loc.equals(scanLoc)){
                            obret.setErrCode("E");
                            obret.setErrMSG("Invalid Location.");
                            return  obret;
                        }
                    }else{
                        obret.setErrCode("C");
                        return  obret;
                    }
                }

            }
            else {
                if ((loc != null && loc.length() > 0) && (locStat != null && locStat.length() > 0)) {
                    if (loc.equalsIgnoreCase("M") || locStat.equalsIgnoreCase("M")) {
                        obret.setErrCode("M");
                        return obret;
                    }
                }
                if((siteType!=null && siteType.length()>0)) {
                    if (siteType.equalsIgnoreCase("GEN")) {
                        if (loc != null && loc.length() > 0) {
                            if (!loc.equals(scanLoc)) {
                                obret.setErrCode("E");
                                obret.setErrMSG("Invalid Location.");
                                return obret;
                            }
                        } else {
                            obret.setErrCode("C");
                            return obret;
                        }
                    }
                }
            }

        }
        catch (Exception ex)
        {
            obret.setErrCode("E");
            obret.setErrMSG(ex.getMessage());
            ex.printStackTrace();
        }
        return  obret;
    }

    public ValidRespSet ValidatePalletDifSave(List<GACDynFuncs> gacFuncList , String RefCode, String OriginalPalNo, String NewPalNo)
    {
        ValidRespSet obret = new ValidRespSet();
        GACDynFuncs matchingfunc;
        Pattern regxpattern = null;
        Matcher matchr = null;
        String regx = "";

        try
        {
            matchingfunc = gacFuncList.stream().
                    filter(p -> p.getFUNCTION_ID().equals("PALLETIDNOTVALIDATE_VAL")
                            && (p.getREF_CODE().equals(RefCode))).
                    findAny().orElse(null);
            if (matchingfunc != null)
            {
                //If old Lot Status (LOT_STATUS)  "H" or "R" or "B" and new Lot Status <> “D”
                obret.setErrCode("I");
                return  obret;
            }
            else
            {
                if (!(OriginalPalNo.equals(NewPalNo)))
                {
                    //You cannot change the Lot Status other than Damage.
                    obret.setErrCode("C");
                    obret.setErrMSG("The actual Pallet ID/SSCC doesn't match with database!, do you wish to proceed?");
                    return  obret;
                }
            }

        }
        catch (Exception ex)
        {
            obret.setErrCode("E");
            obret.setErrMSG(ex.getMessage());
        }
        return  obret;
    }

    public ValidRespSet ValidateGS1Barcode(List<GACDynFuncs> gacFuncList , String RefCode)
    {
        ValidRespSet obret = new ValidRespSet();
        GACDynFuncs matchingfunc;
        Pattern regxpattern = null;
        Matcher matchr = null;
        String regx = "";

        try
        {
            matchingfunc = gacFuncList.stream().
                    filter(p -> p.getFUNCTION_ID().equals("ENABLE_2D_BARCODE_DM_GS1")
                            && (p.getREF_CODE().equals(RefCode)) && p.getREGEX().length()>0).
                    findAny().orElse(null);
            if (matchingfunc != null)
            {

                obret.setErrCode("S");
                obret.setErrMSG(matchingfunc.getREGEX());
                return  obret;

            }
            else
            {
                    obret.setErrCode("I");
                    return  obret;
            }

        }
        catch (Exception ex)
        {
            obret.setErrCode("E");
            obret.setErrMSG(ex.getMessage());
        }
        return  obret;
    }

    public Date CalculateMfgDate(String RefCode, String LotNo)
    {
        Date mfgDate = Calendar.getInstance().getTime();
        Pattern regxpattern = null;
        Matcher matchr = null;
        String regx = "^[0-9]{1}\\w{7}$";  //ideally it should be ^[0-9]{4}\w{4}$ as 2 and 3 position is week and  4th is day
        String LotNoFirstDigit = LotNo.substring(0,1);
        int yearBDF = 0;
        String strbdf = "";
        int weekBDF = 0;
        int dayBDF = 0;
        int dayofweek=0;
        try
        {
            if (RefCode.equals("745"))
            {
                regxpattern = Pattern.compile(regx);
                matchr = regxpattern.matcher(LotNo);
                if (matchr.find())
                {
                    if (!LotNoFirstDigit.equals("0"))
                    {
                        yearBDF = Integer.parseInt(LotNoFirstDigit);
                    }
                    if (yearBDF>=6)
                    {
                        strbdf = "201" + LotNoFirstDigit;
                    }
                    else
                    {
                        strbdf = "202" + LotNoFirstDigit;
                    }
                    weekBDF = Integer.parseInt(LotNo.substring(1, 3));
                    dayBDF = Integer.parseInt(LotNo.substring(3, 4));
                    Calendar c = Calendar.getInstance();
                    c.set(yearBDF, 1,1);
                    Date startOfYear  = c.getTime();
                    dayofweek = c.get(Calendar.DAY_OF_WEEK);
                    int daysToFirstCorrectDay = (((int)dayBDF - dayofweek) + 7) % 7;
                    Date calculatedMfgDate = AddDays(startOfYear,7 * (weekBDF - 1) + daysToFirstCorrectDay);
                    return  calculatedMfgDate;
                    /*
                     int weekBDF = Convert.ToInt16(LotNo_.Substring(1, 2));
                    int dayBDF = Convert.ToInt16(LotNo_.Substring(3, 1));

                    DateTime startOfYear = new DateTime(yearBDF, 1, 1);

                    // The +7 and %7 stuff is to avoid negative numbers etc.
                    int daysToFirstCorrectDay = (((int)dayBDF - (int)startOfYear.DayOfWeek) + 7) % 7;

                    DateTime calculatedMfgDate = startOfYear.AddDays(7 * (weekBDF - 1) + daysToFirstCorrectDay);
                     */
                }
                else  //if not lot no start with digit or lot no length <> 8
                {
                    return mfgDate;
                }
            }
        }
        catch (Exception ex)
        {
            //do nothing as mfgDate already set to today
        }
        return  mfgDate;
    }

    private Date AddDays(Date Startdate, int Days)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime( Startdate);
        cal.add( Calendar.DATE,Days);
        return  cal.getTime();
    }
}
