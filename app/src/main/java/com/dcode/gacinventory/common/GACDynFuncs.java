package com.dcode.gacinventory.common;

public class GACDynFuncs {
    private String FUNCTION_ID;
    private String REGEX;
    private String REF_CODE;
    private String DESCRIPTION;

    public GACDynFuncs(String FUNCTION_ID, String REF_CODE , String DESCRIPTION, String REGEX)
    {
        this.FUNCTION_ID = FUNCTION_ID;

        this.REF_CODE = REF_CODE;
        this.DESCRIPTION = DESCRIPTION;
        this.REGEX = REGEX;
    }

    public String getREGEX() {
        return REGEX;
    }

    public void setREGEX(String REGEX) {
        this.REGEX = REGEX;
    }

    public String getREF_CODE() {
        return REF_CODE;
    }

    public void setREF_CODE(String REF_CODE) {
        this.REF_CODE = REF_CODE;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    public String getFUNCTION_ID() {
        return FUNCTION_ID;
    }

    public void setFUNCTION_ID(String FUNCTION_ID) {
        this.FUNCTION_ID = FUNCTION_ID;
    }
}
