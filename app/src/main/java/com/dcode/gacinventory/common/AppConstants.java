package com.dcode.gacinventory.common;

public class AppConstants {

    public static final int DEFAULT = 65534;
    public static final int UPLOADS = 69;
    public static final int DOWNLOADS = 96;
    public static final int USERS = 99;
    public static final int LOCATION = 1001;
    public static final int ORTYP = 1002;
    public static final int RECTYP = 1003;
    public static final int GOODS_RECEIPT = 2002;
    public static final int MOVEMENT = 2003;
    public static final int REASON = 1004;
    public static final long INBOUND_FULL = 20;
    public static final long INBOUND_PARTIAL = 21;
    public static final long INVEST_PUT = 30;
    public static final long INVEST_STAG = 31;
    public static final long INVEST_HEADER = 32;
    public static final long PROD_UPDATE_HEADER = 33;
    public static final long DIRECT_STAG = 34;
    public static final long DIRECT_PUT = 35;
    public static final long LOGISTIC_LABEL = 36;

    public static final String SIGN_BASE64 = "SIGN_BASE64";
    public static final String SIGN_PNG = "SIGN_PNG";
    public static final String SIGN_NAME = "SIGN_NAME";
    public static final String REQUEST_CODE = "REQUEST_CODE";
    public static final String MODULE_ID = "MODULE_ID";
    public static final String MASTER_ID = "MASTER_ID";
    public static final String SELECTED_ID = "SELECTED_ID";
    public static final String SELECTED_CODE = "SELECTED_CODE";
    public static final String SELECTED_NAME = "SELECTED_NAME";
    public static final String PARENT_ID = "PARENT_ID";
    public static final String FILTER_KEY_INT_ARRAY = "FILTER_KEY_INT_ARRAY";
    public static final String SELECTED_OBJECT = "SELECTED_OBJECT";
    public static final String SELECTED_OBJECT_NEW = "SELECTED_OBJECT_NEW";
    public static final String SELECTED_OBJECT_NEW2 = "SELECTED_OBJECT_NEW2";
    public static final String SOURCE_ID = "SOURCE_ID";
    public static final String GS1_PARAM = "GS1_PARAM";

    public static final String CODE_TITLE = "CODE_TITLE";
    public static final String NAME_TITLE = "NAME_TITLE";
    public static final String DESC_TITLE = "DESC_TITLE";
    public static final String TITLE = "TITLE";
    public static final String MESSAGE = "MESSAGE";
    public static final String LEFT = "LEFT";
    public static final String RIGHT = "RIGHT";
    public static final int SLOC = 101;
    public static final int HELPER = 102;
    public static final int VEH = 103;
    public static final String SLOC_KEY = "SLOC";
    public static final String HELPER_KEY = "HELPER";
    public static final String VEH_KEY = "VEH";
    public static final float DIFF_VALUE = (float) 0.0001;
    public static final int VISIT_IMAGE = 64534;
    public static final String NO_EXT_PATH = "/storage/emulated/0/Download/";

    public static final String IMAGES_PATH = "IMAGES_PATH";
    public static final String MASTERS_PATH = "MASTERS_PATH";
    public static final String OUT_PATH = "OUT_PATH";
    public static final String PDT_ID = "PDT_ID";
    public static final String PRINTER_TYPE = "PRINTER_TYPE";
    public static final String PRINTER_TYPE_INT = "PRINTER_TYPE_INT";
    public static final String IP_ADDRESS = "IP_ADDRESS";
    public static final String PORT = "PORT";
    public static final String MAC = "MAC";
    public static final String NETWORK = "NETWORK";
    public static final String NETWORK_INT = "NETWORK_INT";
    public static final int DAMAGE_ID = 201;
    public static final int PUT_ID = 202;
    public static final String DAMAGE_KEY = "DAMAGE";
    public static final String PUT_AWAY_KEY = "PUT_AWAY";
    public static final int JOB_ID = 202;
    public static final String JOB_KEY = "JOB_KEY";

    public static final int SS_PRN_ID = 203;
    public static final String SS_PRN_KEY = "PRNCODE";

    public static final int SS_JOB_ID = 204;
    public static final String SS_JOB_KEY = "JOB";

    public static final String ACCESS_TOKEN = "ACCESS_TOKEN";
    public static final String SUBSCRIPTION_KEY = "SUBSCRIPTION_KEY";
    public static final long SERIAL_NO_CONSTANT = 1622491200000L;
    public static final String SERVER_URL = "SERVER_URL";
    public static final String DEFAULT_PUTAWAY = "DEFAULT_PUTAWAY";
    public static final String SELECTED_SP_SCN_AN_OUT_LOC_BY_JOB = "DEFAULT_PUTAWAY";

    public enum PadDirection {
        idRight,
        idLeft,
        idCenter,
    }

    public enum DatabaseEntities {
        USERS,
        LOCATION,
        ORTYP,
        RECTYP,
        PRINTER_LIST,
        PRINT_FORMATS,
        PRINTER_LIST_SERVICE,
        LOT_STATUS,
        INVEST_REASON,
        USER_PRIN_INFO,
        COUNTRY_LIST,
        MASTER_LIST,
    }
}
