package com.dcode.gacinventory.common;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public  class GS1Identifiers {
    public String AICode;
    public String AIDesc;
    public String Regx ;
    public String AIValue;

    public int minLength ;
    public int maxLength ;
    public int type ;



    public static String AI_SSCC = "00";
    public static String AI_GTIN = "01";
    public static String AI_GTIN_B = "02";
    public static String AI_LOT_NUMBER = "10";
    public static String AI_DATE_PRODUCTION = "11";
    public static String AI_DATE_DUE = "12";
    public static String AI_DATE_PACKING = "13";
    public static String AI_DATE_BEST_BEFORE = "15";
    public static String AI_DATE_SELL_BY = "16";
    public static String AI_DATE_EXPIRATION = "17";
    public static String AI_PRODUCT_VARIANT = "20";
    public static String AI_SERIAL_NUMBER = "21";
    public static String AI_QTY = "30";

    public static int AI_TYPE_DATE = 1;
    public static int AI_TYPE_ALPHANUMERIC = 2;
    public static int AI_TYPE_NUMERIC = 3;

    private static Map<String, GS1Identifiers> GS1_AI = new LinkedHashMap<String, GS1Identifiers>() {
        {

            put(AI_SSCC, new GS1Identifiers(AI_SSCC, "SSCC", 18, 18, AI_TYPE_NUMERIC,"^00\\d{18}"));
            put(AI_GTIN, new GS1Identifiers(AI_GTIN, "Identification of a Variable Measure Trade Item (GTIN)", 14, 14, AI_TYPE_NUMERIC,"^01\\d{14}"));
            put(AI_GTIN_B, new GS1Identifiers(AI_GTIN_B, "Identification of Variable Measure Trade Items Contained in a Logistic", 14, 14, AI_TYPE_NUMERIC));
            put(AI_LOT_NUMBER, new GS1Identifiers(AI_LOT_NUMBER, "Batch or lot number", 1, 20, AI_TYPE_ALPHANUMERIC,"^10\\w{1,20}\\x7E{0,1}"));
            put(AI_DATE_PRODUCTION, new GS1Identifiers(AI_DATE_PRODUCTION, "Production Date", 6, 6, AI_TYPE_NUMERIC,"^11\\d{6}"));//YYMMDD day not mandatory 00
            put(AI_DATE_DUE, new GS1Identifiers(AI_DATE_DUE, "Due Date for Amount on Payment Slip", 6, 6, AI_TYPE_NUMERIC,"^12\\d{6}")); //YYMMDD day not mandatory 00
            put(AI_DATE_PACKING, new GS1Identifiers(AI_DATE_PACKING, "Packaging Date", 6, 6, AI_TYPE_NUMERIC,"^13\\d{6}"));//YYMMDD day not mandatory 00
            put(AI_DATE_BEST_BEFORE, new GS1Identifiers(AI_DATE_BEST_BEFORE, "Best before date", 6, 6, AI_TYPE_NUMERIC,"^15\\d{6}"));//YYMMDD day not mandatory 00
            put(AI_DATE_SELL_BY, new GS1Identifiers(AI_DATE_SELL_BY, "Sell By Date", 6, 6, AI_TYPE_NUMERIC,"^16\\d{6}"));//YYMMDD day not mandatory 00
            put(AI_DATE_EXPIRATION, new GS1Identifiers(AI_DATE_EXPIRATION, "Expiration Date", 6, 6, AI_TYPE_NUMERIC,"^17\\d{6}"));//YYMMDD day not mandatory 00
            put(AI_PRODUCT_VARIANT, new GS1Identifiers(AI_PRODUCT_VARIANT, "Product Variant", 2, 2, AI_TYPE_ALPHANUMERIC,"^20\\d{2}"));
            put(AI_SERIAL_NUMBER, new GS1Identifiers(AI_SERIAL_NUMBER, "Serial Number", 1, 20, AI_TYPE_ALPHANUMERIC,"^21\\w{1,20}\\x7E{0,1}"));
            //put(AI_SERIAL_NUMBER, new GS1Identifiers(AI_SERIAL_NUMBER, "Serial Number", 1, 20, AI_TYPE_ALPHANUMERIC,"21\\w{1,20}\\x7E{0,1}"));
            put(AI_QTY, new GS1Identifiers(AI_QTY, "Qty", 1, 20, AI_TYPE_ALPHANUMERIC,"^30\\d{1,8}\\x7E{0,1}"));        }};

    public GS1Identifiers(String aicode, String aiDesc, int minLen, int maxLen, int type)
    {

        this.AICode = aicode;
        this.AIDesc = aiDesc;
        this.minLength = minLen;
        this.maxLength = maxLen;
        this.type = type;
        this.AIValue = "";
        this.Regx = "";

        if (type == AI_TYPE_ALPHANUMERIC)
        {
            this.Regx = aicode + "\\w{" + minLen + "," + maxLen + "}";
        }
        else if (type == AI_TYPE_NUMERIC)
        {
            this.Regx = aicode + "\\d{" + minLen + "," + maxLen + "}";
            //strRegExMatch = Regex.Match(barcode, strAI + @"\d{" + intMin + "," + intMax + "}").ToString();
        }
    }

    public GS1Identifiers(String aicode, String aiDesc, int minLen, int maxLen, int type, String Regx)
    {
        this.AICode = aicode;
        this.AIDesc = aiDesc;
        this.minLength = minLen;
        this.maxLength = maxLen;
        this.type = type;
        this.AIValue = "";
        this.Regx = Regx;
    }
    public GS1Identifiers(GS1Identifiers ai)
    {
        this.AICode = ai.AICode;
        this.AIDesc = ai.AIDesc;
        this.minLength = ai.minLength;
        this.maxLength = ai.maxLength;
        this.type = ai.type;
        this.AIValue = ai.AIValue;
        this.Regx = ai.Regx;
    }

    public static Map<String, String> decodeBarcodeGS1_128(String barcode, String SepcharOverride, boolean Override, String ParseOrder)
    {

        String ParseSep = ":";
        Pattern regxpattern = null;
        Matcher matchr = null;

        Pattern regxinnerpattern = null;
        Matcher innermatchr = null;
        String[] parseoradarray = ParseOrder.split(SepcharOverride);

        Map<String, String> barcode_decoded = new HashMap<String, String>();
        barcode = barcode.replace(")", "").replace("(", "");
        for (String AICode: parseoradarray) {
            //Do your stuff here

            //for (Map.Entry mapElement : GS1_AI.entrySet()) {
            //(Map.Entry mapElement : GS1_AI.entrySet()) {
            //String key = (String)mapElement.getKey();
            if (!GS1_AI.containsKey(AICode))
            {
                continue;
            }
            //GS1Identifiers entry = ((GS1Identifiers)mapElement.getValue());
            GS1Identifiers entry =  (GS1Identifiers)  GS1_AI.get(AICode);
            String strAI = entry.getAICode();
            int intMin = entry.getminLength();
            int intMax = entry.getmaxLength();
            int strType = entry.gettype();
            String regx = entry.getRegx();

            String strRegExMatch = "";
            String matchString;

            regxpattern = Pattern.compile(regx);
            matchr = regxpattern.matcher(barcode);

            if (matchr.find())
            {
                strRegExMatch = matchr.group();
                barcode = matchr.replaceFirst("");
                if (barcode.startsWith(String.valueOf((char) 29)))
                {
                    barcode = barcode.substring(1);
                }
                regxinnerpattern = Pattern.compile("^" + strAI);
                innermatchr = regxinnerpattern.matcher(strRegExMatch);
                if (innermatchr.find())
                {
                    strRegExMatch = strRegExMatch.replaceFirst(strAI, "");
                }
                barcode_decoded.put(strAI,strRegExMatch);
            }
            //strRegExMatch = Regex.Match(barcode, regx).ToString();

        }

        return  barcode_decoded;
    }


    public static Map<String, String> decodeBarcodeGS1_128_working(String barcode, String SepcharOverride, boolean Override, String ParseOrder)
    {

        String ParseSep = ":";
        Pattern regxpattern = null;
        Matcher matchr = null;

        Pattern regxinnerpattern = null;
        Matcher innermatchr = null;
        String[] parseoradarray = ParseOrder.split(":");

        Map<String, String> barcode_decoded = new HashMap<String, String>();
        barcode = barcode.replace(")", "").replace("(", "");
        for (Map.Entry mapElement : GS1_AI.entrySet()) {
            String key = (String)mapElement.getKey();
            GS1Identifiers entry = ((GS1Identifiers)mapElement.getValue());

            String strAI = entry.getAICode();
            int intMin = entry.getminLength();
            int intMax = entry.getmaxLength();
            int strType = entry.gettype();
            String regx = entry.getRegx();

            String strRegExMatch = "";
            String matchString;

            regxpattern = Pattern.compile(regx);
            matchr = regxpattern.matcher(barcode);

            if (matchr.find())
            {
                strRegExMatch = matchr.group();
                barcode = matchr.replaceFirst("");
                regxinnerpattern = Pattern.compile(strAI);
                innermatchr = regxinnerpattern.matcher(strRegExMatch);
                if (innermatchr.find())
                {
                    strRegExMatch = strRegExMatch.replaceFirst(strAI, "");
                }
                barcode_decoded.put(strAI,strRegExMatch);
            }
            //strRegExMatch = Regex.Match(barcode, regx).ToString();

        }

        return  barcode_decoded;
    }

    public String getAICode() { return this.AICode; }
    public void setAICode(String name) { this.AICode = name; }

    public String getAIDesc() { return this.AIDesc; }
    public void setAIDesc(String name) { this.AIDesc = name; }

    public String getRegx() { return this.Regx; }
    public void setRegx(String name) { this.Regx = name; }

    public String getAIValue() { return this.AIValue; }
    public void setAIValue(String name) { this.AIValue = name; }

    public int  getminLength() { return this.minLength; }
    public void setminLength(int  value) { this.minLength = value; }

    public int  getmaxLength() { return this.maxLength; }
    public void setmaxLength(int  value) { this.maxLength = value; }

    public int  gettype() { return this.type; }
    public void settype(int  value) { this.type = value; }

    public static Map<String, String> decodeBarcodeGS1WithNpoOrder(String barcodescanned)
    {


        Map<String, String> barcode_decoded = new HashMap<String, String>();
        Pattern regxpattern = null;
        Matcher matchr = null;

        Pattern regxinnerpattern = null;
        Matcher innermatchr = null;

        String barcode =  barcodescanned;
        String fncchar = String.valueOf((char) 29);
        String []  gs1valuelist = barcode.split(fncchar);
        String [] aisupportedlist = "01,11,17,10,21".split(",");
        int Gs1suppcount = 5;
        int gs1foundcount = 0;
        int Counter = 0;
        boolean continueloop = true;
        while(continueloop)
        {
            continueloop = false;
            for (String AICode: aisupportedlist)
            {
                if (barcode.startsWith(fncchar))
                {
                    barcode = barcode.substring(1);
                }
                if (barcode.startsWith(AICode))
                {
                    continueloop = true;
                    gs1foundcount++;
                    GS1Identifiers entry =  (GS1Identifiers)  GS1_AI.get(AICode);
                    String strAI = entry.getAICode();
                    int intMin = entry.getminLength();
                    int intMax = entry.getmaxLength();
                    int strType = entry.gettype();
                    String regx = entry.getRegx();

                    String strRegExMatch = "";
                    String matchString;

                    regxpattern = Pattern.compile(regx);
                    matchr = regxpattern.matcher(barcode);

                    if (matchr.find())
                    {
                        strRegExMatch = matchr.group();
                        barcode = matchr.replaceFirst("");

                        regxinnerpattern = Pattern.compile("^" + strAI);
                        innermatchr = regxinnerpattern.matcher(strRegExMatch);
                        if (innermatchr.find())
                        {
                            strRegExMatch = strRegExMatch.replaceFirst(strAI, "");
                        }
                        barcode_decoded.put(strAI,strRegExMatch);

                        if (Gs1suppcount == gs1foundcount)
                        {
                            break;
                        }
                        if (barcode.length() <=0)
                        {
                            break;
                        }
                    }
                    else {
                        continueloop = false;
                    }

                }
            }
        }



        return  barcode_decoded;


    }


}
