package com.dcode.gacinventory.common;

public class ValidRespSet {

    private String ErrCode;
    private String ErrMSG;
    private String TargetValue ;


    public ValidRespSet() {
        ErrCode = "I";  //I Ignore E Error S Success R Chec value in TargetValue C Confirm and Proceed F Call Function as per DOC
        ErrMSG = "Not Set";
        TargetValue = "";
    }

    public String getErrCode() {
        return ErrCode;
    }

    public void setErrCode(String errCode) {
        ErrCode = errCode;
    }

    public String getTargetValue() {
        return TargetValue;
    }

    public void setTargetValue(String targetValue) {
        TargetValue = targetValue;
    }

    public String getErrMSG() {
        return ErrMSG;
    }

    public void setErrMSG(String errMSG) {
        ErrMSG = errMSG;
    }
}
