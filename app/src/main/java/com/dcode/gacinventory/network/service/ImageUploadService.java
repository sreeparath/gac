package com.dcode.gacinventory.network.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.data.model.PHOTO_MASTER;

import java.io.File;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ImageUploadService extends Service {

        //public static final int notify = Integer.valueOf(devConfig.getTimer())*60*1000;  //interval between two services(Here Service run every 5 Minute)
        private Handler mHandler = new Handler();   //run on another Thread to avoid crash
        private Timer mTimer = null;    //timer handling

        @Override
        public IBinder onBind(Intent intent) {
            throw new UnsupportedOperationException("Not yet implemented");
        }

        @Override
        public void onCreate() {
            //Log.d("notify##",String.valueOf(notify));
            if (mTimer != null) {// Cancel if already existed
                mTimer.cancel();
            }else {
                mTimer = new Timer();
            }//recreate new
            //Log.d("notify##",String.valueOf(devConfig.getTimer()));
            mTimer.scheduleAtFixedRate(new TimeDisplay(), 3000, Integer.valueOf(20*1000));   //Schedule task
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            mTimer.cancel();
        }

        //class TimeDisplay for handling task
        class TimeDisplay extends TimerTask {
            @Override
            public void run() {
                // run on another thread
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("heereeeeeeeeeeeeeeee","in service");
                        MultipartBody.Builder bodyBuilder = new MultipartBody.Builder();
                        MultipartBody.Builder builder = bodyBuilder.setType(MultipartBody.FORM);
                        builder.addFormDataPart("CreatedUser", "Robert");
                        builder.addFormDataPart("ReferenceNumber", "123");
                        builder.addFormDataPart("FileId", "f1");
                        builder.addFormDataPart("Module","job");
                        List<PHOTO_MASTER> photoMasterList = App.getDatabaseClient().getAppDatabase().genericDao().getPhotoMasterUpload();
                        for(PHOTO_MASTER photoMaster : photoMasterList){
                            Log.d("PHOTO_PATH##",photoMaster.PHOTO_PATH);
                            File file = new File(photoMaster.PHOTO_PATH);
                            builder.addFormDataPart("Files",file.getName(), RequestBody.create(MediaType.parse("image/png"),file) );
                            builder.addFormDataPart("Files",file.getName(),RequestBody.create(MediaType.parse("image/png"),file) );
                        }
                        builder.build();
                        MultipartBody requestBody = builder.build();
                        //App.getNetworkClientUploadPhoto().uploadImagetoServer(requestBody);
                        App.getNetworkClientUploadPhoto();
                    }

                });
            }
        }
    }