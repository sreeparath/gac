package com.dcode.gacinventory.network;


import android.util.Log;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppVariables;
import com.dcode.gacinventory.network.adapter.ItemTypeAdapterFactory;
import com.dcode.gacinventory.network.service.GenericRetService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

public class NetworkClient {
    private static NetworkClient mInstance;
    private GenericRetService apiService;

    private NetworkClient() {

        String BASE_URL = App.appSettings().getBaseUrl();
        //String BASE_URL = AppVariables.getUrl();
        Log.d("BASE_URL#2#",BASE_URL);
        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(new ItemTypeAdapterFactory())
                .create();

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(50, TimeUnit.SECONDS);
        client.setReadTimeout(60, TimeUnit.SECONDS);
        client.setWriteTimeout(60, TimeUnit.SECONDS);

        String auth = "Bearer " + "1234567";


        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(BASE_URL)
                .setClient(new OkClient(client))
                .setConverter(new GsonConverter(gson))
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        request.addHeader("Authorization", auth);
                        request.addHeader("Accept", "application/json");
                    }
                })
                .build();

        apiService = restAdapter.create(GenericRetService.class);
    }

    public static synchronized NetworkClient getInstance() {
        if (mInstance == null) {
            mInstance = new NetworkClient();
        }

        return mInstance;
    }

    public GenericRetService getAPIService() {
        return apiService;
    }

    public void destroyInstance() {
        mInstance = null;
    }
}
