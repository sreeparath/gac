package com.dcode.gacinventory.network.service;


import com.dcode.gacinventory.network.model.GenericRetResponse;
import com.dcode.gacinventory.network.model.GenericSubmissionResponse;
import com.dcode.gacinventory.network.model.OAuthTokenResponse;
import com.dcode.gacinventory.network.model.ValidUserResponse;
import com.google.gson.JsonObject;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.Response;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.Headers;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.PartMap;
import retrofit.mime.TypedFile;

public interface GenericRetService {
    @POST("/GACJsonSrv.svc/GetPrinterList")
    void getGenericRetPrinterList(@Body JsonObject objBody, Callback<GenericRetResponse> callback);

    @POST("/GACJsonSrv.svc/GetPrintFormats")
    void getGenericRetPrintFormat(@Body JsonObject objBody, Callback<GenericRetResponse> callback);

    @POST("/GACJsonSrv.svc/GetXMLContentAsDatatable")
    void getXMLContentAsDatatable(@Body JsonObject objBody, Callback<GenericRetResponse> callback);

    @POST("/GACJsonSrv.svc/ReadGenericRetJson")
    void getRetGeneric(@Body JsonObject objBody, Callback<GenericRetResponse> callback);

    @POST("/GACJsonSrv.svc/GetContype")
    void getAppModeGeneric(@Body JsonObject objBody,Callback<GenericRetResponse> callback);

    @POST("/GACJsonSrv.svc/SaveImage")
    void submitWithLines(@Body JsonObject objBody, Callback<GenericRetResponse> callback);

    @POST("/GACJsonSrv.svc/SaveDataForSPAndParamJson")
    void submitNoLines(@Body JsonObject objBody, Callback<GenericSubmissionResponse> callback);

    @POST("/MSSQLRest.svc/ValidateUser")
    void ValidateUser(@Body JsonObject objBody, Callback<ValidUserResponse> callback);

    @POST("/MSSQLRest.svc/SendMail")
    void SendMail(@Body JsonObject objBody, Callback<GenericRetResponse> callback);

    @POST("/MSSQLRest.svc/GetPrinterList")
    void GetPrinterList(@Body JsonObject objBody, Callback<GenericRetResponse> callback);

    @POST("/MSSQLRest.svc/GetPrintFormats")
    void GetPrintFormats(@Body JsonObject objBody, Callback<GenericRetResponse> callback);

    @POST("/MSSQLRest.svc/PrintBarcodeLabel")
    void PrintBarcodeLabel(@Body JsonObject objBody, Callback<GenericSubmissionResponse> callback);

    @FormUrlEncoded
    @POST("/oauth2/token")
    void getOauthToken(@Field("grant_type") String grantType, Callback<OAuthTokenResponse> callback);

    @POST("/GACJsonSrv.svc/SaveDataForSPAndParamJson")
    void submitWithLinesData(@Body JsonObject objBody, Callback<GenericRetResponse> callback);

    @POST("/GACJsonSrv.svc/GetPrinterList")
    void getImageUploadService(@Body JsonObject objBody, Callback<GenericRetResponse> callback);

    @POST("http://api-test.gacgroup.net/internal/documents/dev/ad-v1/api/documents/download?module=job&documentId=2aa14184-7070-4284-b93a-37a3e5e9576c")
    void getImageViewService(@Body JsonObject objBody, Callback<Response> callback);

    /**
     * ProcName":"SP_SCN_AN_OUT_LOC_BY_JOB"
     *
     * @param objBody
     * @param callback
     */
    @POST("/GACJsonSrv.svc/ReadGenericRetJson")
    void getSpScnOutLocByJob(@Body JsonObject objBody, Callback<GenericRetResponse> callback);

}
