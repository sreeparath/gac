package com.dcode.gacinventory.network.model;


import com.google.gson.annotations.SerializedName;

public class GenericRetResponse {
    @SerializedName("ErrCode")
    private String errCode;
    @SerializedName("ErrMessage")
    private String errMessage;
    @SerializedName("xmlDoc")
    private String xmlDoc;
    @SerializedName("JsonString")
    private String JsonString;

    public String getJsonString() {
        return JsonString;
    }


    public String getErrCode() {
        return errCode;
    }

    public String getErrMessage() {
        return errMessage;
    }

    public String getXmlDoc() {
        return xmlDoc;
    }
}
