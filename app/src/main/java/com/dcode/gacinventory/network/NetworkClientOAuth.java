package com.dcode.gacinventory.network;


import com.dcode.gacinventory.App;
import com.dcode.gacinventory.network.adapter.ItemTypeAdapterFactory;
import com.dcode.gacinventory.network.service.GenericRetService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.Credentials;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;

import java.util.concurrent.TimeUnit;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

public class NetworkClientOAuth {
    private static NetworkClientOAuth mInstance;
    private GenericRetService apiService;

    private NetworkClientOAuth() {

        String BASE_URL = "https://login.microsoftonline.com/6e9db74c-11b7-459e-b199-9f044662d260/";
        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(new ItemTypeAdapterFactory())
                .create();

//        Interceptor interceptor = chain -> {
//            Request original = chain.request();
//            Request request = original.newBuilder()
//                    .header("Authorization", Credentials.basic("e1195be9-cb92-477e-b7a5-ec04b92d780f", "Hrliid?l/Q-om@RYSVqMANVkp3O0Cg31"))
//                    .header("Ocp-Apim-Subscription-Key","8bb005d79dc9445baf46e93ea9129c3f")
//                    .method(original.method(), original.body())
//                    .build();
//            return chain.proceed(request);
//        };

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(50, TimeUnit.SECONDS);
        client.setReadTimeout(60, TimeUnit.SECONDS);
        client.setWriteTimeout(60, TimeUnit.SECONDS);



        String auth = "Bearer " + "1234567";

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(BASE_URL)
                .setClient(new OkClient(client))
                .setConverter(new GsonConverter(gson))

                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        request.addHeader("Authorization", Credentials.basic("e1195be9-cb92-477e-b7a5-ec04b92d780f", "Hrliid?l/Q-om@RYSVqMANVkp3O0Cg31"));
                        //request.addHeader("Ocp-Apim-Subscription-Key","8bb005d79dc9445baf46e93ea9129c3f");

                        request.addHeader("Accept", "application/json");

                    }
                })
                .build();

        apiService = restAdapter.create(GenericRetService.class);
    }

    public static synchronized NetworkClientOAuth getInstance() {
        if (mInstance == null) {
            mInstance = new NetworkClientOAuth();
        }

        return mInstance;
    }

    public GenericRetService getAPIService() {
        return apiService;
    }

    public void destroyInstance() {
        mInstance = null;
    }
}
