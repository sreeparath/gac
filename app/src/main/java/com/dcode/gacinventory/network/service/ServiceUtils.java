package com.dcode.gacinventory.network.service;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;


import androidx.room.ColumnInfo;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.common.AppVariables;
import com.dcode.gacinventory.common.Utils;
import com.dcode.gacinventory.data.model.BARCODE_FRMT;
import com.dcode.gacinventory.data.model.BOX_SCAN_GACID;
import com.dcode.gacinventory.data.model.COUNTRY_LIST;
import com.dcode.gacinventory.data.model.INVESIGATE_REGISTER;
import com.dcode.gacinventory.data.model.INVEST_REASONS;
import com.dcode.gacinventory.data.model.JOB_CLOSURE_INFO;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SUMBIT;
import com.dcode.gacinventory.data.model.LOT_STATUS_LIST;
import com.dcode.gacinventory.data.model.MASTER_LIST;
import com.dcode.gacinventory.data.model.PHOTO_MASTER;
import com.dcode.gacinventory.data.model.PRINTER_LIST;
import com.dcode.gacinventory.data.model.PRINTER_LIST_SERVICE;
import com.dcode.gacinventory.data.model.PRINT_FORMAT;
import com.dcode.gacinventory.data.model.PROD_UPD_INFO;
import com.dcode.gacinventory.data.model.PUTAWAY_SUMBIT;
import com.dcode.gacinventory.data.model.USERS;
import com.dcode.gacinventory.data.model.USER_PRIN_INFO;
import com.dcode.gacinventory.network.model.GenericRetResponse;
import com.dcode.gacinventory.ui.dialog.CustomProgressDialog;
import com.example.gacinventory.R;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ServiceUtils {


    public static JsonObject createJsonObject(String paramName, String ParamType, String Value) {
        JsonObject jsonObject;
        jsonObject = new JsonObject();
        jsonObject.addProperty("Name", paramName);
        jsonObject.addProperty("ParamType", ParamType);
        jsonObject.addProperty("TypName", "");
        jsonObject.addProperty("Value", Value);
        return jsonObject;
    }

    public static JSONObject oauthTokenObject() throws JSONException {
        JSONObject obj=new JSONObject();
        obj.put("grant_type","client_credentials");
        return obj;
    }

    public static JsonObject createJsonObject(String paramName, String ParamType, String ParamTypName, String Value) {
        JsonObject jsonObject;
        jsonObject = new JsonObject();
        jsonObject.addProperty("Name", paramName);
        jsonObject.addProperty("ParamType", ParamType);
        jsonObject.addProperty("TypName", ParamTypName);
        jsonObject.addProperty("Value", Value);
        return jsonObject;
    }

    public static JsonObject createJsonObject(String paramName, String ParamType, long Value) {
        JsonObject jsonObject;
        jsonObject = new JsonObject();
        jsonObject.addProperty("Name", paramName);
        jsonObject.addProperty("ParamType", ParamType);
        jsonObject.addProperty("TypName", "");
        jsonObject.addProperty("Value", Value);
        return jsonObject;
    }

    public static JsonObject createJsonObject(String paramName, String ParamType, float Value) {
        JsonObject jsonObject;
        jsonObject = new JsonObject();
        jsonObject.addProperty("Name", paramName);
        jsonObject.addProperty("ParamType", ParamType);
        jsonObject.addProperty("TypName", "");
        jsonObject.addProperty("Value", Value);
        return jsonObject;
    }

    public static JsonElement GenerateCommonReadParams(long PK_ID) {
        JsonArray array = new JsonArray();
        JsonObject jsonObject;

        jsonObject = new JsonObject();
        jsonObject.addProperty("Name", "USER_ID");
        jsonObject.addProperty("ParamType", "22");
        jsonObject.addProperty("TypName", "");
        jsonObject.addProperty("Value", App.currentUser.USER_ID);
        array.add(jsonObject);

        jsonObject = new JsonObject();
        jsonObject.addProperty("Name", "ID");
        jsonObject.addProperty("ParamType", "8");
        jsonObject.addProperty("TypName", "");
        jsonObject.addProperty("Value", PK_ID);
        array.add(jsonObject);
        return array;
    }

    public static class MastersDownload {

        static JsonObject getMastersRequestObject(final int masterID) {
            JsonObject jsonObject;

            JsonArray array = new JsonArray();
            jsonObject = createJsonObject("MASTER_ID", "8", masterID);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "APP.HHT_OBJECTS_MIN_SPR");
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        public static JsonObject getLocationsObject() {
            JsonObject jsonObject;
            JsonArray array = new JsonArray();
            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "pdt.LOCLIST_SPR");
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        static JsonObject getOrTypObject() {
            JsonObject jsonObject;
            JsonArray array = new JsonArray();
            jsonObject = createJsonObject("USER_ID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);
            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "pdt.ORTYP_SPR");
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        static JsonObject geRecTypObject() {
            JsonObject jsonObject;
            JsonArray array = new JsonArray();
            jsonObject = createJsonObject("USER_ID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);
            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "pdt.REC_TYPE_LIST_SPR");
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        static JsonObject geReasonTypObject() {
            JsonObject jsonObject;
            JsonArray array = new JsonArray();
            jsonObject = createJsonObject("USER_ID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);
            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "pdt.REASON_TYPE_LIST_SPR");
            jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        static JsonObject getAuditInstructionsRequestObject() {
            JsonObject jsonObject;

            JsonArray array = new JsonArray();
//            jsonObject = createJsonObject("MASTER_ID", "8", masterID);
//            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "DBO.HHT_AUDIT_INSTRUCT_SPR");
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        static JsonObject getMovementInstructionsRequestObject() {
            JsonObject jsonObject;

            JsonArray array = new JsonArray();
//            jsonObject = createJsonObject("MASTER_ID", "8", masterID);
//            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "DBO.HHT_MOVEMENT_INSTRUCT_SPR");
            jsonObject.add("dbparams", array);
            return jsonObject;
        }
    }

    public static class LabelPrint {

        public static JsonObject getPrintList(final String ordNo, final int count) {
            JsonObject jsonObject;

//            jsonObject = new JsonObject();
//            jsonObject.addProperty("XMLFileName", "labellist.xml");
            JsonArray array = new JsonArray();

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("SRNO_COUNT", "22", count);
            array.add(jsonObject);


            jsonObject = new JsonObject();
            jsonObject = createJsonObject("JOB_NO", "22", ordNo);
            array.add(jsonObject);

//            jsonObject = new JsonObject();
//            jsonObject = createJsonObject("PO_NUMBER", "22", "");
//            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEVICE_ID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEV_GUID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("USERID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "SP_SCN_AN_GET_NEW_GACSCAN_ID");
            jsonObject.add("dbparams", array);

            return jsonObject;
        }
    }


    public static class LogisticLabel {

        public static JsonObject getLogisticLabel(final String labelNo, final String gacScanId,final String barcode,
                                                  final String prinCode,final String putAwayCode,final String jobNo ,final String lotNo) {
            JsonObject jsonObject;
            JsonArray array = new JsonArray();

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("NEW_SRNO", "22", gacScanId);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("LOGISTIC_LABEL", "22", labelNo);
            array.add(jsonObject);


            jsonObject = new JsonObject();
            jsonObject = createJsonObject("BARCODE", "22", barcode);
            array.add(jsonObject);


            jsonObject = new JsonObject();
            jsonObject = createJsonObject("PRIN_CODE", "22", prinCode);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("JOB_NO", "22", jobNo);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("PUTAWAY_TYPE", "22", putAwayCode);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("LOT_NO", "22", lotNo);
            array.add(jsonObject);


            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEVICE_ID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEV_GUID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("USERID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            //jsonObject.addProperty("ProcName", "SP_SCAN_ANDRD_GET_STKLED_BY_SRNO");
            jsonObject.addProperty("ProcName", "SP_SCN_AN_INB_STG_GET_INFO");
            jsonObject.add("dbparams", array);


            return jsonObject;
        }


        public static JsonObject getPutAwayInfo(final String gacScanId,final String barcode,final String putAwayCode ) {
            JsonObject jsonObject;
            JsonArray array = new JsonArray();

//            jsonObject = new JsonObject();
//            jsonObject = createJsonObject("NEW_SRNO", "22", gacScanId);
//            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("SRNO", "22", gacScanId);
            array.add(jsonObject);

//            jsonObject = new JsonObject();
//            jsonObject = createJsonObject("LOGISTIC_LABEL", "22", "");
//            array.add(jsonObject);


            jsonObject = new JsonObject();
            jsonObject = createJsonObject("BARCODE", "22", barcode);
            array.add(jsonObject);


            jsonObject = new JsonObject();
            jsonObject = createJsonObject("PRIN_CODE", "22", "");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("JOB_NO", "22", "");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("PUTAWAY_TYPE", "22", putAwayCode);
            array.add(jsonObject);


            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEVICE_ID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEV_GUID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("USERID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "SP_SCN_AN_INB_PUT_GET_INFO");
            jsonObject.add("dbparams", array);


            return jsonObject;
        }

        public static JsonObject getPutAwayInfoByJobNo(final String jobNo, final String gacScanId, final String barcode, final String putAwayCode) {
            JsonObject jsonObject;
            JsonArray array = new JsonArray();

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("SRNO", "22", gacScanId);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("BARCODE", "22", barcode);
            array.add(jsonObject);


            jsonObject = new JsonObject();
            jsonObject = createJsonObject("PRIN_CODE", "22", "");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("JOB_NO", "22", jobNo);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("PUTAWAY_TYPE", "22", putAwayCode);
            array.add(jsonObject);


            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEVICE_ID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEV_GUID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("USERID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "SP_SCN_AN_INB_PUT_GET_INFO");
            jsonObject.add("dbparams", array);


            return jsonObject;
        }

        public static JsonObject getInfoBySrNo(final String gacScanId, final String barcode, final String putAwayCode) {
            JsonObject jsonObject;
            JsonArray array = new JsonArray();

//            jsonObject = new JsonObject();
//            jsonObject = createJsonObject("NEW_SRNO", "22", gacScanId);
//            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("SRNO", "22", gacScanId);
            array.add(jsonObject);

//            jsonObject = new JsonObject();
//            jsonObject = createJsonObject("LOGISTIC_LABEL", "22", "");
//            array.add(jsonObject);


            jsonObject = new JsonObject();
            jsonObject = createJsonObject("BARCODE", "22", barcode);
            array.add(jsonObject);


            jsonObject = new JsonObject();
            jsonObject = createJsonObject("PRIN_CODE", "22", "");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("JOB_NO", "22", "");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("PUTAWAY_TYPE", "22", putAwayCode);
            array.add(jsonObject);


            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEVICE_ID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEV_GUID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("USERID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "SP_SCN_AN_INB_STG_GET_INFO_BY_SRNO");
            jsonObject.add("dbparams", array);


            return jsonObject;
        }

        public static JsonObject getInboundHeader(final String labelNo, final String mode) {
            JsonObject jsonObject;
            JsonArray array = new JsonArray();

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("VALUE", "22", labelNo);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("VALUE_TYPE", "22", mode);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEVICE_ID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEV_GUID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("USERID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "SP_SCN_AN_VRFY_JB_PO_ASN_HD");
            jsonObject.add("dbparams", array);

            return jsonObject;
        }

        public static JsonObject getPrnCodeList(final String prnCode, final String mode) {
            JsonObject jsonObject;
            JsonArray array = new JsonArray();

//            jsonObject = new JsonObject();
//            jsonObject = createJsonObject("PRIN_CODE", "22", prnCode);
//            array.add(jsonObject);
//
//            jsonObject = new JsonObject();
//            jsonObject = createJsonObject("BARCODE_TYPE", "22", mode);
//            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEVICE_ID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEV_GUID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("USERID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "SP_SCN_AN_GET_PRIN");
            jsonObject.add("dbparams", array);

            return jsonObject;
        }

        public static JsonObject getJobCodeList(final String prnCode, final String mode) {
            JsonObject jsonObject;
            JsonArray array = new JsonArray();

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("PRIN_CODE", "22", prnCode);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("JOB_TYPE", "22", mode);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEVICE_ID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEV_GUID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("USERID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "SP_SCN_AN_GET_JOBS");
            jsonObject.add("dbparams", array);

            return jsonObject;
        }

        public static JsonObject getPrnCodeHeader(final String prnCode, final String mode) {
            JsonObject jsonObject;
            JsonArray array = new JsonArray();

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("PRIN_CODE", "22", prnCode);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("BARCODE_TYPE", "22", mode);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEVICE_ID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEV_GUID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("USERID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "SP_SCN_AN_GET_BARCODE_FRMT");
            jsonObject.add("dbparams", array);

            return jsonObject;
        }

        public static JsonObject getJobCodeHeader(final String jobNo, final String prnCode) {
            JsonObject jsonObject;
            JsonArray array = new JsonArray();

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("PRIN_CODE", "22", prnCode);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("JOB_NO", "22", jobNo);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEVICE_ID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEV_GUID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("USERID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "SP_SCN_AN_SN_GET_INFO");
            jsonObject.add("dbparams", array);

            return jsonObject;
        }

        public static JsonObject getLOGISTIC_LABEL_SUMBIT_FULL(LOGISTIC_LABEL_SUMBIT logistic_label_sumbit,String directPutAway) {
            if (logistic_label_sumbit == null ) {
                return null;
            }

            JsonArray jsonArray = new JsonArray();
            jsonArray.add(createJsonObject("USERID", "22", App.currentUser.USER_ID));
            jsonArray.add(createJsonObject("DEVICE_ID", "22",  "App.DeviceID"));
            jsonArray.add(createJsonObject("DEV_GUID", "22", Utils.GetGUID()));
            //jsonArray.add(createJsonObject("I_SUBMISSION_DATE", "22", Utils.GetCurrentDateTime(Utils.PRINT_DATE_FORMAT)));
            //jsonArray.add(createJsonObject("I_DEVICE_DATE", "22", Utils.GetCurrentDateTime(Utils.DB_DATE_FORMAT)));
            jsonArray.add(createJsonObject("SRNO", "22", logistic_label_sumbit.SRNO==null ? "":logistic_label_sumbit.SRNO));
            jsonArray.add(createJsonObject("MFG_DATE", "22", logistic_label_sumbit.MFG_DATE==null ? "":logistic_label_sumbit.MFG_DATE));
            jsonArray.add(createJsonObject("EXP_DATE", "22", logistic_label_sumbit.EXP_DATE==null ? "":logistic_label_sumbit.EXP_DATE));
            jsonArray.add(createJsonObject("LOT_NO", "22", logistic_label_sumbit.LOT_NO==null ? "":logistic_label_sumbit.LOT_NO));
            jsonArray.add(createJsonObject("LOC_CODE", "22", logistic_label_sumbit.LOC_CODE==null?"":logistic_label_sumbit.LOC_CODE));
            jsonArray.add(createJsonObject("FORMULA_NO", "22", logistic_label_sumbit.FORMULA_NO==null?"":logistic_label_sumbit.FORMULA_NO));
            jsonArray.add(createJsonObject("FIL_CODE", "22", logistic_label_sumbit.FIL_CODE==null?"":logistic_label_sumbit.FIL_CODE));
            jsonArray.add(createJsonObject("LOT_STATUS", "22", logistic_label_sumbit.LOT_STATUS==null?"":logistic_label_sumbit.LOT_STATUS));
            jsonArray.add(createJsonObject("INPUT_PQTY", "22", (logistic_label_sumbit.INPUT_PQTY==null?"":logistic_label_sumbit.INPUT_PQTY)));
            jsonArray.add(createJsonObject("INPUT_MQTY", "22", (logistic_label_sumbit.INPUT_MQTY==null?"":logistic_label_sumbit.INPUT_MQTY)));
            jsonArray.add(createJsonObject("INPUT_LQTY", "22", (logistic_label_sumbit.INPUT_LQTY==null?"":logistic_label_sumbit.INPUT_LQTY)));
            jsonArray.add(createJsonObject("USER_NAME", "22", App.currentUser.USER_NAME==null?"":App.currentUser.USER_NAME));
            jsonArray.add(createJsonObject("UPDATE_FROM", "22", logistic_label_sumbit.UPDATE_FROM==null?"":logistic_label_sumbit.UPDATE_FROM));
            jsonArray.add(createJsonObject("PALLET_ID", "22", (logistic_label_sumbit.PALLET_ID==null ? "":logistic_label_sumbit.PALLET_ID)));
            //jsonArray.add(createJsonObject("SITE", "22", logistic_label_sumbit.SITE));
            jsonArray.add(createJsonObject("SCAN_PUT_LOC_CODE", "22", logistic_label_sumbit.SCAN_PUT_LOC_CODE==null?"":logistic_label_sumbit.SCAN_PUT_LOC_CODE));
            jsonArray.add(createJsonObject("DIRECT_PUTAWAY", "22", directPutAway));



            JsonObject jsonObject = new JsonObject();
            //jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.addProperty("ProcName","SP_SCN_AN_STG_CON");
            jsonObject.add("dbparams", jsonArray);

            return jsonObject;
        }

        public static JsonObject getLOGISTIC_LABEL_SUMBIT_PARTIAL(LOGISTIC_LABEL_SUMBIT logistic_label_sumbit) {
            if (logistic_label_sumbit == null ) {
                return null;
            }

            JsonArray jsonArray = new JsonArray();
            jsonArray.add(createJsonObject("USERID", "22", App.currentUser.USER_ID));
            jsonArray.add(createJsonObject("DEVICE_ID", "22",  "App.DeviceID"));
            jsonArray.add(createJsonObject("DEV_GUID", "22", Utils.GetGUID()));
            //jsonArray.add(createJsonObject("I_SUBMISSION_DATE", "22", Utils.GetCurrentDateTime(Utils.PRINT_DATE_FORMAT)));
            //jsonArray.add(createJsonObject("I_DEVICE_DATE", "22", Utils.GetCurrentDateTime(Utils.DB_DATE_FORMAT)));
            jsonArray.add(createJsonObject("SRNO_ORIGINAL", "22", logistic_label_sumbit.SRNO_ORIGINAL == null ? "" : logistic_label_sumbit.SRNO_ORIGINAL));
            jsonArray.add(createJsonObject("SRNO", "22", logistic_label_sumbit.SRNO == null ? "" : logistic_label_sumbit.SRNO));
            jsonArray.add(createJsonObject("MFG_DATE", "22", logistic_label_sumbit.MFG_DATE == null ? "" : logistic_label_sumbit.MFG_DATE));
            jsonArray.add(createJsonObject("EXP_DATE", "22", logistic_label_sumbit.EXP_DATE == null ? "" : logistic_label_sumbit.EXP_DATE));
            jsonArray.add(createJsonObject("LOT_NO", "22", (logistic_label_sumbit.LOT_NO == null ? "" : logistic_label_sumbit.LOT_NO)));
            jsonArray.add(createJsonObject("LOC_CODE", "22", logistic_label_sumbit.LOC_CODE == null ? "" : logistic_label_sumbit.LOC_CODE));
            jsonArray.add(createJsonObject("FORMULA_NO", "22", logistic_label_sumbit.FORMULA_NO == null ? "" : logistic_label_sumbit.FORMULA_NO));
            jsonArray.add(createJsonObject("FIL_CODE", "22", logistic_label_sumbit.FIL_CODE == null ? "" : logistic_label_sumbit.FIL_CODE));
            jsonArray.add(createJsonObject("LOT_STATUS", "22", logistic_label_sumbit.LOT_STATUS == null ? "" : logistic_label_sumbit.LOT_STATUS));
            jsonArray.add(createJsonObject("INPUT_PQTY", "22", (logistic_label_sumbit.INPUT_PQTY == null ? "" : logistic_label_sumbit.INPUT_PQTY)));
            jsonArray.add(createJsonObject("INPUT_MQTY", "22", (logistic_label_sumbit.INPUT_MQTY == null ? "" : logistic_label_sumbit.INPUT_MQTY)));
            jsonArray.add(createJsonObject("INPUT_LQTY", "22", (logistic_label_sumbit.INPUT_LQTY == null ? "" : logistic_label_sumbit.INPUT_LQTY)));
            jsonArray.add(createJsonObject("USER_NAME", "22", App.currentUser.USER_NAME));
            jsonArray.add(createJsonObject("UPDATE_FROM", "22", logistic_label_sumbit.UPDATE_FROM == null ? "" : logistic_label_sumbit.UPDATE_FROM));
            Log.d("PALLET_ID##", logistic_label_sumbit.PALLET_ID);
            jsonArray.add(createJsonObject("PALLET_ID", "22", (logistic_label_sumbit.PALLET_ID.trim().length() == 0 || logistic_label_sumbit.PALLET_ID.trim() == null ? "ABCD123" : logistic_label_sumbit.PALLET_ID)));
            //jsonArray.add(createJsonObject("SITE", "22", logistic_label_sumbit.SITE));
            jsonArray.add(createJsonObject("SCAN_PUT_LOC_CODE", "22", logistic_label_sumbit.SCAN_PUT_LOC_CODE));
            //jsonArray.add(createJsonObject("ERROR_MESSAGE", "22", ""));


            JsonObject jsonObject = new JsonObject();
            //jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.addProperty("ProcName", "SP_SCN_AN_STG_CON_PARTIAL");
            jsonObject.add("dbparams", jsonArray);

            return jsonObject;
        }

        public static JsonObject getPUTAWAY_SUMBIT(PUTAWAY_SUMBIT putaway_sumbit) {
            if (putaway_sumbit == null) {
                return null;
            }

            JsonArray jsonArray = new JsonArray();
            jsonArray.add(createJsonObject("USERID", "22", App.currentUser.USER_ID));
            jsonArray.add(createJsonObject("DEVICE_ID", "22", "App.DeviceID"));
            jsonArray.add(createJsonObject("DEV_GUID", "22", Utils.GetGUID()));
            //jsonArray.add(createJsonObject("I_SUBMISSION_DATE", "22", Utils.GetCurrentDateTime(Utils.PRINT_DATE_FORMAT)));
            //jsonArray.add(createJsonObject("I_DEVICE_DATE", "22", Utils.GetCurrentDateTime(Utils.DB_DATE_FORMAT)));
            jsonArray.add(createJsonObject("SRNO", "22", putaway_sumbit.SRNO));
            jsonArray.add(createJsonObject("MFG_DATE", "22", putaway_sumbit.MFG_DATE));
            jsonArray.add(createJsonObject("EXP_DATE", "22", putaway_sumbit.EXP_DATE));
            jsonArray.add(createJsonObject("LOT_NO", "22", putaway_sumbit.LOT_NO));
            jsonArray.add(createJsonObject("LOC_CODE", "22", putaway_sumbit.LOC_CODE));
            jsonArray.add(createJsonObject("FORMULA_NO", "22", putaway_sumbit.FORMULA_NO));
            jsonArray.add(createJsonObject("FIL_CODE", "22", putaway_sumbit.FIL_CODE));
            jsonArray.add(createJsonObject("LOT_STATUS", "22", putaway_sumbit.LOT_STATUS));
            jsonArray.add(createJsonObject("INPUT_PQTY", "22", (putaway_sumbit.INPUT_PQTY)));
            jsonArray.add(createJsonObject("INPUT_MQTY", "22", (putaway_sumbit.INPUT_MQTY)));
            jsonArray.add(createJsonObject("INPUT_LQTY", "22", (putaway_sumbit.INPUT_LQTY)));
            jsonArray.add(createJsonObject("USER_NAME", "22", App.currentUser.USER_NAME));
            jsonArray.add(createJsonObject("UPDATE_FROM", "22", putaway_sumbit.UPDATE_FROM));
            jsonArray.add(createJsonObject("PALLET_ID", "22", putaway_sumbit.PALLET_ID));
            jsonArray.add(createJsonObject("SCAN_LOCATION", "22", putaway_sumbit.SCAN_LOCATION));
            jsonArray.add(createJsonObject("SCAN_PUT_LOC_CODE", "22", putaway_sumbit.SCAN_PUT_LOC_CODE));
            jsonArray.add(createJsonObject("SITE", "22", putaway_sumbit.SITE));
            //jsonArray.add(createJsonObject("ERROR_MESSAGE", "22", ""));


            JsonObject jsonObject = new JsonObject();
            //jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.addProperty("ProcName", "SP_SCN_AN_PUT_CON");
            jsonObject.add("dbparams", jsonArray);

            return jsonObject;
        }

        public static JsonObject getPutAwayMixedSite(String site, String scanLoc) {
            JsonObject jsonObject;
            JsonArray array = new JsonArray();

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("SITE", "22", site);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("LOCATION", "22", scanLoc);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEV_GUID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("USERID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "SP_LOCATION_GET_MIXED_BYSITE");
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        public static JsonObject getNewGACIds(final String jobNo, final int count) {
            JsonObject jsonObject;
            JsonArray array = new JsonArray();

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("JOB_NO", "22", jobNo);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("SRNO_COUNT", "8", count);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEVICE_ID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEV_GUID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("USERID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "SP_SCN_AN_GET_NEW_GACSCAN_ID");
            jsonObject.add("dbparams", array);

            return jsonObject;
        }


        public static JsonObject getConfirmationList(final String jobNo, final String prnCode) {
            JsonObject jsonObject;
            JsonArray array = new JsonArray();

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("JOB_NO", "22", jobNo);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("PRIN_CODE", "22", prnCode);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEVICE_ID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEV_GUID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("USERID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "SP_SCN_AN_INB_STG_SHORT_GET_INFO");
            jsonObject.add("dbparams", array);

            return jsonObject;
        }

        public static JsonObject getJobClosureList(final String prinCode) {
            JsonObject jsonObject;
            JsonArray array = new JsonArray();

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("PRIN_CODE", "22", prinCode);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEVICE_ID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEV_GUID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("USERID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "SP_SCN_AN_GET_JOB_INFO");
            jsonObject.add("dbparams", array);

            return jsonObject;
        }

        public static JsonObject getImageDetails(final String jobNo, final String prnCode) {
            JsonObject jsonObject;
            JsonArray array = new JsonArray();

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DOC_NO", "22", "000000000010202428");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DOC_TYPE", "22", "PRODUCT");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("FILE_TYPE", "22", "png");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEVICE_ID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEV_GUID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("USERID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "SP_SCN_AN_GET_DOCUMENT_API");
            jsonObject.add("dbparams", array);

            return jsonObject;
        }

        public static JsonObject getInvestigationData(INVESIGATE_REGISTER invesigateRegister) {
            if (invesigateRegister == null) {
                return null;
            }

            JsonArray jsonArray = new JsonArray();
            jsonArray.add(createJsonObject("USERID", "22", App.currentUser.USER_ID));
            jsonArray.add(createJsonObject("DEVICE_ID", "22", "App.DeviceID"));
            jsonArray.add(createJsonObject("DEV_GUID", "22", Utils.GetGUID()));
            jsonArray.add(createJsonObject("JOB_NO", "22", invesigateRegister.JOB_NO));
            jsonArray.add(createJsonObject("INVESTIGATION_REASON", "22", invesigateRegister.INVESTIGATION_REASON));
            jsonArray.add(createJsonObject("INVESTIGATION_REMARKS", "22", invesigateRegister.INVESTIGATION_REMARKS));
            jsonArray.add(createJsonObject("INVESTIGATION_NO", "22", invesigateRegister.INVESTIGATION_NO));

            JsonObject jsonObject = new JsonObject();
            //jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.addProperty("ProcName", "SP_SCN_AN_INVESIGATE_REG");
            jsonObject.add("dbparams", jsonArray);

            return jsonObject;
        }

        public static JsonObject getSerialScanData(final BARCODE_FRMT barcode_frmt, final String jobNo, List<BOX_SCAN_GACID> scanGacids) {
            if (scanGacids.size() <=0 ) {
                return null;
            }

            JsonArray jsonArray = new JsonArray();
            jsonArray.add(createJsonObject("USERID", "22", App.currentUser.USER_ID));
            jsonArray.add(createJsonObject("DEVICE_ID", "22",  "App.DeviceID"));
            jsonArray.add(createJsonObject("DEV_GUID", "22", Utils.GetGUID()));
            jsonArray.add(createJsonObject("PRIN_CODE", "22", barcode_frmt.PRIN_CODE));
            jsonArray.add(createJsonObject("PROD_CODE", "22", barcode_frmt.PRIN_CODE));
            jsonArray.add(createJsonObject("GACSCANID", "22", ""));
            jsonArray.add(createJsonObject("JOB_NO", "22", jobNo));
            jsonArray.add(createJsonObject("BARCODE", "22", getSerialBarcodeInFormat(scanGacids)));

            JsonObject jsonObject = new JsonObject();
            //jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.addProperty("ProcName", "SP_SCN_AN_SN_EXP_SAVE");
            jsonObject.add("dbparams", jsonArray);

            return jsonObject;
        }

        public static String getSerialBarcodeInFormat( List<BOX_SCAN_GACID> dataList){
            StringBuffer barcodeData= new StringBuffer();
            int size = dataList.size();
            for (BOX_SCAN_GACID boxScanGacid:dataList) {
                size--;
                barcodeData.append(boxScanGacid.GTIN==null? "":boxScanGacid.GTIN);
                barcodeData.append(App.SSBarCodeSeparator);
                barcodeData.append(boxScanGacid.LOT_NO==null ? "":boxScanGacid.LOT_NO);
                barcodeData.append(App.SSBarCodeSeparator);
                barcodeData.append(boxScanGacid.MANF_DT==null? "000000":boxScanGacid.MANF_DT);
                barcodeData.append(App.SSBarCodeSeparator);
                barcodeData.append(boxScanGacid.EXP_DT==null? "000000":boxScanGacid.EXP_DT);
                barcodeData.append(App.SSBarCodeSeparator);
                barcodeData.append(boxScanGacid.SLNO==null? "":boxScanGacid.SLNO);
                barcodeData.append(App.SSBarCodeSeparator);
                barcodeData.append(boxScanGacid.SSCC==null? "":boxScanGacid.SSCC);
                if(size>0)
                    barcodeData.append(App.SSBarCodelINESeparator);
            }
            return  barcodeData.toString();
        }


        public static JsonObject getProdUpdateInfo(PROD_UPD_INFO prodUpdInfo) {
            if (prodUpdInfo == null ) {
                return null;
            }

            JsonArray jsonArray = new JsonArray();
            jsonArray.add(createJsonObject("USERID", "22", App.currentUser.USER_ID));
            jsonArray.add(createJsonObject("DEVICE_ID", "22", "App.DeviceID"));
            jsonArray.add(createJsonObject("DEV_GUID", "22", Utils.GetGUID()));
            jsonArray.add(createJsonObject("JOB_NO", "22", prodUpdInfo.JOB_NO));
            jsonArray.add(createJsonObject("PROD_CODE", "22", prodUpdInfo.PROD_CODE));
            jsonArray.add(createJsonObject("PRIN_CODE", "22", prodUpdInfo.PRIN_CODE));
            jsonArray.add(createJsonObject("STORAGE_TYPE", "22", prodUpdInfo.STORAGE_TYPE));
            jsonArray.add(createJsonObject("COO", "22", prodUpdInfo.COO));
            jsonArray.add(createJsonObject("PART_SET", "8", prodUpdInfo.PART_SET));
            jsonArray.add(createJsonObject("IMAGE_CENSOR", "8", prodUpdInfo.IMAGE_CENSOR));
            jsonArray.add(createJsonObject("DANGEROUS_GOODS", "8", prodUpdInfo.DANGEROUS_GOODS));

            JsonObject jsonObject = new JsonObject();
            //jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.addProperty("ProcName", "SP_SCN_AN_PROD_UPD_INFO");
            jsonObject.add("dbparams", jsonArray);

            return jsonObject;
        }


        public static JsonObject getJobClosureData(final JOB_CLOSURE_INFO jobClosureInfo) {
            if (jobClosureInfo == null) {
                return null;
            }

            JsonArray jsonArray = new JsonArray();
            jsonArray.add(createJsonObject("USERID", "22", App.currentUser.USER_ID));
            jsonArray.add(createJsonObject("DEVICE_ID", "22", "App.DeviceID"));
            jsonArray.add(createJsonObject("DEV_GUID", "22", Utils.GetGUID()));
            jsonArray.add(createJsonObject("JOB_NO", "22", jobClosureInfo.JOB_NO));
            jsonArray.add(createJsonObject("PRIN_CODE", "22", jobClosureInfo.PRIN_CODE));

            JsonObject jsonObject = new JsonObject();
            //jsonObject.addProperty("DBName", App.currentCompany.DbName);
            jsonObject.addProperty("ProcName", "SP_SCN_AN_JOB_CLOSE_AUTO");
            jsonObject.add("dbparams", jsonArray);

            return jsonObject;
        }

    }


    public static class SyncActivity {
        int callCounter = 0;

        private CustomProgressDialog progressDialog;
        private boolean mIsSilentMode;
        private Context mContext;

        public SyncActivity(Context context, boolean isSilentMode) {
            this.mContext = context;
            this.mIsSilentMode = isSilentMode;
        }



        void showToast(String message) {
            Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
        }

        void showProgress(boolean isCancelable) {
            if (progressDialog != null && progressDialog.isShowing())
                return;

            progressDialog = new CustomProgressDialog(mContext, isCancelable);
            progressDialog.show();
        }

        public void updateProgressMessage(String message) {
            if (mIsSilentMode) {
                showToast(message);
                return;
            } else {
                writeProgressToUIActivity(message);
            }

/*            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.updateMessage(message);
            }*/
        }

        void writeProgressToUIActivity(String message) {
            try {

                if (TextUtils.isEmpty(message)) return;

                TextView textView;
                Activity activity = (Activity) mContext;
                if (activity != null) {
                    textView = activity.findViewById(R.id.tvMessage);
                } else {
                    return;
                }

                if (textView == null) return;

                activity.runOnUiThread(() -> {
                    textView.append("\n");
                    textView.append(message);
//                    textView.setMovementMethod(new ScrollingMovementMethod()); // no effect seen
                });

//                if (callCounter == 0 && textView.getText().toString().contains("failed")) {
//                    showToast("Something failed");
//                }

            } catch (Exception e) {
                Log.d(App.TAG, e.toString());
            }
        }

        void dismissProgress() {
            if (callCounter > 0) return;
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.cancel();
            }
        }

        public void Downloads(final AppConstants.DatabaseEntities entities, final int PK_ID) {
            showProgress(mIsSilentMode);
            JsonObject requestObject = null;
            switch (entities) {
                case LOCATION:
                    requestObject = MastersDownload.getLocationsObject();
                    break;
                case ORTYP:
                    requestObject = MastersDownload.getOrTypObject();
                    break;
                case RECTYP:
                    requestObject = MastersDownload.geRecTypObject();
                    break;
                default:
                    requestObject = MastersDownload.getMastersRequestObject(PK_ID);
                    break;
            }
            callCounter++;

            //DownloadFromWeb(entities, PK_ID, requestObject);
        }


        public void DownloadPrinterList(AppConstants.DatabaseEntities entities) {
            showProgress(false);
            callCounter++;
            JsonObject jsonObject;
            JsonArray array = new JsonArray();

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("WH_CODE", "22", App.currentWarehouse.WH_CODE);
            array.add(jsonObject);


            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEVICE_ID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEV_GUID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("USERID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "SP_SCN_AN_GET_PRINTER");
            jsonObject.add("dbparams", array);

            System.out.println("requestObject##" + jsonObject.toString());

            //showProgress(false);
            App.getNetworkClient().getAPIService().getRetGeneric(jsonObject, new Callback<GenericRetResponse>() {
                public void success(GenericRetResponse genericRetResponse, Response response) {
                    updateProgressMessage(String.format("Receiving %s...", entities.name()));
                    if (genericRetResponse.getErrCode().equals("S")) {
                        insertLocalDB(genericRetResponse.getJsonString(), entities, -1, "", "");
                    } else {
                        String msg = genericRetResponse.getErrMessage();
                        Log.d(App.TAG, msg);
                        showToast(msg);
                        updateProgressMessage("Download request failed.");
                    }

                    callCounter--;
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    updateProgressMessage(String.format("Receiving %s...", entities.name()));
                    String msg = error.getMessage();
                    Log.d(App.TAG, msg);
                    //showToast(msg);
                    callCounter--;
                    Log.d("INfailure", "INfailure" + String.valueOf(callCounter));
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                    updateProgressMessage("Download request failed." + msg);
                }
            });
        }

        public void DownloadPrintFormats(AppConstants.DatabaseEntities entities) {
            showProgress(false);
            callCounter++;
            JsonObject jsonObject;
            JsonArray array = new JsonArray();


            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEVICE_ID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEV_GUID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("USERID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "SP_SCN_AN_PRINT_LBL_FORMAT");
            jsonObject.add("dbparams", array);

            System.out.println("requestObject##"+jsonObject.toString());

            //showProgress(false);
            App.getNetworkClient().getAPIService().getRetGeneric(jsonObject, new Callback<GenericRetResponse>() {
                public void success(GenericRetResponse genericRetResponse, Response response) {
                    updateProgressMessage(String.format("Receiving %s...", entities.name()));
                    if (genericRetResponse.getErrCode().equals("S")) {
                        insertLocalDB(genericRetResponse.getJsonString(), entities, -1, "", "");
                    } else {
                        String msg = genericRetResponse.getErrMessage();
                        Log.d(App.TAG, msg);
                        showToast(msg);
                        updateProgressMessage("Download request failed.");
                    }

                    callCounter--;
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    updateProgressMessage(String.format("Receiving %s...", entities.name()));
                    String msg = error.getMessage();
                    Log.d(App.TAG, msg);
                    //showToast(msg);
                    callCounter--;
                    Log.d("INfailure", "INfailure" + String.valueOf(callCounter));
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                    updateProgressMessage("Download request failed." + msg);
                }
            });
        }

        public void DownloadLotStatus(AppConstants.DatabaseEntities entities) {
            showProgress(false);
            callCounter++;
            JsonObject jsonObject;
            JsonArray array = new JsonArray();

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("PRIN_CODE", "22", "566");
            array.add(jsonObject);


            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEVICE_ID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEV_GUID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("USERID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "SP_SCN_AN_GET_LOT_STATUS");
            jsonObject.add("dbparams", array);

            System.out.println("requestObject##" + jsonObject.toString());

            //showProgress(false);
            App.getNetworkClient().getAPIService().getRetGeneric(jsonObject, new Callback<GenericRetResponse>() {
                public void success(GenericRetResponse genericRetResponse, Response response) {
                    updateProgressMessage(String.format("Receiving %s...", entities.name()));
                    if (genericRetResponse.getErrCode().equals("S")) {
                        insertLocalDB(genericRetResponse.getJsonString(), entities, -1, "", "");
                    } else {
                        String msg = genericRetResponse.getErrMessage();
                        Log.d(App.TAG, msg);
                        showToast(msg);
                        updateProgressMessage("Download request failed.");
                    }

                    callCounter--;
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    updateProgressMessage(String.format("Receiving %s...", entities.name()));
                    String msg = error.getMessage();
                    Log.d(App.TAG, msg);
                    //showToast(msg);
                    callCounter--;
                    Log.d("INfailure", "INfailure" + String.valueOf(callCounter));
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                    updateProgressMessage("Download request failed." + msg);
                }
            });
        }

        public void DownloadUserPrincipleInfo(AppConstants.DatabaseEntities entities) {
            showProgress(false);
            callCounter++;
            JsonObject jsonObject;
            JsonArray array = new JsonArray();

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEVICE_ID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEV_GUID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("USERID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "SP_SCN_AN_USR_ACS_INFO_BY_USERID");
            jsonObject.add("dbparams", array);

            System.out.println("requestObject##" + jsonObject.toString());

            //showProgress(false);
            App.getNetworkClient().getAPIService().getRetGeneric(jsonObject, new Callback<GenericRetResponse>() {
                public void success(GenericRetResponse genericRetResponse, Response response) {
                    updateProgressMessage(String.format("Receiving %s...", entities.name()));
                    if (genericRetResponse.getErrCode().equals("S")) {
                        insertLocalDB(genericRetResponse.getJsonString(), entities, -1, "", "");
                    } else {
                        String msg = genericRetResponse.getErrMessage();
                        Log.d(App.TAG, msg);
                        showToast(msg);
                        updateProgressMessage("Download request failed.");
                    }

                    callCounter--;
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    updateProgressMessage(String.format("Receiving %s...", entities.name()));
                    String msg = error.getMessage();
                    Log.d(App.TAG, msg);
                    //showToast(msg);
                    callCounter--;
                    Log.d("INfailure", "INfailure" + String.valueOf(callCounter));
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                    updateProgressMessage("Download request failed." + msg);
                }
            });
        }

        public void DownloadInvestReason(AppConstants.DatabaseEntities entities) {
            showProgress(false);
            callCounter++;
            JsonObject jsonObject;
            JsonArray array = new JsonArray();


            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEVICE_ID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEV_GUID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("USERID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "SP_SCN_AN_GET_INVES_REASON");
            jsonObject.add("dbparams", array);

            System.out.println("requestObject##" + jsonObject.toString());

            //showProgress(false);
            App.getNetworkClient().getAPIService().getRetGeneric(jsonObject, new Callback<GenericRetResponse>() {
                public void success(GenericRetResponse genericRetResponse, Response response) {
                    updateProgressMessage(String.format("Receiving %s...", entities.name()));
                    if (genericRetResponse.getErrCode().equals("S")) {
                        insertLocalDB(genericRetResponse.getJsonString(), entities, -1, "", "");
                    } else {
                        String msg = genericRetResponse.getErrMessage();
                        Log.d(App.TAG, msg);
                        showToast(msg);
                        updateProgressMessage("Download request failed.");
                    }

                    callCounter--;
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    updateProgressMessage(String.format("Receiving %s...", entities.name()));
                    String msg = error.getMessage();
                    Log.d(App.TAG, msg);
                    //showToast(msg);
                    callCounter--;
                    Log.d("INfailure", "INfailure" + String.valueOf(callCounter));
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                    updateProgressMessage("Download request failed." + msg);
                }
            });
        }

        public void DownloadCountryList(AppConstants.DatabaseEntities entities) {
            showProgress(false);
            callCounter++;
            JsonObject jsonObject;
            JsonArray array = new JsonArray();

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEVICE_ID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEV_GUID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("USERID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "SP_SCN_AN_GET_COUNTRY");
            jsonObject.add("dbparams", array);

            System.out.println("requestObject##" + jsonObject.toString());

            //showProgress(false);
            App.getNetworkClient().getAPIService().getRetGeneric(jsonObject, new Callback<GenericRetResponse>() {
                public void success(GenericRetResponse genericRetResponse, Response response) {
                    updateProgressMessage(String.format("Receiving %s...", entities.name()));
                    if (genericRetResponse.getErrCode().equals("S")) {
                        insertLocalDB(genericRetResponse.getJsonString(), entities, -1, "", "");
                    } else {
                        String msg = genericRetResponse.getErrMessage();
                        Log.d(App.TAG, msg);
                        showToast(msg);
                        updateProgressMessage("Download request failed.");
                    }

                    callCounter--;
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    updateProgressMessage(String.format("Receiving %s...", entities.name()));
                    String msg = error.getMessage();
                    Log.d(App.TAG, msg);
                    //showToast(msg);
                    callCounter--;
                    Log.d("INfailure", "INfailure" + String.valueOf(callCounter));
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                    updateProgressMessage("Download request failed." + msg);
                }
            });
        }

        public void DownloadMasterList(AppConstants.DatabaseEntities entities, final String type, final String prinCode) {
            showProgress(false);
            callCounter++;
            JsonObject jsonObject;
            JsonArray array = new JsonArray();

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEVICE_ID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEV_GUID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("USERID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("PRIN_CODE", "22", prinCode);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("TYPE", "22", type);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "SP_SCN_AN_GET_MASTER");
            jsonObject.add("dbparams", array);

            System.out.println("requestObject##" + jsonObject.toString());

            //showProgress(false);
            App.getNetworkClient().getAPIService().getRetGeneric(jsonObject, new Callback<GenericRetResponse>() {
                public void success(GenericRetResponse genericRetResponse, Response response) {
                    updateProgressMessage(String.format("Receiving %s...", entities.name()));
                    if (genericRetResponse.getErrCode().equals("S")) {
                        insertLocalDB(genericRetResponse.getJsonString(), entities, -1, "", "");
                    } else {
                        String msg = genericRetResponse.getErrMessage();
                        Log.d(App.TAG, msg);
                        showToast(msg);
                        updateProgressMessage("Download request failed.");
                    }

                    callCounter--;
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    updateProgressMessage(String.format("Receiving %s...", entities.name()));
                    String msg = error.getMessage();
                    Log.d(App.TAG, msg);
                    //showToast(msg);
                    callCounter--;
                    Log.d("INfailure", "INfailure" + String.valueOf(callCounter));
                    if (callCounter == 0) {
                        dismissProgress();
                    }
                    updateProgressMessage("Download request failed." + msg);
                }
            });
        }

        public void insertLocalDB(final String jsonString, final AppConstants.DatabaseEntities entities, final long PK_ID, final String DocNo, final String DocType) {
            if (jsonString == null || jsonString.trim().length() == 0) {
                updateProgressMessage(entities.name() + ": No data received.");
                return;
            }

            Thread thread = new Thread(() -> {
                Gson gson = new Gson();
                switch (entities) {
                    case PRINTER_LIST:
                        try {
                            PRINTER_LIST[] printer_lists = gson.fromJson(jsonString, PRINTER_LIST[].class);
                            App.getDatabaseClient().getAppDatabase().genericDao().deleteAllPRINTER_LIST();
                            App.getDatabaseClient().getAppDatabase().genericDao().insertPRINTER_LIST(printer_lists);
                            updateProgressMessage(entities.name() + ": " + printer_lists.length + " data received.");
                        } catch (Exception e) {
                            Log.d(App.TAG, e.toString());
//                            updateProgressMessage("Download failed: " + entities.name());
                        }
                        break;
                    case PRINTER_LIST_SERVICE:
                        try {
                            PRINTER_LIST_SERVICE[] printer_lists = gson.fromJson(jsonString, PRINTER_LIST_SERVICE[].class);
                            App.getDatabaseClient().getAppDatabase().genericDao().deleteAllPRINTER_LIST();
                            App.getDatabaseClient().getAppDatabase().genericDao().insertPRINTER_LIST_SERVICE(printer_lists);
                            updateProgressMessage(entities.name() + ": " + printer_lists.length + " data received.");
                        } catch (Exception e) {
                            Log.d(App.TAG, e.toString());
//                            updateProgressMessage("Download failed: " + entities.name());
                        }
                        break;
                    case PRINT_FORMATS:
                        try {
                            PRINT_FORMAT[] print_formats = gson.fromJson(jsonString, PRINT_FORMAT[].class);
                            App.getDatabaseClient().getAppDatabase().genericDao().deleteAllPRINT_FORMAT();
                            App.getDatabaseClient().getAppDatabase().genericDao().insertPRINT_FORMAT(print_formats);
                            updateProgressMessage(entities.name() + ": " + print_formats.length + " data received.");
                        } catch (Exception e) {
                            Log.d(App.TAG, e.toString());
//                            updateProgressMessage("Download failed: " + entities.name());
                        }
                        break;
                    case LOT_STATUS:
                        try {
                            LOT_STATUS_LIST[] lot_status_lists = gson.fromJson(jsonString, LOT_STATUS_LIST[].class);
                            App.getDatabaseClient().getAppDatabase().genericDao().deleteAllLOT_STATUS_LIST();
                            App.getDatabaseClient().getAppDatabase().genericDao().insertLOT_STATUS_LIST(lot_status_lists);
                            updateProgressMessage(entities.name() + ": " + lot_status_lists.length + " data received.");
                        } catch (Exception e) {
                            Log.d(App.TAG, e.toString());
//                            updateProgressMessage("Download failed: " + entities.name());
                        }
                        break;
                    case INVEST_REASON:
                        try {
                            INVEST_REASONS[] investReasons = gson.fromJson(jsonString, INVEST_REASONS[].class);
                            App.getDatabaseClient().getAppDatabase().genericDao().deleteAllINVEST_REASONS();
                            App.getDatabaseClient().getAppDatabase().genericDao().insertINVEST_REASONS(investReasons);
                            updateProgressMessage(entities.name() + ": " + investReasons.length + " data received.");
                        } catch (Exception e) {
                            Log.d(App.TAG, e.toString());
//                            updateProgressMessage("Download failed: " + entities.name());
                        }
                        break;
                    case USER_PRIN_INFO:
                        try {
                            USER_PRIN_INFO[] userPrinInfos = gson.fromJson(jsonString, USER_PRIN_INFO[].class);
                            App.getDatabaseClient().getAppDatabase().genericDao().deleteAllUSER_PRIN_INFO();
                            App.getDatabaseClient().getAppDatabase().genericDao().insertUSER_PRIN_INFO(userPrinInfos);
                            updateProgressMessage(entities.name() + ": " + userPrinInfos.length + " data received.");
                        } catch (Exception e) {
                            Log.d(App.TAG, e.toString());
//                            updateProgressMessage("Download failed: " + entities.name());
                        }
                        break;
                    case COUNTRY_LIST:
                        try {
                            COUNTRY_LIST[] countryLists = gson.fromJson(jsonString, COUNTRY_LIST[].class);
                            App.getDatabaseClient().getAppDatabase().genericDao().deleteAllCOUNTRY_LIST();
                            App.getDatabaseClient().getAppDatabase().genericDao().insertCOUNTRY_LIST(countryLists);
                            updateProgressMessage(entities.name() + ": " + countryLists.length + " data received.");
                        } catch (Exception e) {
                            Log.d(App.TAG, e.toString());
//                            updateProgressMessage("Download failed: " + entities.name());
                        }
                        break;
                    case MASTER_LIST:
                        try {
                            MASTER_LIST[] masterLists = gson.fromJson(jsonString, MASTER_LIST[].class);
                            App.getDatabaseClient().getAppDatabase().genericDao().deleteAllMASTER_LIST();
                            App.getDatabaseClient().getAppDatabase().genericDao().insertMASTER_LIST(masterLists);
                            updateProgressMessage(entities.name() + ": " + masterLists.length + " data received.");
                        } catch (Exception e) {
                            Log.d(App.TAG, e.toString());
//                            updateProgressMessage("Download failed: " + entities.name());
                        }
                        break;
                    default:
                        break;
                }
            });
            thread.start();
        }

        public static JsonObject GetImagesToUpload(PHOTO_MASTER photo_master) {
            JsonObject resultJsonObject = null;
            if (photo_master == null) {
                return null;
            }

            byte[] image = null;
            try {
                File file = new File(photo_master.PHOTO_PATH);
                image = new byte[(int) file.length()];
                InputStream is = new FileInputStream(file);
                is.read(image);
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                JSONObject imageObjectToSend = new JSONObject();
                imageObjectToSend.put("ProcName", "APP.TRACK_IMAGE_SPS");
                imageObjectToSend.put("GUID", "123");
                imageObjectToSend.put("RefGUID", "data.MASTER_ID");
                imageObjectToSend.put("Remarks", "data.SEQ_NO");
                imageObjectToSend.put("SeqNo", "1");
                imageObjectToSend.put("FileExt", ".jpg");

                JSONArray imageContent = new JSONArray();
                for (int i = 0; i < image.length; i++) {
                    imageContent.put(0xFF & image[i]);
                }
                imageObjectToSend.put("Content", imageContent);

                JsonParser jsonParser = new JsonParser();
                resultJsonObject = (JsonObject) jsonParser.parse(imageObjectToSend.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return resultJsonObject;
        }

        public static JsonObject GetImpoundImages(long pk_id) {
            JsonObject resultJsonObject = null;
            JsonArray array = new JsonArray();
            List<PHOTO_MASTER> data = App.getDatabaseClient().getAppDatabase().genericDao().getPhotoMasterUpload();
            if (data == null) {
                return null;
            }

            for (PHOTO_MASTER photo : data) {

                byte[] image = null;
                try {
                    File file = new File(photo.PHOTO_PATH);
                    image = new byte[(int) file.length()];
                    InputStream is = new FileInputStream(file);
                    is.read(image);
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    JSONObject imageObjectToSend = new JSONObject();
                    imageObjectToSend.put("ProcName", "APP.TRACK_IMAGE_SPS");
//                    imageObjectToSend.put("TRANS_ID", data.TRACK_ID);
//                    imageObjectToSend.put("MASTER_ID", data.MASTER_ID);
//                    imageObjectToSend.put("SeqNo", data.SEQ_NO);
//                    imageObjectToSend.put("FileExt", data.FILE_EXT);
//                    imageObjectToSend.put("Remarks", data.REMARKS);

                    JSONArray imageContent = new JSONArray();
                    for (int i = 0; i < image.length; i++) {
                        imageContent.put(0xFF & image[i]);
                    }
                    imageObjectToSend.put("Content", imageContent);

                    JsonParser jsonParser = new JsonParser();
                    resultJsonObject = (JsonObject) jsonParser.parse(imageObjectToSend.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


            return resultJsonObject;
        }


    }


    public static JsonObject ValidateUserLogin(String userName, String password) {
        JsonObject jsonObject;

        JsonArray array = new JsonArray();
        jsonObject = createJsonObject("USER_NAME", "22", userName);
        array.add(jsonObject);
        jsonObject = createJsonObject("USER_PASSWORD", "22", password);
        array.add(jsonObject);

        jsonObject = new JsonObject();
        jsonObject.addProperty("SPName", "SAP.VALIDATEUSER");
        jsonObject.addProperty("DBName", App.currentCompany.DbName);
        jsonObject.addProperty("USER_PASSWORD", userName);
        jsonObject.addProperty("USER_NAME", password);
        //jsonObject.add("dbparams", array);
        return jsonObject;
    }


    public static class UserValidation {

        public static JsonObject getUserLoginDetails(final String userName, final String passWord) {
            JsonObject jsonObject;
            JsonArray array = new JsonArray();

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("USER_ID", "22", userName);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("PASSWORD", "22", passWord);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEVICE_ID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEV_GUID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("USERID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "SP_SCN_AN_USR_PCKETPC_LOGIN");
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        public static JsonObject getWareHouseList() {
            JsonObject jsonObject;
            JsonArray array = new JsonArray();

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEVICE_ID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEV_GUID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("USERID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "SP_SCN_AN_GET_WAREHOUSE");
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        public static JsonObject getAppMode() {
            JsonObject jsonObject;
            JsonArray array = new JsonArray();

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEVICE_ID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEV_GUID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("USERID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "SP_SCN_AN_GET_WAREHOUSE");
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        public static JsonObject setAfterLoginProcess(final USERS user ) {
            JsonObject jsonObject;
            JsonArray array = new JsonArray();

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("USER_ID", "22", user.USER_ID);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("STATUS", "22", user.STATUS);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("SCAN_ID", "22", user.SCAN_ID);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("SCAN_MODULE", "22", user.SCAN_MODULE);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("MAC_ADDRESS", "22", user.MAC_ADDRESS);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEVICE_ID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEV_GUID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("USERID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "SP_SCN_AN_USR_PCKETPC_UPD");
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        public static JsonObject setDynamicValidationData() {
            JsonObject jsonObject;
            JsonArray array = new JsonArray();

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEVICE_ID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEV_GUID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("USERID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "SP_SCN_AN_LIST_OF_FUNCTIONID");
            jsonObject.add("dbparams", array);
            return jsonObject;
        }
    }

    public static class OutBound {
        public static JsonObject getSP_SCN_AN_OUT_LOC_BY_JOB() {
            JsonObject jsonObject;
            JsonArray array = new JsonArray();

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEVICE_ID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEV_GUID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("USERID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("USER", "22", App.currentUser.USER_ID);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "SP_SCN_AN_OUT_LOC_BY_JOB");
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        public static JsonObject getSP_SCN_AN_OUT_ASSIGNED_TRUCKS() {
            JsonObject jsonObject;
            JsonArray array = new JsonArray();

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEVICE_ID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("DEV_GUID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("USERID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject = createJsonObject("USER", "22", App.currentUser.USER_ID);
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "SP_SCN_AN_OUT_ASSIGNED_TRUCKS");
            jsonObject.add("dbparams", array);
            return jsonObject;
        }

        public static JsonObject getSP_SCN_AN_OUT_PICK_PENDING() {
            JsonObject jsonObject;
            JsonArray array = new JsonArray();

            jsonObject = createJsonObject("DEVICE_ID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = createJsonObject("DEV_GUID", "22", "DEV-Test");
            array.add(jsonObject);

            jsonObject = createJsonObject("USERID", "22", App.currentUser.USER_ID);
            array.add(jsonObject);

            jsonObject = createJsonObject("USER", "22", App.currentUser.USER_ID);
            array.add(jsonObject);

            jsonObject = createJsonObject("VALUE_TYPE", "22", "JOB");
            array.add(jsonObject);

            jsonObject = new JsonObject();
            jsonObject.addProperty("ProcName", "SP_SCN_AN_OUT_PICK_PENDING");
            jsonObject.add("dbparams", array);
            return jsonObject;
        }
    }


}
