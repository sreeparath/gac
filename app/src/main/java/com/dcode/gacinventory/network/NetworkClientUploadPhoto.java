package com.dcode.gacinventory.network;


import android.util.Log;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppVariables;

import java.io.File;
import java.io.IOException;

import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class NetworkClientUploadPhoto {
    private static NetworkClientUploadPhoto mInstance;
    String FILE_UPLOAD_URL;
    okhttp3.OkHttpClient client;

    private NetworkClientUploadPhoto() {
        try{
            FILE_UPLOAD_URL  = App.appSettings().getFileUploadUrl();
            Log.d("FILE_UPLOAD_URL##",FILE_UPLOAD_URL);
            client = new okhttp3.OkHttpClient();

            File file = new File("/storage/emulated/0/DCIM/Camera/20210804105824.png");

                okhttp3.RequestBody body = new okhttp3.MultipartBody.Builder()
                .setType(MultipartBody.FORM)


                .addFormDataPart("Files",file.getName(), RequestBody.create(MediaType.parse("image/png"),file) )
                .addFormDataPart("Files",file.getName(),RequestBody.create(MediaType.parse("image/png"),file) )
                .addFormDataPart("CreatedUser", "Robert")
                .addFormDataPart("ReferenceNumber", "123")
                .addFormDataPart("FileId", "f1")
                .addFormDataPart("Module","job")
                .build();

            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(FILE_UPLOAD_URL)
                    .post(body)
                    .addHeader("Authorization", AppVariables.getAccessToken())
                    .addHeader("Accept", "*/*")
                    .addHeader("Ocp-Apim-Subscription-Key",App.appSettings().getSUBSCRIPTION_KEY())
                    .build();

            try  {
                Log.d("request#",request.body().toString());
                okhttp3.Call call = client.newCall(request);
                // Execute the call asynchronously.
                call.enqueue(new okhttp3.Callback() {


                    @Override
                    public void onFailure(Call call, IOException e) {
                        Log.d("TAG_OK_HTTP_FILE_OPERATE", e.getMessage());
                        destroyInstance();
                    }

                    @Override
                    public void onResponse(Call call, okhttp3.Response response) throws IOException {
                        int respCode = response.code();
                        String respMsg = response.message();
                        String respBody = response.body().string();

                        Log.d("Response##", "Response code : " + respCode);
                        Log.d("Response##", "Response message : " + respMsg);
                        Log.d("Response##", "Response body : " + respBody);
                        destroyInstance();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();

                destroyInstance();
            }

        } catch (Exception e) {
            e.printStackTrace();
            destroyInstance();
        }
    }

    public void uploadImagetoServer(RequestBody body){
        try{

//        MultipartBody.Builder bodyBuilder = new MultipartBody.Builder();
//        MultipartBody.Builder builder = bodyBuilder.setType(MultipartBody.FORM);
//        builder.addFormDataPart("Files",file.getName(), RequestBody.create(MediaType.parse("image/png"),file) );
//        builder.addFormDataPart("Files",file.getName(),RequestBody.create(MediaType.parse("image/png"),file) );
//        builder.addFormDataPart("CreatedUser", "Robert");
//        builder.addFormDataPart("ReferenceNumber", "123");
//        builder.addFormDataPart("FileId", "f1");
//        builder.addFormDataPart("Module","job");
//        builder.build();

//        Arrays.stream(keys)
//                .forEach(key -> {
//                    builder.addFormDataPart(key, object.optString(key));
//                });
//
//        okhttp3.RequestBody body = new okhttp3.MultipartBody.Builder()
//                .setType(MultipartBody.FORM)
//                .addFormDataPart("Files",file.getName(), RequestBody.create(MediaType.parse("image/png"),file) )
//                .addFormDataPart("Files",file.getName(),RequestBody.create(MediaType.parse("image/png"),file) )
//                .addFormDataPart("CreatedUser", "Robert")
//                .addFormDataPart("ReferenceNumber", "123")
//                .addFormDataPart("FileId", "f1")
//                .addFormDataPart("Module","job")
//                .build();


            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(FILE_UPLOAD_URL)
                    .post(body)
                    .addHeader("Authorization", AppVariables.getAccessToken())
                    .addHeader("Accept", "*/*")
                    .addHeader("Ocp-Apim-Subscription-Key",App.appSettings().getSUBSCRIPTION_KEY())
                    .build();

            // Thread thread = new Thread(new Runnable() {

            //   @Override
            // public void run() {
            try  {
                Log.d("request#",request.body().toString());
                okhttp3.Call call = client.newCall(request);
                // Execute the call asynchronously.
                call.enqueue(new okhttp3.Callback() {


                    @Override
                    public void onFailure(Call call, IOException e) {
                        Log.d("TAG_OK_HTTP_FILE_OPERATE", e.getMessage());
                        destroyInstance();
                    }

                    @Override
                    public void onResponse(Call call, okhttp3.Response response) throws IOException {
                        int respCode = response.code();
                        String respMsg = response.message();
                        String respBody = response.body().string();

                        Log.d("Response##", "Response code : " + respCode);
                        Log.d("Response##", "Response message : " + respMsg);
                        Log.d("Response##", "Response body : " + respBody);
                        destroyInstance();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();

                destroyInstance();
            }
            // }
            // });
//
            //thread.start();

        } catch (Exception e) {
            e.printStackTrace();
            destroyInstance();
        }
    }


    public static synchronized NetworkClientUploadPhoto getInstance() {
        if (mInstance == null) {
            mInstance = new NetworkClientUploadPhoto();
        }
        Log.d("mInstance##","mInstance#");
        return mInstance;
    }

    public void destroyInstance() {
        mInstance = null;
    }
}
