package com.dcode.gacinventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "OBJECTS")
public class OBJECTS implements Serializable {

    @PrimaryKey
    @ColumnInfo(name = "OBJECT_ID")
    public long OBJECT_ID;

    @ColumnInfo(name = "PARENT_OBJECT_ID")
    public long PARENT_OBJECT_ID;

    @ColumnInfo(name = "MASTER_ID")
    public long MASTER_ID;

    @ColumnInfo(name = "OBJECT_CODE")
    public String OBJECT_CODE;

    @ColumnInfo(name = "OBJECT_NAME")
    public String OBJECT_NAME;

    @Override
    public String toString() {
        return   String.format("%s ( %s )", OBJECT_NAME, OBJECT_CODE);
    }
}
