package com.dcode.gacinventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "LOGISTIC_LABEL_SUMBIT")
public class LOGISTIC_LABEL_SUMBIT implements Serializable {

    @ColumnInfo(name = "SRNO")
    @PrimaryKey()
    public String SRNO;

    @ColumnInfo(name = "SRNO_ORIGINAL")
    public String SRNO_ORIGINAL;

    @ColumnInfo(name = "JOB_NO")
    public String JOB_NO;

    @ColumnInfo(name = "MFG_DATE")
    public String MFG_DATE;

    @ColumnInfo(name = "EXP_DATE")
    public String EXP_DATE;

    @ColumnInfo(name = "EXPDATE")
    public String EXPDATE;


    @ColumnInfo(name = "LOT_NO")
    public String LOT_NO;

    @ColumnInfo(name = "LOT_STATUS")
    public String LOT_STATUS;

    @ColumnInfo(name = "FORMULA_NO")
    public String FORMULA_NO;

    @ColumnInfo(name = "FIL_CODE")
    public String FIL_CODE;

    @ColumnInfo(name = "BARCODE")
    public String BARCODE;

    @ColumnInfo(name = "INPUT_PQTY")
    public String INPUT_PQTY;

    @ColumnInfo(name = "INPUT_MQTY")
    public String INPUT_MQTY;

    @ColumnInfo(name = "INPUT_LQTY")
    public String INPUT_LQTY;

    @ColumnInfo(name = "USER_NAME")
    public String USER_NAME;

    @ColumnInfo(name = "UPDATE_FROM")
    public String UPDATE_FROM;

    @ColumnInfo(name = "SCAN_PUT_LOC_CODE")
    public String SCAN_PUT_LOC_CODE;

    @ColumnInfo(name = "PALLET_ID")
    public String PALLET_ID;

    @ColumnInfo(name = "LOC_CODE")
    public String LOC_CODE;

    @ColumnInfo(name = "SITE")
    public String SITE;

    @ColumnInfo(name = "LOC")
    public String LOC;

    @ColumnInfo(name = "Pqty")
    public String Pqty;

    @ColumnInfo(name = "Mqty")
    public String Mqty;

    @ColumnInfo(name = "Lqty")
    public String Lqty;

    @ColumnInfo(name = "IS_STAGED")
    public String IS_STAGED;

    @ColumnInfo(name = "PRIN_NAME")
    public String PRIN_NAME;

    @ColumnInfo(name = "DESCRIPTION")
    public String DESCRIPTION;

    @ColumnInfo(name = "PRIN_CODE")
    public String PRIN_CODE;

    @ColumnInfo(name = "STG_QTY")
    public String STG_QTY;

    @ColumnInfo(name = "TOTAL_QTY")
    public String TOTAL_QTY;

    @ColumnInfo(name = "PUT_QTY")
    public String PUT_QTY;


    @ColumnInfo(name = "PUOM")
    public String PUOM;

    @ColumnInfo(name = "LUOM")
    public String LUOM;

    @ColumnInfo(name = "MUOM")
    public String MUOM;

    @ColumnInfo(name = "LOC_STAT")
    public String LOC_STAT;

    @ColumnInfo(name = "SITE_TYPE")
    public String SITE_TYPE;

    @ColumnInfo(name = "IS_PUTAWAY")
    public String IS_PUTAWAY;

    @ColumnInfo(name = "UPPP")
    public int UPPP;

    @ColumnInfo(name = "LUPPP")
    public int LUPPP;

    @ColumnInfo(name = "QUANTITY")
    public int QUANTITY;

    @ColumnInfo(name = "TOTAL_PO_QTY")
    public int TOTAL_PO_QTY;

    @ColumnInfo(name = "BARCODE_SCAN_CHECK")
    public String BARCODE_SCAN_CHECK;

    @ColumnInfo(name = "PALLET_SCAN_CHECK")
    public String PALLET_SCAN_CHECK;

    @ColumnInfo(name = "ISLOTREQUIRED")
    public String ISLOTREQUIRED;

    @ColumnInfo(name = "ISMANUREQUIRED")
    public String ISMANUREQUIRED;

    @ColumnInfo(name = "ISEXPDATEREQUIRED")
    public String ISEXPDATEREQUIRED;

    @ColumnInfo(name = "CAL_BATCHNO")
    public String CAL_BATCHNO;

    @ColumnInfo(name = "SHELF_LIFE")
    public int SHELF_LIFE;

    @ColumnInfo(name = "JOBCLASS")
    public String JOBCLASS;

    @ColumnInfo(name = "BARCODE2")
    public String BARCODE2;

    @ColumnInfo(name = "BARCODE3")
    public String BARCODE3;

    @ColumnInfo(name = "BARCODE4")
    public String BARCODE4;

    @ColumnInfo(name = "BARCODE5")
    public String BARCODE5;

    @ColumnInfo(name = "BARCODE6")
    public String BARCODE6;

    @ColumnInfo(name = "COUNTRYNAME")
    public String COUNTRYNAME;

    @ColumnInfo(name = "PACK_KEY")
    public String PACK_KEY;

    @ColumnInfo(name = "DEPT_CODE")
    public String DEPT_CODE;

    @ColumnInfo(name = "STROKE")
    public String STROKE;

    @ColumnInfo(name = "COLOR_CODE")
    public String COLOR_CODE;

    @ColumnInfo(name = "SIZE")
    public String SIZE;

    @ColumnInfo(name = "SKU")
    public String SKU;

    @ColumnInfo(name = "TOTAL_PUT_QTY")
    public int TOTAL_PUT_QTY;


}
