package com.dcode.gacinventory.data.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "BARCODE_FRMT")
public class BARCODE_FRMT implements Serializable {

    @ColumnInfo(name = "SL")
    @PrimaryKey(autoGenerate = true)
    public long SL;

    @ColumnInfo(name = "BARCODE_SEQUENCE")
    @NonNull
    public String BARCODE_SEQUENCE;

    @ColumnInfo(name = "PRIN_NAME")
    @NonNull
    public String PRIN_NAME;

    @ColumnInfo(name = "PRIN_CODE")
    @NonNull
    public String PRIN_CODE;

    @Override
    public String toString() {
        return  BARCODE_SEQUENCE;
    }

}
