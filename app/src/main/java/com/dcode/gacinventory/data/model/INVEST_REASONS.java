package com.dcode.gacinventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;


@Entity(tableName = "INVEST_REASONS")
public class INVEST_REASONS {

    @ColumnInfo(name = "ID")
    @PrimaryKey(autoGenerate = true)
    public int ID;

    @ColumnInfo(name = "REASON_CODE")
    public String REASON_CODE;

    @ColumnInfo(name = "REASON_DESC")
    public String REASON_DESC;

    @ColumnInfo(name = "ERR_CODE")
    public String ERR_CODE;

    @Override
    public String toString() {
        return  REASON_DESC;
    }
}