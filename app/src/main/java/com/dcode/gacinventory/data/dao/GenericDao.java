package com.dcode.gacinventory.data.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.RawQuery;
import androidx.sqlite.db.SupportSQLiteQuery;


import com.dcode.gacinventory.data.model.APP_SETTINGS;
import com.dcode.gacinventory.data.model.BOX_SCAN_GACID;
import com.dcode.gacinventory.data.model.BOX_SCAN_PROD;
import com.dcode.gacinventory.data.model.CONFIRMATION_LIST;
import com.dcode.gacinventory.data.model.COUNTRY_LIST;
import com.dcode.gacinventory.data.model.GENERATE_LABELS;
import com.dcode.gacinventory.data.model.GENERIC_COUNT;
import com.dcode.gacinventory.data.model.INVEST_REASONS;
import com.dcode.gacinventory.data.model.LOT_STATUS_LIST;
import com.dcode.gacinventory.data.model.MASTER_LIST;
import com.dcode.gacinventory.data.model.MAX_VALUE;
import com.dcode.gacinventory.data.model.OBJECTS;
import com.dcode.gacinventory.data.model.PHOTO_MASTER;
import com.dcode.gacinventory.data.model.PRINTER_LIST;
import com.dcode.gacinventory.data.model.PRINTER_LIST_SERVICE;
import com.dcode.gacinventory.data.model.PRINT_FORMAT;
import com.dcode.gacinventory.data.model.USERS;
import com.dcode.gacinventory.data.model.USER_PRIN_INFO;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface GenericDao {

    @RawQuery
    MAX_VALUE getMaxValue(SupportSQLiteQuery query);

    @RawQuery
    GENERIC_COUNT getCount(SupportSQLiteQuery query);



    @Query("DELETE FROM APP_SETTINGS")
    void deleteAllAppSettings();

    @Insert(onConflict = REPLACE)
    void insertAllAppSettings(APP_SETTINGS... app_settings);

    @Query("SELECT * FROM APP_SETTINGS ORDER BY ID DESC LIMIT 1")
    APP_SETTINGS getAppSettings();

    // region " USERS "

    @Query("SELECT * FROM USERS LIMIT 1")
    USERS getAllUsers();

    @Query("SELECT * FROM USERS WHERE USER_NAME=:userName AND USER_PWD=:userPassword LIMIT 1")
    USERS validateUser(String userName, String userPassword);

    @Insert(onConflict = REPLACE)
    void insertUser(USERS... user);

    @Query("DELETE FROM USERS WHERE USER_ID=:userID")
    void deleteUser(String userID);

    @Query("DELETE FROM USERS")
    void deleteAllUser();

    @Query("SELECT * FROM USERS WHERE USER_NAME=:loginName AND USER_PWD=:passWd")
    USERS getUser(String loginName, String passWd);

    // endregion: " USERS "

    // region " OBJECTS "

    @Query("DELETE FROM OBJECTS WHERE MASTER_ID=:master_id")
    void deleteAllMasters(long master_id);

    @Insert(onConflict = REPLACE)
    void insertAllMasters(OBJECTS... objects);

    @Query("SELECT OBJECT_ID, PARENT_OBJECT_ID, MASTER_ID, OBJECT_CODE, OBJECT_NAME " +
            "FROM OBJECTS " +
            "WHERE MASTER_ID=:master_id ")
    List<OBJECTS> getMastersByMasterID(long master_id);

    @Query("SELECT OBJECT_ID, PARENT_OBJECT_ID, MASTER_ID, OBJECT_CODE, OBJECT_NAME " +
            "FROM OBJECTS " +
            "WHERE OBJECT_CODE=:master_code " +
            "AND PARENT_OBJECT_ID=:parent_id")
    OBJECTS getMastersByCode(String master_code, long parent_id);


    @Query("SELECT * FROM PHOTO_MASTER WHERE MODULE_ID=:mo_id AND JOB_ID =:jobId")
    List<PHOTO_MASTER> getPhotoMasterList(int mo_id,String jobId);

    @Insert(onConflict = REPLACE)
    void insertPhotoMaster(PHOTO_MASTER... photo_masters);

    @Insert(onConflict = REPLACE)
    void insertPRINTER_LIST(PRINTER_LIST... printer_lists);

    @Insert(onConflict = REPLACE)
    void insertPRINTER_LIST_SERVICE(PRINTER_LIST_SERVICE... printer_lists);

    @Query("SELECT * FROM PRINTER_LIST_SERVICE WHERE WH_CODE =:wCode")
    List<PRINTER_LIST_SERVICE> getPrintServiceList(String wCode);

    @Query("DELETE FROM PRINTER_LIST ")
    void deleteAllPRINTER_LIST();

    @Query("SELECT * FROM PRINTER_LIST")
    List<PRINTER_LIST> getPrintList();

    @Insert(onConflict = REPLACE)
    void insertPRINT_FORMAT(PRINT_FORMAT... print_formats);

    @Query("DELETE FROM PRINT_FORMAT ")
    void deleteAllPRINT_FORMAT();

    @Query("DELETE FROM LOT_STATUS_LIST ")
    void deleteAllLOT_STATUS_LIST();

    @Insert(onConflict = REPLACE)
    void insertLOT_STATUS_LIST(LOT_STATUS_LIST... lot_status_lists);

    @Insert(onConflict = REPLACE)
    void insertINVEST_REASONS(INVEST_REASONS... investReasons);

    @Query("DELETE FROM INVEST_REASONS ")
    void deleteAllINVEST_REASONS();

    @Query("DELETE FROM USER_PRIN_INFO ")
    void deleteAllUSER_PRIN_INFO();

    @Insert()
    void insertUSER_PRIN_INFO(USER_PRIN_INFO... infos);

    @Query("DELETE FROM COUNTRY_LIST ")
    void deleteAllCOUNTRY_LIST();

    @Query("DELETE FROM MASTER_LIST ")
    void deleteAllMASTER_LIST();

    @Insert()
    void insertMASTER_LIST(MASTER_LIST... masterLists);

    @Insert()
    void insertCOUNTRY_LIST(COUNTRY_LIST... countryLists);

    @Query("SELECT * FROM LOT_STATUS_LIST")
    List<LOT_STATUS_LIST> getLotStatus();

    @Query("SELECT * FROM INVEST_REASONS")
    List<INVEST_REASONS> getInvestReason();

    @Query("SELECT * FROM COUNTRY_LIST")
    List<COUNTRY_LIST> getCountryList();

    @Query("SELECT * FROM MASTER_LIST ")
    List<MASTER_LIST> getStorageList();

    @Query("SELECT DISTINCT PRIN_CODE FROM USER_PRIN_INFO")
    List<String> getUserPrinCodes();

    @Query("SELECT * FROM LOT_STATUS_LIST WHERE NEW_LOT_STATUS =:newLotStatus")
    LOT_STATUS_LIST getLotStatusFromKey(String newLotStatus);

    @Query("SELECT * FROM PRINT_FORMAT")
    List<PRINT_FORMAT> getPrintFormats();

    @Insert(onConflict = REPLACE)
    void insertGENERATE_LABELS(GENERATE_LABELS... generate_labels);

    @Query("SELECT * FROM GENERATE_LABELS WHERE JOB_NO =:jobNo ")
    List<GENERATE_LABELS> getGENERATE_LABELS(String jobNo);

    @Query("DELETE FROM CONFIRMATION_LIST ")
    void deleteAllCONFIRMATION_LIST();

    @Insert(onConflict = REPLACE)
    void insertCONFIRMATION_LIST(CONFIRMATION_LIST... confirmationLists);

//    @Query("SELECT * FROM CONFIRMATION_LIST WHERE JOB_NO =:jobNo ")
//    List<CONFIRMATION_LIST> getCONFIRMATION_LIST(String jobNo);

    @Query("SELECT  * FROM CONFIRMATION_LIST "+
            "WHERE "+
            "CASE " +
            "WHEN :filter = 'STAGE'  THEN IS_STAGED = 'N' "+
            "WHEN :filter = 'CONFIRM'   THEN IS_STAGED = 'Y' " +
            "WHEN :filter = 'ALL'   THEN IS_STAGED IS NOT NULL END ")
    List<CONFIRMATION_LIST> getCONFIRMATION_LIST(String filter);

    @Query("SELECT  * FROM CONFIRMATION_LIST "+
            "WHERE "+
            "CASE " +
            "WHEN :filter = 'PUTAWAY'  THEN IS_PUTAWAY = 'N' "+
            "WHEN :filter = 'CONFIRM'   THEN IS_PUTAWAY = 'Y' " +
            "WHEN :filter = 'ALL'   THEN IS_PUTAWAY IS NOT NULL END ")
    List<CONFIRMATION_LIST> getPutCONFIRMATION_LIST(String filter);

    @Query("UPDATE CONFIRMATION_LIST SET IS_CONFIRMED = 1 WHERE SRNO=:srNo AND PRIN_CODE =:prCode AND SKU =:sku")
    void updateConfirmedSerialNo(String srNo,String prCode,String sku);

    @Query("SELECT  * FROM PHOTO_MASTER WHERE IS_UPLOAD = 0")
    List<PHOTO_MASTER> getPhotoMasterUpload();

    @Insert(onConflict = REPLACE)
    void insertBOX_SCAN_GACID(BOX_SCAN_GACID... boxScanGacids);

    @Query("SELECT * FROM BOX_SCAN_GACID WHERE PRIN_CODE=:prnCode AND JOB_NO =:jobNo ")
    List<BOX_SCAN_GACID> getAllBOX_SCAN_GACID(String prnCode,String jobNo);

    @Query("SELECT  * FROM BOX_SCAN_GACID WHERE PRIN_CODE=:prnCode AND JOB_NO =:jobNo AND GTIN =:gtin")
    BOX_SCAN_GACID getBOX_SCAN_GACID_GTIN(String prnCode,String jobNo,String gtin);

    @Query("UPDATE BOX_SCAN_GACID SET QTY = QTY+200 WHERE PRIN_CODE=:prnCode AND JOB_NO =:jobNo AND GTIN =:gtin")
    void updateBOX_SCAN_GACID_GTIN(String prnCode,String jobNo,String gtin);

    @Insert(onConflict = REPLACE)
    void insertBOX_SCAN_PROD(BOX_SCAN_PROD... boxScanProds);

    @Query("DELETE FROM BOX_SCAN_PROD")
    void deleteAllBOX_SCAN_PROD();

    @Query("SELECT  * FROM BOX_SCAN_PROD WHERE GTIN=:gtin ")
    List<BOX_SCAN_PROD> getAllBOX_SCAN_PROD_GTIN(String gtin);

    @Query("SELECT  SUM(TOT_PCS) FROM BOX_SCAN_PROD WHERE GTIN=:gtin ")
    int getTotalBOX_SCAN_PROD_GTIN(String gtin);

    @Query("SELECT  SUM(QTY) FROM BOX_SCAN_GACID WHERE  PRIN_CODE=:prnCode AND JOB_NO =:jobNo ")
    int getTotalBOX_SCAN_GACID_GTIN(String prnCode,String jobNo);

    @Query("SELECT  * FROM BOX_SCAN_GACID WHERE  PRIN_CODE=:prnCode AND JOB_NO =:jobNo AND GTIN =:gtin AND SLNO =:slno")
    List<BOX_SCAN_GACID> getBOX_SCAN_GACID_GTIN_SLNO(String prnCode,String jobNo,String gtin,String slno);

    @Query("DELETE FROM BOX_SCAN_GACID WHERE PRIN_CODE=:prnCode AND JOB_NO =:jobNo")
    void deleteAllBOX_SCAN_GACID(String prnCode,String jobNo);
}
