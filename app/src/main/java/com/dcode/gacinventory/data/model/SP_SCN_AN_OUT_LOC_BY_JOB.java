package com.dcode.gacinventory.data.model;

import java.io.Serializable;

/**
 * @Created by: VIJIL DHAS A.S
 * @Date: 08-09-2021
 */
public class SP_SCN_AN_OUT_LOC_BY_JOB implements Serializable {
    public String SITE_CODE;
    public String LOC_CODE;
    public String PUOM;
    public String NUOM;
    public String SUOM;
    public int PQTY;
    public int MQTY;
    public int LQTY;
    public int QUANTITY;
    public int UPPP;
    public int LUPPP;
    public String LOT_NO;
    public String MFG_DATE;
    public String EXP_DATE;
    public String SCAN_FLAG;
    public int PRIN_CODE;
    public String PROD_NAME;
    public String PROD_CODE;
    public String SRNO;
    public double IDNO;
    public String SITE_TYPE;
    public String LOC_STAT;
    public double PICK_SEQNO;
    public String PRIN_NAME;
    public String JOB_NO;
    public int PRIORITY;
    public String USER_SCAN_ID;
    public String ALLOW_EDIT_PCKQTY;
    public String BARCODE_SCAN_CHECK;
    public String LOTNO_SCAN_CHECK;
    public String BARCODE;
    public String BARCODE2;
    public String BARCODE3;
    public String JOBCLASS;
    public double QTY_PCB;
    public int CTN_QTY;
    public int REM_PCS_QTY;
    public String ORDER_TYPE;
}
