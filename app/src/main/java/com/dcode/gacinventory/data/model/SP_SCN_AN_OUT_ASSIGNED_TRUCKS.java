package com.dcode.gacinventory.data.model;

import java.io.Serializable;

/**
 * @Created by: VIJIL DHAS A.S
 * @Date: 12-09-2021
 */
public class SP_SCN_AN_OUT_ASSIGNED_TRUCKS implements Serializable {
    public String PRIN_CODE;
    public String TRUCKID;
}
