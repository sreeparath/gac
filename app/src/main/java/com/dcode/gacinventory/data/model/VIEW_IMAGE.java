package com.dcode.gacinventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "VIEW_IMAGE")
public class VIEW_IMAGE implements Serializable {

    @ColumnInfo(name = "API_DOC_ID")
    public String API_DOC_ID;

    @ColumnInfo(name = "DOC_TYPE")
    public String DOC_TYPE;

    @ColumnInfo(name = "DOC_NO")
    public String DOC_NO;

    @ColumnInfo(name = "FILE_TYPE")
    public String FILE_TYPE;

}
