package com.dcode.gacinventory.data.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "PHOTO_MASTER")
public class PHOTO_MASTER implements Serializable {

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "SLNO")
    public long SLNO;

    @ColumnInfo(name = "MODULE_ID")
    public int MODULE_ID;

    @ColumnInfo(name = "JOB_ID")
    public String JOB_ID;

    @ColumnInfo(name = "PHOTO_PATH")
    public String PHOTO_PATH;

    @ColumnInfo(name = "IS_UPLOAD")
    public long IS_UPLOAD;

    @Override
    public String toString() {
        return "PHOTO_MASTER{" +
                "SLNO='" + SLNO + '\'' +
                ", MODULE_ID=" + MODULE_ID +
                ", JOB_ID='" + JOB_ID + '\'' +
                ", PHOTO_PATH='" + PHOTO_PATH + '\'' +
                ", IS_UPLOAD='" + IS_UPLOAD + '\'' +
                '}';
    }
}
