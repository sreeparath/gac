package com.dcode.gacinventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "PRINTER_LIST")
public class PRINTER_LIST {

        @ColumnInfo(name = "PrinterID")
        @PrimaryKey
        public int PrinterID;

        @ColumnInfo(name = "PrinterType")
        public String PrinterType;

        @ColumnInfo(name = "FriendlyName")
        public String FriendlyName;

        @ColumnInfo(name = "PrinterNameOrIP")
        public String PrinterNameOrIP;

        @ColumnInfo(name = "PrinterPort")
        public String PrinterPort;

        @ColumnInfo(name = "IsDefault")
        public String IsDefault;

        @Override
        public String toString() {
                return  FriendlyName;
        }
}
