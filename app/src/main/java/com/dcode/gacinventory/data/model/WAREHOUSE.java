package com.dcode.gacinventory.data.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "WAREHOUSE_LIST")
public class WAREHOUSE implements Serializable {

    @ColumnInfo(name = "WH_CODE")
    @PrimaryKey()
    @NonNull
    public String WH_CODE;

    @ColumnInfo(name = "WH_NAME")
    public String WH_NAME;

    @ColumnInfo(name = "WH_TYPE")
    public String WH_TYPE;

    @ColumnInfo(name = "ISACTIVE")
    public String ISACTIVE;

    @ColumnInfo(name = "ERR_CODE")
    public String ERR_CODE;

    @ColumnInfo(name = "ERR_MSG")
    public String ERR_MSG;

    @Override
    public String toString() {
        return  WH_NAME ;
    }
}
