package com.dcode.gacinventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "PRN_CODE_LIST")
public class PRN_CODE_LIST implements Serializable {

    @ColumnInfo(name = "SL")
    @PrimaryKey(autoGenerate = true)
    public long SL;

    @ColumnInfo(name = "PRIN_CODE")
    public String PRIN_CODE;

    @ColumnInfo(name = "PRIN_NAME")
    public String PRIN_NAME;

    @Override
    public String toString() {
        return "PRN_CODE_LIST{" +
                "SL=" + SL +
                ", PRIN_CODE='" + PRIN_CODE + '\'' +
                ", PRIN_NAME='" + PRIN_NAME + '\'' +
                '}';
    }
}
