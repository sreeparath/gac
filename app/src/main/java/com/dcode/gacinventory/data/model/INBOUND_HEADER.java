package com.dcode.gacinventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "INBOUND_HEADER")
public class INBOUND_HEADER implements Serializable {

    @ColumnInfo(name = "JOB_NO")
    @PrimaryKey()
    public String JOB_NO;

    @ColumnInfo(name = "SEAL_NO")
    public String SEAL_NO;

    @ColumnInfo(name = "TRUCK_NO")
    public String TRUCK_NO;

    @ColumnInfo(name = "PRIN_CODE")
    public String PRIN_CODE;

    @ColumnInfo(name = "PRIN_NAME")
    public String PRIN_NAME;

    @ColumnInfo(name = "AWB_NO")
    public String AWB_NO;

//    @ColumnInfo(name = "NO_GAC")
//    public int NO_GAC ;

    @ColumnInfo(name = "PO_NO")
    public String PO_NO;

    @ColumnInfo(name = "GACSCANID_COUNT")
    public int GACSCANID_COUNT ;

    @ColumnInfo(name = "GS1_LOT_NO")
    public String GS1_LOT_NO;

    @ColumnInfo(name = "GS1_EXP_DT")
    public String GS1_EXP_DT;

    @ColumnInfo(name = "GS1_QTY")
    public int GS1_QTY;



}
