package com.dcode.gacinventory.data.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "PROD_UPD_INFO")
public class PROD_UPD_INFO implements Serializable {

    @ColumnInfo(name = "SL")
    @PrimaryKey(autoGenerate = true)
    public long SL;

    @ColumnInfo(name = "JOB_NO")
    public String JOB_NO;

    @ColumnInfo(name = "PROD_CODE")
    public String PROD_CODE;

    @ColumnInfo(name = "PRIN_CODE")
    public String PRIN_CODE;

    @ColumnInfo(name = "STORAGE_TYPE")
    public String STORAGE_TYPE;

    @ColumnInfo(name = "COO")
    public String COO;

    @ColumnInfo(name = "PART_SET")
    public int PART_SET;

    @ColumnInfo(name = "IMAGE_CENSOR")
    public int IMAGE_CENSOR ;

    @ColumnInfo(name = "DANGEROUS_GOODS")
    public int DANGEROUS_GOODS ;

}
