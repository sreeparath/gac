package com.dcode.gacinventory.data.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "MASTER_LIST")
public class MASTER_LIST implements Serializable {

    @ColumnInfo(name = "SL")
    @PrimaryKey(autoGenerate = true)
    public long SL;

    @ColumnInfo(name = "CODE")
    @NonNull
    public String CODE;

    @ColumnInfo(name = "DESCRIPTION")
    @NonNull
    public String DESCRIPTION;

    @ColumnInfo(name = "STOCK_STATUS")
    public String STOCK_STATUS;

    @ColumnInfo(name = "ERR_CODE")
    public String ERR_CODE;

    @Override
    public String toString() {
        return  DESCRIPTION;
    }

}
