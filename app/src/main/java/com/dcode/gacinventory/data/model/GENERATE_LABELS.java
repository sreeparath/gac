package com.dcode.gacinventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "GENERATE_LABELS")
public class GENERATE_LABELS implements Serializable {

    @ColumnInfo(name = "ID")
    @PrimaryKey(autoGenerate = true)
    public int ID;

    @ColumnInfo(name = "JOB_NO")
    public String JOB_NO;

    @ColumnInfo(name = "LABEL")
    public String LABEL;

    @ColumnInfo(name = "USER_ID")
    public String USER_ID;

    @ColumnInfo(name = "IS_SELECT")
    public boolean IS_SELECT;



}
