package com.dcode.gacinventory.data.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "PRINTER_LIST_SERVICE")
public class PRINTER_LIST_SERVICE {

        @ColumnInfo(name = "PRIN_CODE")
        @PrimaryKey
        @NonNull
        public String PRIN_CODE;

        @ColumnInfo(name = "PRINTER_NAME")
        public String PRINTER_NAME;

        @ColumnInfo(name = "PRINTER_IP")
        public String PRINTER_IP;

        @ColumnInfo(name = "PRINTER_PORT")
        public String PRINTER_PORT;

        @ColumnInfo(name = "WH_CODE")
        public String WH_CODE;

        @Override
        public String toString() {
                return  PRINTER_NAME;
        }
}
