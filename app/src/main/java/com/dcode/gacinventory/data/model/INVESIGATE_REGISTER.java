package com.dcode.gacinventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "INVESIGATE_REGISTER")
public class INVESIGATE_REGISTER {

        @ColumnInfo(name = "ID")
        @PrimaryKey(autoGenerate = true)
        public int ID;

        @ColumnInfo(name = "JOB_NO")
        public String JOB_NO;

        @ColumnInfo(name = "INVESTIGATION_NO")
        public String INVESTIGATION_NO;

        @ColumnInfo(name = "INVESTIGATION_REASON")
        public String INVESTIGATION_REASON;

        @ColumnInfo(name = "INVESTIGATION_REMARKS")
        public String INVESTIGATION_REMARKS;



}
