package com.dcode.gacinventory.data;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.dcode.gacinventory.data.dao.GenericDao;
import com.dcode.gacinventory.data.model.APP_SETTINGS;
import com.dcode.gacinventory.data.model.BARCODE_FRMT;
import com.dcode.gacinventory.data.model.BOX_SCAN_GACID;
import com.dcode.gacinventory.data.model.BOX_SCAN_PROD;
import com.dcode.gacinventory.data.model.CONFIRMATION_LIST;
import com.dcode.gacinventory.data.model.COUNTRY_LIST;
import com.dcode.gacinventory.data.model.GENERATE_LABELS;
import com.dcode.gacinventory.data.model.INVESIGATE_REGISTER;
import com.dcode.gacinventory.data.model.INVEST_REASONS;
import com.dcode.gacinventory.data.model.JOB_CLOSURE_INFO;
import com.dcode.gacinventory.data.model.LOCATIONS;
import com.dcode.gacinventory.data.model.LOT_STATUS_LIST;
import com.dcode.gacinventory.data.model.MASTER_LIST;
import com.dcode.gacinventory.data.model.OBJECTS;
import com.dcode.gacinventory.data.model.PHOTO_MASTER;
import com.dcode.gacinventory.data.model.PRINTER_LIST;
import com.dcode.gacinventory.data.model.PRINTER_LIST_SERVICE;
import com.dcode.gacinventory.data.model.PRINT_FORMAT;
import com.dcode.gacinventory.data.model.PRN_CODE_LIST;
import com.dcode.gacinventory.data.model.USERS;
import com.dcode.gacinventory.data.model.USER_PRIN_INFO;


@Database(entities = {
        USERS.class,
        OBJECTS.class,
        LOCATIONS.class,
        PHOTO_MASTER.class,
        PRINTER_LIST.class,
        APP_SETTINGS.class,
        PRINT_FORMAT.class,
        PRINTER_LIST_SERVICE.class,
        GENERATE_LABELS.class,
        LOT_STATUS_LIST.class,
        CONFIRMATION_LIST.class,
        INVEST_REASONS.class,
        INVESIGATE_REGISTER.class,
        USER_PRIN_INFO.class,
        JOB_CLOSURE_INFO.class,
        COUNTRY_LIST.class,
        MASTER_LIST.class,
        BARCODE_FRMT.class,
        BOX_SCAN_PROD.class,
        BOX_SCAN_GACID.class,
        PRN_CODE_LIST.class,
}, version = 1, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract GenericDao genericDao();
}
