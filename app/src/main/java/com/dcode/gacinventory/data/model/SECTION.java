package com.dcode.gacinventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "MENU")
public class SECTION implements Serializable {

    @PrimaryKey
    @ColumnInfo(name = "SECTION_ID")
    public long SECTION_ID;

    @ColumnInfo(name = "PARENT_MENU_ID")
    public int PARENT_MENU_ID;


    @ColumnInfo(name = "SECTION_ID_CODE")
    public String SECTION_ID_CODE;

    @ColumnInfo(name = "SECTION_MENU_NAME")
    public String SECTION_MENU_NAME;

    @ColumnInfo(name = "SECTION_MENU_DESC")
    public String SECTION_MENU_DESC;

    @ColumnInfo(name = "MENU_ICON")
    public int SECTION_ICON;
}
