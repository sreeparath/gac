package com.dcode.gacinventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "LABEL_ITEM_LIST")
public class LABEL_ITEM_LIST {

    @ColumnInfo(name = "ITEM_ID")
    @PrimaryKey()
    public int ITEM_ID;

    @ColumnInfo(name="SRNO")
    public String SRNO;

    @ColumnInfo(name = "ITEM_NAME")
    public String ITEM_NAME;

    @ColumnInfo(name = "BARCODE")
    public String BARCODE;

    @ColumnInfo(name = "QRCODE")
    public String QRCODE;

    @ColumnInfo(name = "UNIT_CODE")
    public String UNIT_CODE;

    @ColumnInfo(name = "IS_SELECT")
    public boolean IS_SELECT;

    @ColumnInfo(name = "GACSCANID")
    public String GACSCANID;

    @ColumnInfo(name = "PROD_NAME")
    public String PROD_NAME;

    @ColumnInfo(name = "QTY1")
    public float QTY1;

    @ColumnInfo(name = "SEAL_NO")
    public String SEAL_NO;

    @ColumnInfo(name = "TRUCK_NO")
    public String TRUCK_NO;

    @ColumnInfo(name = "PRIN_CODE")
    public String PRIN_CODE;

    @ColumnInfo(name = "PRIN_NAME")
    public String PRIN_NAME;

    @ColumnInfo(name = "AWB_NO")
    public String AWB_NO;


    public boolean isIS_SELECT() {
        return IS_SELECT;
    }

    public void setIS_SELECT(boolean IS_SELECT) {
        this.IS_SELECT = IS_SELECT;
    }
}
