package com.dcode.gacinventory.data.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "BOX_SCAN_GACID")
public class BOX_SCAN_GACID implements Serializable {

    @ColumnInfo(name = "ID")
    @PrimaryKey(autoGenerate = true)
    public long ID;

    @ColumnInfo(name = "GTIN")
    @NonNull
    public String GTIN;

    @ColumnInfo(name = "SLNO")
    @NonNull
    public String SLNO;

    @ColumnInfo(name = "JOB_NO")
    @NonNull
    public String JOB_NO;

    @ColumnInfo(name = "PRIN_CODE")
    @NonNull
    public String PRIN_CODE;

    @ColumnInfo(name = "LOT_NO")
    public String LOT_NO;

    @ColumnInfo(name = "MANF_DT")
    public String MANF_DT;

    @ColumnInfo(name = "MANF_DT_DISPLAY")
    public String MANF_DT_DISPLAY;

    @ColumnInfo(name = "EXP_DT")
    public String EXP_DT;

    @ColumnInfo(name = "EXP_DT_DISPLAY")
    public String EXP_DT_DISPLAY;

    @ColumnInfo(name = "SSCC")
    public String SSCC;

    @ColumnInfo(name = "QTY")
    @NonNull
    public int QTY;


}
