package com.dcode.gacinventory.data.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;


@Entity(tableName = "JOB_CLOSURE_INFO")
public class JOB_CLOSURE_INFO implements Serializable {

    @ColumnInfo(name = "JOB_NO")
    @PrimaryKey()
    @NonNull
    public String JOB_NO;

    @ColumnInfo(name = "JOB_DATE")
    public String JOB_DATE;

    @ColumnInfo(name = "PRIN_CODE")
    public String PRIN_CODE;

    @ColumnInfo(name = "ISALLOCTED")
    public String ISALLOCTED;

    @ColumnInfo(name = "ALLOCTED DATE")
    public String ALLOCTED_DATE;

    @ColumnInfo(name = "ISCONFIRMED")
    public String ISCONFIRMED;

    @ColumnInfo(name = "CONFIRMED DATE")
    public String CONFIRMED_DATE;

    @ColumnInfo(name = "ISCANCELLED")
    public String ISCANCELLED;

    @ColumnInfo(name = "CANCELLED DATE")
    public String CANCELLED_DATE;

    @ColumnInfo(name = "PRIN_NAME")
    public String PRIN_NAME;

    @ColumnInfo(name = "JOBCLASS")
    public String JOBCLASS;

    @ColumnInfo(name = "ERR_CODE")
    public String ERR_CODE;

    @ColumnInfo(name = "ERR_MSG")
    public String ERR_MSG;

    @ColumnInfo(name = "IS_SCANJOB")
    public String IS_SCANJOB;

    @ColumnInfo(name = "TOTAL_PO_QTY")
    public String TOTAL_PO_QTY;

    @ColumnInfo(name = "TOTAL_SYS_PUT_QTY")
    public String TOTAL_SYS_PUT_QTY;

    @ColumnInfo(name = "TOTAL_PUT_QTY")
    public String TOTAL_PUT_QTY;

    @ColumnInfo(name = "TOTAL_STG_QTY")
    public String TOTAL_STG_QTY;


    @ColumnInfo(name = "IS_SELECT")
    public boolean IS_SELECT ;

    @ColumnInfo(name = "UPDATE_STATUS")
    public int UPDATE_STATUS ;

    @ColumnInfo(name = "STATUS_MSG")
    public String STATUS_MSG;
}
