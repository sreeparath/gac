package com.dcode.gacinventory.data.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "USERS")
public class USERS {

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "USER_ID")
    public String USER_ID;

    @ColumnInfo(name = "USER_NAME")
    public String USER_NAME;

    @ColumnInfo(name = "USER_PWD")
    public String USER_PWD;

    @ColumnInfo(name = "PASSWORD")
    public String PASSWORD;

    @ColumnInfo(name = "STATUS")
    public String STATUS;

    @ColumnInfo(name = "SCAN_ID")
    public String SCAN_ID;

    @ColumnInfo(name = "SCAN_MODULE")
    public String SCAN_MODULE;

    @ColumnInfo(name = "MAC_ADDRESS")
    public String MAC_ADDRESS;

    @ColumnInfo(name = "WH_CODE")
    public String WH_CODE;

    @ColumnInfo(name = "ACTIVE")
    public String ACTIVE;

    @ColumnInfo(name = "ERR_CODE")
    public String ERR_CODE;

    @ColumnInfo(name = "USER_PIN")
    public String USER_PIN;

    @ColumnInfo(name = "EMPLOYEE_NUMBER")
    public String EMPLOYEE_NUMBER;

    @ColumnInfo(name = "FULL_NAME")
    public String FULL_NAME;

    @ColumnInfo(name = "FIRST_NAME")
    public String FIRST_NAME;

    @ColumnInfo(name = "LAST_NAME")
    public String LAST_NAME;

    @ColumnInfo(name = "EMAIL_ADDRESS")
    public String EMAIL_ADDRESS;

    @ColumnInfo(name = "TEAM_ID")
    public String TEAM_ID;

    @ColumnInfo(name = "CUR_DATE_TIME")
    public String CUR_DATE_TIME;

    @ColumnInfo(name = "DAY_END_DEVICE_DATE")
    public String DAY_END_DEVICE_DATE;


}
