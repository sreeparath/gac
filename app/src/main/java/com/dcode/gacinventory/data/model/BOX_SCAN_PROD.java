package com.dcode.gacinventory.data.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "BOX_SCAN_PROD")
public class BOX_SCAN_PROD implements Serializable {

    @ColumnInfo(name = "SL")
    @PrimaryKey(autoGenerate = true)
    public long SL;

    @ColumnInfo(name = "PROD_CODE")
    @NonNull
    public String PROD_CODE;

    @ColumnInfo(name = "PRIN_NAME")
    public String PRIN_NAME;

    @ColumnInfo(name = "GTIN")
    @NonNull
    public String GTIN;

    @ColumnInfo(name = "TOT_PCS")
    @NonNull
    public int TOT_PCS;

    @ColumnInfo(name = "RTN")
    public int RTN;

}
