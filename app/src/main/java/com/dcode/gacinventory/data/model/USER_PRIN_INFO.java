package com.dcode.gacinventory.data.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;



@Entity(tableName = "USER_PRIN_INFO",primaryKeys = {"USER_ID","PRIN_CODE","MENU_ID"})
public class USER_PRIN_INFO {

//    @ColumnInfo(name = "ID")
//    @PrimaryKey(autoGenerate = true)
//    public int ID;

    @NonNull
    @ColumnInfo(name = "USER_ID")
    public String USER_ID;

    @NonNull
    @ColumnInfo(name = "PRIN_CODE")
    public String PRIN_CODE;

    @NonNull
    @ColumnInfo(name = "MENU_ID")
    public String MENU_ID;

    @ColumnInfo(name = "MENU_NAME")
    public String MENU_NAME;

    @ColumnInfo(name = "IND_INSERT")
    public String IND_INSERT;

    @ColumnInfo(name = "IND_MODIFY")
    public String IND_MODIFY;

    @ColumnInfo(name = "IND_DELETE")
    public String IND_DELETE;

    @ColumnInfo(name = "IND_VIEW")
    public String IND_VIEW;

    @ColumnInfo(name = "PARENT_MENU")
    public String PARENT_MENU;

    @ColumnInfo(name = "ERR_CODE")
    public String ERR_CODE;

    @Override
    public String toString() {
        return  USER_ID;
    }
}
