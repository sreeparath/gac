package com.dcode.gacinventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "PRINT_FORMAT")
public class PRINT_FORMAT {

        @ColumnInfo(name = "SLNO")
        @PrimaryKey(autoGenerate = true)
        public int SLNO;

        @ColumnInfo(name = "PRIN_CODE")
        public String PRIN_CODE;

        @ColumnInfo(name = "REPORT_MODULE")
        public String REPORT_MODULE;

        @ColumnInfo(name = "REPORT_ID")
        public String REPORT_ID;

        @ColumnInfo(name = "REPORT_NAME")
        public String REPORT_NAME;

        @ColumnInfo(name = "PRINT_BODY")
        public String PRINT_BODY;


        @Override
        public String toString() {
                return  REPORT_NAME;
        }

}
