package com.dcode.gacinventory.data.model;

/**
 * @Created by: VIJIL DHAS A.S
 * @Date: 18-09-2021
 */
public class SP_SCN_AN_OUT_PICK_PENDING {
    public String PRIN_CODE;
    public String TRUCK_ID;
    public String JOB_NO;
    public int SELECTED;
    public String ERR_CODE;
}
