package com.dcode.gacinventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "JOB_CODE_LIST")
public class JOB_CODE_LIST implements Serializable {

    @ColumnInfo(name = "SL")
    @PrimaryKey(autoGenerate = true)
    public long SL;

    @ColumnInfo(name = "JOB_NO")
    public String JOB_NO;

    @ColumnInfo(name = "JOB_DATE")
    public String JOB_DATE;

    @ColumnInfo(name = "PRIN_CODE")
    public String PRIN_CODE;

    @ColumnInfo(name = "PRIN_NAME")
    public String PRIN_NAME;

    @Override
    public String toString() {
        return "JOB_CODE_LIST{" +
                "SL=" + SL +
                ", JOB_NO='" + JOB_NO + '\'' +
                ", JOB_DATE='" + JOB_DATE + '\'' +
                ", PRIN_CODE='" + PRIN_CODE + '\'' +
                ", PRIN_NAME='" + PRIN_NAME + '\'' +
                '}';
    }
}
