package com.dcode.gacinventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "LOT_STATUS_LIST")
public class LOT_STATUS_LIST {

        @ColumnInfo(name = "ID")
        @PrimaryKey(autoGenerate = true)
        public int ID;

        @ColumnInfo(name = "NEW_LOT_STATUS")
        public String NEW_LOT_STATUS;

        @ColumnInfo(name = "DESCRIPTION")
        public String DESCRIPTION;

        @ColumnInfo(name = "ERR_CODE")
        public String ERR_CODE;

        @Override
        public String toString() {
                return  DESCRIPTION;
        }
}
