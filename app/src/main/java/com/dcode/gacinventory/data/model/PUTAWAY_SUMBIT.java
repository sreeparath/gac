package com.dcode.gacinventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "PUTAWAY_SUMBIT")
public class PUTAWAY_SUMBIT implements Serializable {

    @ColumnInfo(name = "SRNO")
    @PrimaryKey()
    public String SRNO;

    @ColumnInfo(name = "JOB_NO")
    public String JOB_NO;

    @ColumnInfo(name = "MFG_DATE")
    public String MFG_DATE;

    @ColumnInfo(name = "EXP_DATE")
    public String EXP_DATE;

    @ColumnInfo(name = "LOT_NO")
    public String LOT_NO;

    @ColumnInfo(name = "LOT_STATUS")
    public String LOT_STATUS;

    @ColumnInfo(name = "FORMULA_NO")
    public String FORMULA_NO;

    @ColumnInfo(name = "FIL_CODE")
    public String FIL_CODE;

    @ColumnInfo(name = "BARCODE")
    public String BARCODE;

    @ColumnInfo(name = "INPUT_PQTY")
    public String INPUT_PQTY;

    @ColumnInfo(name = "INPUT_MQTY")
    public String INPUT_MQTY;

    @ColumnInfo(name = "INPUT_LQTY")
    public String INPUT_LQTY;

    @ColumnInfo(name = "USER_NAME")
    public String USER_NAME;

    @ColumnInfo(name = "UPDATE_FROM")
    public String UPDATE_FROM;

    @ColumnInfo(name = "SCAN_PUT_LOC_CODE")
    public String SCAN_PUT_LOC_CODE;

    @ColumnInfo(name = "SCAN_LOCATION")
    public String SCAN_LOCATION;

    @ColumnInfo(name = "PALLET_ID")
    public String PALLET_ID;

    @ColumnInfo(name = "LOC_CODE")
    public String LOC_CODE;

    @ColumnInfo(name = "SITE")
    public String SITE;

    @ColumnInfo(name = "LOC")
    public String LOC;

    @ColumnInfo(name = "PUOM")
    public String PUOM;

    @ColumnInfo(name = "LUOM")
    public String LUOM;

    @ColumnInfo(name = "MUOM")
    public String MUOM;

    @ColumnInfo(name = "DESCRIPTION")
    public String DESCRIPTION;

    @ColumnInfo(name = "PRIN_CODE")
    public String PRIN_CODE;

    @ColumnInfo(name = "STG_QTY")
    public String STG_QTY;

    @ColumnInfo(name = "TOTAL_QTY")
    public String TOTAL_QTY;

}
