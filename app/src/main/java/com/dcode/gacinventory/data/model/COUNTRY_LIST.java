package com.dcode.gacinventory.data.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "COUNTRY_LIST")
public class COUNTRY_LIST implements Serializable {

    @ColumnInfo(name = "SL")
    @PrimaryKey(autoGenerate = true)
    public long SL;

    @ColumnInfo(name = "COUNTRY_CODE")
    @NonNull
    public String COUNTRY_CODE;

    @ColumnInfo(name = "COUNTRYNAME")
    @NonNull
    public String COUNTRYNAME;

    @ColumnInfo(name = "COUNTRY_GCC")
    public String COUNTRY_GCC;

    @ColumnInfo(name = "COUNTRY_NAME2")
    public String COUNTRY_NAME2;

    @ColumnInfo(name = "ERR_CODE")
    public String ERR_CODE;

    @Override
    public String toString() {
        return  COUNTRYNAME;
    }

}
