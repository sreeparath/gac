package com.dcode.gacinventory.data.model;

import java.io.Serializable;

/**
 * @Created by: VIJIL DHAS A.S
 * @Date: 08-09-2021
 */
public class SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS implements Serializable {
    public String itemLabel;
    public String itemValue;
}
