package com.dcode.gacinventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "APP_SETTINGS")
public class APP_SETTINGS {

    @ColumnInfo(name = "ID")
    @PrimaryKey(autoGenerate = true)
    public int PK_ID;

    @ColumnInfo(name = "IS_GROUP_VISIBLE")
    public int IS_GROUP_VISIBLE;

    @ColumnInfo(name = "IS_SUB_GROUP_VISIBLE")
    public int IS_SUB_GROUP_VISIBLE;

    @ColumnInfo(name = "IS_CATEGORY_VISIBLE")
    public int IS_CATEGORY_VISIBLE;

    @ColumnInfo(name = "IS_SUB_CATEGORY_VISIBLE")
    public int IS_SUB_CATEGORY_VISIBLE;

    @ColumnInfo(name = "IS_ROOM_VISIBLE")
    public int IS_ROOM_VISIBLE;

    @ColumnInfo(name = "IS_SUB_GROUP_IN")
    public int IS_SUB_GROUP_IN;

    @ColumnInfo(name = "IS_SUB_CATEGORY_IN")
    public int IS_SUB_CATEGORY_IN;

    @ColumnInfo(name = "MANDATORY_ASSET_FIELDS")
    public int MANDATORY_ASSET_FIELDS;

    @ColumnInfo(name = "MANDATORY_SERIAL_FIELDS")
    public int MANDATORY_SERIAL_FIELDS;


}
