package com.dcode.gacinventory.data.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "LOCATIONS")
public class LOCATIONS implements Serializable {
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "LOCODE")
    public String LOCODE;

    @ColumnInfo(name = "LONAME")
    public String LONAME;

    @ColumnInfo(name = "COCODE")
    public String COCODE;

    @Override
    public String toString() {
        return LONAME; // String.format("%s : %s", LOCODE, LONAME);
    }
}
