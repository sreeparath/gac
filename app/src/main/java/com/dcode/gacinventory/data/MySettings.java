package com.dcode.gacinventory.data;


import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppVariables;
import com.dcode.gacinventory.common.GACDynFuncs;
import com.dcode.gacinventory.data.model.APP_SETTINGS;

import java.util.List;

public class MySettings {
    private static MySettings mySettings;
    private APP_SETTINGS appSettings;
    private String BASE_URL;
    private String FILE_UPLOAD_URL;
    private String SUBSCRIPTION_KEY;


    private MySettings() {
        if (appSettings == null || appSettings.PK_ID <= 0) {
            appSettings = App.getDatabaseClient().getAppDatabase().genericDao().getAppSettings();
        }
    }

    public static synchronized MySettings getInstance() {
        if (mySettings == null) {
            mySettings = new MySettings();
        }

        return mySettings;
    }

    public String getSUBSCRIPTION_KEY() {
        return SUBSCRIPTION_KEY;
    }

    public void setSUBSCRIPTION_KEY(String SUBSCRIPTION_KEY) {
        this.SUBSCRIPTION_KEY = SUBSCRIPTION_KEY;
    }

    public String getBaseUrl() {
        return BASE_URL;
    }

    public void setBaseUrl(String baseUrl) {
        BASE_URL = baseUrl;
    }

    public String getFileUploadUrl() {
        return FILE_UPLOAD_URL;
    }

    public void setFileUploadUrl(String fileUploadUrl) {
        this.FILE_UPLOAD_URL = fileUploadUrl;
    }

    public void destroyInstance() {
        mySettings = null;
    }

    public boolean IsGroupVisible() {
        return appSettings.IS_GROUP_VISIBLE == 1;
    }

    public boolean IsSubGroupVisible() {
        return appSettings.IS_SUB_GROUP_VISIBLE == 1;
    }

    public boolean IsCategoryVisible() {
        return appSettings.IS_CATEGORY_VISIBLE == 1;
    }

    public boolean IsSubCategoryVisible() {
        return appSettings.IS_SUB_CATEGORY_VISIBLE == 1;
    }

    public boolean IsRoomVisible() {
        return appSettings.IS_ROOM_VISIBLE == 1;
    }

    public boolean IsSubGroupIn() {
        return appSettings.IS_SUB_CATEGORY_IN == 1;
    }

    public boolean IsSubCategoryIn() {
        return appSettings.IS_SUB_CATEGORY_IN == 1;
    }
}
