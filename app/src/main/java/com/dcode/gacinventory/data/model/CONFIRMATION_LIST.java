package com.dcode.gacinventory.data.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "CONFIRMATION_LIST")
public class CONFIRMATION_LIST implements Serializable {

    @ColumnInfo(name = "SL")
    @PrimaryKey(autoGenerate = true)
    public long SL;

    @ColumnInfo(name = "JOB_NO")
    public String JOB_NO;

    @ColumnInfo(name = "SRNO")
    @NonNull
    public String SRNO;

    @ColumnInfo(name = "PRIN_CODE")
    public String PRIN_CODE;

    @ColumnInfo(name = "SKU")
    public String SKU;

    @ColumnInfo(name = "DESCRIPTION")
    public String DESCRIPTION;

    @ColumnInfo(name = "STG_QTY")
    public int STG_QTY;

    @ColumnInfo(name = "TOTAL_QTY")
    public int TOTAL_QTY ;


    @ColumnInfo(name = "ERR_CODE")
    public String ERR_CODE ;

    @ColumnInfo(name = "ERR_MSG")
    public String ERR_MSG ;

    @ColumnInfo(name = "IS_SELECT")
    public boolean IS_SELECT ;

    @ColumnInfo(name = "IS_STAGED")
    public String IS_STAGED ;

    @ColumnInfo(name = "IS_CONFIRMED")
    public String IS_CONFIRMED ;

    @ColumnInfo(name = "IS_PUTAWAY")
    public String IS_PUTAWAY ;

    @ColumnInfo(name = "MFG_DATE")
    public String MFG_DATE ;

    @ColumnInfo(name = "EXP_DATE")
    public String EXP_DATE ;

    @ColumnInfo(name = "LOT_NO")
    public String LOT_NO ;

    @ColumnInfo(name = "LOT_STATUS")
    public String LOT_STATUS ;

    @ColumnInfo(name = "FORMULA_NO")
    public String FORMULA_NO ;

    @ColumnInfo(name = "FIL_CODE")
    public String FIL_CODE ;

    @ColumnInfo(name = "LOC_CODE")
    public String LOC_CODE ;

    @ColumnInfo(name = "SITE")
    public String SITE ;


    @ColumnInfo(name = "UPDATE_FROM")
    public String UPDATE_FROM ;

    @ColumnInfo(name = "PALLET_ID")
    public String PALLET_ID ;

    @ColumnInfo(name = "SCAN_PUT_LOC_CODE")
    public String SCAN_PUT_LOC_CODE ;


}
