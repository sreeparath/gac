package com.dcode.gacinventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "LOGISTIC_LABEL_SCAN")
public class LOGISTIC_LABEL_SCAN implements Serializable {

    @ColumnInfo(name = "ID")
    @PrimaryKey(autoGenerate = true)
    public long ID;

    @ColumnInfo(name = "SRNO")
    public String SRNO;

    @ColumnInfo(name = "JOB_NO")
    public String JOB_NO;

    @ColumnInfo(name = "PRIN_CODE")
    public String PRIN_CODE;

    @ColumnInfo(name = "PRIN_NAME")
    public String PRIN_NAME;


    @ColumnInfo(name = "MFG_DATE")
    public String MFG_DATE;

    @ColumnInfo(name = "EXPDATE")
    public String EXPDATE;

    @ColumnInfo(name = "LOT_NO")
    public String LOT_NO;

    @ColumnInfo(name = "LOT_STATUS")
    public String LOT_STATUS;

    @ColumnInfo(name = "FORMULA_NO")
    public String FORMULA_NO;

    @ColumnInfo(name = "FIL_CODE")
    public String FIL_CODE;

    @ColumnInfo(name = "BARCODE")
    public String BARCODE;

    @ColumnInfo(name = "Pqty")
    public String Pqty;

    @ColumnInfo(name = "Mqty")
    public String Mqty;

    @ColumnInfo(name = "Lqty")
    public String Lqty;

    @ColumnInfo(name = "LOC_CODE")
    public String LOC_CODE;

    @ColumnInfo(name = "SITE")
    public String SITE;

    @ColumnInfo(name = "LOC")
    public String LOC;

    @ColumnInfo(name = "SRNO_ORIGINAL")
    public String SRNO_ORIGINAL;

    @ColumnInfo(name = "SKU")
    public String SKU;

    @ColumnInfo(name = "PALLET_ID")
    public String PALLET_ID;

    @ColumnInfo(name = "STG_QTY")
    public String STG_QTY;

    @ColumnInfo(name = "TOTAL_QTY")
    public String TOTAL_QTY;

    @ColumnInfo(name = "DESCRIPTION")
    public String DESCRIPTION;

    @ColumnInfo(name = "BARCODE_SCAN_CHECK")
    public String BARCODE_SCAN_CHECK;

    @ColumnInfo(name = "PALLET_SCAN_CHECK")
    public String PALLET_SCAN_CHECK;

    @ColumnInfo(name = "ISLOTREQUIRED")
    public String ISLOTREQUIRED;

    @ColumnInfo(name = "ISMANUREQUIRED")
    public String ISMANUREQUIRED;

    @ColumnInfo(name = "ISEXPDATEREQUIRED")
    public String ISEXPDATEREQUIRED;

    @ColumnInfo(name = "CAL_BATCHNO")
    public String CAL_BATCHNO;

    @ColumnInfo(name = "SHELF_LIFE")
    public int SHELF_LIFE;

    @ColumnInfo(name = "PUOM")
    public String PUOM;

    @ColumnInfo(name = "LUOM")
    public String LUOM;

    @ColumnInfo(name = "MUOM")
    public String MUOM;

    @ColumnInfo(name = "UPPP")
    public int UPPP;

    @ColumnInfo(name = "LUPPP")
    public int LUPPP;

    @ColumnInfo(name = "QUANTITY")
    public int QUANTITY;

    @ColumnInfo(name = "JOBCLASS")
    public String JOBCLASS;

    @ColumnInfo(name = "BARCODE2")
    public String BARCODE2;

    @ColumnInfo(name = "BARCODE3")
    public String BARCODE3;

    @ColumnInfo(name = "BARCODE4")
    public String BARCODE4;

    @ColumnInfo(name = "BARCODE5")
    public String BARCODE5;

    @ColumnInfo(name = "BARCODE6")
    public String BARCODE6;

    @ColumnInfo(name = "COUNTRYNAME")
    public String COUNTRYNAME;

    @ColumnInfo(name = "PACK_KEY")
    public String PACK_KEY;

    @ColumnInfo(name = "TOTAL_PO_QTY")
    public int TOTAL_PO_QTY;

    @ColumnInfo(name = "IS_STAGED")
    public String IS_STAGED;

    @ColumnInfo(name = "DEPT_CODE")
    public String DEPT_CODE;

    @ColumnInfo(name = "STROKE")
    public String STROKE;

    @ColumnInfo(name = "COLOR_CODE")
    public String COLOR_CODE;

    @ColumnInfo(name = "SIZE")
    public String SIZE;

    @ColumnInfo(name = "PUT_QTY")
    public String PUT_QTY;


}
