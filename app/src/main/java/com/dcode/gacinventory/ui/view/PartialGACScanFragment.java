package com.dcode.gacinventory.ui.view;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.common.Utils;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SCAN;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SUMBIT;
import com.dcode.gacinventory.data.model.LOT_STATUS;
import com.dcode.gacinventory.network.model.GenericRetResponse;
import com.dcode.gacinventory.network.model.OAuthTokenResponse;
import com.dcode.gacinventory.network.service.ServiceUtils;
import com.example.gacinventory.R;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.dcode.gacinventory.common.Utils.ISSUE_DATE_FORMAT2;
import static com.dcode.gacinventory.common.Utils.PRINT_DATE_FORMAT;

public class PartialGACScanFragment extends BaseFragment {
    private TextInputEditText edDocNo;
    private TextInputEditText edLUnit;
    private TextInputEditText edMunit;
    private TextInputEditText edPUnit;
    private TextInputEditText edPl;
    private TextInputEditText edBarcode;
    private TextInputEditText edFillCode;
    private TextInputEditText edFormula;
    //private TextInputEditText edLotStatus;
    private TextInputEditText edLotNo;
    private TextInputEditText edLocCode;
    private TextInputEditText edExp;
    private TextInputEditText edManf;
    private TextInputEditText edJobCode;




//    private AutoCompleteTextView tvDocTypes;
//    private ArrayAdapter<DocType> objectsAdapter;
    private int promptResource;
    private String DocNo,barcode;
    private long HDR_ID;
    private String[] uiCheck = {"JOB_NO"};
    private List<LOT_STATUS> lot_statuses;
    private ArrayAdapter<LOT_STATUS> companyAdapter;
    private AutoCompleteTextView edLotStatus;
    private LOGISTIC_LABEL_SCAN items_prints;
    private View root;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                DocNo = bundle.getString(AppConstants.SELECTED_ID);
                barcode = bundle.getString(AppConstants.MASTER_ID);
                items_prints = (LOGISTIC_LABEL_SCAN) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
            }

        }

        root = inflater.inflate(R.layout.fragment_partial_gac_scan, container, false);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupView();
    }

    private void setupView() {
        edDocNo = root.findViewById(R.id.edScanCode);
        edLUnit = root.findViewById(R.id.edLUnit);
        edMunit= root.findViewById(R.id.edMunit);
        edPUnit= root.findViewById(R.id.edPUnit);
        edPl= root.findViewById(R.id.edPl);
        edBarcode= root.findViewById(R.id.edBarcode);
        edFillCode= root.findViewById(R.id.edFillCode);
        edFormula= root.findViewById(R.id.edFormula);
        edLotStatus= root.findViewById(R.id.edLotStatus);
        edLotNo= root.findViewById(R.id.edLotNo);
        edLocCode= root.findViewById(R.id.edLocCode);
        edExp= root.findViewById(R.id.edExp);
        edManf= root.findViewById(R.id.edManf);
        edManf.setOnClickListener(v -> onDateEntryClick(1));
        edExp.setOnClickListener(v -> onDateEntryClick(2));
        edJobCode= root.findViewById(R.id.edJobCode);

//        edDocNo = root.findViewById(R.id.edScanCode);
//        edDocNo.setOnKeyListener((v, keyCode, event) -> {
//            if(keyCode == KeyEvent.KEYCODE_ENTER){
//                if(event.getAction() == KeyEvent.ACTION_DOWN){
//                    return onScanCodeKeyEvent();
//                }else{
//                    return false;
//                }
//            }else{
//                return false;
//            }
//        });

        MaterialButton btPhoto = root.findViewById(R.id.btnBack);
        btPhoto.setOnClickListener(v -> OnBackClick());

        MaterialButton btnPrint = root.findViewById(R.id.btnNext);
        btnPrint.setOnClickListener(v -> OnNextClick());

        FillLotStatus();

        fillLogisticData(items_prints);

    }

    private void onDateEntryClick(int ID) {
        Calendar currentDate = Calendar.getInstance();
        int curYear = currentDate.get(Calendar.YEAR);
        int curMonth = currentDate.get(Calendar.MONTH);
        int curDay = currentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(getContext(), (datePicker, cYear, cMonth, cDay) -> {
            String dateString = String.format(Locale.ENGLISH, "%d-%d-%d", cDay, (cMonth + 1), cYear);
            String selectedDate = Utils.getFormattedDate(dateString, Utils.ISSUE_DATE_FORMAT, PRINT_DATE_FORMAT);

            if (ID == 1) {
                edManf.setText(selectedDate);
                edManf.setError(null);
            } else if (ID == 2) {
                edExp.setText(selectedDate);
                edExp.setError(null);
            }
        }, curYear, curMonth, curDay);
        mDatePicker.setTitle("Select date");
        mDatePicker.show();
    }

/*    private boolean onDocNoKeyEvent() {
        DocNo = edDocNo.getText().toString();

        if (DocNo.isEmpty() || DocNo.trim().length() == 0) {
            showToast(String.format("%s required", getString(promptResource)));
            return false;
        }
        return true;
    }*/

    private void confirmDamageReport() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putString(AppConstants.SELECTED_ID, DocNo);
        bundle.putSerializable(AppConstants.SELECTED_CODE, "");
        bundle.putLong(AppConstants.PARENT_ID, HDR_ID);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.action_nav_damage_confirmation, bundle);
    }

    private void OnNextClick()  {
        if(Validate()){
//            LOGISTIC_LABEL_SUMBIT logistic_label_sumbit = new LOGISTIC_LABEL_SUMBIT();
//            logistic_label_sumbit.SRNO = edDocNo.getText().toString();
//            logistic_label_sumbit.JOB_NO = edJobCode.getText().toString();
//            logistic_label_sumbit.MFG_DATE = edManf.getText().toString();
//            logistic_label_sumbit.EXP_DATE = edExp.getText().toString();
//            logistic_label_sumbit.LOT_NO = edLotNo.getText().toString();
//            logistic_label_sumbit.LOT_STATUS = edLotStatus.getText().toString();
//            logistic_label_sumbit.FORMULA_NO = edFormula.getText().toString();
//            logistic_label_sumbit.FIL_CODE = edFillCode.getText().toString();
//            logistic_label_sumbit.BARCODE = edBarcode.getText().toString();
//            logistic_label_sumbit.INPUT_PQTY = Float.parseFloat(edPUnit.getText().toString());
//            logistic_label_sumbit.INPUT_MQTY = Float.parseFloat(edMunit.getText().toString());
//            logistic_label_sumbit.INPUT_LQTY = Float.parseFloat(edLUnit.getText().toString());
//            logistic_label_sumbit.LOC_CODE = edLocCode.getText().toString();
//            logistic_label_sumbit.USER_NAME ="";
//            logistic_label_sumbit.UPDATE_FROM = "";
//            logistic_label_sumbit.PALLET_ID = "";

            //uploadGACScanData(logistic_label_sumbit);

            //confirmDamageReport();
            try {
                OnConfirmationClick(AppConstants.DAMAGE_KEY, AppConstants.DAMAGE_ID, -1,getString(R.string.damage_conf),R.string.no,R.string.yes,null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (resultCode != Activity.RESULT_OK) {
            Log.d("resultCode##",String.valueOf(requestCode));
            try {
                if (requestCode == AppConstants.PUT_ID) {
                    OpenLogisticLabel();
                    return;
                }
                OnConfirmationClick(AppConstants.PUT_AWAY_KEY, AppConstants.PUT_ID, -1,getString(R.string.put_away_conf),R.string.no,R.string.yes,null);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return;
        }

        if (requestCode == AppConstants.DAMAGE_ID) {
            int yesNo = intent.hasExtra(AppConstants.SELECTED_ID) ?  intent.getIntExtra(AppConstants.SELECTED_ID,-1) : -1;
            if(yesNo==1){
                OpenDamageFragment();
            }
            return;
        }

        if (requestCode == AppConstants.PUT_ID) {
            int yesNo = intent.hasExtra(AppConstants.SELECTED_ID) ?  intent.getIntExtra(AppConstants.SELECTED_ID,-1) : -1;
            if(yesNo==1){
                OpenPutAwayFragment();
            }
            return;
        }
    }

    private void OpenLogisticLabel() {
        if(items_prints==null){
            showAlert("Warning","Data not found!");
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putString(AppConstants.SELECTED_ID, DocNo);
        //bundle.putSerializable(AppConstants.SELECTED_OBJECT,  items_prints);
        bundle.putLong(AppConstants.PARENT_ID, HDR_ID);
        ((MainActivity) requireActivity()).NavigateBackStack(null, false);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_logistic_label, bundle);

    }


    private boolean Validate()  {
//        String errMessage;
//        DocNo = edDocNo.getText().toString();
//        if (DocNo.length() == 0) {
//            errMessage = String.format("%s required", "GACScanId");
//            showToast(errMessage);
//            edDocNo.setError(errMessage);
//            return false;
//        }

        return true;

    }


    private void DownloadOauthToken(){
        App.getNetworkClientOAuth().getAPIService().getOauthToken("client_credentials",new Callback<OAuthTokenResponse>() {
            @Override
            public void success(OAuthTokenResponse response, Response response2) {
                Log.d("success##", response.toString());
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("failure##", error.toString());
            }
        }) ;
    }

    private void OnBackClick() {
        getActivity().onBackPressed();
    }



    private void OpenDamageFragment() {
        if(items_prints==null){
            showAlert("Warning","Data not found!");
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putString(AppConstants.SELECTED_ID, DocNo);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT,  items_prints);
        bundle.putLong(AppConstants.PARENT_ID, HDR_ID);
        ((MainActivity) requireActivity()).NavigateBackStack(null, false);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_damage_confirmation, bundle);

    }

    private void OpenPutAwayFragment() {
        if(items_prints==null){
            showAlert("Warning","Data not found!");
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putString(AppConstants.SELECTED_ID, DocNo);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT,  items_prints);
        bundle.putLong(AppConstants.PARENT_ID, HDR_ID);
        ((MainActivity) requireActivity()).NavigateBackStack(null, false);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_put_away, bundle);
    }


    private void fillLogisticData(LOGISTIC_LABEL_SCAN logistic_label_scan){
        edDocNo.setText(logistic_label_scan.SRNO);
        edLUnit.setText(String.valueOf(logistic_label_scan.Lqty));
        edMunit.setText(String.valueOf(logistic_label_scan.Mqty));
        edPUnit.setText(String.valueOf(logistic_label_scan.Pqty));
        edBarcode.setText(logistic_label_scan.BARCODE);
        edFillCode.setText(logistic_label_scan.FIL_CODE);
        edFormula.setText(logistic_label_scan.FORMULA_NO);
        edLotStatus.setText(logistic_label_scan.LOT_STATUS);
        edLotNo.setText(logistic_label_scan.LOT_NO);
        //edExp.setText(logistic_label_scan.EXPDATE);
        //edManf.setText(logistic_label_scan.MFG_DATE);
        edJobCode.setText(logistic_label_scan.JOB_NO);
        if(logistic_label_scan.MFG_DATE!=null){
            edManf.setText(Utils.getForamtedDate(logistic_label_scan.MFG_DATE,ISSUE_DATE_FORMAT2,PRINT_DATE_FORMAT));
        }
        if(logistic_label_scan.EXPDATE!=null){
            edExp.setText(Utils.getForamtedDate(logistic_label_scan.EXPDATE,ISSUE_DATE_FORMAT2,PRINT_DATE_FORMAT));
        }

    }

    private void resetUI(){
        edDocNo.setText("");
        edLUnit.setText("");
        edMunit.setText("");
        edPUnit.setText("");
        edPl.setText("");
        edBarcode.setText("");
        edFillCode.setText("");
        edFormula.setText("");
        edLotStatus.setText("");
        edLotNo.setText("");
        edLocCode.setText("");
        edExp.setText("");
        edManf.setText("");
        edJobCode.setText("");
        items_prints = null;
    }


    private void uploadGACScanData(LOGISTIC_LABEL_SUMBIT logistic_label_sumbit) {
        showToast(getString(R.string.send_data_to_server));
        JsonObject requestObject = ServiceUtils.LogisticLabel.getLOGISTIC_LABEL_SUMBIT_FULL(logistic_label_sumbit,"N");
        showProgress(false);
        App.getNetworkClient().getAPIService().submitWithLinesData(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericSubmissionResponse, Response response) {
                dismissProgress();
                if (genericSubmissionResponse.getErrCode().equals("S")) {
                    if (genericSubmissionResponse.getErrCode().equals("S")) {
                        String msg = genericSubmissionResponse.getErrMessage();
                        resetUI();
                        showToastLong(msg);
//                        if (msg.length > 1) {
//                            ReceiptNo = msg[2];
//                        }
                        getActivity().onBackPressed();
                    } else {
                        showAlert("Error",genericSubmissionResponse.getErrMessage());
                        //showToast(genericSubmissionResponse.getErrMessage());
                    }
                } else {
                    Log.d(App.TAG, genericSubmissionResponse.getErrMessage());
                    showToast(genericSubmissionResponse.getErrMessage());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(App.TAG, error.getMessage());
                showToast(error.getMessage());
                dismissProgress();
            }
        });
    }

    private void FillLotStatus() {
        lot_statuses = new ArrayList<LOT_STATUS>();
        LOT_STATUS lot_status;

        lot_status = new LOT_STATUS();
        lot_status.Code = "01";
        lot_status.Name = "A";
        lot_statuses.add(lot_status);

        lot_status = new LOT_STATUS();
        lot_status.Code = "02";
        lot_status.Name = "B";
        lot_statuses.add(lot_status);

        lot_status = new LOT_STATUS();
        lot_status.Code = "03";
        lot_status.Name = "C";
        lot_statuses.add(lot_status);

        lot_status = new LOT_STATUS();
        lot_status.Code = "04";
        lot_status.Name = "J";
        lot_statuses.add(lot_status);

        companyAdapter = new ArrayAdapter<>(getContext(), R.layout.dropdown_menu_popup_item, lot_statuses);
        edLotStatus.setAdapter(companyAdapter);
        edLotStatus.setThreshold(100);
    }

}
