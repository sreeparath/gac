package com.dcode.gacinventory.ui.view.outbound;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.data.model.SP_SCN_AN_OUT_LOC_BY_JOB;
import com.dcode.gacinventory.network.model.GenericRetResponse;
import com.dcode.gacinventory.network.service.ServiceUtils;
import com.dcode.gacinventory.ui.adapter.PickAndPackListAdapter;
import com.dcode.gacinventory.ui.adapter.PickAndPackListDetailedAdapter;
import com.dcode.gacinventory.ui.adapter.SiteAndLoactionListAdapter;
import com.dcode.gacinventory.ui.view.BaseFragment;
import com.dcode.gacinventory.ui.view.MainActivity;
import com.example.gacinventory.R;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * @Created by: VIJIL DHAS A.S
 * @Date: 14-09-2021
 */
public class SiteAndLocationListFragment extends BaseFragment implements View.OnClickListener {
    private String putawayType;
    private View root;

    private RecyclerView mItemList;
    private SiteAndLoactionListAdapter packListAdapter;
    private List<SP_SCN_AN_OUT_LOC_BY_JOB> formattedItems;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);
            putawayType = this.getArguments().getString("PutAwayType", "");
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                if (bundle.containsKey(AppConstants.SELECTED_ID)) {
                    putawayType = bundle.getString(AppConstants.SELECTED_ID);
                }
            }
        }
        root = inflater.inflate(R.layout.fragment_site_and_location, container, false);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupView();
        setHeaderToolbar();
    }

    private void setupView() {
        mItemList = root.findViewById(R.id.rcv_item_list);
        initRecyclerView();
        apiFetchLocByJob();
    }

    private void initRecyclerView() {
        packListAdapter = new SiteAndLoactionListAdapter(getContext(), new ArrayList<>(), this,
                new SiteAndLoactionListAdapter.OnNavigateFragment() {
                    @Override
                    public void onNavigateToFragment(SP_SCN_AN_OUT_LOC_BY_JOB spScnAnOutLocByJob) {
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(AppConstants.SELECTED_SP_SCN_AN_OUT_LOC_BY_JOB, (Serializable) getFormattedItems());
                        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_pick_and_pack, bundle);
                    }
                });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mItemList.setLayoutManager(linearLayoutManager);
        mItemList.setAdapter(packListAdapter);
    }

    /**
     * ProcName":"SP_SCN_AN_OUT_LOC_BY_JOB"
     */
    private void apiFetchLocByJob() {
        JsonObject requestObject = ServiceUtils.OutBound.getSP_SCN_AN_OUT_LOC_BY_JOB();
        showProgress(false);

        App.getNetworkClient().getAPIService().getSpScnOutLocByJob(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("E")) {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showAlert("Error", msg);
                } else {
                    String xmlDoc = genericRetResponse.getJsonString();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showAlert("Error", "No data received.");
                        return;
                    }
                    try {
                        SP_SCN_AN_OUT_LOC_BY_JOB[] spScnAnOutLocByJobs = new Gson().fromJson(xmlDoc, SP_SCN_AN_OUT_LOC_BY_JOB[].class);
                        if (spScnAnOutLocByJobs.length == 0) {
                            showAlert("Error", "No data received.");
                            return;
                        }
                        List<SP_SCN_AN_OUT_LOC_BY_JOB> formattedList = new ArrayList<>();
                        for (int i = 0; i < spScnAnOutLocByJobs.length; i++) {
                            SP_SCN_AN_OUT_LOC_BY_JOB spScnAnOutLocByJob = new SP_SCN_AN_OUT_LOC_BY_JOB();
                            spScnAnOutLocByJob = spScnAnOutLocByJobs[i];
                            formattedList.add(spScnAnOutLocByJob);
                        }
                        setFormattedItems(getItemsByGroup(formattedList));
                        setupListView(removeDuplicates(formattedList));

                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                        showAlert("Error", e.getMessage());
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showAlert("Error", msg);
            }
        });
    }

    /**
     * function to remove duplicate site
     *
     * @param list SP_SCN_AN_OUT_LOC_BY_JOB
     * @return
     */
    private List<SP_SCN_AN_OUT_LOC_BY_JOB> removeDuplicates(List<SP_SCN_AN_OUT_LOC_BY_JOB> list) {
        Set set = new TreeSet(new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                if (((SP_SCN_AN_OUT_LOC_BY_JOB) o1).SITE_CODE.equalsIgnoreCase(((SP_SCN_AN_OUT_LOC_BY_JOB) o2).SITE_CODE)) {
                    return 0;
                }
                return 1;
            }
        });
        set.addAll(list);
        final List newList = new ArrayList(set);
        return newList;

    }

    /**
     * function to group items by site id
     *
     * @param list SP_SCN_AN_OUT_LOC_BY_JOB
     * @return
     */
    private List<SP_SCN_AN_OUT_LOC_BY_JOB> getItemsByGroup(List<SP_SCN_AN_OUT_LOC_BY_JOB> list) {
        Set set = new TreeSet(new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                if (((SP_SCN_AN_OUT_LOC_BY_JOB) o1).SITE_CODE.equalsIgnoreCase("A4")) {
                    return 1;
                }
                return 0;
            }
        });
        set.addAll(list);
        final List newList = new ArrayList(set);
        return newList;
    }


    private void setupListView(List<SP_SCN_AN_OUT_LOC_BY_JOB> spScnAnOutLocByJob) {
        packListAdapter.addItems(spScnAnOutLocByJob);
    }

    private void setHeaderToolbar() {
        TextView info1 = root.findViewById(R.id.info1);
        info1.setText(App.currentUser.USER_ID);
    }

    public List<SP_SCN_AN_OUT_LOC_BY_JOB> getFormattedItems() {
        return formattedItems;
    }

    public void setFormattedItems(List<SP_SCN_AN_OUT_LOC_BY_JOB> formattedItems) {
        this.formattedItems = formattedItems;
    }

    @Override
    public void onClick(View v) {

    }
}
