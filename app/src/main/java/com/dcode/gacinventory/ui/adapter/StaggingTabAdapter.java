package com.dcode.gacinventory.ui.adapter;


import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.data.model.INBOUND_HEADER;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SCAN;
import com.dcode.gacinventory.ui.view.ConfirmationListFragment;
import com.dcode.gacinventory.ui.view.ConfirmationPutListFragment;
import com.dcode.gacinventory.ui.view.GACScanFragment;
import com.dcode.gacinventory.ui.view.ProductUpdateInfoFragment;

import java.util.Map;

public class StaggingTabAdapter extends FragmentPagerAdapter {
    private Context myContext;
    int totalTabs;
    private Bundle bundle;
    public StaggingTabAdapter(Context context, FragmentManager fm, int totalTabs, Bundle bundle) {
        super(fm);
        myContext = context;
        this.totalTabs = totalTabs;
        this.bundle = bundle;

    }
    // this is for fragment tabs
    @Override
    public Fragment getItem(int position) {



        switch (position) {
            case 0:
                GACScanFragment gacScanFragment = new GACScanFragment();
                if (bundle != null) {
                    Bundle newBundle = new Bundle();
                    newBundle.putBoolean(AppConstants.SELECTED_ID, bundle.containsKey(AppConstants.SELECTED_ID) ? bundle.getBoolean(AppConstants.SELECTED_ID) : false);
                    newBundle.putSerializable(AppConstants.SELECTED_OBJECT,bundle.containsKey(AppConstants.SELECTED_OBJECT) ? (LOGISTIC_LABEL_SCAN) bundle.getSerializable(AppConstants.SELECTED_OBJECT) : null);
                    newBundle.putSerializable(AppConstants.SELECTED_OBJECT_NEW,bundle.containsKey(AppConstants.SELECTED_OBJECT_NEW) ? (INBOUND_HEADER) bundle.getSerializable(AppConstants.SELECTED_OBJECT_NEW) : null);
                    newBundle.putLong(AppConstants.SOURCE_ID , bundle.containsKey(AppConstants.SOURCE_ID) ?  bundle.getLong(AppConstants.SOURCE_ID) : -1);
                    newBundle.putSerializable(AppConstants.GS1_PARAM,bundle.containsKey(AppConstants.GS1_PARAM) ?  bundle.getSerializable(AppConstants.GS1_PARAM) : null);
                    gacScanFragment.setArguments(bundle);
                    //logistic_label_sumbit = bundle.containsKey(AppConstants.SELECTED_OBJECT_NEW) ? (LOGISTIC_LABEL_SUMBIT) bundle.getSerializable(AppConstants.SELECTED_OBJECT_NEW) : null;
                }
                return gacScanFragment;
            case 1:
                ProductUpdateInfoFragment productUpdateInfoFragment = new ProductUpdateInfoFragment();
                if (bundle != null) {
                    Bundle newBundle = new Bundle();
                    newBundle.putSerializable(AppConstants.SELECTED_OBJECT,bundle.containsKey(AppConstants.SELECTED_OBJECT) ? (LOGISTIC_LABEL_SCAN) bundle.getSerializable(AppConstants.SELECTED_OBJECT) : null);
                    newBundle.putSerializable(AppConstants.SELECTED_OBJECT_NEW,bundle.containsKey(AppConstants.SELECTED_OBJECT_NEW) ? (INBOUND_HEADER) bundle.getSerializable(AppConstants.SELECTED_OBJECT_NEW) : null);
                    productUpdateInfoFragment.setArguments(bundle);
                    //Log.d("data###",productUpdateInfoFragment.getProdUpdInfo().COO);
                }
                return productUpdateInfoFragment;
            default:
                return null;
        }
    }
    // this counts total number of tabs
    @Override
    public int getCount() {
        return totalTabs;
    }
}