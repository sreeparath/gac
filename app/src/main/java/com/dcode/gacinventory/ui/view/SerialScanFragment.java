package com.dcode.gacinventory.ui.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.common.GS1Identifiers;
import com.dcode.gacinventory.common.Utils;
import com.dcode.gacinventory.data.model.BARCODE_FRMT;
import com.dcode.gacinventory.data.model.BOX_SCAN_GACID;
import com.dcode.gacinventory.data.model.BOX_SCAN_PROD;
import com.dcode.gacinventory.data.model.JOB_CODE_LIST;
import com.dcode.gacinventory.data.model.PRN_CODE_LIST;
import com.dcode.gacinventory.network.model.GenericRetResponse;
import com.dcode.gacinventory.network.service.ServiceUtils;
import com.dcode.gacinventory.ui.adapter.BarcodeAdapterList;
import com.dcode.gacinventory.ui.adapter.ImageViewAdapter;
import com.example.gacinventory.R;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.dcode.gacinventory.common.GS1Identifiers.AI_DATE_EXPIRATION;
import static com.dcode.gacinventory.common.GS1Identifiers.AI_DATE_PRODUCTION;
import static com.dcode.gacinventory.common.GS1Identifiers.AI_GTIN;
import static com.dcode.gacinventory.common.GS1Identifiers.AI_LOT_NUMBER;
import static com.dcode.gacinventory.common.GS1Identifiers.AI_SERIAL_NUMBER;
import static com.dcode.gacinventory.common.Utils.GS_DATE_NUM_FORMAT;

public class SerialScanFragment extends BaseFragment implements View.OnLongClickListener{
    private TextInputEditText edPrinCode,edJobNo,edGscan,edSSCC,edSample,edDisplayQty,edPartialScan,edCurrTobeScan;
    private long HDR_ID=-1;
    private FloatingActionButton btnCamera;
    private Bitmap photo;
    private ImageViewAdapter imageViewAdapter;
    View root;
    private RecyclerView recyclerView;
    private Drawable icon;
    private final ColorDrawable background = new ColorDrawable(Color.RED);
    //private MENU menu;
    private BARCODE_FRMT barcodeFrmts;
    private TextView edTotalQty,edScannedQty,edCurrScan;
    private HashMap<String, BOX_SCAN_GACID> dataList;
    private BarcodeAdapterList batchesAdapter;
    Map<String, String> gs1Map;
    BOX_SCAN_PROD[] boxScanProds;
    int scannedQty  = 0;
    private PRN_CODE_LIST prnCodeType;
    private JOB_CODE_LIST jobCodeType;
    TextToSpeech tts;

    private Toolbar toolbar ;
    private TextView info1 ;
    private TextView info2 ;
    private HashMap tempMap;
    private int sampleSrNoLen = 0;
    private int partialCounter = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        //setHasOptionsMenu(true);

        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);
            Bundle bundle = this.getArguments();
//            if (bundle != null) {
//                menu = (MENU) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
//            }

        }

        root = inflater.inflate(R.layout.fragment_serial_scan, container, false);
        icon = getResources().getDrawable(R.drawable.ic_delete_black);


        tts = new TextToSpeech(getContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {
                if (i == TextToSpeech.SUCCESS) {
                    int result = tts.setLanguage(Locale.UK);
                    if (result == TextToSpeech.LANG_MISSING_DATA ||
                            result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Log.e("TTS", "Lenguage not supported");
                    }
                } else {
                    Log.e("TTS", "Initialization failed");
                }
            }
        });

        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                // Handle the back button even
                showToast("Back not allowed");
                shortVibration();
                Log.d("BACKBUTTON", "Back button clicks");
            }
        };

        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), callback);


        return root;
    }

    private void setupView(){
        dataList = new HashMap<>();

        tempMap = new HashMap<>();

        toolbar = root.findViewById(R.id.toolbar);
        info1 = root.findViewById(R.id.info1);
        info1.setText(App.currentUser.USER_ID);
        info2 = root.findViewById(R.id.info2);

        edPrinCode = root.findViewById(R.id.edPrinCode);
        edPrinCode.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_ENTER){
                if(event.getAction() == KeyEvent.ACTION_DOWN){
                    return onPrnCodeKeyEvent();
                }else{
                    return false;
                }
            }else{
                return false;
            }
        });

        edJobNo = root.findViewById(R.id.edJobNo);
        edJobNo.setEnabled(false);
        //edJobNo.setText("5520008448");
        edJobNo.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_ENTER){
                if(event.getAction() == KeyEvent.ACTION_DOWN){
                    return onJOBKeyEvent();
                }else{
                    return false;
                }
            }else{
                return false;
            }
        });

        edGscan = root.findViewById(R.id.edGscan);
        edGscan.setEnabled(false);
        edGscan.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_ENTER){
                if(event.getAction() == KeyEvent.ACTION_DOWN){
                    return onGACKeyEvent();
                }else{
                    return true;
                }
            }else{
                return false;
            }
        });
        edScannedQty= root.findViewById(R.id.edScannedQty);
        edTotalQty= root.findViewById(R.id.edTotalQty);
        edCurrScan= root.findViewById(R.id.edCurrScan);
        edSSCC = root.findViewById(R.id.edSSCC);
        edSSCC.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_ENTER){
                if(event.getAction() == KeyEvent.ACTION_DOWN){
                    edGscan.requestFocus();
                    return true;
                }else{
                    return true;
                }
            }else{
                return false;
            }
        });

        edCurrTobeScan= root.findViewById(R.id.edCurrTobeScan);
        edCurrTobeScan.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_ENTER){
                if(event.getAction() == KeyEvent.ACTION_DOWN){
                    edGscan.requestFocus();
                    return true;
                }else{
                    return true;
                }
            }else{
                return false;
            }
        });

        edPartialScan = root.findViewById(R.id.edPartialScan);
        enableDisable(false);

        recyclerView = root.findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));


        batchesAdapter = new BarcodeAdapterList(new ArrayList<>());
        //batchesAdapter = new BarcodeListAdapter(new ArrayList<>());
        recyclerView.setAdapter(batchesAdapter);

        MaterialButton btnNext = root.findViewById(R.id.btnNext);
        btnNext.setOnClickListener(v -> {
            OnNextClick();
        });



        MaterialButton btnPrnCode = root.findViewById(R.id.btnPrnCode);
        btnPrnCode.setOnClickListener(v -> {
            int listCount = batchesAdapter.getItemCount();
            if(listCount==0){
                try {
                    OnPRNLookupClick(AppConstants.SS_PRN_KEY, AppConstants.SS_PRN_ID, -1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else{
                showToast("Clear list before selecting new prn code!");
                return;
            }
        });

        MaterialButton btnJobCode = root.findViewById(R.id.btnJobCode);
        btnJobCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int listCount = batchesAdapter.getItemCount();
                if(listCount==0 && barcodeFrmts != null){
                    try {
                        OnJOBLookupClick(AppConstants.SS_JOB_KEY, AppConstants.SS_JOB_ID, -1, barcodeFrmts);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else{
                    showToast("Clear list before selecting new job code!");
                    return;
                }
            }
        });


        MaterialButton btnClear = root.findViewById(R.id.btnClear);
        btnClear.setOnClickListener(v -> {
            //getActivity().onBackPressed();
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            AlertDialog alertDialog = builder
                    .setTitle("Waring")
                    //.setView(view)
                    .setIcon(R.drawable.logo)
                    .setMessage("Scanned Data will lose!Are you sure want to clear ?")
                    .setPositiveButton("YES", (dialog, which) -> {
                        App.getDatabaseClient().getAppDatabase().genericDao().deleteAllBOX_SCAN_GACID(barcodeFrmts.PRIN_CODE,edJobNo.getText().toString());
                        resetUI();
                        tempMap.clear();
                        partialCounter=0;
                    })
                    .setNegativeButton("NO", (dialog, which) -> {

                    })
                    .create();
            alertDialog.show();
            shortVibration();
            ringtone();

        });

        edDisplayQty = root.findViewById(R.id.edDisplayQty);
        edSample = root.findViewById(R.id.edSample);
        edSample.setEnabled(false);
        edSample.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_ENTER){
                if(event.getAction() == KeyEvent.ACTION_DOWN){
                    return onSampleGS1KeyEvent();
                }else{
                    return false;
                }
            }else{
                return false;
            }
        });

        //edPartialScan= root.findViewById(R.id.edPartialScan);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (resultCode != Activity.RESULT_OK) return;




        if (requestCode == AppConstants.SS_PRN_ID) {
            prnCodeType = intent.hasExtra(AppConstants.SELECTED_OBJECT) ? (PRN_CODE_LIST) intent.getSerializableExtra(AppConstants.SELECTED_OBJECT) : null;
            if (prnCodeType != null) {
                edPrinCode.setText(prnCodeType.PRIN_CODE);
                ParsePrnCode(prnCodeType.PRIN_CODE);
                edJobNo.requestFocus();
            } else {
                edPrinCode.setText("");
            }
        }

        if (requestCode == AppConstants.SS_JOB_ID) {
            jobCodeType = intent.hasExtra(AppConstants.SELECTED_OBJECT) ? (JOB_CODE_LIST) intent.getSerializableExtra(AppConstants.SELECTED_OBJECT) : null;
            if (jobCodeType != null) {
                edJobNo.setText(jobCodeType.JOB_NO);
                ParseJobCode(jobCodeType.JOB_NO);
                edSSCC.requestFocus();
            } else {
                edJobNo.setText("");
            }
        }
    }


    private boolean onPrnCodeKeyEvent() {
        String ScanCode = edPrinCode.getText().toString();

        if (ScanCode.isEmpty() || ScanCode.trim().length() == 0) {
            showToast("Invalid PrnCode");
            edPrinCode.requestFocus();
            ringtone();
            shortVibration();
            return false;
        } else {
            ParsePrnCode(ScanCode);
        }
        return true;
    }

    private boolean onGACKeyEvent() {

        if(barcodeFrmts == null || edPrinCode.getText().length()==0){
            ringtone();
            shortVibration();
            showToast("invalid PRN");
            edGscan.setText("");
            return false;
        }

        String jobNo = edJobNo.getText().toString();
        if (jobNo.isEmpty() || jobNo.trim().length() == 0) {
            ringtone();
            shortVibration();
            edGscan.setText("");
            showToast("invalid JOB");
            return false;
        }


        String ScanCode = edGscan.getText().toString();
        edGscan.setText("");
        if (ScanCode.isEmpty() || ScanCode.trim().length() == 0) {
            showToast("Invalid Gscan");
            edGscan.setText("");
            edGscan.requestFocus();
            ringtone();
            shortVibration();
            return false;
        }

//        String currToBe = edCurrTobeScan.getText().toString();
//        if (currToBe.isEmpty() || currToBe.trim().length() == 0) {
//            showToast("Invalid Curr scan to be!");
//            edGscan.setText("");
//            edCurrTobeScan.requestFocus();
//            ringtone();
//            shortVibration();
//            return false;
//        }

        ParseGACBarCode(ScanCode);
        return true;
    }

    private boolean onSampleGS1KeyEvent() {
        String ScanCode = edSample.getText().toString();
        edSample.setText("");
        if (ScanCode.trim().length() > 0) {
            Map<String, String> sampleMap = GS1Identifiers.decodeBarcodeGS1WithNpoOrder(ScanCode);
            if (sampleMap != null && sampleMap.size() > 0) {
                Log.d("sampleMap##",sampleMap.toString());
                if ((sampleMap.containsKey(AI_SERIAL_NUMBER)) && (sampleMap.get(AI_SERIAL_NUMBER) != null && sampleMap.get(AI_SERIAL_NUMBER).length() > 0)) {
                    sampleSrNoLen = sampleMap.get(AI_SERIAL_NUMBER).length();
                    edDisplayQty.setText(String.valueOf(sampleSrNoLen));
                    enableDisable(true);
                    edSSCC.requestFocus();
                } else {
                    sampleSrNoLen = 0;
                    edSSCC.requestFocus();
                    enableDisable(false);
                }
            }
            return true;
        } else {
            ParseJobCode(ScanCode);
        }
        return false;
    }


    private boolean onJOBKeyEvent() {
        String ScanCode = edJobNo.getText().toString();

        if (ScanCode.isEmpty() || ScanCode.trim().length() == 0) {
            showToast("Invalid Job No");
            edJobNo.requestFocus();
            ringtone();
            shortVibration();
            return false;
        } else {
            ParseJobCode(ScanCode);
        }
        return true;
    }

    private void ParsePrnCode(String scanCode) {
        if (scanCode.length() <= 0 ) {
            return;
        }
        DownloadPrnHeader(scanCode);
    }

    private void ParseJobCode(String scanCode) {
        if (scanCode.length() <= 0 ) {
            return;
        }
        DownloadJobHeader(scanCode,barcodeFrmts.PRIN_CODE);
    }

    private void ParseGACBarCode(String scanCode) {
        try {
            if (scanCode.length() <= 0) {
                return;
            }

            if (scanCode.length() < 17) {
                return;
            }



//            if(scanCode.length() > 80 ){
//                edGscan.setText("");
//                Log.d("LEN50",scanCode);
//                return;
//            }


            int scanTotal = batchesAdapter.getItemCount();



            //CURR QTY CHECK
            String currToBeScan = edCurrTobeScan.getText().toString();
            if (currToBeScan.length() > 0) {
                if (scanTotal >= Integer.parseInt(currToBeScan)) {
                    edGscan.setText("");
                    String toSpeak = "Box Done";
                    tts.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
                    //showAlert("Error", "Scan Qty should be less or equal to " + getString(R.string.curr_qty_to__be_scan));
                    showToast("Scan Qty should be less or equal to " + getString(R.string.curr_qty_to__be_scan));
                    ringtone();
                    shortVibration();
                    return;
                }
            }

            if(tempMap.containsKey(scanCode)){
                edGscan.setText("");
                //String toSpeak = "Duplicate ignored";
                //tts.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
                //showToast("duplicate");
                return;
            }else{
                tempMap.put(scanCode,scanCode);
            }




            BOX_SCAN_GACID barCodeData = new BOX_SCAN_GACID();
            barCodeData.SSCC = edSSCC.getText().toString();
            barCodeData.JOB_NO = edJobNo.getText().toString();
            barCodeData.PRIN_CODE = barcodeFrmts.PRIN_CODE;
            barCodeData.QTY = 1;
            gs1Map = GS1Identifiers.decodeBarcodeGS1WithNpoOrder(scanCode);
            Log.d("gs1Map##",gs1Map.toString());
            if (gs1Map != null && gs1Map.size() > 0) {
                if ((gs1Map.containsKey(AI_GTIN)) && (gs1Map.get(AI_GTIN) != null && gs1Map.get(AI_GTIN).length() > 0)) {
                    barCodeData.GTIN = gs1Map.get(AI_GTIN);
                    //edBarcode.setText(gs1Map.get(AI_GTIN));
                } else {
                    ringtone();
                    shortVibration();
                    edGscan.setText("");
                    showToast("invalid GTIN");
                    tempMap.remove(scanCode);
                    return;
                }
                if ((gs1Map.containsKey(AI_LOT_NUMBER)) && (gs1Map.get(AI_LOT_NUMBER) != null && gs1Map.get(AI_LOT_NUMBER).length() > 0)) {
                    barCodeData.LOT_NO = gs1Map.get(AI_LOT_NUMBER);
                }
                if ((gs1Map.containsKey(AI_DATE_PRODUCTION)) && (gs1Map.get(AI_DATE_PRODUCTION) != null && gs1Map.get(AI_DATE_PRODUCTION).length() > 0)) {

                    String manf = gs1Map.get(AI_DATE_PRODUCTION);
                    if(manf.length()==2){
                        manf = manf +"0101";
                    }
                    if(manf.length()==4){
                        manf = manf +"01";
                    }
                    barCodeData.MANF_DT = manf;
                    barCodeData.MANF_DT_DISPLAY = Utils.getForamtedDate(manf, GS_DATE_NUM_FORMAT, Utils.ISSUE_DATE_FORMAT2);
                }
                if ((gs1Map.containsKey(AI_DATE_EXPIRATION)) && (gs1Map.get(AI_DATE_EXPIRATION) != null && gs1Map.get(AI_DATE_EXPIRATION).length() > 0)) {
                    String exp = gs1Map.get(AI_DATE_EXPIRATION);
                    if(exp.length()==2){
                        exp = exp +"0101";
                    }
                    if(exp.length()==4){
                        exp = exp +"01";
                    }
                    barCodeData.EXP_DT =exp;
                    barCodeData.EXP_DT_DISPLAY = Utils.getForamtedDate(exp, GS_DATE_NUM_FORMAT, Utils.ISSUE_DATE_FORMAT2);
                }
                if ((gs1Map.containsKey(AI_SERIAL_NUMBER)) && (gs1Map.get(AI_SERIAL_NUMBER) != null && gs1Map.get(AI_SERIAL_NUMBER).length() > 0)
                ) {
                    barCodeData.SLNO = gs1Map.get(AI_SERIAL_NUMBER);
                }else{
                    ringtone();
                    shortVibration();
                    edGscan.setText("");
                    showToast("invalid SERIAL NUMBER");
                    tempMap.remove(scanCode);
                    return;
                }
                if(sampleSrNoLen>0 && sampleSrNoLen!=barCodeData.SLNO.length()){
                    //showToast("sampleSrNoLen!");
                    tempMap.remove(scanCode);
                    return;
                }
            }

            if (validate(barCodeData)) {
                BOX_SCAN_GACID gtinExist = App.getDatabaseClient().getAppDatabase().genericDao().getBOX_SCAN_GACID_GTIN(barcodeFrmts.PRIN_CODE, edJobNo.getText().toString(), barCodeData.GTIN);
                //if(gtinExist==null ){
                //    App.getDatabaseClient().getAppDatabase().genericDao().insertBOX_SCAN_GACID(barCodeData);
                //}else{
                //    App.getDatabaseClient().getAppDatabase().genericDao().updateBOX_SCAN_GACID_GTIN(barcodeFrmts.PRIN_CODE,edJobNo.getText().toString(),barCodeData.GTIN);
                //}
                App.getDatabaseClient().getAppDatabase().genericDao().insertBOX_SCAN_GACID(barCodeData);
                partialCounter++;
                refreshData();
                edGscan.setText("");

                //partial check
                String partial = edPartialScan.getText().toString();
                if (partial.length() > 0) {
                    if (partialCounter == Integer.parseInt(partial)) {
                        String toSpeak = "Next";
                        tts.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
                        partialCounter=0;
                    }
                }

                if(currToBeScan.length()>0) {
                    if (batchesAdapter.getItemCount() == Integer.parseInt(currToBeScan)) {
                        String toSpeak = "Box Done";
                        tts.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
                    }
                }


            } else {
                edGscan.setText("");
                //edGscan.requestFocus();
            }
            edGscan.setText("");
        }catch (Exception e){
            e.printStackTrace();
            showAlert("Error",e.getMessage());
            edGscan.setText("");
        }

        //edGscan.requestFocus();

    }

    private boolean validate(BOX_SCAN_GACID barCodeData){
        try {



            //GTIN EXIST
            List<BOX_SCAN_PROD> gtinMasterExist = App.getDatabaseClient().getAppDatabase().genericDao().getAllBOX_SCAN_PROD_GTIN(barCodeData.GTIN);
            if (gtinMasterExist.size() <= 0) {
                showToast("GTIN Not Exist!");
                ringtone();
                shortVibration();
                return false;
            }

            List<BOX_SCAN_GACID> serailExist = App.getDatabaseClient().getAppDatabase().genericDao().getBOX_SCAN_GACID_GTIN_SLNO(barcodeFrmts.PRIN_CODE, edJobNo.getText().toString(), barCodeData.GTIN, barCodeData.SLNO);
            if (serailExist.size() > 0) {
                //showAlert("Error", "SLNO Already Exist!");
                showToast("SLNO Already Exist!");
                ringtone();
                shortVibration();
                return false;
            }

            //QTY CHECK
            int totalPCS = App.getDatabaseClient().getAppDatabase().genericDao().getTotalBOX_SCAN_PROD_GTIN(barCodeData.GTIN);
            int scanTotal = batchesAdapter.getItemCount();
            if (totalPCS < scanTotal) {
                //showAlert("Error", "Scan Qty should be less than Total Qty!");
                showToast("Scan Qty should be less than Total Qty!");
                ringtone();
                shortVibration();
                return false;
            }

            //CURR QTY CHECK
            String currToBeScan = edCurrTobeScan.getText().toString();
            if (currToBeScan.length() > 0) {
                if (scanTotal >= Integer.parseInt(currToBeScan)) {
                    //showAlert("Error", "Scan Qty should be less or equal to " + getString(R.string.curr_qty_to__be_scan));
                    showToast("Scan Qty should be less or equal to " + getString(R.string.curr_qty_to__be_scan));
                    ringtone();
                    shortVibration();
                    return false;
                }
            }

        }catch (Exception e){
            e.printStackTrace();
            showAlert("Error",e.getMessage());
        }


        return true;

    }


   @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       setupView();
   }

/*    private boolean onDocNoKeyEvent() {
        DocNo = edDocNo.getText().toString();

        if (DocNo.isEmpty() || DocNo.trim().length() == 0) {
            showToast(String.format("%s required", getString(promptResource)));
            return false;
        }
        return true;
    }*/

    private void enableDisable(boolean flag){
        edGscan.setEnabled(flag);
        edSSCC.setEnabled(flag);
        edCurrTobeScan.setEnabled(flag);
        edPartialScan.setEnabled(flag);
    }
    private void OnNextClick() {
        if(ValidateSubmit()){
            //CURR QTY CHECK
            int scanTotal = batchesAdapter.getItemCount();
            String currToBeScan = edCurrTobeScan.getText().toString();
            if (currToBeScan.length() > 0) {
                if (scanTotal < Integer.parseInt(currToBeScan)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    AlertDialog alertDialog = builder
                            .setTitle("Warning")
                            //.setView(view)
                            .setIcon(R.drawable.logo)
                            .setMessage("Scanned Qty not equal to curr qty to be scanned! Are you sure want to continue ?")
                            .setPositiveButton("YES", (dialog, which) -> {
                                uploadISerialScan();
                                //showToast("Submit");
                            })
                            .setNegativeButton("NO", (dialog, which) -> {
                            })
                            .create();
                    alertDialog.show();
                    shortVibration();
                    ringtone();
                }else{
                    uploadISerialScan();
                }
            }else{
                showToast("curr qty to be scan should not be empty.");
                ringtone();
                shortVibration();
            }

        }
    }

    private boolean ValidateSubmit() {
        String errMessage;

        if(barcodeFrmts==null){
            errMessage = "Prn Code shouldn't be empty!";
            showToast(errMessage);
            edPrinCode.setError(errMessage);
            ringtone();
            shortVibration();
            return false;
        }

        String jobNo = edJobNo.getText().toString();
        if (jobNo.length() == 0) {
            errMessage = String.format("%s required", "Job no");
            showToast(errMessage);
            edJobNo.setError(errMessage);
            ringtone();
            shortVibration();
            return false;
        }

        if(batchesAdapter.getItemCount()<=0){
            errMessage = String.format("%s required", "Please scan any items!");
            showToast(errMessage);
            ringtone();
            shortVibration();
            return false;
        }


        return true;

    }

    private void uploadISerialScan() {
        try {
            List<BOX_SCAN_GACID> scanList = batchesAdapter.getItems();
            if (scanList.size() <= 0) {
                return;
            }
            String listData = ServiceUtils.LogisticLabel.getSerialBarcodeInFormat(scanList);
            Log.d("listData##", listData);
            showToast(getString(R.string.send_data_to_server));
            JsonObject requestObject = ServiceUtils.LogisticLabel.getSerialScanData(barcodeFrmts, edJobNo.getText().toString(), scanList);
            showProgress(false);
            App.getNetworkClient().getAPIService().submitWithLinesData(requestObject, new Callback<GenericRetResponse>() {
                @Override
                public void success(GenericRetResponse genericSubmissionResponse, Response response) {
                    dismissProgress();
                    if (genericSubmissionResponse.getErrCode().equals("S")) {
                        String msg = genericSubmissionResponse.getErrMessage();
                        App.getDatabaseClient().getAppDatabase().genericDao().deleteAllBOX_SCAN_GACID(barcodeFrmts.PRIN_CODE, edJobNo.getText().toString());
                        resetUI();
                        tempMap.clear();
                        showToast(msg);
                        //ringtone();
                        shortVibration();
                    } else {
                        Log.d(App.TAG, genericSubmissionResponse.getErrMessage());
                        showAlert("Error", genericSubmissionResponse.getErrMessage());
                        //ringtone();
                        //shortVibration();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.d(App.TAG, error.getMessage());
                    showAlert("Error", error.getMessage());
                    dismissProgress();
                    ringtone();
                    shortVibration();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
            showAlert("Error",e.getMessage());
        }
    }




    @Override
    public boolean onLongClick(View view) {


        return true;
    }

    private void setHeaderToolbar(BARCODE_FRMT barcodeFrmt) {
        if (barcodeFrmt != null) {
            info2.setText(barcodeFrmt.PRIN_CODE + " - " + barcodeFrmt.PRIN_NAME );
        }else{
            info2.setText("");
        }
    }

    private void setEmptyHeaderToolbar() {
        info2.setText("");
    }

    private void setJobHeaderData(BOX_SCAN_PROD[] boxScanProd) {
        if (boxScanProd != null) {
            int totQty = 0;
            int totScan = 0;
            for (BOX_SCAN_PROD b :
                    boxScanProd) {
                totQty = totQty + b.TOT_PCS;
                totScan = totScan + b.RTN;
            }
            edTotalQty.setText(getString(R.string.tot_qty_to_scan) + ":" + String.valueOf(totQty));
        }
        //int scannedQty = App.getDatabaseClient().getAppDatabase().genericDao().getTotalBOX_SCAN_GACID_GTIN(barcodeFrmts.PRIN_CODE,edJobNo.getText().toString());
        //edScannedQty.setText(getString(R.string.scanned_qty)+":"+String.valueOf(scannedQty));
    }


    private void DownloadPrnHeader(String prnCode) {
        if (prnCode.length() <= 0) {
            return;
        }

        JsonObject requestObject = ServiceUtils.LogisticLabel.getPrnCodeHeader(prnCode,"GS1DM");

        showProgress(false);
        App.getNetworkClient().getAPIService().getRetGeneric(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if ((genericRetResponse.getErrCode().equals("S") || genericRetResponse.getErrCode().equals("")) &&
                        genericRetResponse.getJsonString().length() > 0) {
                    String xmlDoc = genericRetResponse.getJsonString();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showAlert("Error", "No data received.");
                        return;
                    }
                    try {
                        BARCODE_FRMT[] barcodeFrmts1 = new Gson().fromJson(xmlDoc, BARCODE_FRMT[].class);
                        if(barcodeFrmts1.length==0){
                            showAlert("Error", "No data received."+prnCode);
                            setEmptyHeaderToolbar();
                            edPrinCode.setText("");
                            edPrinCode.requestFocus();
                            edJobNo.setText("");
                            barcodeFrmts = null;
                            return;
                        }
                        barcodeFrmts = barcodeFrmts1[0];
                        barcodeFrmts.PRIN_CODE = prnCode;
                        setHeaderToolbar(barcodeFrmts);
                        edJobNo.setEnabled(true);
                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                        showAlert("Error", e.getMessage());
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showAlert("Error", msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showAlert("Error", msg);
            }

        });
    }

    private void DownloadJobHeader(String jobNo,String prnCode) {
        if (prnCode.length() <= 0 || jobNo.length() <= 0) {
            return;
        }

        JsonObject requestObject = ServiceUtils.LogisticLabel.getJobCodeHeader(jobNo,prnCode);

        showProgress(false);
        App.getNetworkClient().getAPIService().getRetGeneric(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if ((genericRetResponse.getErrCode().equals("S") || genericRetResponse.getErrCode().equals("")) &&
                        genericRetResponse.getJsonString().length() > 0) {
                    String xmlDoc = genericRetResponse.getJsonString();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showAlert("Error", "No data received.");
                        return;
                    }
                    try {
                        boxScanProds = new Gson().fromJson(xmlDoc, BOX_SCAN_PROD[].class);
                        if(boxScanProds.length==0){
                            showAlert("Error", "No data received."+jobNo);
                            edJobNo.setText("");
                            edJobNo.requestFocus();
                            return;
                        }
                        App.getDatabaseClient().getAppDatabase().genericDao().deleteAllBOX_SCAN_PROD();
                        App.getDatabaseClient().getAppDatabase().genericDao().insertBOX_SCAN_PROD(boxScanProds);
                        setJobHeaderData(boxScanProds);
                        refreshData();
                        enableDisable(false);
                        edSample.setEnabled(true);
                        edSample.requestFocus();
                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                        showAlert("Error", e.getMessage());
                        edJobNo.setText("");
                        edJobNo.requestFocus();
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showAlert("Error", msg);
                    edJobNo.setText("");
                    edJobNo.requestFocus();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showAlert("Error", msg);
            }
        });
    }

    public void refreshData() {
        List<BOX_SCAN_GACID> data = App.getDatabaseClient().getAppDatabase().genericDao().getAllBOX_SCAN_GACID(barcodeFrmts.PRIN_CODE,edJobNo.getText().toString());
        batchesAdapter.addItems(data);
        edCurrScan.setText(getString(R.string.curr_scan_qty)+":"+String.valueOf(batchesAdapter.getItemCount()));
        edScannedQty.setText(getString(R.string.scanned_qty)+":"+String.valueOf(scannedQty++));



    }


    private void resetUI() {
        batchesAdapter.clearList();
        edCurrScan.setText(getString(R.string.curr_scan_qty)+":"+String.valueOf(batchesAdapter.getItemCount()));
        edGscan.setText("");
        edSSCC.setText("");
        edSSCC.requestFocus();
    }





    @Override
    public void onCreateOptionsMenu(@NonNull @NotNull Menu menu, @NonNull @NotNull MenuInflater inflater) {
        menu.clear();

        // Add the new menu items
        inflater.inflate(R.menu.main, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.actionJobStatus:
                return true;
            case R.id.actionImages:
                //openImages();
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
