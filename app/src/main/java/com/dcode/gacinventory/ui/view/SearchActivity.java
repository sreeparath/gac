package com.dcode.gacinventory.ui.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.data.model.INBOUND_HEADER;
import com.dcode.gacinventory.ui.adapter.SearchTypeAdapter;
import com.example.gacinventory.R;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;

public class SearchActivity
        extends BaseActivity
        implements View.OnClickListener, SearchView.OnQueryTextListener {
    private int Master_ID;
    private int Parent_ID;
    private String[] TitlesArray;
    private String filterString = "";
    private SearchTypeAdapter objectsAdapter;
    private int[] fkey_intArray;
    private INBOUND_HEADER[] inbound_headerList;
    private String screenTitile = "";


    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_search;
    }

    @Override
    protected void onViewReady(Bundle savedInstanceState) {
        try {
            Intent intent = this.getIntent();
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Master_ID = !bundle.containsKey(AppConstants.MASTER_ID) ? -1 : bundle.getInt(AppConstants.MASTER_ID, -1);
                Parent_ID = !bundle.containsKey(AppConstants.PARENT_ID) ? -1 : bundle.getInt(AppConstants.PARENT_ID, -1);
                TitlesArray = !bundle.containsKey(AppConstants.TITLE) ? null : bundle.getStringArray(AppConstants.TITLE);
                //Log.d("TitlesArray##",bundle.getStringArray(AppConstants.TITLE).toString());
                if (bundle.containsKey(AppConstants.SELECTED_OBJECT)) {
                    inbound_headerList = (INBOUND_HEADER[]) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
                }

                if (bundle.containsKey(AppConstants.FILTER_KEY_INT_ARRAY)) {
                    fkey_intArray = bundle.getIntArray(AppConstants.FILTER_KEY_INT_ARRAY);
                }
            }

            if (Master_ID != 0 && inbound_headerList!=null) {
                setupView();
            } else {
                setResult(RESULT_CANCELED, intent);
                finish();
            }
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
            showAlert("Error", e.getMessage(), false);
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        objectsAdapter.getFilter().filter(newText);
        filterString = newText;
        return false;
    }

    @Override
    public void onClick(View view) {
        try {
            Bundle bundle = new Bundle();

            INBOUND_HEADER objectSearch = (INBOUND_HEADER) view.getTag();
            //bundle.putLong(AppConstants.SELECTED_ID, objectSearch.PK_ID);
            bundle.putString(AppConstants.SELECTED_CODE, objectSearch.JOB_NO);
            //bundle.putLong(AppConstants.PARENT_ID, objectSearch.PARENT_ID);
            bundle.putSerializable(AppConstants.SELECTED_OBJECT, objectSearch);

            Intent intent = this.getIntent();
            intent.putExtras(bundle);
            setResult(RESULT_OK, intent);
            finishAndRemoveTask();
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
            showAlert("Error", e.getMessage(), false);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        setResult(RESULT_CANCELED);
        finishAndRemoveTask();
    }

    private void setupView() {
        try {
            String ScreenTitle = getString(R.string.search);
            String CodeTitle = getString(R.string.job_no);
            String NameTitle = getString(R.string.container_seal);
            String DescTitle = getString(R.string.truck);
            if (TitlesArray != null) {

                ScreenTitle = TitlesArray.length >= 1 && TitlesArray[0] != null ? TitlesArray[0] : getString(R.string.search);
                CodeTitle = TitlesArray.length >= 2 && TitlesArray[1] != null ? TitlesArray[1] : getString(R.string.job_no);
                NameTitle = TitlesArray.length >= 3 && TitlesArray[2] != null ? TitlesArray[2] : getString(R.string.container_seal);
                DescTitle = TitlesArray.length >= 3 && TitlesArray[3] != null ? TitlesArray[3] : getString(R.string.truck);
            }

            Toolbar toolbar = findViewById(R.id.toolbar);
            toolbar.setTitle(ScreenTitle+" "+getString(R.string.job_no));

            SearchView searchView = findViewById(R.id.search_bar);

            searchView.setOnQueryTextListener(this);

//            TextView tvUserName = findViewById(R.id.tvUserName);
//            if (App.currentUser != null) {
//                tvUserName.setText(App.currentUser.USER_ID);
//            }

            RecyclerView recyclerView = findViewById(R.id.recyclerView);
            ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);

            objectsAdapter = new SearchTypeAdapter(SearchActivity.this, new ArrayList<>(), this, CodeTitle, NameTitle,DescTitle);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));

            switch (Master_ID) {
                case AppConstants.JOB_ID:
                    List headerList = new ArrayList(Arrays.asList(inbound_headerList));
                    objectsAdapter.addItems( headerList);
                    break;
                default:
                    break;
            }

            recyclerView.setAdapter(objectsAdapter);
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
            showAlert("Error", e.getMessage(), false);
        }
    }


}

