package com.dcode.gacinventory.ui.view;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.common.GS1Identifiers;
import com.dcode.gacinventory.common.StagingValidation;
import com.dcode.gacinventory.common.ValidRespSet;
import com.dcode.gacinventory.data.model.INBOUND_HEADER;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SCAN;
import com.dcode.gacinventory.network.model.GenericRetResponse;
import com.dcode.gacinventory.network.service.ServiceUtils;
import com.example.gacinventory.R;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.Serializable;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.dcode.gacinventory.common.GS1Identifiers.AI_GTIN;
import static com.dcode.gacinventory.common.GS1Identifiers.AI_LOT_NUMBER;

public class LogisticLabelFragment extends BaseFragment {
    private TextInputEditText edDocNo,edBarcode,edGacScan,edLotNo;
    private TextInputLayout ilLotNo;
//    private AutoCompleteTextView tvDocTypes;
//    private ArrayAdapter<DocType> objectsAdapter;
    private int promptResource;
    private String scanId;
    private long HDR_ID;
    private CheckBox chkSelect;
    private LOGISTIC_LABEL_SCAN[] items_prints;
    private View root;
    private INBOUND_HEADER inbound_header;
    private RadioGroup rdoGroup;
    boolean scanIdMandatory = true;
    private TextInputLayout ilGacScan;
    private StagingValidation stagingValidation;
    Map<String, String> gs1Map;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                //DocNo = bundle.getString(AppConstants.SELECTED_ID);
                scanId = bundle.containsKey(AppConstants.SELECTED_ID) ?  bundle.getString(AppConstants.SELECTED_ID) : "";
                inbound_header = (INBOUND_HEADER) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
               // Log.d("items_prints##",items_prints.toString());
            }
        }
        root = inflater.inflate(R.layout.fragment_logistic_label, container, false);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupView();
    }

    private void setupView(){
        //ImageView imgDocNo = root.findViewById(R.id.imgDocNo);
        //imgDocNo.setImageResource(R.drawable.ic_005);
        stagingValidation = new StagingValidation();
        setHeaderToolbar();
        ilGacScan = root.findViewById(R.id.ilGacScan);
        ilGacScan.setHint(getString(R.string.new_id));
        edDocNo = root.findViewById(R.id.edScanCode);
        edDocNo.setEnabled(false);
        edBarcode = root.findViewById(R.id.edBarcode);
        edBarcode.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_ENTER){
                if(event.getAction() == KeyEvent.ACTION_DOWN){
                    return onBarcodeCodeKeyEvent();
                }else{
                    return false;
                }
            }else{
                return false;
            }
        });

        edGacScan = root.findViewById(R.id.edGacScan);

        if(scanId.trim()!=null && scanId.trim().length()>0) {
            edGacScan.setText(scanId);
            //edBarcode.requestFocus();
        }else {
            //edGacScan.requestFocus();
        }


        edLotNo = root.findViewById(R.id.edLotNo);
        //edLotNo.setVisibility(View.GONE);
        ilLotNo = root.findViewById(R.id.ilLotNo);
        //ilLotNo.setVisibility(View.GONE);


        chkSelect= root.findViewById(R.id.chkSelect);
        chkSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                edDocNo.setText("");
                edDocNo.setEnabled(isChecked);
                if(isChecked){
                    //edLotNo.setVisibility(View.GONE);
                    //ilLotNo.setVisibility(View.GONE);
                    edDocNo.requestFocus();
                }else{
                    //edLotNo.setVisibility(View.GONE);
                    //ilLotNo.setVisibility(View.GONE);
                    edGacScan.requestFocus();
                }

            }
        });


        MaterialButton btPhoto = root.findViewById(R.id.btnBack);
        btPhoto.setOnClickListener(v -> OnBackClick());

        MaterialButton btnPrint = root.findViewById(R.id.btNext);
        btnPrint.setOnClickListener(v -> OnNextClick());

/*        tvDocTypes = root.findViewById(R.id.acDocType);
        tvDocTypes.setOnItemClickListener((parent, view, position, id) -> {
            if (position > 0) {
                ordType = objectsAdapter.getItem(position);
            } else {
                ordType = null;
            }
        });*/

        rdoGroup = root.findViewById(R.id.radioGroup);
        int check = rdoGroup.getCheckedRadioButtonId();
        if (check==-1) {
            showToast("Logistic Type is Mandatory!");
            return;
        }
        rdoGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int check = group.getCheckedRadioButtonId();
                RadioButton rdoButton = (RadioButton) group.findViewById(check);
                showToast(rdoButton.getText().toString());
                if((rdoButton.getText().toString().equalsIgnoreCase("FULL"))){
                    scanIdMandatory = true;
                    ilGacScan.setHint(getString(R.string.new_id_optional)+getColoredString("*"));
                }else{
                    scanIdMandatory = false;
                    ilGacScan.setHint(getString(R.string.new_id_optional));
                    //ilGacScan.setHint(Html.fromHtml("<font color='rgb'>"+getString(R.string.new_id_optional)+"</font>"));
                }
            }
        });

        //edBarcode.setText("000000000010199461");
        //edGacScan.setText("I55200080350002");
    }

    public String getColoredString(String start) {
        int color = Color.RED;
        Spannable wordToSpan = new SpannableString(start);
        wordToSpan.setSpan(new ForegroundColorSpan(color), 0, start.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return wordToSpan.toString();
    }

    private boolean onBarcodeCodeKeyEvent() {
        String scanCode = edBarcode.getText().toString();
        if (scanCode.isEmpty() || scanCode.trim().length() == 0) {
            showToast("Invalid Barcode");
            edBarcode.requestFocus();
            return false;
        } else {
            ParseBarCode(scanCode);
        }
        return true;
    }

    private void ParseBarCode(String barcode){
        if (barcode.length() <= 0 ) {
            return;
        }

        ValidRespSet validObject = stagingValidation.ValidateGS1Barcode(App.dynFuncs, inbound_header.PRIN_CODE);
        if (validObject.getErrCode().length() > 0 && validObject.getErrCode().equals("S")) {
            showToast(validObject.getErrMSG());
            if (validObject.getErrMSG().length() > 0) {
                gs1Map  = GS1Identifiers.decodeBarcodeGS1_128(barcode,",",true,validObject.getErrMSG());
                if(gs1Map!=null && gs1Map.size()>0) {
                    if ((gs1Map.containsKey(AI_GTIN)) && (gs1Map.get(AI_GTIN) != null && gs1Map.get(AI_GTIN).length() > 0)) {
                        edBarcode.setText(gs1Map.get(AI_GTIN));
                    }
                    if((gs1Map.containsKey(AI_LOT_NUMBER) )&& (gs1Map.get(AI_LOT_NUMBER)!=null && gs1Map.get(AI_LOT_NUMBER).length()>0)){
                        edLotNo.setText(gs1Map.get(AI_LOT_NUMBER));
                    }
                }
            }
        }

    }

    private void setHeaderToolbar() {
        if (inbound_header != null) {
            Toolbar toolbar = root.findViewById(R.id.toolbar);
            TextView info1 = root.findViewById(R.id.info1);
            info1.setText(App.currentUser.USER_ID);
            TextView info2 = root.findViewById(R.id.info2);
            info2.setText(inbound_header.JOB_NO + " - "+inbound_header.PRIN_NAME);
        }
    }

    private void OnNextClick() {
        if(Validate()){
            String putAwaycode = "";
            if(scanIdMandatory){
                putAwaycode = "P";
            }else{
                putAwaycode = "M";
            }
            DownloadLogistic_Label(edDocNo.getText().toString(),edGacScan.getText().toString(),edBarcode.getText().toString(),
                    inbound_header.PRIN_CODE,putAwaycode,inbound_header.JOB_NO,edLotNo.getText().toString());
        }
    }

    private void OnBackClick() {
        getActivity().onBackPressed();
    }

    private boolean Validate() {
        String errMessage;
        String DocNo = edDocNo.getText().toString();
        if (chkSelect.isChecked() && DocNo.length() == 0) {
            errMessage = String.format("%s required", "Logistic Label");
            showToast(errMessage);
            edDocNo.setError(errMessage);
            return false;
        }

        String gac = edGacScan.getText().toString();
        if (gac.length() == 0 && scanIdMandatory) {
            errMessage = String.format("%s required", "GAC Scan Id");
            showToast(errMessage);
            edGacScan.setError(errMessage);
            return false;
        }
        String barcode = edBarcode.getText().toString();
        if (barcode.length() == 0) {
            errMessage = String.format("%s required", "Product Barcode");
            showToast(errMessage);
            edBarcode.setError(errMessage);
            return false;
        }

        if(inbound_header==null){
            errMessage = String.format("%s required", "Inbound Header");
            showToast(errMessage);
            return false;
        }

        return true;

    }

    private void DownloadLogistic_Label(String labelNo,String gacScan,String prodBarcode,String prinCode,String putAwayCode,String jobNo,String lotNo) {
        if (prodBarcode.length() <= 0)  {
            return;
        }

        JsonObject requestObject = ServiceUtils.LogisticLabel.getLogisticLabel(labelNo,gacScan,prodBarcode,prinCode,putAwayCode,jobNo,lotNo);
        showProgress(false);
        App.getNetworkClient().getAPIService().getRetGeneric(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (!genericRetResponse.getErrCode().equals("E")) {
                    String xmlDoc = genericRetResponse.getJsonString();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showAlert("Error", "No data received.");
                        return;
                    }
                    try {
                        items_prints = new Gson().fromJson(xmlDoc, LOGISTIC_LABEL_SCAN[].class);
                        if(items_prints.length==0){
                            showAlert("Error", "No data received.");
                            return;
                        }
                        OpenGACScan();

                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                        showAlert("Error", e.getMessage());
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showAlert("Error", msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showAlert("Error", msg);
            }
        });
    }

/*    private void getDocTypes() {
        List<DocType> orTypList = App.getDatabaseClient().getAppDatabase().genericDao().getAllOrTyps();
        objectsAdapter = new ArrayAdapter<>(requireContext(),
                R.layout.dropdown_menu_popup_item, orTypList);
        tvDocTypes.setAdapter(objectsAdapter);
        tvDocTypes.setSelection(0);
        tvDocTypes.setThreshold(100);

        objectsAdapter.notifyDataSetChanged();
    }*/

    private void OpenGACScan() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        //bundle.putString(AppConstants.SELECTED_ID, DocNo);
        bundle.putString(AppConstants.MASTER_ID, edBarcode.getText().toString());
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, items_prints[0]);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT_NEW,inbound_header);
        bundle.putSerializable(AppConstants.GS1_PARAM, (Serializable) gs1Map);
        bundle.putLong(AppConstants.PARENT_ID, HDR_ID);
        //((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_gac_scan, bundle);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_stagging_list_tablayout, bundle);
    }

//    private void DownloadLogisticLabel(final String docNo, final String docType) {
//        JsonObject requestObject = ServiceUtils.GoodsReceipt.getGoodsReceiptHdr(docNo, docType);
//
//
//        showProgress(false);
//        App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
//            @Override
//            public void success(GenericRetResponse genericRetResponse, Response response) {
//                dismissProgress();
//                if (genericRetResponse.getErrCode().equals("S")) {
//                    String xmlDoc = genericRetResponse.getXmlDoc();
//                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
//                        showToast(" Error: No data received.");
//                        return;
//                    }
//                    try {
//                        PO_HDR[] po_hdr = new Gson().fromJson(xmlDoc, PO_HDR[].class);
//                        if (po_hdr==null || po_hdr.length<=0) {
//                            showToast(" Error: No data received.");
//                            return;
//                        }
//                        if (po_hdr.length > 0) {
//                            ordType = new DocType();
//                            ordType.CODE = po_hdr[0]. ORTYP;
//                            ordType.NAME = po_hdr[0]. ORTYP;
//                        }
//                        App.getDatabaseClient().getAppDatabase().genericDao().deletePO_HDR(docNo, ordType.CODE);
//                        App.getDatabaseClient().getAppDatabase().genericDao().insertPO_HDR(po_hdr);
//                        DownloadPO_DET(docNo, ordType.CODE);
//                    } catch (Exception e) {
//                        Log.d(App.TAG, e.toString());
//                    }
//                } else {
//                    String msg = genericRetResponse.getErrMessage();
//                    Log.d(App.TAG, msg);
//                    showToast(msg);
//                }
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                dismissProgress();
//                String msg = error.getMessage();
//                Log.d(App.TAG, msg);
//                showToast(msg);
//            }
//        });
//    }
//
//    private void DownloadPO_DET(final String docNo, final String docType) {
//        JsonObject requestObject = ServiceUtils.GoodsReceipt.getGoodsReceiptDet(docNo, docType);
//
//        showProgress(false);
//        App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
//            @Override
//            public void success(GenericRetResponse genericRetResponse, Response response) {
//                dismissProgress();
//                if (genericRetResponse.getErrCode().equals("S")) {
//                    String xmlDoc = genericRetResponse.getXmlDoc();
//                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
//                        showToast(" Error: No data received.");
//                        return;
//                    }
//                    try {
//                        PO_DET[] poDet = new Gson().fromJson(xmlDoc, PO_DET[].class);
//                        App.getDatabaseClient().getAppDatabase().genericDao().deletePO_DET(docNo, docType);
//                        App.getDatabaseClient().getAppDatabase().genericDao().insertPO_DET(poDet);
//                        OpenDocHeader();
//                    } catch (Exception e) {
//                        Log.d(App.TAG, e.toString());
//                    }
//                } else {
//                    String msg = genericRetResponse.getErrMessage();
//                    Log.d(App.TAG, msg);
//                    showToast(msg);
//                }
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                dismissProgress();
//                String msg = error.getMessage();
//                Log.d(App.TAG, msg);
//                showToast(msg);
//            }
//        });
//    }


}
