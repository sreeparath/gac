package com.dcode.gacinventory.ui.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.data.model.INBOUND_HEADER;
import com.dcode.gacinventory.data.model.PRN_CODE_LIST;
import com.dcode.gacinventory.network.model.GenericRetResponse;
import com.dcode.gacinventory.network.service.ServiceUtils;
import com.dcode.gacinventory.ui.adapter.SearchAdapter;
import com.dcode.gacinventory.ui.adapter.SearchPRNAdapter;
import com.example.gacinventory.R;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SearchPRNActivity
        extends BaseActivity
        implements View.OnClickListener, SearchView.OnQueryTextListener {
    private int Master_ID;
    private int Parent_ID;
    private String[] TitlesArray;
    private String filterString = "";
    private SearchPRNAdapter searchPRNAdapter;
    private int[] fkey_intArray;
    private PRN_CODE_LIST[] prnCodeLists;
    private String screenTitile = "";
    private RecyclerView recyclerView;


    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_search;
    }

    @Override
    protected void onViewReady(Bundle savedInstanceState) {
        try {
            Intent intent = this.getIntent();
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Master_ID = !bundle.containsKey(AppConstants.MASTER_ID) ? -1 : bundle.getInt(AppConstants.MASTER_ID, -1);
                Parent_ID = !bundle.containsKey(AppConstants.PARENT_ID) ? -1 : bundle.getInt(AppConstants.PARENT_ID, -1);
                //TitlesArray = !bundle.containsKey(AppConstants.TITLE) ? null : bundle.getStringArray(AppConstants.TITLE);
                //Log.d("TitlesArray##",bundle.getStringArray(AppConstants.TITLE).toString());
                if (bundle.containsKey(AppConstants.SELECTED_OBJECT)) {
                    prnCodeLists = (PRN_CODE_LIST[]) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
                }

                if (bundle.containsKey(AppConstants.FILTER_KEY_INT_ARRAY)) {
                    fkey_intArray = bundle.getIntArray(AppConstants.FILTER_KEY_INT_ARRAY);
                }
            }

            if (Master_ID != 0 ) {
                setupView();
            } else {
                setResult(RESULT_CANCELED, intent);
                finish();
            }
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
            showAlert("Error", e.getMessage(), false);
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        searchPRNAdapter.getFilter().filter(newText);
        filterString = newText;
        return false;
    }

    @Override
    public void onClick(View view) {
        try {
            Bundle bundle = new Bundle();

            PRN_CODE_LIST objectSearch = (PRN_CODE_LIST) view.getTag();
            //bundle.putLong(AppConstants.SELECTED_ID, objectSearch.PK_ID);
            bundle.putString(AppConstants.SELECTED_CODE, objectSearch.PRIN_CODE);
            //bundle.putLong(AppConstants.PARENT_ID, objectSearch.PARENT_ID);
            bundle.putSerializable(AppConstants.SELECTED_OBJECT, objectSearch);

            Intent intent = this.getIntent();
            intent.putExtras(bundle);
            setResult(RESULT_OK, intent);
            finishAndRemoveTask();
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
            showAlert("Error", e.getMessage(), false);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        setResult(RESULT_CANCELED);
        finishAndRemoveTask();
    }

    private void setupView() {
        try {
            String ScreenTitle = getString(R.string.search);
            String CodeTitle = getString(R.string.prn_code);
            String NameTitle = getString(R.string.code);
            String DescTitle = getString(R.string.name);
            if (TitlesArray != null) {

                ScreenTitle = TitlesArray.length >= 1 && TitlesArray[0] != null ? TitlesArray[0] : getString(R.string.search);
                CodeTitle = TitlesArray.length >= 2 && TitlesArray[1] != null ? TitlesArray[1] : getString(R.string.prn_code);
                NameTitle = TitlesArray.length >= 3 && TitlesArray[2] != null ? TitlesArray[2] : getString(R.string.code);
                DescTitle = TitlesArray.length >= 3 && TitlesArray[3] != null ? TitlesArray[3] : getString(R.string.name);
            }

            Toolbar toolbar = findViewById(R.id.toolbar);
            toolbar.setTitle(ScreenTitle+" "+getString(R.string.prn_code));

            SearchView searchView = findViewById(R.id.search_bar);

            searchView.setOnQueryTextListener(this);

//            TextView tvUserName = findViewById(R.id.tvUserName);
//            if (App.currentUser != null) {
//                tvUserName.setText(App.currentUser.USER_ID);
//            }

            recyclerView = findViewById(R.id.recyclerView);
            ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);

            searchPRNAdapter = new SearchPRNAdapter(SearchPRNActivity.this, new ArrayList<>(), this, CodeTitle, NameTitle,DescTitle);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));

            switch (Master_ID) {
                case AppConstants.SS_PRN_ID:
                    DownloadPrnList();
                    break;
                default:
                    break;
            }

            //recyclerView.setAdapter(objectsAdapter);
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
            showAlert("Error", e.getMessage(), false);
        }
    }

    private void DownloadPrnList() {


        JsonObject requestObject = ServiceUtils.LogisticLabel.getPrnCodeList("","");

        showProgress(false);
        App.getNetworkClient().getAPIService().getRetGeneric(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if ((genericRetResponse.getErrCode().equals("S") || genericRetResponse.getErrCode().equals("")) &&
                        genericRetResponse.getJsonString().length() > 0) {
                    String xmlDoc = genericRetResponse.getJsonString();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showAlert("Error", "No data received.");
                        return;
                    }
                    try {
                        PRN_CODE_LIST[] prnCodeLists = new Gson().fromJson(xmlDoc, PRN_CODE_LIST[].class);
                        if(prnCodeLists.length==0){
                            showAlert("Error", "No data received.");
                            return;
                        }
                        List prList = new ArrayList(Arrays.asList(prnCodeLists));
                        searchPRNAdapter.addItems( prList);
                        recyclerView.setAdapter(searchPRNAdapter);
                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                        showAlert("Error", e.getMessage());
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showAlert("Error", msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showAlert("Error", msg);
            }
        });
    }


}

