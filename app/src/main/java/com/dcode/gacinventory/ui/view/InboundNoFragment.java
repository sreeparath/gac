package com.dcode.gacinventory.ui.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.common.AppVariables;
import com.dcode.gacinventory.common.Utils;
import com.dcode.gacinventory.data.model.GENERATE_LABELS;
import com.dcode.gacinventory.data.model.INBOUND_HEADER;
import com.dcode.gacinventory.data.model.PHOTO_MASTER;
import com.dcode.gacinventory.network.model.GenericRetResponse;
import com.dcode.gacinventory.network.model.OAuthTokenResponse;
import com.dcode.gacinventory.network.service.ServiceUtils;
import com.dcode.gacinventory.ui.adapter.ImageViewAdapter;
import com.example.gacinventory.R;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.squareup.okhttp.OkHttpClient;


import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.MultipartBody;

import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

import static com.dcode.gacinventory.common.AppConstants.VISIT_IMAGE;

public class InboundNoFragment extends BaseFragment implements View.OnLongClickListener{
    private TextInputEditText edPoNo,edContainer,edTruck,edGscan,edAsn,edJobNo,edGacScan;
//    private AutoCompleteTextView tvDocTypes;
//    private ArrayAdapter<DocType> objectsAdapter;
    private int promptResource;
    //private String DocNo;
    private long HDR_ID=-1;
    private FloatingActionButton btnCamera;
    private Bitmap photo;
    private ImageViewAdapter imageViewAdapter;
    View root;
    private RecyclerView recyclerView;
    private Drawable icon;
    private final ColorDrawable background = new ColorDrawable(Color.RED);
    //private MENU menu;
    private INBOUND_HEADER inbound_header;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true);

        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);
            Bundle bundle = this.getArguments();
//            if (bundle != null) {
//                menu = (MENU) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
//            }

        }

        root = inflater.inflate(R.layout.fragment_inbound_no, container, false);
        icon = getResources().getDrawable(R.drawable.ic_delete_black);



        return root;
    }

    private void setupView(){
        //ImageView imgDocNo = root.findViewById(R.id.imgDocNo);
        //imgDocNo.setImageResource(R.drawable.ic_005);

        //edPrinciple = root.findViewById(R.id.edPrinciple);
        edPoNo = root.findViewById(R.id.edPoNo);
        //edDocNo.requestFocus();
        edPoNo.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_ENTER){
                if(event.getAction() == KeyEvent.ACTION_DOWN){
                    return onPOnoKeyEvent();
                }else{
                    return true;
                }
            }else{
                return false;
            }
        });
        //edDocNo.requestFocus();

        edAsn = root.findViewById(R.id.edAsn);
        edAsn.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_ENTER){
                if(event.getAction() == KeyEvent.ACTION_DOWN){
                    return onASNKeyEvent();
                }else{
                    return true;
                }
            }else{
                return false;
            }
        });

        edJobNo = root.findViewById(R.id.edJobNo);
        edJobNo.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_ENTER){
                if(event.getAction() == KeyEvent.ACTION_DOWN){
                    return onJOBKeyEvent();
                }else{
                    return false;
                }
            }else{
                return false;
            }
        });


//        edJobNo.clearFocus();
//        edJobNo.setFocusableInTouchMode(true);
//        edJobNo.requestFocus();
//        edJobNo.requestFocusFromTouch();
//        edJobNo.setCursorVisible(true);

        edContainer = root.findViewById(R.id.edContainer);
        edTruck = root.findViewById(R.id.edTruck);
        edGscan = root.findViewById(R.id.edGscan);

        MaterialButton btnPrint = root.findViewById(R.id.btnPrint);
        btnPrint.setOnClickListener(v -> {
            //validateGacScanLabelExist();
            OpenPrintLabel(null);
        });

        MaterialButton btnNext = root.findViewById(R.id.btnNext);
        btnNext.setOnClickListener(v -> {
            OnNextClick();
            //App.getNetworkClientUploadPhoto();
            //DownloadOauthToken();
//            Location locationA = new Location("point A");
//
//            locationA.setLatitude(25.287350);
//            locationA.setLongitude(55.401084);
//
//            Location locationB = new Location("point B");
//
//            locationB.setLatitude(25.277577815082722);
//            locationB.setLongitude(55.38487478230739);
//
//            float distance = locationA.distanceTo(locationB);
//            Log.d("distance##", String.valueOf(distance));
        });

        btnCamera = root.findViewById(R.id.btnCamera);
        btnCamera.setOnClickListener(v -> {
            DownloadOauthToken();
            onCameraClick();

        });

//        FloatingActionButton btnConfList = root.findViewById(R.id.btnConfList);
//        btnConfList.setOnClickListener(v -> {
//            openConfirmationList();
//
//        });

/*        tvDocTypes = root.findViewById(R.id.acDocType);
        tvDocTypes.setOnItemClickListener((parent, view, position, id) -> {
            if (position > 0) {
                ordType = objectsAdapter.getItem(position);
            } else {
                ordType = null;
            }
        });*/

        recyclerView = root.findViewById(R.id.recycleView);
        imageViewAdapter = new ImageViewAdapter(getContext(), new ArrayList<>(),this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(imageViewAdapter);

        //edJobNo.setText("5520008035");
        //onJOBKeyEvent();
        //edGacScan= root.findViewById(R.id.edGacScan);
        //root.requestFocus();

//        edJobNo.requestFocus();
//        try {
//            Thread.sleep(1000);
//
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }



    }



//    @Override
//    public void onResume() {
//        super.onResume();
////        edJobNo.clearFocus();
////        edJobNo.setFocusableInTouchMode(true);
////        edJobNo.requestFocus();
////        edJobNo.requestFocusFromTouch();
//    }

    private void DownloadOauthToken(){
        App.getNetworkClientOAuth().getAPIService().getOauthToken("client_credentials",new Callback<OAuthTokenResponse>() {
            @Override
            public void success(OAuthTokenResponse response, Response response2) {
                Log.d("success##", response.toString());
                if(response.getAccessToken().length()==0){
                    showAlert("Warning","Authorization Token Not Found!");
                    return;
                }
                AppVariables.setAccessToken(response.getAccessToken());
            }
            @Override
            public void failure(RetrofitError error) {
                Log.d("failure##", error.toString());
            }
        }) ;
    }


private void openConfirmationList(){
       // String jobNo = edJobNo.getText().toString();
    if (inbound_header==null) {
        showToast("Enter Job Id");
        return;
    }

//    if(inbound_header==null){
//        String errMessage = "Inbound header data not found!";
//        showToast(errMessage);
//        return;
//    }

    Bundle bundle = new Bundle();
    bundle.putInt(AppConstants.MODULE_ID, ModuleID);
    bundle.putString(AppConstants.SELECTED_ID, inbound_header.JOB_NO);
    bundle.putSerializable(AppConstants.SELECTED_OBJECT, inbound_header);
    ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_confirmation_list_tablayout, bundle);

}


    private boolean onPOnoKeyEvent() {
        String ScanCode = edPoNo.getText().toString();

        if (ScanCode.isEmpty() || ScanCode.trim().length() == 0) {
            showToast("Invalid PO no");
            edPoNo.requestFocus();
            return false;
        } else {
            ParseBarCode(ScanCode,"PO");
        }
        return true;
    }

    private boolean onASNKeyEvent() {
        String ScanCode = edAsn.getText().toString();

        if (ScanCode.isEmpty() || ScanCode.trim().length() == 0) {
            showToast("Invalid ASN");
            edAsn.requestFocus();
            return false;
        } else {
            ParseBarCode(ScanCode,"ASN");
        }
        return true;
    }

    private boolean onJOBKeyEvent() {
        String ScanCode = edJobNo.getText().toString();

        if (ScanCode.isEmpty() || ScanCode.trim().length() == 0) {
            showToast("Invalid Job No");
            edJobNo.requestFocus();
            return false;
        } else {
            ParseBarCode(ScanCode,"JOB");
        }
        return true;
    }

    private void ParseBarCode(String scanCode,String mode) {
        if (scanCode.length() <= 0 ) {
            return;
        }
        DownloadInboundHeader(scanCode,mode);
        fetchPhotos();
    }

    private void fetchPhotos(){
        List<PHOTO_MASTER> photo_masters = App.getDatabaseClient().getAppDatabase().genericDao().getPhotoMasterList(ModuleID,edJobNo.getText().toString());
        imageViewAdapter.addItems(photo_masters);
        imageViewAdapter.notifyDataSetChanged();
    }

   @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

       setupView();



   }

/*    private boolean onDocNoKeyEvent() {
        DocNo = edDocNo.getText().toString();

        if (DocNo.isEmpty() || DocNo.trim().length() == 0) {
            showToast(String.format("%s required", getString(promptResource)));
            return false;
        }
        return true;
    }*/

    private void OnNextClick() {
        Validate();
        //UploadInbound();
        //uploadMultiFile();
    }

    private void Validate() {
        String errMessage;
        String jobNo = edJobNo.getText().toString();
        if (jobNo.length() == 0) {
            errMessage = String.format("%s required", "Job no");
            showToast(errMessage);
            edJobNo.setError(errMessage);
            return;
        }
        if(inbound_header==null){
            errMessage = "Inbound header data not found!";
            showToast(errMessage);
            return;
        }

        if(ModuleID==AppConstants.INBOUND_FULL){
            OpenLogisticLabel();
        }else if(ModuleID==AppConstants.INBOUND_PARTIAL){
            OpenLogisticLabelPartial();
        }


    }


    private void onCameraClick(){
        if (edJobNo.getText().toString().length()==0) {
            showToast("Enter Job Id");
            return;
        }

        if(inbound_header==null){
            String errMessage = "Inbound header data not found!";
            showToast(errMessage);
            return;
        }

        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, VISIT_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (resultCode != Activity.RESULT_OK) return;

        if (requestCode == VISIT_IMAGE) {
            photo = (Bitmap) intent.getExtras().get("data");
            SavePhoto();
        }


        if (requestCode == AppConstants.JOB_ID) {
            inbound_header = intent.hasExtra(AppConstants.SELECTED_OBJECT) ? (INBOUND_HEADER) intent.getSerializableExtra(AppConstants.SELECTED_OBJECT) : null;
            if (inbound_header != null) {
                fillHeader(inbound_header);
            }
        }
    }

    private void SavePhoto() {
        String photoFileName = getPhotoFileName();
        Log.d("photoFileName##",photoFileName);
        if (photo != null) {
//            pngBase64 = Utils.getBase64EncodedPNG(photo);
            try (FileOutputStream out = new FileOutputStream(photoFileName)) {
                photo.compress(Bitmap.CompressFormat.PNG, 100, out);
                fetchPhotos();
                // PNG is a lossless format, the compression factor (100) is ignored
            } catch (IOException e) {
                e.printStackTrace();
            }

            PHOTO_MASTER photo_master = new PHOTO_MASTER();
            photo_master.MODULE_ID = ModuleID;
            long seqNo = Utils.GetMaxValue("PHOTO_MASTER", "SLNO", false,null , null);
            photo_master.SLNO = seqNo;
            photo_master.JOB_ID = edJobNo.getText().toString();
            photo_master.IS_UPLOAD = 0;
            photo_master.PHOTO_PATH = photoFileName;
            App.getDatabaseClient().getAppDatabase().genericDao().insertPhotoMaster(photo_master);
            fetchPhotos();
        }
    }

    public String getPhotoFileName() {
        SimpleDateFormat dF = new SimpleDateFormat(Utils.DATETIME_NUM_FORMAT, Locale.getDefault());
        String fileName = dF.format(new Date()) + ".png";
        File outputFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath() + "/Camera/");
        try {
            if (!outputFile.exists() && !outputFile.isDirectory()) {
                outputFile.mkdir();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return outputFile.getAbsolutePath() + "/" + fileName;
    }

    private void OpenLogisticLabel() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        //bundle.putString(AppConstants.SELECTED_ID, edJobNo.getText().toString());
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, inbound_header);
        bundle.putLong(AppConstants.PARENT_ID, HDR_ID);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_logistic_label, bundle);
    }

    private void OpenLogisticLabelPartial() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putString(AppConstants.SELECTED_ID, edJobNo.getText().toString());
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, inbound_header);
        bundle.putLong(AppConstants.PARENT_ID, HDR_ID);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_partial_logistic_label, bundle);
    }

    private void OpenPrintLabel(List<GENERATE_LABELS> generate_labels) {
        if(inbound_header==null){
            String errMessage = "Inbound header data not found!";
            showToast(errMessage);
            return;
        }

        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putString(AppConstants.SELECTED_ID, edJobNo.getText().toString());
        bundle.putInt(AppConstants.SELECTED_CODE, Integer.parseInt(edGscan.getText().toString()));
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, (Serializable) inbound_header);
        bundle.putInt(AppConstants.PARENT_ID, -1);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_label_print, bundle);
        dismissProgress();
    }


    private void validateGacScanLabelExist(){
        showProgress(false);
        String jobCode= edJobNo.getText().toString();
        List<GENERATE_LABELS> generate_labels = App.getDatabaseClient().getAppDatabase().genericDao().getGENERATE_LABELS(jobCode);
        if(generate_labels!=null && generate_labels.size() >0 ){
            Log.d("here in null##","generate_labels");
            OpenPrintLabel(generate_labels);
        }else{
            Log.d("here in else##","generate_labels");
            doLabelGenerate();
        }
    }

    private void doLabelGenerate(){
        String noOfGac = edGscan.getText().toString();
        String jobCode= edJobNo.getText().toString();

        GENERATE_LABELS generate_labels;
        if(noOfGac.length()==0){
            showAlert("Warning","No of GAC should be greater than zero!");
            return;
        }
        int count = Integer.parseInt(noOfGac);
        GENERATE_LABELS[] generate_labels_list = new GENERATE_LABELS[count];
        String fullJobCode = Utils.PadLeft(jobCode, 5, '0') ;
        fullJobCode = fullJobCode.substring(fullJobCode.length() - 5);
        String keyType = "I";// AS per GAC processflow
        String userStamp = "123"; // THREE DIGIT
        String generatedKey = keyType+fullJobCode+userStamp;
        long keyFromTypeStamp = System.currentTimeMillis();
        for(int i=0;i<count;i++){
            generate_labels = new GENERATE_LABELS();
            generate_labels.JOB_NO = jobCode;
            generate_labels.USER_ID = "userStamp";
            generate_labels.LABEL = generatedKey+Utils.PadLeft(String.valueOf(i), 5, '0') ;;
            generate_labels_list[i] = generate_labels;
        }
        App.getDatabaseClient().getAppDatabase().genericDao().insertGENERATE_LABELS(generate_labels_list);
        OpenPrintLabel(Arrays.asList(generate_labels_list));
    }




    @Override
    public boolean onLongClick(View view) {

        PHOTO_MASTER photo_master = (PHOTO_MASTER) view.getTag();

//        RecyclerView.ViewHolder holder = (RecyclerView.ViewHolder) view.getTag();
//        if (view.getId() == holder.itemView.getId()) {
//
//        }

        Log.d("tag## ", String.valueOf(photo_master.PHOTO_PATH));

       // String photoSelected = (String) view.getTag();

//
//
//       Log.d("selected##",photoSelected);
////
        if (photo_master == null) {
            return false;
        }
//
        Snackbar snackbar = Snackbar
                .make(root, "Do you want to delete photo ?", Snackbar.LENGTH_LONG)
                .setAction("YES", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        File file = new File(photo_master.PHOTO_PATH);
                        file.delete();
                        if(file.exists()){
                            try {
                                file.getCanonicalFile().delete();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            if(file.exists()){
                                getContext().deleteFile(file.getName());
                            }
                        }
                        Snackbar mSnackbar = Snackbar.make(root, "Successfully deleted!", Snackbar.LENGTH_SHORT);
                        mSnackbar.show();
                        fetchPhotos();
                    }

                });

        snackbar.show();

        return true;
    }

    private void UploadInbound(){
        List<PHOTO_MASTER> photo_masters = App.getDatabaseClient().getAppDatabase().genericDao().getPhotoMasterList(ModuleID,edJobNo.getText().toString());
        if (photo_masters == null ) {
            return;
        }

        for(PHOTO_MASTER photo_master : photo_masters){
            UploadInboundImage(photo_master);
        }
    }

    private void DownloadInboundHeader(String docNo,String mode) {
        if (docNo.length() <= 0) {
            return;
        }

        JsonObject requestObject = ServiceUtils.LogisticLabel.getInboundHeader(docNo,mode);

        showProgress(false);
        App.getNetworkClient().getAPIService().getRetGeneric(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("S")) {
                    String xmlDoc = genericRetResponse.getJsonString();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showAlert("Error", "No data received.");
                        return;
                    }
                    try {
                        INBOUND_HEADER[] items_prints = new Gson().fromJson(xmlDoc, INBOUND_HEADER[].class);
                        if(items_prints.length==0){
                            showAlert("Error", "No data received.");
                            return;
                        }
                        if(items_prints.length>1){
                            try {
                                OnJobNoLookupClick(AppConstants.JOB_KEY, AppConstants.JOB_ID, -1,items_prints);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }else {
                            inbound_header = items_prints[0];
                            fillHeader(inbound_header);
                        }

                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                        showAlert("Error", e.getMessage());
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showAlert("Error", msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showAlert("Error", msg);
            }
        });
    }

    private void fillHeader(INBOUND_HEADER inbound_header){
        edJobNo.setText(inbound_header.JOB_NO);
        edContainer.setText(String.valueOf(inbound_header.SEAL_NO));
        edTruck.setText(String.valueOf(inbound_header.TRUCK_NO));
        if(inbound_header.GACSCANID_COUNT==0 ){
            inbound_header.GACSCANID_COUNT = 5;
        }
        edGscan.setText(String.valueOf(inbound_header.GACSCANID_COUNT));
    }

    private void resetUI() {
        edJobNo.setText("");
        edAsn.setText("");
        edPoNo.setText("");
        edContainer.setText("");
        edTruck.setText("");
        edGscan.setText("");
        inbound_header = new INBOUND_HEADER();
    }

    private void UploadInboundImage(PHOTO_MASTER photo_master) {
        //showToast(getString(R.string.send_data_to_server));
        JsonObject requestObject = ServiceUtils.SyncActivity.GetImagesToUpload(photo_master);
        showProgress(false);
        App.getNetworkClient().getAPIService().submitWithLines(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("S")) {
                    if (genericRetResponse.getErrCode().equals("S")) {
                        //String ReceiptNo = String.valueOf(genericRetResponse.getRetId());
                        String[] msg = genericRetResponse.getErrMessage().split("~");
//                        if (msg.length > 1) {
//                            edReturnNo.setText(msg[1]);
//                        }
//                        edReceiptNo.setText(ReceiptNo);
//                        btnPrintLabels.setEnabled(true);
                        showToast(msg[0]);
                    } else {
                        showAlert("Alert", genericRetResponse.getErrMessage());
                    }
                } else {
                    Log.d(App.TAG, genericRetResponse.getErrMessage());
                    showAlert("Error", genericRetResponse.getErrMessage());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(App.TAG, error.getMessage());
                dismissProgress();
                showAlert("Error", error.getMessage());
            }
        });
    }


    private class SyncIncoData extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... urls) {
//            for (int i = 0; i <= 100; i++) {
//
//            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(String result) {

        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.actionJobStatus).setVisible(true);
        menu.findItem(R.id.actionImages).setVisible(true);
        super.onPrepareOptionsMenu(menu);
    }


//    @Override
//    public void onCreateOptionsMenu(@NonNull @NotNull Menu menu, @NonNull @NotNull MenuInflater inflater) {
//        menu.clear();
//
//        // Add the new menu items
//        inflater.inflate(R.menu.main, menu);
//
//        super.onCreateOptionsMenu(menu, inflater);
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.actionJobStatus:
                openConfirmationList();
                return true;
            case R.id.actionImages:
                openImages();
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openImages(){
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        //bundle.putString(AppConstants.SELECTED_ID, inbound_header.JOB_NO);
        //bundle.putSerializable(AppConstants.SELECTED_OBJECT, inbound_header);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_view_image, bundle);

    }
}
