package com.dcode.gacinventory.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.gacinventory.data.model.SP_SCN_AN_OUT_LOC_BY_JOB;
import com.example.gacinventory.R;

import java.text.MessageFormat;
import java.util.List;

/**
 * @Created by: VIJIL DHAS A.S
 * @Date: 08-09-2021
 */
public class SiteAndLoactionListAdapter extends RecyclerView.Adapter<SiteAndLoactionListAdapter.RecycleViewHolder> {
    private List<SP_SCN_AN_OUT_LOC_BY_JOB> anOutLocByJobs;
    private Context context;
    private View.OnClickListener itemClickListener;
    private OnNavigateFragment navigateFragment;

    public SiteAndLoactionListAdapter(Context context,
                                      List<SP_SCN_AN_OUT_LOC_BY_JOB> anOutLocByJobs,
                                      View.OnClickListener itemClickListener,
                                      OnNavigateFragment navigateFragment) {
        this.anOutLocByJobs = anOutLocByJobs;
        this.context = context;
        this.itemClickListener = itemClickListener;
        this.navigateFragment = navigateFragment;
    }

    @NonNull
    @Override
    public RecycleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SiteAndLoactionListAdapter.RecycleViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.site_and_location_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecycleViewHolder holder, @SuppressLint("RecyclerView") int position) {
        ((RecycleViewHolder) holder).txtSite.setText(anOutLocByJobs.get(position).SITE_CODE);
        ((RecycleViewHolder) holder).txtLoc.setText(anOutLocByJobs.get(position).LOC_CODE);


        ((RecycleViewHolder) holder).labelContainer.setTag(anOutLocByJobs.get(position));
        ((RecycleViewHolder) holder).labelContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (navigateFragment != null) {
                    navigateFragment.onNavigateToFragment(anOutLocByJobs.get(position));
                }
            }
        });

    }

    public void addItems(List<SP_SCN_AN_OUT_LOC_BY_JOB> detList) {
        anOutLocByJobs = detList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return anOutLocByJobs.size();
    }

    static class RecycleViewHolder extends RecyclerView.ViewHolder {
        private TextView txtSite;
        private TextView txtLoc;
        private LinearLayout labelContainer;

        public RecycleViewHolder(@NonNull View itemView) {
            super(itemView);
            txtSite = itemView.findViewById(R.id.txt_site);
            txtLoc = itemView.findViewById(R.id.txt_loc);
            labelContainer = itemView.findViewById(R.id.top_label_container);
        }
    }

    public interface OnNavigateFragment {
        void onNavigateToFragment(SP_SCN_AN_OUT_LOC_BY_JOB spScnAnOutLocByJob);
    }
}
