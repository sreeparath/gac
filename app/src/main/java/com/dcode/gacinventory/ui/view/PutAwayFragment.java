package com.dcode.gacinventory.ui.view;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.common.AppPreferences;
import com.dcode.gacinventory.common.AppVariables;
import com.dcode.gacinventory.common.StagingValidation;
import com.dcode.gacinventory.common.Utils;
import com.dcode.gacinventory.common.ValidRespSet;
import com.dcode.gacinventory.data.model.INBOUND_HEADER;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SCAN;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SUMBIT;
import com.dcode.gacinventory.data.model.LOT_STATUS_LIST;
import com.dcode.gacinventory.data.model.PUTAWAY_SUMBIT;
import com.dcode.gacinventory.data.model.WAREHOUSE;
import com.dcode.gacinventory.network.model.GenericRetResponse;
import com.dcode.gacinventory.network.service.ServiceUtils;
import com.example.gacinventory.R;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.Date;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.dcode.gacinventory.common.AppConstants.INVEST_HEADER;
import static com.dcode.gacinventory.common.AppConstants.INVEST_PUT;
import static com.dcode.gacinventory.common.AppConstants.INVEST_STAG;
import static com.dcode.gacinventory.common.Utils.ISSUE_DATE_FORMAT3;
import static com.dcode.gacinventory.common.Utils.PRINT_DATE_FORMAT;

public class PutAwayFragment extends BaseFragment {
    private TextView edInfoNew ;
//    private AutoCompleteTextView tvDocTypes;
//    private ArrayAdapter<DocType> objectsAdapter;
    private int promptResource;
    private String DocNo;
    private long HDR_ID;
    private RadioGroup rdoGroup;
    private LOGISTIC_LABEL_SUMBIT logistic_label_sumbit;
    private TextInputEditText edDocNo;
    private TextInputEditText edLUnit;
    private TextInputEditText edMunit;
    private TextInputEditText edPUnit;
    //private TextInputEditText edPl;
    //private TextInputEditText edBarcode;
    private TextInputEditText edFillCode;
    private TextInputEditText edFormula;
    private TextInputEditText edDisplayQty;
    private TextInputEditText edLotNo;
    private TextInputEditText edExp;
    private TextInputEditText edManf;
    private TextInputEditText edJobCode;
    private TextInputEditText edSite;
    private TextInputEditText edScanLoc;
    private TextInputEditText edLocCode;
    private TextInputEditText edProdCode;
    private View root;
    private PUTAWAY_SUMBIT putaway_sumbit;
    private INBOUND_HEADER inboundHeader;
    private TextInputLayout punit,munit,lunit;
    private AutoCompleteTextView edLotStatus;

    StagingValidation stagingValidation;
    private boolean mResult = false;
    private boolean fromHeader = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

//
//
//        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
//            @Override
//            public void handleOnBackPressed() {
//                // Handle the back button even
//                Log.d("BACKBUTTON", "Back button clicks");
//            }
//        };
//
//        ((MainActivity) requireActivity()).getOnBackPressedDispatcher().addCallback(callback);

        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                logistic_label_sumbit = (LOGISTIC_LABEL_SUMBIT) bundle.getSerializable(AppConstants.SELECTED_OBJECT_NEW);
                if(bundle.containsKey(AppConstants.SELECTED_OBJECT_NEW2)){
                    inboundHeader = (INBOUND_HEADER) bundle.getSerializable(AppConstants.SELECTED_OBJECT_NEW2);
                }
                fromHeader = bundle.containsKey(AppConstants.SELECTED_ID) ? bundle.getBoolean(AppConstants.SELECTED_ID) : false;
                //Log.d("logistic_label_sumbit##",logistic_label_sumbit.SITE);
            }
        }

        root = inflater.inflate(R.layout.fragment_put_away_enable, container, false);

        setupView();
        return root;
    }


    private void setupView(){
        setHeaderToolbar();

        stagingValidation = new StagingValidation();

        edDocNo = root.findViewById(R.id.edScanCode);
        edLUnit = root.findViewById(R.id.edLUnit);
        edMunit= root.findViewById(R.id.edMunit);
        edPUnit= root.findViewById(R.id.edPUnit);
        edJobCode= root.findViewById(R.id.edJobCode);
        edExp= root.findViewById(R.id.edExp);
        edManf= root.findViewById(R.id.edManf);
        edDisplayQty = root.findViewById(R.id.edDisplayQty);
        edLocCode= root.findViewById(R.id.edLocCode);
        edSite = root.findViewById(R.id.edSite);
        edScanLoc = root.findViewById(R.id.edScanLoc);
        edScanLoc.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_ENTER){
                if(event.getAction() == KeyEvent.ACTION_DOWN){
                    return onDefaultPutawayUpdate();
                }else{
                    return false;
                }
            }else{
                return false;
            }
        });

        //edScanLoc.requestFocus();
        //edManf.setOnClickListener(v -> onDateEntryClick(1));
        //edExp.setOnClickListener(v -> onDateEntryClick(2));

        edFillCode= root.findViewById(R.id.edFillCode);
        edFormula= root.findViewById(R.id.edFormula);
        edLotStatus= root.findViewById(R.id.edLotStatus);
        edLotNo= root.findViewById(R.id.edLotNo);
        edProdCode= root.findViewById(R.id.edProdCode);


        MaterialButton btnBack = root.findViewById(R.id.btnBack);
        btnBack.setOnClickListener(v -> OnBackClick());

        MaterialButton btnNext = root.findViewById(R.id.btnNext);
        btnNext.setOnClickListener(v -> OnNextClick());

        punit = root.findViewById(R.id.punit);
        munit = root.findViewById(R.id.munit);
        lunit = root.findViewById(R.id.lunit);

        showToast(String.valueOf(fromHeader));
    }

    private void OnBackClick() {
        getActivity().onBackPressed();
    }

    private void setHeaderToolbar() {
        if (logistic_label_sumbit != null) {
            Toolbar toolbar = root.findViewById(R.id.toolbar);
            TextView info1 = root.findViewById(R.id.info1);
            info1.setText(App.currentUser.USER_ID);
            TextView info2 = root.findViewById(R.id.info2);
            info2.setText(logistic_label_sumbit.JOB_NO + " - "+ logistic_label_sumbit.PRIN_NAME);
        }
    }


   @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       fillPutAwayData();
    }

    private void fillPutAwayData(){
        if(logistic_label_sumbit==null){
            return;
        }

        try{
            punit.setHint(punit.getHint()+":"+logistic_label_sumbit.PUOM);
            munit.setHint(munit.getHint()+":"+logistic_label_sumbit.MUOM);
            lunit.setHint(lunit.getHint()+":"+logistic_label_sumbit.LUOM);
            edDocNo.setText(logistic_label_sumbit.SRNO);
            if(logistic_label_sumbit.INPUT_LQTY==null){
                edLUnit.setText(String.valueOf(logistic_label_sumbit.Lqty));
            }else{
                edLUnit.setText(String.valueOf(logistic_label_sumbit.INPUT_LQTY));
            }
            if(logistic_label_sumbit.INPUT_MQTY==null){
                edMunit.setText(String.valueOf(logistic_label_sumbit.Mqty));
            }else{
                edMunit.setText(String.valueOf(logistic_label_sumbit.INPUT_MQTY));
            }
            if(logistic_label_sumbit.INPUT_PQTY==null){
                edPUnit.setText(String.valueOf(logistic_label_sumbit.Pqty));
            }else{
                edPUnit.setText(String.valueOf(logistic_label_sumbit.INPUT_PQTY));
            }

            edJobCode.setText(logistic_label_sumbit.DESCRIPTION);
            //if(logistic_label_sumbit.MFG_DATE!=null && logistic_label_sumbit.MFG_DATE!){
            edManf.setText(logistic_label_sumbit.MFG_DATE);

            if(logistic_label_sumbit.EXP_DATE!=null && logistic_label_sumbit.EXP_DATE.trim().length()>0){
                edExp.setText(logistic_label_sumbit.EXP_DATE);
            }else{
                edExp.setText(logistic_label_sumbit.EXPDATE);
            }
            edLocCode.setText(logistic_label_sumbit.LOC_CODE);
            edSite.setText(logistic_label_sumbit.SITE);
            //edDisplayQty.setText(logistic_label_sumbit.STG_QTY+"/"+logistic_label_sumbit.TOTAL_QTY);
            edDisplayQty.setText(logistic_label_sumbit.PUT_QTY+"/"+String.valueOf(logistic_label_sumbit.TOTAL_PUT_QTY));

            Log.d("logistic_label_sumbit.SKU##",logistic_label_sumbit.SKU);
            edProdCode.setText(logistic_label_sumbit.SKU);

            edFillCode.setText(logistic_label_sumbit.FIL_CODE);
            edFormula.setText(logistic_label_sumbit.FORMULA_NO);
            edLotNo.setText(logistic_label_sumbit.LOT_NO);
            if(logistic_label_sumbit.LOT_STATUS!=null && logistic_label_sumbit.LOT_STATUS.length()>0){
                LOT_STATUS_LIST lotStatusList  = App.getDatabaseClient().getAppDatabase().genericDao().getLotStatusFromKey(logistic_label_sumbit.LOT_STATUS);
                if(lotStatusList!=null){
                    edLotStatus.setText(lotStatusList.DESCRIPTION);
                }
            }

    }catch(Exception e){
        showAlert("Error",e.getMessage());
        e.printStackTrace();
    }

    }

/*    private boolean onDocNoKeyEvent() {
        DocNo = edDocNo.getText().toString();

        if (DocNo.isEmpty() || DocNo.trim().length() == 0) {
            showToast(String.format("%s required", getString(promptResource)));
            return false;
        }
        return true;
    }*/

    private boolean onDefaultPutawayUpdate() {
        String scanCode = edScanLoc.getText().toString();
        if (scanCode.isEmpty() || scanCode.trim().length() == 0) {
            showToast("Invalid scanLoc");
            edScanLoc.requestFocus();
            return false;
        } else {
            ParseBarCode(scanCode);
        }
        return true;
    }

    private void ParseBarCode(String barcode){
        String defaultPutaway = AppPreferences.getValue(getContext(), AppPreferences.DEFAULT_PUTAWAY, false);
        if (defaultPutaway.length() > 0 && defaultPutaway.equalsIgnoreCase("true")){
            OnNextClick();
        }

    }

    private void OnNextClick() {
        try {
            if (Validate() && DynamicValidationSave()) {
                putaway_sumbit = new PUTAWAY_SUMBIT();
                putaway_sumbit.SRNO = edDocNo.getText().toString();
                putaway_sumbit.JOB_NO = edJobCode.getText().toString();
                putaway_sumbit.MFG_DATE = edManf.getText().toString();
                putaway_sumbit.EXP_DATE = edExp.getText().toString();
                putaway_sumbit.LOT_NO = edLotNo.getText().toString();
                putaway_sumbit.LOT_STATUS = logistic_label_sumbit.LOT_STATUS;
                putaway_sumbit.FORMULA_NO = edFormula.getText().toString();
                putaway_sumbit.FIL_CODE = edFillCode.getText().toString();
                putaway_sumbit.BARCODE = edProdCode.getText().toString();
                putaway_sumbit.INPUT_PQTY = (edPUnit.getText().toString());
                putaway_sumbit.INPUT_MQTY = (edMunit.getText().toString());
                putaway_sumbit.INPUT_LQTY = (edLUnit.getText().toString());
                putaway_sumbit.LOC_CODE = edLocCode.getText().toString();
                putaway_sumbit.SITE = edSite.getText().toString();
                putaway_sumbit.USER_NAME = App.currentUser.USER_ID;
                putaway_sumbit.UPDATE_FROM = "S";
                putaway_sumbit.PALLET_ID = "";
                putaway_sumbit.LOC = edLocCode.getText().toString();
                putaway_sumbit.SCAN_LOCATION = edScanLoc.getText().toString();
                putaway_sumbit.SCAN_PUT_LOC_CODE = "";

                boolean varianceFlag = false;
                int ActualStgQty = (Integer.parseInt(logistic_label_sumbit.Pqty) * logistic_label_sumbit.UPPP) + (Integer.parseInt(logistic_label_sumbit.Mqty) * logistic_label_sumbit.LUPPP) + Integer.parseInt(logistic_label_sumbit.Lqty);
                if (ActualStgQty > 0 && (ActualStgQty < logistic_label_sumbit.TOTAL_PO_QTY)) {
                    //varianceFlag = true;
                }
                uploadPutAwayData(varianceFlag);
                //showToast("hereeee in true");
            }
        }catch(Exception e){
            showAlert("Error",e.getMessage());
            e.printStackTrace();
        }
    }

    private boolean DynamicValidationSave(){


        AlertDialog alertDialog;
        ValidRespSet validObject;

        //validatePutAwayConfirmation
        validObject = stagingValidation.validatePutAwayConfirmation(App.dynFuncs, logistic_label_sumbit.PRIN_CODE,logistic_label_sumbit.LOC_STAT,logistic_label_sumbit.LOC,logistic_label_sumbit.SITE_TYPE,edScanLoc.getText().toString());
        Log.d("validObject##",validObject.getErrCode());
        if(validObject.getErrCode().length()>0 && validObject.getErrCode().equals("E")){
            edScanLoc.setError(validObject.getErrMSG());
            edScanLoc.requestFocus();
            return false;
        }else if(validObject.getErrCode().length()>0 && validObject.getErrCode().equals("M")){
            return getLocationMixedBySite();
        }else if(validObject.getErrCode().length()>0 && validObject.getErrCode().equals("C")){

        }


        return true;
    }

    public boolean getLocationMixedBySite() {
        mResult = false;
        final Handler handler = new Handler(Looper.getMainLooper()) {

            @Override
            public void handleMessage(Message mesg) {
                throw new RuntimeException();
            }
        };

        try {
            showProgress(false);
            JsonObject requestObject = ServiceUtils.LogisticLabel.getPutAwayMixedSite(logistic_label_sumbit.SITE,edScanLoc.getText().toString());
            Log.d("requestObject##", requestObject.toString());
            App.getNetworkClient().getAPIService().getRetGeneric(requestObject, new Callback<GenericRetResponse>() {
                @Override
                public void success(GenericRetResponse genericRetResponse, Response response) {
                    showAlert("Warning", "Invalid Location, you must enter a valid mixed location within the site"+logistic_label_sumbit.SITE);
                    mResult= true;
                    handler.sendMessage(handler.obtainMessage());
//                    if (genericRetResponse.getErrCode().equals("S")  ){
//                        //String xmlDoc = genericRetResponse.getJsonString();
//                        //if (xmlDoc == null || xmlDoc.trim().length() == 0) {
//                        showAlert("Warning", "Invalid Location, you must enter a valid mixed location within the site"+logistic_label_sumbit.SITE);
//                        mResult= false;
//                        handler.sendMessage(handler.obtainMessage());
//                        //}
//                    } else {
//                        String msg = genericRetResponse.getErrMessage();
//                        Log.d(App.TAG, msg);
//                        showAlert("Error",msg);
//                        mResult= false;
//                        handler.sendMessage(handler.obtainMessage());
//                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    String msg = error.getMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                }
            });
        } catch (Exception e) {
            String msg = e.getMessage();
            Log.d(App.TAG, msg);
            showToast(msg);
        } finally {
            dismissProgress();
        }

        try {
            Looper.loop();
        } catch (RuntimeException e) {
        }

        return mResult;
    }


    private void backToHome() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        ((MainActivity) requireActivity()).NavigateFullBackStack();
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_home, bundle);
    }

    private void backToDirectPutaway() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        ((MainActivity) requireActivity()).NavigateFullBackStack();
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_put_away_header, bundle);
    }

    private void backToLogistic() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
//        bundle.putString(AppConstants.SELECTED_ID, inboundHeader.JOB_NO);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, inboundHeader);
//        bundle.putLong(AppConstants.PARENT_ID, HDR_ID);
        ((MainActivity) requireActivity()).NavigateFullBackStack();
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_logistic_label, bundle);
    }

    private boolean Validate() {
        String errMessage;
        String scanLoc = edScanLoc.getText().toString();
        if (scanLoc.length() == 0) {
            errMessage = String.format("%s required", "Scan Location");
            showToast(errMessage);
            edScanLoc.setError(errMessage);
            return false;
        }
        return true;

    }

    private void OpenLogisticLabel() {
        if(logistic_label_sumbit==null){
            showAlert("Warning","Data not found!");
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putString(AppConstants.SELECTED_ID, DocNo);
        //bundle.putSerializable(AppConstants.SELECTED_OBJECT,  items_prints);
        bundle.putLong(AppConstants.PARENT_ID, HDR_ID);
        ((MainActivity) requireActivity()).NavigateBackStack(null, false);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_inbound, bundle);
    }

/*    private void getDocTypes() {
        List<DocType> orTypList = App.getDatabaseClient().getAppDatabase().genericDao().getAllOrTyps();
        objectsAdapter = new ArrayAdapter<>(requireContext(),
                R.layout.dropdown_menu_popup_item, orTypList);
        tvDocTypes.setAdapter(objectsAdapter);
        tvDocTypes.setSelection(0);
        tvDocTypes.setThreshold(100);

        objectsAdapter.notifyDataSetChanged();
    }*/

    private void OpenGACScan() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putString(AppConstants.SELECTED_ID, DocNo);
        bundle.putSerializable(AppConstants.SELECTED_CODE, "");
        bundle.putLong(AppConstants.PARENT_ID, HDR_ID);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_gac_scan, bundle);
    }


    private void uploadPutAwayData(boolean varianceFlag) {
        if(putaway_sumbit==null){
            return;
        }
        showToast(getString(R.string.send_data_to_server));
        JsonObject requestObject = ServiceUtils.LogisticLabel.getPUTAWAY_SUMBIT(putaway_sumbit);
        showProgress(false);
        App.getNetworkClient().getAPIService().submitWithLinesData(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericSubmissionResponse, Response response) {
                dismissProgress();
                if (genericSubmissionResponse.getErrCode().equals("S")) {
                    if (genericSubmissionResponse.getErrCode().equals("S")) {
                        String msg = genericSubmissionResponse.getErrMessage();
                        resetUI();
                        showToast(msg);
                        if(varianceFlag){
                            if(inboundHeader==null){
                                inboundHeader  = new INBOUND_HEADER();
                                inboundHeader.JOB_NO = logistic_label_sumbit.JOB_NO;
                                inboundHeader.PRIN_NAME = logistic_label_sumbit.PRIN_NAME;
                            }
                            if (fromHeader) {
                                OpenInvestigation(INVEST_PUT);
                            }else{
                                OpenInvestigation(INVEST_STAG);
                            }
                        }else {
                            if (fromHeader) {
                                backToDirectPutaway();
                            } else {
                                backToLogistic();
                            }
                        }

//                        if (msg.length > 1) {
//                            ReceiptNo = msg[2];
//                        }
//                        try {
//                            OnConfirmationClick(AppConstants.DAMAGE_KEY, AppConstants.DAMAGE_ID, -1,getString(R.string.damage_conf),R.string.no,R.string.yes,logistic_label_sumbit);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
                    } else {
                        showAlert("Error",genericSubmissionResponse.getErrMessage());
                        //showToast(genericSubmissionResponse.getErrMessage());
                    }
                } else {
                    Log.d(App.TAG, genericSubmissionResponse.getErrMessage());
                    showAlert("Error",genericSubmissionResponse.getErrMessage());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(App.TAG, error.getMessage());
                showToast(error.getMessage());
                dismissProgress();
            }
        });
    }

    private void OpenInvestigation(long fromModule) {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, inboundHeader);
        bundle.putLong(AppConstants.SELECTED_ID, fromModule);
        ((MainActivity) requireActivity()).NavigateFullBackStack();
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_fragment_investigation, bundle);
    }

    private void resetUI(){
        edDocNo.setText("");
        edLUnit.setText("");
        edMunit.setText("");
        edPUnit.setText("");
        //edSite.setText("");
        //edFillCode.setText("");
        //edFormula.setText("");
        //edLotStatus.setText("");
        //edLotNo.setText("");
        edLocCode.setText("");
        edExp.setText("");
        edManf.setText("");
        edJobCode.setText("");
        putaway_sumbit = null;
        logistic_label_sumbit = null;
    }

}
