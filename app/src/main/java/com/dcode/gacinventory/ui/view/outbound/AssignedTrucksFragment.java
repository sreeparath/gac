package com.dcode.gacinventory.ui.view.outbound;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.data.model.SP_SCN_AN_OUT_ASSIGNED_TRUCKS;
import com.dcode.gacinventory.data.model.SP_SCN_AN_OUT_LOC_BY_JOB;
import com.dcode.gacinventory.data.model.SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS;
import com.dcode.gacinventory.network.model.GenericRetResponse;
import com.dcode.gacinventory.network.service.ServiceUtils;
import com.dcode.gacinventory.ui.adapter.AssignedTruckListAdapter;
import com.dcode.gacinventory.ui.adapter.PickAndPackListDetailedAdapter;
import com.dcode.gacinventory.ui.view.BaseFragment;
import com.dcode.gacinventory.ui.view.MainActivity;
import com.example.gacinventory.R;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * @Created by: VIJIL DHAS A.S
 * @Date: 12-09-2021
 */
public class AssignedTrucksFragment extends BaseFragment implements View.OnClickListener {
    private String putawayType;
    private View root;
    private RecyclerView mItemTruckList;
    private RecyclerView mItemDetailedList;

    private AssignedTruckListAdapter assignedTruckListAdapter;
    private PickAndPackListDetailedAdapter packListDetailedAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);
            putawayType = this.getArguments().getString("PutAwayType", "");
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                if (bundle.containsKey(AppConstants.SELECTED_ID)) {
                    putawayType = bundle.getString(AppConstants.SELECTED_ID);
                }
            }
        }
        root = inflater.inflate(R.layout.fragment_assigned_truck, container, false);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupView();
        setHeaderToolbar();
    }

    private void setHeaderToolbar() {
        TextView info1 = root.findViewById(R.id.info1);
        info1.setText(App.currentUser.USER_ID);
    }

    private void setupView() {
        mItemTruckList = root.findViewById(R.id.list_assigned_trucks);
        mItemDetailedList = root.findViewById(R.id.rcv_item_short_list);


        initRecyclerView();
        apiFetchTrucksList();
    }


    private void initRecyclerView() {
        assignedTruckListAdapter = new AssignedTruckListAdapter(getActivity(),
                new ArrayList<>(),
                this,
                new AssignedTruckListAdapter.OnNavigateFragment() {
                    @Override
                    public void onNavigateToFragment(SP_SCN_AN_OUT_ASSIGNED_TRUCKS outAssignedTrucks) {
                        apiTruckDetails(outAssignedTrucks,true);
                    }
                });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mItemTruckList.setLayoutManager(linearLayoutManager);
        mItemTruckList.setAdapter(assignedTruckListAdapter);

        packListDetailedAdapter = new PickAndPackListDetailedAdapter(getContext(), new ArrayList<>(), this);
        mItemDetailedList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mItemDetailedList.setAdapter(packListDetailedAdapter);
    }

    private void apiFetchTrucksList() {
        JsonObject requestObject = ServiceUtils.OutBound.getSP_SCN_AN_OUT_ASSIGNED_TRUCKS();
        showProgress(false);

        App.getNetworkClient().getAPIService().getSpScnOutLocByJob(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("E")) {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showAlert("Error", msg);
                } else {
                    String xmlDoc = genericRetResponse.getJsonString();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showAlert("Error", "No data received.");
                        return;
                    }
                    try {
                        SP_SCN_AN_OUT_ASSIGNED_TRUCKS[] spScnAnOutAssignedTrucks = new Gson().fromJson(xmlDoc, SP_SCN_AN_OUT_ASSIGNED_TRUCKS[].class);
                        if (spScnAnOutAssignedTrucks.length == 0) {
                            showAlert("Error", "No data received.");
                            return;
                        }
                        List<SP_SCN_AN_OUT_ASSIGNED_TRUCKS> formattedList = new ArrayList<>();
                        for (int i = 0; i < spScnAnOutAssignedTrucks.length; i++) {
                            SP_SCN_AN_OUT_ASSIGNED_TRUCKS spScnAnOutLocByJob = new SP_SCN_AN_OUT_ASSIGNED_TRUCKS();
                            spScnAnOutLocByJob = spScnAnOutAssignedTrucks[i];
                            formattedList.add(spScnAnOutLocByJob);
                        }
                        setupListView(formattedList);

                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                        showAlert("Error", e.getMessage());
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showAlert("Error", msg);
            }
        });
    }

    private void setupListView(List<SP_SCN_AN_OUT_ASSIGNED_TRUCKS> formattedList) {
        assignedTruckListAdapter.addItems(formattedList);
    }

    private void apiTruckDetails(SP_SCN_AN_OUT_ASSIGNED_TRUCKS assignedTrucks,
                                 boolean isNavigate) {

        //TODO fetch details by truck id here, and remove isNavigate
        JsonObject requestObject = ServiceUtils.OutBound.getSP_SCN_AN_OUT_LOC_BY_JOB();
        showProgress(false);

        App.getNetworkClient().getAPIService().getSpScnOutLocByJob(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("E")) {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showAlert("Error", msg);
                } else {
                    String xmlDoc = genericRetResponse.getJsonString();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showAlert("Error", "No data received.");
                        return;
                    }
                    try {
                        SP_SCN_AN_OUT_LOC_BY_JOB[] spScnAnOutLocByJobs = new Gson().fromJson(xmlDoc, SP_SCN_AN_OUT_LOC_BY_JOB[].class);
                        if (spScnAnOutLocByJobs.length == 0) {
                            showAlert("Error", "No data received.");
                            return;
                        }
                        List<SP_SCN_AN_OUT_LOC_BY_JOB> formattedList = new ArrayList<>();
                        for (int i = 0; i < spScnAnOutLocByJobs.length; i++) {
                            SP_SCN_AN_OUT_LOC_BY_JOB spScnAnOutLocByJob = new SP_SCN_AN_OUT_LOC_BY_JOB();
                            spScnAnOutLocByJob = spScnAnOutLocByJobs[i];
                            formattedList.add(spScnAnOutLocByJob);
                        }
                        setupTruckDetailsList(formattedList.get(0), isNavigate);

                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                        showAlert("Error", e.getMessage());
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showAlert("Error", msg);
            }
        });
    }

    /**
     * show basic details of truck
     *
     * @param truckDetails
     */
    private void setupTruckDetailsList(SP_SCN_AN_OUT_LOC_BY_JOB truckDetails,
                                       boolean isNavigate) {

        if (isNavigate) {
            Bundle bundle = new Bundle();
            bundle.putSerializable(AppConstants.SELECTED_SP_SCN_AN_OUT_LOC_BY_JOB, truckDetails);
            ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_pick_and_pack_details, bundle);
        } else {
            showProgress(true);
            List<SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS> itemDetails = new ArrayList<>();

            SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS itemName = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
            itemName.itemLabel = "GACScan ID";
            itemName.itemValue = truckDetails.SRNO;
            itemDetails.add(itemName);

            SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS itemCode = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
            itemCode.itemLabel = "Picking Sequence";
            itemCode.itemValue = String.valueOf(truckDetails.PICK_SEQNO);
            itemDetails.add(itemCode);

            packListDetailedAdapter.addItems(itemDetails);
            packListDetailedAdapter.notifyDataSetChanged();

            TextView info2 = root.findViewById(R.id.info2);
            info2.setText(truckDetails.SRNO);
        }
        dismissProgress();
    }

    @Override
    public void onClick(View v) {
        apiTruckDetails((SP_SCN_AN_OUT_ASSIGNED_TRUCKS) v.getTag(),
                false);
    }


}
