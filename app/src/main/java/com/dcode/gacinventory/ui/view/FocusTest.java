package com.dcode.gacinventory.ui.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.common.AppVariables;
import com.dcode.gacinventory.common.Utils;
import com.dcode.gacinventory.data.model.GENERATE_LABELS;
import com.dcode.gacinventory.data.model.INBOUND_HEADER;
import com.dcode.gacinventory.data.model.PHOTO_MASTER;
import com.dcode.gacinventory.network.model.GenericRetResponse;
import com.dcode.gacinventory.network.model.OAuthTokenResponse;
import com.dcode.gacinventory.network.service.ServiceUtils;
import com.dcode.gacinventory.ui.adapter.ImageViewAdapter;
import com.example.gacinventory.R;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.dcode.gacinventory.common.AppConstants.VISIT_IMAGE;

public class FocusTest extends BaseFragment {
    private TextInputEditText edBarcode,edContainer,edTruck,edGscan,edAsn,edJobNo,edGacScan;
    View root;
    private RecyclerView recyclerView;
    private Drawable icon;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true);

        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);
            Bundle bundle = this.getArguments();
//            if (bundle != null) {
//                menu = (MENU) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
//            }

        }

        root = inflater.inflate(R.layout.fragment_focustest, container, false);
        icon = getResources().getDrawable(R.drawable.ic_delete_black);

        edBarcode = root.findViewById(R.id.edBarcode);
        edGacScan = root.findViewById(R.id.edGacScan);
        edGacScan.requestFocus();

        return root;
    }

    private void setupView(){



    }



    @Override
    public void onResume() {
        super.onResume();
//        edJobNo.clearFocus();
//        edJobNo.setFocusableInTouchMode(true);
//        edJobNo.requestFocus();
//        edJobNo.requestFocusFromTouch();
    }



   @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

       setupView();

   }


}
