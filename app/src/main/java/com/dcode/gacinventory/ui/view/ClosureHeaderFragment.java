package com.dcode.gacinventory.ui.view;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.data.model.JOB_CLOSURE_INFO;
import com.dcode.gacinventory.network.model.GenericRetResponse;
import com.dcode.gacinventory.network.service.ServiceUtils;
import com.dcode.gacinventory.ui.adapter.ClosureAdapter;
import com.example.gacinventory.R;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.dcode.gacinventory.common.AppConstants.INVEST_HEADER;
import static com.dcode.gacinventory.common.AppConstants.INVEST_PUT;
import static com.dcode.gacinventory.common.AppConstants.INVEST_STAG;

public class ClosureHeaderFragment extends BaseFragment  implements View.OnClickListener,
        SearchView.OnQueryTextListener{
    private View root;
    private AutoCompleteTextView edPrinCode;
    private ArrayAdapter<String> userPrinInfoArrayAdapter;
    private String prinCode = "";
    private ClosureAdapter closureAdapter;
    private TextView edInfo;
    private TextInputEditText edSearch;
    private CheckBox chkSelect;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);

        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);
            //putawayType = this.getArguments().getString("PutAwayType", "");
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                //DocNo = bundle.getString(AppConstants.SELECTED_ID);
                //barcode = bundle.getString(AppConstants.MASTER_ID);
                if (bundle.containsKey(AppConstants.SELECTED_ID)) {
                    //putawayType = bundle.getString(AppConstants.SELECTED_ID);
                }
            }
        }

        root = inflater.inflate(R.layout.fragment_closure_header, container, false);
        setupView();
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FillPrinCodes();

        edSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                closureAdapter.getFilter().filter(s);
            }
        });
    }

    private void setupView() {

        edPrinCode = root.findViewById(R.id.edPrinCode);
        edInfo  = root.findViewById(R.id.edInfo);
        edInfo.setVisibility(View.GONE);

        MaterialButton btPhoto = root.findViewById(R.id.btnBack);
        btPhoto.setOnClickListener(v -> OnBackClick());

        MaterialButton btnPrint = root.findViewById(R.id.btNext);
        btnPrint.setOnClickListener(v -> OnNextClick());

        RecyclerView recyclerView = root.findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));

        closureAdapter = new ClosureAdapter(new ArrayList<>(),this);
        recyclerView.setAdapter(closureAdapter);

        edSearch = root.findViewById(R.id.edsearch);
        edSearch.setVisibility(View.GONE);


        chkSelect = root.findViewById(R.id.chkSelect);
        chkSelect.setVisibility(View.GONE);
        chkSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                closureAdapter.selectAllItem(isChecked);
                //set your object's last status
                //showToast(String.valueOf(isChecked));
                //itemsLabelsPrintAdapter.selectAll(isChecked);
                //itemsLabelsPrintAdapter.notifyDataSetChanged();
            }
        });
    }


    private void setHeaderToolbar() {
//        if (inbound_header != null) {
//            Toolbar toolbar = root.findViewById(R.id.toolbar);
//            TextView info1 = root.findViewById(R.id.info1);
//            info1.setText(App.currentUser.USER_ID);
//            TextView info2 = root.findViewById(R.id.info2);
//            info2.setText(inbound_header.PRIN_CODE);
//        }
    }

    private void OnNextClick() {
        if (Validate()) {
            for (JOB_CLOSURE_INFO jobClose:closureAdapter.getSelected()
                 ) {
               Log.d("jobClose##",jobClose.JOB_NO);
               uploadJobClosure(jobClose);
            }
        }
    }

    private void OnBackClick() {
        getActivity().onBackPressed();
    }

    private boolean Validate() {
        if (!(closureAdapter.getSelected().size()>0)) {
            showToast("Please select any item");
            return false;
        }

        return true;

    }

    private void DwonloadJobClosureList(String prinCode) {
        if (prinCode.length() <= 0) {
            return;
        }

        JsonObject requestObject = ServiceUtils.LogisticLabel.getJobClosureList(prinCode);

        showProgress(false);
        App.getNetworkClient().getAPIService().getRetGeneric(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("S")) {
                    String xmlDoc = genericRetResponse.getJsonString();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showAlert("Error", "No data received.");
                        return;
                    }
                    try {
                        JOB_CLOSURE_INFO[] jobClosureInfo = new Gson().fromJson(xmlDoc, JOB_CLOSURE_INFO[].class);
                        if (jobClosureInfo.length == 0) {
                            showAlert("Error", "No data received.");
                            return;
                        }
                        chkSelect.setVisibility(View.VISIBLE);
                        edInfo.setVisibility(View.VISIBLE);
                        edSearch.setVisibility(View.VISIBLE);
                        //pl_itemsList = Arrays.asList(pl_items);
                        closureAdapter.addItems( Arrays.asList(jobClosureInfo));
                        edInfo.setText("Total Items : "+closureAdapter.getItemCount());

                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                        showAlert("Error", e.getMessage());
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showAlert("Error", msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showAlert("Error", msg);
            }
        });
    }


    private void uploadJobClosure(JOB_CLOSURE_INFO jobClosureInfo) {
        if(jobClosureInfo==null){
            return;
        }

        showToast(getString(R.string.send_data_to_server));
        JsonObject requestObject = ServiceUtils.LogisticLabel.getJobClosureData(jobClosureInfo);
        showProgress(false);
        App.getNetworkClient().getAPIService().submitWithLinesData(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericSubmissionResponse, Response response) {
                dismissProgress();
                String msg = genericSubmissionResponse.getErrMessage();
                if (genericSubmissionResponse.getErrCode().equals("S")) {
                    //resetUI();
                    showToast("Success");
                    closureAdapter.updateStatus(jobClosureInfo.JOB_NO,1,msg);
                } else {
                    Log.d(App.TAG, genericSubmissionResponse.getErrMessage());
                    showToast(genericSubmissionResponse.getErrMessage());
                    closureAdapter.updateStatus(jobClosureInfo.JOB_NO,2,msg);
                }

            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(App.TAG, error.getMessage());
                showToast(error.getMessage());
                dismissProgress();
            }
        });
    }


    private void OpenPutAway() {
//        Bundle bundle = new Bundle();
//        bundle.putSerializable(AppConstants.SELECTED_OBJECT_NEW, items_prints[0]);
//        bundle.putBoolean(AppConstants.SELECTED_ID, true);
//        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_put_away, bundle);
    }


    private void FillPrinCodes() {
        List<String> user_prin_infoList = App.getDatabaseClient().getAppDatabase().genericDao().getUserPrinCodes();
        if (user_prin_infoList != null && user_prin_infoList.size() > 0) {
            userPrinInfoArrayAdapter = new ArrayAdapter<>(getContext(), R.layout.dropdown_menu_popup_item, user_prin_infoList);
            edPrinCode.setAdapter(userPrinInfoArrayAdapter);
            edPrinCode.setThreshold(100);
            edPrinCode.setOnItemClickListener((parent, view, position, id) -> {
                if (position >= 0) {
                    prinCode = userPrinInfoArrayAdapter.getItem(position);
                    DwonloadJobClosureList(prinCode);
                } else {
                    prinCode = "";
                }

            });
        }
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        closureAdapter.getFilter().filter(s);
        String filterString = s;
        return false;
    }
}
