package com.dcode.gacinventory.ui.menus;

import com.example.gacinventory.R;

import java.util.ArrayList;
import java.util.List;

/**
 * @Created by: VIJIL DHAS A.S
 * @Date: 22-09-2021
 */
public class MenuItemsBuilder {
    private static MenuItemsBuilder instance;

    public static MenuItemsBuilder getInstance() {
        if (instance == null) {
            instance = new MenuItemsBuilder();
        }
        return instance;
    }

    public List<MenuItems> getFilterItems() {
        List<MenuItems> menuItems = new ArrayList<>();

        MenuItems menuFilterAll = new MenuItems();
        menuFilterAll.setMenuID(100);
        menuFilterAll.setMenuName("Any");
        menuFilterAll.setMenuImage(R.drawable.ic_filter);
        menuFilterAll.setImageShown(true);
        menuFilterAll.setSelected(false);
        menuItems.add(menuFilterAll);

        MenuItems menuPrinCode = new MenuItems();
        menuPrinCode.setMenuID(101);
        menuPrinCode.setMenuName("Principle Code");
        menuPrinCode.setMenuImage(R.drawable.ic_person_black_24dp);
        menuPrinCode.setImageShown(true);
        menuPrinCode.setSelected(false);
        menuItems.add(menuPrinCode);

        MenuItems menuJobNumber = new MenuItems();
        menuJobNumber.setMenuID(102);
        menuJobNumber.setMenuName("Job Number");
        menuJobNumber.setMenuImage(R.drawable.ic_job);
        menuJobNumber.setImageShown(true);
        menuJobNumber.setSelected(false);
        menuItems.add(menuJobNumber);

        MenuItems menuTruckId = new MenuItems();
        menuTruckId.setMenuID(103);
        menuTruckId.setMenuName("Truck ID");
        menuTruckId.setMenuImage(R.drawable.ic_truck);
        menuTruckId.setImageShown(true);
        menuTruckId.setSelected(false);
        menuItems.add(menuTruckId);

        return menuItems;
    }
}
