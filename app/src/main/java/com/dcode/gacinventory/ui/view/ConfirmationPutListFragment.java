package com.dcode.gacinventory.ui.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.data.model.CONFIRMATION_LIST;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SCAN;
import com.dcode.gacinventory.data.model.PRINTER_LIST_SERVICE;
import com.dcode.gacinventory.data.model.PRINT_FORMAT;
import com.dcode.gacinventory.network.model.GenericRetResponse;
import com.dcode.gacinventory.network.service.ServiceUtils;
import com.dcode.gacinventory.ui.adapter.ConfirmationListAdapter;
import com.dcode.gacinventory.ui.adapter.ConfirmationPutAwayListAdapter;
import com.dcode.gacinventory.ui.adapter.ItemsLabelsPrintGeneratedAdapter;
import com.example.gacinventory.R;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ConfirmationPutListFragment
        extends BaseFragment
        implements View.OnClickListener {

    private String jobNo;
    private View root;
    private AutoCompleteTextView tvPrinter;
    private AutoCompleteTextView tvLabel;
    //private List<GENERATE_LABELS> itemsPrintList;
    //private List<GR_ITEM_PRINT_TYP> grItemPrintTyp;
    private ItemsLabelsPrintGeneratedAdapter labelsPrintGeneratedAdapter;
    private ArrayAdapter<PRINTER_LIST_SERVICE> printersAdapter;
    private ArrayAdapter<PRINT_FORMAT> labelsAdapter;
    private PRINTER_LIST_SERVICE barcode_printers;
    private PRINT_FORMAT label_formats;
    private RadioGroup rdoGroup;
    private TextInputEditText edMac;
    private TextInputLayout ilPrinter;
    private TextInputLayout ilmac;
    private TextView edInfo;
    private CheckBox chkSelect;
    private int noGacScan;
    private List<CONFIRMATION_LIST> itemsPrintList;
    private ConfirmationPutAwayListAdapter confirmationPutAwayListAdapter;
    ///private INBOUND_HEADER inboundHeader;
    private LOGISTIC_LABEL_SCAN items_prints;
    int parentId = -1;
    //private LOGISTIC_LABEL_SUMBIT logistic_label_sumbit;
    //private PUTAWAY_SUMBIT putaway_sumbit;
    private ArrayAdapter<String> orderByAdapter;
    private AutoCompleteTextView orderBy;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);


        jobNo = "";
        if (this.getArguments() != null) {
            Bundle bundle = this.getArguments();
            //parentId = bundle.containsKey(AppConstants.PARENT_ID) ? bundle.getInt(AppConstants.PARENT_ID) : -1;
            jobNo = bundle.containsKey(AppConstants.SELECTED_ID) ? bundle.getString(AppConstants.SELECTED_ID, "") : "";
            //inboundHeader  = (INBOUND_HEADER) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
            //logistic_label_sumbit = bundle.containsKey(AppConstants.SELECTED_OBJECT_NEW) ? (LOGISTIC_LABEL_SUMBIT) bundle.getSerializable(AppConstants.SELECTED_OBJECT_NEW) : null;
        }

        root = inflater.inflate(R.layout.fragment_confirmation_put_list, container, false);
        setupView();
        return root;
    }

    private void setupView(){

        RecyclerView recyclerView = root.findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));


        //labelsPrintGeneratedAdapter = new ItemsLabelsPrintGeneratedAdapter(new ArrayList<>(), this);
        //recyclerView.setAdapter(labelsPrintGeneratedAdapter);
        //FillGACPrintList();

        confirmationPutAwayListAdapter = new ConfirmationPutAwayListAdapter(new ArrayList<>(),this);
        recyclerView.setAdapter(confirmationPutAwayListAdapter);
        //recyclerView.setNestedScrollingEnabled(false);



        MaterialButton btnPrint = root.findViewById(R.id.btnPrint);
        btnPrint.setOnClickListener(v -> {

        });

        MaterialButton btnBack = root.findViewById(R.id.btnBack);
        btnBack.setOnClickListener(v -> {
//            Bundle bundle = new Bundle();
//            bundle.putSerializable(AppConstants.SELECTED_OBJECT, inboundHeader);
//            ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_logistic_label, bundle);
                }
        );


        edInfo = root.findViewById(R.id.edInfo);
        //edInfo.setText("Total GACScan ID : "+noGacScan);

//        chkSelect = root.findViewById(R.id.chkSelect);
//        chkSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                //set your object's last status
//                if(itemsPrintList.size()>0){
//                    if(chkSelect.isChecked()){
//                        confirmationListAdapter.selectAll(true);
//                        confirmationListAdapter.notifyDataSetChanged();
//                    }else{
//                        confirmationListAdapter.selectAll(false);
//                        confirmationListAdapter.notifyDataSetChanged();
//                    }
//                }
//            }
//        });

        orderBy = root.findViewById(R.id.edOrder);
        setOrderBy();
        orderBy.setOnItemClickListener((parent, view, position, id) -> {
            //showToast("position"+String.valueOf(position));
            if (position >= 0) {
                if (position == 0) {
                    loadData("ALL");
                } else if (position == 1) {
                    loadData("PUTAWAY");
                } else if (position == 2) {loadData("CONFIRM");
                }
                edInfo.setText("Total Items : "+confirmationPutAwayListAdapter.getItemCount());
            }

        });

    }

    private void setOrderBy() {
        ArrayList<String> orderList = new ArrayList<String>();
        orderList.add("All");
        orderList.add("Pending");
        orderList.add("Completed");

        orderByAdapter = new ArrayAdapter<>(requireContext(), R.layout.dropdown_menu_popup_item, orderList);
        orderBy.setAdapter(orderByAdapter);
        orderBy.setThreshold(100);
    }

    private void OpenGACScan() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, items_prints);
        //bundle.putSerializable(AppConstants.SELECTED_OBJECT_NEW,inboundHeader);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_gac_scan, bundle);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //showToast("in this tab onViewCreated");
        //DownloadConfirmationList();
        loadData("ALL");

    }


    @Override
    public void onClick(View view) {
        CONFIRMATION_LIST confirmationList = (CONFIRMATION_LIST) view.getTag();
        if(confirmationList==null){
            showAlert("Warning","No confirmation List!");
            return;
        }

    }


    /*private void uploadGACScanData() {

        showToast(getString(R.string.send_data_to_server));
        JsonObject requestObject = ServiceUtils.LogisticLabel.getLOGISTIC_LABEL_SUMBIT_FULL(logistic_label_sumbit);
        showProgress(false);
        App.getNetworkClient().getAPIService().submitWithLinesData(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericSubmissionResponse, Response response) {
                dismissProgress();
                if (genericSubmissionResponse.getErrCode().equals("S")) {
                    if (genericSubmissionResponse.getErrCode().equals("S")) {
                        String msg = genericSubmissionResponse.getErrMessage();
                        //resetUI();
                        //showAlert("Success",msg);
                        OnPutAwayProcess(false);
                        //logistic_label_sumbit = null;

                    } else {
                        showAlert("Error",genericSubmissionResponse.getErrMessage());
                        //showToast(genericSubmissionResponse.getErrMessage());
                    }
                } else {
                    Log.d(App.TAG, genericSubmissionResponse.getErrMessage());
                    showAlert("Error",genericSubmissionResponse.getErrMessage());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(App.TAG, error.getMessage());
                showToast(error.getMessage());
                dismissProgress();
            }
        });
    }*/

    /*private void OnPutAwayProcess(boolean  close) {
        if(putaway_sumbit==null) {
            putaway_sumbit = new PUTAWAY_SUMBIT();
        }
        putaway_sumbit = new PUTAWAY_SUMBIT();
        putaway_sumbit.SRNO = logistic_label_sumbit.SRNO;
        putaway_sumbit.JOB_NO = logistic_label_sumbit.JOB_NO;
        putaway_sumbit.MFG_DATE = logistic_label_sumbit.MFG_DATE;
        putaway_sumbit.EXP_DATE = logistic_label_sumbit.EXP_DATE;
        putaway_sumbit.LOT_NO = logistic_label_sumbit.LOT_NO;
        putaway_sumbit.LOT_STATUS =logistic_label_sumbit.LOT_STATUS;
        putaway_sumbit.FORMULA_NO =logistic_label_sumbit.FORMULA_NO;
        putaway_sumbit.FIL_CODE =logistic_label_sumbit.FIL_CODE;
        putaway_sumbit.BARCODE = logistic_label_sumbit.BARCODE;
        putaway_sumbit.INPUT_PQTY = logistic_label_sumbit.INPUT_PQTY;
        putaway_sumbit.INPUT_MQTY = logistic_label_sumbit.INPUT_MQTY;
        putaway_sumbit.INPUT_LQTY = logistic_label_sumbit.INPUT_LQTY;
        putaway_sumbit.LOC_CODE = logistic_label_sumbit.LOC_CODE;
        putaway_sumbit.SITE = logistic_label_sumbit.SITE;
        putaway_sumbit.USER_NAME = logistic_label_sumbit.USER_NAME;
        putaway_sumbit.UPDATE_FROM = logistic_label_sumbit.UPDATE_FROM;
        putaway_sumbit.PALLET_ID = logistic_label_sumbit.PALLET_ID;
        putaway_sumbit.LOC = logistic_label_sumbit.LOC;
        putaway_sumbit.SCAN_LOCATION = "";
        putaway_sumbit.SCAN_PUT_LOC_CODE = logistic_label_sumbit.SCAN_PUT_LOC_CODE;
        uploadPutAwayData(close);
    }*/


    /*private void uploadPutAwayData(boolean close) {
        if(putaway_sumbit==null){
            return;
        }
        showToast(getString(R.string.send_data_to_server));
        JsonObject requestObject = ServiceUtils.LogisticLabel.getPUTAWAY_SUMBIT(putaway_sumbit);
        showProgress(false);
        App.getNetworkClient().getAPIService().submitWithLinesData(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericSubmissionResponse, Response response) {
                dismissProgress();
                if (genericSubmissionResponse.getErrCode().equals("S")) {
                    if (genericSubmissionResponse.getErrCode().equals("S")) {
                        String msg = genericSubmissionResponse.getErrMessage();
                        showAlert("Success",msg);
                        if(close){
                           // backToHome();
                        }else {
                           // backToLogistic();
                        }
                    } else {
                        showAlert("Error",genericSubmissionResponse.getErrMessage());
                        //showToast(genericSubmissionResponse.getErrMessage());
                    }
                } else {
                    Log.d(App.TAG, genericSubmissionResponse.getErrMessage());
                    showAlert("Error",genericSubmissionResponse.getErrMessage());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(App.TAG, error.getMessage());
                showToast(error.getMessage());
                dismissProgress();
            }
        });
    }*/


    private void DownloadConfirmationList() {
        if (jobNo.length()<= 0 ) {
            return;
        }

        JsonObject requestObject = ServiceUtils.LogisticLabel.getConfirmationList(jobNo,"");

        Log.d("requestObject##",requestObject.toString());

        showProgress(false);
        App.getNetworkClient().getAPIService().getRetGeneric(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("S")) {
                    String xmlDoc = genericRetResponse.getJsonString();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showAlert("Error", "No data received.");
                        return;
                    }
                    try {
//                        String lastId = genericRetResponse.getErrMessage();
//                        if(lastId==null&& lastId.length()==0){
//                            showAlert("Error", "No lastId received.");
//                            return;
//                        }
                        CONFIRMATION_LIST[] items_prints = new Gson().fromJson(xmlDoc, CONFIRMATION_LIST[].class);
                        App.getDatabaseClient().getAppDatabase().genericDao().deleteAllCONFIRMATION_LIST();
                        App.getDatabaseClient().getAppDatabase().genericDao().insertCONFIRMATION_LIST(items_prints);
                        loadData("ALL");

                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                        showAlert("Error", e.getMessage());
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showAlert("Error", msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showAlert("Error", msg);
            }
        });
    }

    private void loadData(String filter) {

        //pl_itemsList = App.getDatabaseClient().getAppDatabase().genericDao().GetLOAD_SLIP_ITEMS_NEW(pl_hdr.LSNO);// summary list
        itemsPrintList = App.getDatabaseClient().getAppDatabase().genericDao().getPutCONFIRMATION_LIST(filter);
        if (itemsPrintList != null && itemsPrintList.size()>0 ) {
            confirmationPutAwayListAdapter.addItems(itemsPrintList);
            confirmationPutAwayListAdapter.notifyDataSetChanged();
            edInfo.setText("Total Items : "+confirmationPutAwayListAdapter.getItemCount());
        }else {
            showAlert(" Alert - Put away","No Put away data found.");
        }
    }


}
