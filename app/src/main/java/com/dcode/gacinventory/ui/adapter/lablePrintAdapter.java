package com.dcode.gacinventory.ui.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.gacinventory.data.model.CONFIRMATION_LIST;
import com.dcode.gacinventory.data.model.LABEL_ITEM_LIST;
import com.example.gacinventory.R;
import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.List;

public class lablePrintAdapter
        extends RecyclerView.Adapter<lablePrintAdapter.RecyclerViewHolder> implements Filterable {

    private LABEL_ITEM_LIST confirmationList;
    private List<LABEL_ITEM_LIST> itemsPrintList;
    private View.OnClickListener shortClickListener;
    //private boolean isSelectedAll = false;

    public Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<LABEL_ITEM_LIST> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(itemsPrintList);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (LABEL_ITEM_LIST item : itemsPrintList) {
                    if (item.GACSCANID.equalsIgnoreCase(filterPattern) ) {
                        filteredList.add(item);
                    }
                }
                Log.d("filteredList##",String.valueOf(filteredList.size()));
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            itemsPrintList  = new ArrayList<>();
            itemsPrintList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };


    public lablePrintAdapter(List<LABEL_ITEM_LIST> itemsPrints,
                             View.OnClickListener shortClickListener) {
        this.itemsPrintList = itemsPrints;
        this.shortClickListener = shortClickListener;
        setHasStableIds(true);
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_card_items_print, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        final LABEL_ITEM_LIST items_print = itemsPrintList.get(position);

        holder.tvName.setText(items_print.GACSCANID);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    shortClickListener.onClick(view);
                } catch (ClassCastException e) {
                    // do something
                }
            }
        });

        items_print.IS_SELECT = Boolean.TRUE;
        holder.chkSelect.setChecked(true);

//        if(items_print.IS_SELECT) {
//            holder.chkSelect.setChecked(true);
//        }else{
//            holder.chkSelect.setChecked(false);
//        }

        //in some cases, it will prevent unwanted situations
        holder.chkSelect.setOnCheckedChangeListener(null);

        //if true, your checkbox will be selected, else unselected
        //items_print.IS_SELECT = Boolean.TRUE;
        //holder.chkSelect.setChecked(true);

//        if (!isSelectedAll) {
//            holder.chkSelect.setChecked(false);
//        }
//        else{
//            holder.chkSelect.setChecked(true);
//            int i = 0;
//            for (LABEL_ITEM_LIST items : itemsPrintList) {
//                if (items.IS_SELECT) {
//                    Log.d("count##", String.valueOf(i++));
//                    Log.d("items.BARCODE## ", items.GACSCANID);
//                }
//            }
//        }


        holder.chkSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //set your object's last status
//                if (isChecked){
//                    holder.chkSelect.setChecked(false);
//                    items_print.IS_SELECT = false;
//                }else{
//                    holder.chkSelect.setChecked(true);
//                    items_print.IS_SELECT = true;
//                }
                if(items_print.IS_SELECT){
                    items_print.IS_SELECT = false ;
                }else{
                    items_print.IS_SELECT = true;
                }

            }
        });
        holder.itemView.setTag(items_print);
        //holder.itemView.setOnClickListener(shortClickListener);
        confirmationList = items_print;
    }

    public void selectAll(boolean select){
        //Log.e("onClickSelectAll",String.valueOf(select));
        //isSelectedAll=select;
        selectAllItem(select);
        notifyDataSetChanged();
    }

    public void selectAllItem(boolean isSelected) {
        try {

            if (itemsPrintList != null) {
                for (LABEL_ITEM_LIST items : itemsPrintList) {
                    items.IS_SELECT = isSelected;
                    Log.d("items.BARCODE## ", String.valueOf(items.IS_SELECT));
                }
            }
            notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        if (itemsPrintList == null) {
            return 0;
        }
        return itemsPrintList.size();
    }

    public List<LABEL_ITEM_LIST> getItems() {
        return this.itemsPrintList;
    }

    public void addItems(List<LABEL_ITEM_LIST> detList) {
        this.itemsPrintList = detList;
        notifyDataSetChanged();
    }

    public void addItems(LABEL_ITEM_LIST items_print) {
        this.itemsPrintList.add(items_print);
        notifyDataSetChanged();
    }

    public LABEL_ITEM_LIST getItemByPosition(int position) {
        return this.itemsPrintList.get(position);
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName;
        private CheckBox chkSelect;

        RecyclerViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.tvName);
            chkSelect= view.findViewById(R.id.chkSelect);
        }
    }

    public interface ConfirmAdapterListener {

        void confirmShortClick(View v, int position);
    }
}
