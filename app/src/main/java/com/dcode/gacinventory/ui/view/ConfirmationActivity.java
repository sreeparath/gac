package com.dcode.gacinventory.ui.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.example.gacinventory.R;
import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.Objects;

public class ConfirmationActivity
        extends BaseActivity
        implements View.OnClickListener, SearchView.OnQueryTextListener {
    private int Master_ID;
    private int Parent_ID;
    private String title,message;
    private int left;
    private int right;
    private String filterString = "";
    private int[] fkey_intArray;
private TextView edInfo;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_confirmation;
    }

    @Override
    protected void onViewReady(Bundle savedInstanceState) {
        try {
            Intent intent = this.getIntent();
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Master_ID = !bundle.containsKey(AppConstants.MASTER_ID) ? -1 : bundle.getInt(AppConstants.MASTER_ID, -1);
                Parent_ID = !bundle.containsKey(AppConstants.PARENT_ID) ? -1 : bundle.getInt(AppConstants.PARENT_ID, -1);
                title = !bundle.containsKey(AppConstants.TITLE) ? null : bundle.getString(AppConstants.TITLE);
                message = !bundle.containsKey(AppConstants.MESSAGE) ? null : bundle.getString(AppConstants.MESSAGE);
                left = !bundle.containsKey(AppConstants.LEFT) ? null : bundle.getInt(AppConstants.LEFT);
                right = !bundle.containsKey(AppConstants.RIGHT) ? null : bundle.getInt(AppConstants.RIGHT);
                if (bundle.containsKey(AppConstants.FILTER_KEY_INT_ARRAY)) {
                    fkey_intArray = bundle.getIntArray(AppConstants.FILTER_KEY_INT_ARRAY);
                }
            }

            if (Master_ID != 0) {
                setupView();
            } else {
                setResult(RESULT_CANCELED, intent);
                finish();
            }
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
            showAlert("Error", e.getMessage(), false);
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        //objectsAdapter.getFilter().filter(newText);
        filterString = newText;
        return false;
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(RESULT_CANCELED);
        finishAndRemoveTask();
    }

    private void setupView() {
        try {

            MaterialButton btnBack = findViewById(R.id.btnBack);
            btnBack.setText(left);
            btnBack.setOnClickListener(v -> onBackPressed());

            MaterialButton btNext = findViewById(R.id.btNext);
            btNext.setText(right);
            btNext.setOnClickListener(v -> OnNextClick());


            String ScreenTitle = getString(R.string.confirmation);

            Toolbar toolbar = findViewById(R.id.toolbar);
            toolbar.setTitle(ScreenTitle);

            edInfo = findViewById(R.id.edInfo);
            Log.d("message##", message);
            edInfo.setText(message);

        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
            showAlert("Error", e.getMessage(), false);
        }
    }

    private void OnNextClick() {
        try {
            Bundle bundle = new Bundle();
            bundle.putInt(AppConstants.SELECTED_ID, 1);
            bundle.putInt(AppConstants.PARENT_ID, Parent_ID);
            Intent intent = this.getIntent();
            intent.putExtras(bundle);
            setResult(RESULT_OK, intent);
            finishAndRemoveTask();
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
            showAlert("Error", e.getMessage(), false);
        }
    }


}

