package com.dcode.gacinventory.ui.view.outbound;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.data.model.SP_SCN_AN_OUT_LOC_BY_JOB;
import com.dcode.gacinventory.data.model.SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS;
import com.dcode.gacinventory.ui.adapter.PickAndPackListDetailedAdapter;
import com.dcode.gacinventory.ui.view.BaseFragment;
import com.example.gacinventory.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * @Created by: VIJIL DHAS A.S
 * @Date: 08-09-2021
 */
public class PickPackDetailsFragment extends BaseFragment implements View.OnClickListener {
    private View root;
    private SP_SCN_AN_OUT_LOC_BY_JOB spScnAnOutLocByJob;
    private RecyclerView recyclerViewDetails;
    private PickAndPackListDetailedAdapter packListDetailedAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if (this.getArguments() != null) {
            Bundle bundle = this.getArguments();
            if (bundle.containsKey(AppConstants.SELECTED_SP_SCN_AN_OUT_LOC_BY_JOB)) {
                spScnAnOutLocByJob = (SP_SCN_AN_OUT_LOC_BY_JOB) bundle.getSerializable(AppConstants.SELECTED_SP_SCN_AN_OUT_LOC_BY_JOB);
            }
        }
        root = inflater.inflate(R.layout.fragment_pick_and_pack_details, container, false);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHeaderToolbar();
        initViews();
        if (spScnAnOutLocByJob != null) {
            processItemDetails(spScnAnOutLocByJob);
        }
    }

    private void initViews() {
        recyclerViewDetails = root.findViewById(R.id.list_details);
        packListDetailedAdapter = new PickAndPackListDetailedAdapter(getContext(), new ArrayList<>(), this);
        recyclerViewDetails.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerViewDetails.setAdapter(packListDetailedAdapter);
    }

    private void setHeaderToolbar() {
        TextView info1 = root.findViewById(R.id.info1);
        info1.setText(App.currentUser.USER_ID);


    }

    /**
     * function to create pick and pack details
     *
     * @param spScnAnOutLocByJob SP_SCN_AN_OUT_LOC_BY_JOB
     */
    private void processItemDetails(SP_SCN_AN_OUT_LOC_BY_JOB spScnAnOutLocByJob) {
        TextView info2 = root.findViewById(R.id.info2);
        info2.setText(spScnAnOutLocByJob.SRNO);


        List<SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS> itemDetails = new ArrayList<>();

        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item1 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item1.itemLabel = "Pick Site Code";
        item1.itemValue = spScnAnOutLocByJob.SITE_CODE;


        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item2 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item2.itemLabel = "Pick Location Code";
        item1.itemValue = String.valueOf(spScnAnOutLocByJob.LOC_CODE);

        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item3 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item3.itemLabel = "Primary Unit of measure";
        item3.itemValue = spScnAnOutLocByJob.PUOM;


        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item4 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item4.itemLabel = "Secondary Unit of measure";
        item4.itemValue = spScnAnOutLocByJob.NUOM;


        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item5 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item5.itemLabel = "Least Unit of measure";
        item5.itemValue = spScnAnOutLocByJob.SUOM;

        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item6 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item6.itemLabel = "Primary UOM Quantity";
        item6.itemValue = String.valueOf(spScnAnOutLocByJob.PQTY);


        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item7 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item7.itemLabel = "Secondary UOM Quantity";
        item7.itemValue = String.valueOf(spScnAnOutLocByJob.MQTY);


        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item8 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item8.itemLabel = "Least UOM Quantity";
        item8.itemValue = String.valueOf(spScnAnOutLocByJob.LQTY);


        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item9 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item9.itemLabel = "Order Quantity";
        item9.itemValue = String.valueOf(spScnAnOutLocByJob.QUANTITY);


        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item10 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item10.itemLabel = "Units per primary pallet";
        item10.itemValue = String.valueOf(spScnAnOutLocByJob.UPPP);


        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item11 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item11.itemLabel = "Least Units per primary pallet";
        item11.itemValue = String.valueOf(spScnAnOutLocByJob.LUPPP);


        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item12 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item12.itemLabel = "Lot Number";
        item12.itemValue = spScnAnOutLocByJob.LOT_NO;


        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item13 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item13.itemLabel = "Manufacture Date";
        item13.itemValue = spScnAnOutLocByJob.MFG_DATE;


        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item14 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item14.itemLabel = "Expiry Date";
        item14.itemValue = spScnAnOutLocByJob.EXP_DATE;


        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item15 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item15.itemLabel = "Pending to be picked (N) Or Already Picked (Y)";
        item15.itemValue = spScnAnOutLocByJob.SCAN_FLAG;


        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item16 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item16.itemLabel = "Principal Code";
        item16.itemValue = String.valueOf(spScnAnOutLocByJob.PRIN_CODE);


        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item17 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item17.itemLabel = "Product Name";
        item17.itemValue = spScnAnOutLocByJob.PROD_NAME;


        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item18 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item18.itemLabel = "Product Code";
        item18.itemValue = spScnAnOutLocByJob.PROD_CODE;


        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item19 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item19.itemLabel = "GACScan ID";
        item19.itemValue = spScnAnOutLocByJob.SRNO;


        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item20 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item20.itemLabel = "Unique Row identifier";
        item20.itemValue = String.valueOf(spScnAnOutLocByJob.IDNO);


        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item21 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item21.itemLabel = "Site Type";
        item21.itemValue = spScnAnOutLocByJob.SITE_TYPE;


        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item22 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item22.itemLabel = "Location Status";
        item22.itemValue = spScnAnOutLocByJob.LOC_STAT;


        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item24 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item24.itemLabel = "Picking Sequence - Data should be sorted in the same order in the Grid";
        item24.itemValue = String.valueOf(spScnAnOutLocByJob.PICK_SEQNO);


        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item25 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item25.itemLabel = "Principal Name";
        item25.itemValue = spScnAnOutLocByJob.PRIN_NAME;


        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item26 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item26.itemLabel = "Job Number";
        item26.itemValue = spScnAnOutLocByJob.JOB_NO;


        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item27 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item27.itemLabel = "Job Priority";
        item27.itemValue = String.valueOf(spScnAnOutLocByJob.PRIORITY);


        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item28 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item28.itemLabel = "GACScan logged in user";
        item28.itemValue = spScnAnOutLocByJob.USER_SCAN_ID;


        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item29 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item29.itemLabel = "Quantity Edit is Allowed or Not (Y/N)";
        item29.itemValue = spScnAnOutLocByJob.ALLOW_EDIT_PCKQTY;


        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item30 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item30.itemLabel = "Barcode Scan Check is mandatory or Not (Y/N)";
        item30.itemValue = spScnAnOutLocByJob.BARCODE_SCAN_CHECK;

        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item31 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item31.itemLabel = "Lot Number Scan Check is mandatory or Not (Y/N)";
        item31.itemValue = spScnAnOutLocByJob.LOTNO_SCAN_CHECK;


        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item32 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item32.itemLabel = "Product Barcode 1";
        item32.itemValue = spScnAnOutLocByJob.BARCODE;


        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item33 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item33.itemLabel = "Product Barcode 2";
        item33.itemValue = spScnAnOutLocByJob.BARCODE2;

        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item34 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item34.itemLabel = "Product Barcode 3";
        item34.itemValue = spScnAnOutLocByJob.BARCODE3;


        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item35 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item35.itemLabel = "Job Classification";
        item35.itemValue = spScnAnOutLocByJob.JOBCLASS;


        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item36 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item36.itemLabel = "If the value is 1 then show CTN_QTY +'C/' + REM_PCS_QTY + 'P' Else Show CTN_QTY +'P/'";
        item36.itemValue = String.valueOf(spScnAnOutLocByJob.QTY_PCB);


        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item37 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item37.itemLabel = "Carton Quantity";
        item37.itemValue = String.valueOf(spScnAnOutLocByJob.CTN_QTY);

        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS item38 = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        item38.itemLabel = "Pallet Qty if QTY_PCB = 1";
        item38.itemValue = String.valueOf(spScnAnOutLocByJob.REM_PCS_QTY);


        itemDetails.add(item1);
        itemDetails.add(item2);
        itemDetails.add(item3);
        itemDetails.add(item4);
        itemDetails.add(item5);
        itemDetails.add(item6);
        itemDetails.add(item7);
        itemDetails.add(item8);
        itemDetails.add(item9);
        itemDetails.add(item10);
        itemDetails.add(item11);
        itemDetails.add(item12);
        itemDetails.add(item13);
        itemDetails.add(item14);
        itemDetails.add(item15);
        itemDetails.add(item16);
        itemDetails.add(item17);
        itemDetails.add(item18);
        itemDetails.add(item19);
        itemDetails.add(item20);
        itemDetails.add(item21);
        itemDetails.add(item22);
        itemDetails.add(item24);
        itemDetails.add(item25);
        itemDetails.add(item26);
        itemDetails.add(item27);
        itemDetails.add(item28);
        itemDetails.add(item29);
        itemDetails.add(item30);
        itemDetails.add(item31);
        itemDetails.add(item32);
        itemDetails.add(item33);
        itemDetails.add(item34);
        itemDetails.add(item35);
        itemDetails.add(item36);
        itemDetails.add(item37);
        itemDetails.add(item38);

        packListDetailedAdapter.addItems(itemDetails);
        packListDetailedAdapter.notifyDataSetChanged();
    }


    @Override
    public void onClick(View v) {

    }
}
