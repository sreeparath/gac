package com.dcode.gacinventory.ui.adapter;


import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.data.model.INBOUND_HEADER;
import com.dcode.gacinventory.ui.view.ConfirmationListFragment;
import com.dcode.gacinventory.ui.view.ConfirmationPutListFragment;
import com.dcode.gacinventory.ui.view.HomeFragment;
import com.dcode.gacinventory.ui.view.SettingsFragment;

public class ConfirmListTabAdapter extends FragmentPagerAdapter {
    private Context myContext;
    int totalTabs;
    private Bundle bundle;
    public ConfirmListTabAdapter(Context context, FragmentManager fm, int totalTabs, Bundle bundle) {
        super(fm);
        myContext = context;
        this.totalTabs = totalTabs;
        this.bundle = bundle;

    }
    // this is for fragment tabs
    @Override
    public Fragment getItem(int position) {



        switch (position) {
            case 0:

                ConfirmationListFragment confirmationListFragment = new ConfirmationListFragment();
                if (bundle != null) {
                    Bundle newBundle = new Bundle();
                    newBundle.putString(AppConstants.SELECTED_ID, bundle.containsKey(AppConstants.SELECTED_ID) ? bundle.getString(AppConstants.SELECTED_ID, "") : "");
                    //newBundle.putSerializable(AppConstants.SELECTED_OBJECT,(INBOUND_HEADER) bundle.getSerializable(AppConstants.SELECTED_OBJECT));
                    confirmationListFragment.setArguments(bundle);
                    //logistic_label_sumbit = bundle.containsKey(AppConstants.SELECTED_OBJECT_NEW) ? (LOGISTIC_LABEL_SUMBIT) bundle.getSerializable(AppConstants.SELECTED_OBJECT_NEW) : null;
                }

                return confirmationListFragment;
            case 1:
                ConfirmationPutListFragment confirmationPutListFragment = new ConfirmationPutListFragment();
                if (bundle != null) {
                    Bundle newBundle = new Bundle();
                    newBundle.putString(AppConstants.SELECTED_ID, bundle.containsKey(AppConstants.SELECTED_ID) ? bundle.getString(AppConstants.SELECTED_ID, "") : "");
                    //newBundle.putSerializable(AppConstants.SELECTED_OBJECT,(INBOUND_HEADER) bundle.getSerializable(AppConstants.SELECTED_OBJECT));
                    confirmationPutListFragment.setArguments(bundle);
                    //logistic_label_sumbit = bundle.containsKey(AppConstants.SELECTED_OBJECT_NEW) ? (LOGISTIC_LABEL_SUMBIT) bundle.getSerializable(AppConstants.SELECTED_OBJECT_NEW) : null;
                }
                return confirmationPutListFragment;
            default:
                return null;
        }
    }
    // this counts total number of tabs
    @Override
    public int getCount() {
        return totalTabs;
    }
}