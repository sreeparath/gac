package com.dcode.gacinventory.ui.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.gacinventory.data.model.LABEL_ITEM_LIST;
import com.example.gacinventory.R;

import java.util.ArrayList;

public class MultiAdapter extends RecyclerView.Adapter<MultiAdapter.MultiViewHolder> {

    private Context context;
    private ArrayList<LABEL_ITEM_LIST> label_item_lists;

    public MultiAdapter(Context context, ArrayList<LABEL_ITEM_LIST> label_item_lists) {
        this.context = context;
        this.label_item_lists = label_item_lists;
        //setHasStableIds(true);
    }

    public void setLableItemList(ArrayList<LABEL_ITEM_LIST> label_item_lists) {
        this.label_item_lists = new ArrayList<>();
        this.label_item_lists = label_item_lists;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MultiViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_card_items_print, viewGroup, false);
        return new MultiViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MultiViewHolder multiViewHolder, int position) {
        //multiViewHolder.bind(label_item_lists.get(position));

        final LABEL_ITEM_LIST labelItemList  = label_item_lists.get(position);
        //multiViewHolder.imageView.setVisibility(labelItemList.IS_SELECT ? View.VISIBLE : View.GONE);
        multiViewHolder.textView.setText(labelItemList.GACSCANID);

//        multiViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                labelItemList.setIS_SELECT(!labelItemList.IS_SELECT);
//                multiViewHolder.imageView.setVisibility(labelItemList.isIS_SELECT() ? View.VISIBLE : View.GONE);
//            }
//        });
        multiViewHolder.chkSelect.setChecked(labelItemList.IS_SELECT ? true : false);
//        multiViewHolder.chkSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                labelItemList.setIS_SELECT(isChecked);
//
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return label_item_lists.size();
    }

    class MultiViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView textView;
        private ImageView imageView;
        private CheckBox chkSelect;

        MultiViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.tvName);
            imageView = itemView.findViewById(R.id.imageView);
            chkSelect= itemView.findViewById(R.id.chkSelect);
            //chkSelect.setClickable(false);
            itemView.setOnClickListener(this);
        }

        void bind(final LABEL_ITEM_LIST labelItemList) {
            imageView.setVisibility(labelItemList.IS_SELECT ? View.VISIBLE : View.GONE);
            textView.setText(labelItemList.GACSCANID);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    labelItemList.setIS_SELECT(!labelItemList.IS_SELECT);
                    imageView.setVisibility(labelItemList.isIS_SELECT() ? View.VISIBLE : View.GONE);
                }
            });
        }

        @Override
        public void onClick(View view) {
            if(getAdapterPosition()!=-1) {
                label_item_lists.get(getAdapterPosition()).setIS_SELECT(!label_item_lists.get(getAdapterPosition()).isIS_SELECT());
                notifyItemChanged(getAdapterPosition());
            }
        }
    }

    public ArrayList<LABEL_ITEM_LIST> getAll() {
        return label_item_lists;
    }

    public ArrayList<LABEL_ITEM_LIST> getSelected() {
        ArrayList<LABEL_ITEM_LIST> selected = new ArrayList<>();
        for (int i = 0; i < label_item_lists.size(); i++) {
            if (label_item_lists.get(i).isIS_SELECT()) {
                selected.add(label_item_lists.get(i));
            }
        }
        return selected;
    }

    public void setSelectedAll(boolean select) {
        ArrayList<LABEL_ITEM_LIST> selected = new ArrayList<>();
        for (int i = 0; i < label_item_lists.size(); i++) {
                label_item_lists.get(i).setIS_SELECT(select);
            Log.d("selected##",String.valueOf(label_item_lists.get(i).isIS_SELECT()));
            notifyDataSetChanged();
        }
    }

}