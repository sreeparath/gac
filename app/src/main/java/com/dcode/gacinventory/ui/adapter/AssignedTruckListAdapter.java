package com.dcode.gacinventory.ui.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.gacinventory.data.model.SP_SCN_AN_OUT_ASSIGNED_TRUCKS;
import com.dcode.gacinventory.data.model.SP_SCN_AN_OUT_LOC_BY_JOB;
import com.example.gacinventory.R;

import java.util.List;

/**
 * @Created by: VIJIL DHAS A.S
 * @Date: 12-09-2021
 */
public class AssignedTruckListAdapter extends RecyclerView.Adapter<AssignedTruckListAdapter.RecyclerViewHolder> {
    private Activity activity;
    private List<SP_SCN_AN_OUT_ASSIGNED_TRUCKS> assignedTrucks;
    private OnNavigateFragment navigateFragment;
    private View.OnClickListener itemClickListener;

    public AssignedTruckListAdapter(Activity activity,
                                    List<SP_SCN_AN_OUT_ASSIGNED_TRUCKS> assignedTrucks,
                                    View.OnClickListener itemClickListener,
                                    OnNavigateFragment navigateFragment) {
        this.activity = activity;
        this.assignedTrucks = assignedTrucks;
        this.itemClickListener = itemClickListener;
        this.navigateFragment = navigateFragment;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new AssignedTruckListAdapter.RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.assigned_truck_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        ((RecyclerViewHolder) holder).txtPrinCode.setText(assignedTrucks.get(position).PRIN_CODE);
        ((RecyclerViewHolder) holder).txtTruckId.setText(assignedTrucks.get(position).TRUCKID);

        ((RecyclerViewHolder) holder).labelContainer.setTag(assignedTrucks.get(position));
        ((RecyclerViewHolder) holder).labelContainer.setOnClickListener(itemClickListener);

        ((RecyclerViewHolder) holder).txtButtonDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (navigateFragment != null) {
                    navigateFragment.onNavigateToFragment(assignedTrucks.get(position));
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return assignedTrucks.size();
    }

    public void addItems(List<SP_SCN_AN_OUT_ASSIGNED_TRUCKS> formattedList) {
        assignedTrucks = formattedList;
        notifyDataSetChanged();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView txtPrinCode;
        private TextView txtTruckId;
        private TextView txtButtonDetails;
        private LinearLayout labelContainer;

        public RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);
            txtPrinCode = itemView.findViewById(R.id.txt_prin_code);
            txtTruckId = itemView.findViewById(R.id.txt_truck_id);
            txtButtonDetails = itemView.findViewById(R.id.txt_details_button);
            labelContainer = itemView.findViewById(R.id.top_label_container);
        }
    }

    public interface OnNavigateFragment {
        void onNavigateToFragment(SP_SCN_AN_OUT_ASSIGNED_TRUCKS outAssignedTrucks);
    }
}
