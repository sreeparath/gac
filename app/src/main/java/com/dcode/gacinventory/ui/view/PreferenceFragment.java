package com.dcode.gacinventory.ui.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.Nullable;


import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.common.AppVariables;
import com.dcode.gacinventory.common.Utils;
import com.example.gacinventory.R;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.io.File;
import java.util.Objects;

public class PreferenceFragment extends BaseFragment {
    Button btn_save_setting,btnBack;
    private TextInputEditText edDeviceID;
    private TextInputEditText edMastersData;
    private TextInputEditText edIp;
    private TextInputEditText edPort;
    private TextInputEditText edMac;
    private TextInputEditText edUrl;
    private TextInputLayout lbUrl;
    private RadioGroup rdoGroup;
    private RadioGroup rdoGroupNet;
    private LinearLayout layoutIp;
    private LinearLayout layoutBlue;
    private ImageButton settings;
    private View root;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);
        }

        root = inflater.inflate(R.layout.fragment_preference, container, false);

        setupView();
        return root;
    }


    private void setupView() {
        new AppVariables(getContext());

        btn_save_setting = root.findViewById(R.id.btnNext);
        btn_save_setting.setOnClickListener(v -> onSave());

        btnBack = root.findViewById(R.id.btnBack);
        btnBack.setOnClickListener(v -> getActivity().onBackPressed());

        layoutIp =  root.findViewById(R.id.layIp);
        layoutBlue =  root.findViewById(R.id.layMac);
        layoutIp.setVisibility(View.GONE);
        layoutBlue.setVisibility(View.GONE);

        // edDeviceID = findViewById(R.id.edDeviceID);
        //edDeviceID.setVisibility(View.GONE);
//        String deviceID = Utils.getValue(PreferenceFragment.this, AppConstants.DEV_UNIQUE_ID, false);
//        edDeviceID.setText(deviceID);

        edIp = root.findViewById(R.id.edIpAddress);
        String ip = AppVariables.getIpAddress();
        edIp.setText(ip);

        edPort = root.findViewById(R.id.edPort);
        String port = AppVariables.getPort();
        edPort.setText(port);

        edMac = root.findViewById(R.id.edMac);
        String mac = AppVariables.getMacAddress();
        Log.d("mac##",mac);
        edMac.setText(mac);

        rdoGroup = root.findViewById(R.id.radioGroup);
        rdoGroup.clearCheck();
        String printTypeInt = AppVariables.getPrinterTypeInt();
        Log.d("printTypeInt##",printTypeInt);
        if(printTypeInt!=null && printTypeInt.length()>0){
            RadioButton rdoButton = (RadioButton) rdoGroup.findViewById(Integer.valueOf(printTypeInt));
            rdoButton.setChecked(true);
        }
        String printType = AppVariables.getPrinterType();
        if(printType.equalsIgnoreCase(getString(R.string.ip))){
            layoutIp.setVisibility(View.VISIBLE);
        }else if(printType.equalsIgnoreCase(getString(R.string.blue))){
            layoutBlue.setVisibility(View.VISIBLE);
        }

        //edUrl =  findViewById(R.id.edUrl);
        //edUrl.setText(AppVariables.getUrl());
        //edUrl.setVisibility(View.GONE);
        //lbUrl =  findViewById(R.id.lbUrl);
        //lbUrl.setVisibility(View.GONE);
        //rdoGroupNet = findViewById(R.id.radioGroupNet);
        //rdoGroupNet.clearCheck();
        //String nettypeInt = AppVariables.getNetworkInt();
        //if(nettypeInt!=null && nettypeInt.length()>0){
        //   RadioButton rdoButton = (RadioButton) rdoGroupNet.findViewById(Integer.valueOf(nettypeInt));
        //    rdoButton.setChecked(true);
        //}

//        String nettype = AppVariables.getNetwork();
//        if(nettype.equalsIgnoreCase(getString(R.string.online))){
//            edUrl.setVisibility(View.VISIBLE);
//            lbUrl.setVisibility(View.VISIBLE);
//        }else if(printType.equalsIgnoreCase(getString(R.string.offline))){
//            edUrl.setVisibility(View.GONE);
//            lbUrl.setVisibility(View.GONE);
//        }


//        edMastersData = findViewById(R.id.edMastersData);
//        String sMastersData = AppVariables.getMastersPath();
//        if (sMastersData.isEmpty()){
//            sMastersData = getString(R.string.def_masters_path);
//        }
//        edMastersData.setText(sMastersData);
//        edMastersData.setOnClickListener(v -> getDirectoryPath(1001));



        rdoGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int check = group.getCheckedRadioButtonId();
                RadioButton rdoButton = (RadioButton) group.findViewById(check);
                if(rdoButton.getText().toString().equalsIgnoreCase(getString(R.string.ip))){
                    layoutIp.setVisibility(View.VISIBLE);
                    layoutBlue.setVisibility(View.GONE);
                }else if(rdoButton.getText().toString().equals(getString(R.string.blue))){
                    layoutIp.setVisibility(View.GONE);
                    layoutBlue.setVisibility(View.VISIBLE);
                }
            }
        });


    }





    private void onSave() {



//        String sMaster = edMastersData.getText().toString().trim();
//        if (sMaster.isEmpty()) {
//            showToast("Master data path is mandatory!");
//            return;
//        }

        int check = rdoGroup.getCheckedRadioButtonId();
        if (check==-1) {
            showToast("Printer Type is Mandatory!");
            return;
        }

        String ipAddr = edIp.getText().toString().trim();
        if (ipAddr.isEmpty() &&  layoutIp.getVisibility()==View.VISIBLE) {
            showToast("Ip address is mandatory!");
            return;
        }

        String port = edPort.getText().toString().trim();
        if (port.isEmpty() && layoutIp.getVisibility()==View.VISIBLE) {
            showToast("Port is mandatory!");
            return;
        }

        String mac = edMac.getText().toString().trim();
        Log.d("mac##",mac);
        if (mac.isEmpty() &&  layoutBlue.getVisibility()==View.VISIBLE) {
            showToast("Mac Address is mandatory!");
            return;
        }

//        int checkNet = rdoGroupNet.getCheckedRadioButtonId();
//        if (checkNet==-1) {
//            showToast("Network is Mandatory!");
//            return;
//        }

//        String url = edUrl.getText().toString().trim();
//        if (url.isEmpty() &&  edUrl.getVisibility()==View.VISIBLE) {
//            showToast("URL is mandatory!");
//            return;
//        }

        showToast("Restart application for setting to apply");


        //AppVariables.setMastersPath(sMaster);
        //Log.d("sMaster##",AppVariables.getMastersPath());
        Utils.SetValue(getContext(), AppConstants.MASTERS_PATH, AppVariables.getMastersPath());

        RadioButton rdoButton = (RadioButton) rdoGroup.findViewById(check);
        AppVariables.setPrinterType(rdoButton.getText().toString());
        AppVariables.setPrinterTypeInt(String.valueOf(check));
        AppVariables.setMacAddress(mac);
        AppVariables.setIpAddress(ipAddr);
        AppVariables.setPort(port);
        //AppVariables.setUrl(url);
        //RadioButton rdoButtonNet = (RadioButton) rdoGroupNet.findViewById(checkNet);
        //AppVariables.setNetwork(rdoButtonNet.getText().toString());
        //AppVariables.setNetworkInt(String.valueOf(checkNet));
//        Intent intent = new Intent(getApplicationContext(), PrintLabelActivity.class);
//        startActivity(intent);
        showToast("Saved Successfully");
    }
}
