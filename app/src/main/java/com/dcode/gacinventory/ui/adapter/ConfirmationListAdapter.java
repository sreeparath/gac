package com.dcode.gacinventory.ui.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.gacinventory.data.model.CONFIRMATION_LIST;
import com.dcode.gacinventory.data.model.INBOUND_HEADER;
import com.dcode.gacinventory.data.model.LABEL_ITEM_LIST;
import com.example.gacinventory.R;
import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.List;

public class ConfirmationListAdapter
        extends RecyclerView.Adapter<ConfirmationListAdapter.RecyclerViewHolder> implements Filterable {

    private CONFIRMATION_LIST confirmationList;
    private List<CONFIRMATION_LIST> itemsPrintList;
    private View.OnClickListener shortClickListener;
    private boolean isSelectedAll = false;

    public Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<CONFIRMATION_LIST> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(itemsPrintList);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (CONFIRMATION_LIST item : itemsPrintList) {
                    if (item.IS_STAGED.equalsIgnoreCase(filterPattern) ) {
                        filteredList.add(item);
                    }
                }
                Log.d("filteredList##",String.valueOf(filteredList.size()));
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            itemsPrintList  = new ArrayList<>();
            itemsPrintList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };


    public ConfirmationListAdapter(List<CONFIRMATION_LIST> itemsPrints,
                                   View.OnClickListener shortClickListener) {
        this.itemsPrintList = itemsPrints;
        this.shortClickListener = shortClickListener;
        setHasStableIds(true);
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_card_confirmation_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        final CONFIRMATION_LIST items_print = itemsPrintList.get(position);

        holder.tvDesc.setText(items_print.DESCRIPTION);
        holder.tvSrNo.setText(items_print.SRNO);
        holder.tvSku.setText(String.valueOf(items_print.SKU));
        holder.tvPrnCode.setText(String.valueOf(items_print.PRIN_CODE));
        holder.tvTotQty.setText(String.valueOf(items_print.TOTAL_QTY));
        holder.tvtStgQty.setText(String.valueOf(items_print.STG_QTY));
        //holder.itemView.setTag(items_print);
        holder.btnShort.setTag(items_print);
        holder.btnShort.setOnClickListener(shortClickListener);
        if(items_print.IS_STAGED.equalsIgnoreCase("Y")){
            holder.btnShort.setVisibility(View.GONE);
        }else{
            holder.btnShort.setVisibility(View.VISIBLE);
        }

//        holder.btnShort.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                try {
//                    items_print.IS_CONFIRMED = "1";
//                    notifyDataSetChanged();
//                } catch (ClassCastException e) {
//                    // do something
//                }
//            }
//        });


        //in some cases, it will prevent unwanted situations
        //holder.chkSelect.setOnCheckedChangeListener(null);

        //if true, your checkbox will be selected, else unselected
        //items_print.IS_SELECT = Boolean.TRUE;
        //holder.chkSelect.setChecked(true);

//        if (!isSelectedAll) {
//            holder.chkSelect.setChecked(false);
//        }
//        else{
//            holder.chkSelect.setChecked(true);
//        }

//        holder.chkSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                //set your object's last status
////                if (isChecked){
////                    holder.chkSelect.setChecked(false);
////                    items_print.IS_SELECT = false;
////                }else{
////                    holder.chkSelect.setChecked(true);
////                    items_print.IS_SELECT = true;
////                }
//                if(items_print.IS_SELECT){
//                    items_print.IS_SELECT = false ;
//                }else{
//                    items_print.IS_SELECT = true;
//                }
//
//            }
//        });
        holder.itemView.setTag(items_print);
        //holder.itemView.setOnClickListener(shortClickListener);
        confirmationList = items_print;
    }

    public void selectAll(boolean select){
        Log.e("onClickSelectAll",String.valueOf(select));
        isSelectedAll=select;
        notifyDataSetChanged();
    }

    public void selectAllItem(boolean isSelectedAll) {
        try {

            if (itemsPrintList != null) {
                for (int i = 0; i < itemsPrintList.size(); i++) {
                    itemsPrintList.get(i).IS_SELECT = isSelectedAll;
                    Log.d("isSelectedAll",String.valueOf(itemsPrintList.get(i).IS_SELECT));
                }
            }
            notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        if (itemsPrintList == null) {
            return 0;
        }
        return itemsPrintList.size();
    }

    public List<CONFIRMATION_LIST> getItems() {
        return this.itemsPrintList;
    }

    public void addItems(List<CONFIRMATION_LIST> detList) {
        this.itemsPrintList = detList;
        notifyDataSetChanged();
    }

    public void addItems(CONFIRMATION_LIST items_print) {
        this.itemsPrintList.add(items_print);
        notifyDataSetChanged();
    }

    public CONFIRMATION_LIST getItemByPosition(int position) {
        return this.itemsPrintList.get(position);
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView tvDesc;
        private TextView tvSrNo;
        private TextView tvSku;
        private TextView tvPrnCode;
        private TextView tvTotQty;
        private TextView tvtStgQty;
        //private CheckBox chkSelect;
        private MaterialButton btnShort;

        RecyclerViewHolder(View view) {
            super(view);
            tvDesc = view.findViewById(R.id.tvDesc);
            tvSrNo = view.findViewById(R.id.tvSrNo);
            tvSku = view.findViewById(R.id.tvSku);
            tvPrnCode = view.findViewById(R.id.tvPrnCode);
            tvTotQty = view.findViewById(R.id.tvTotQty);
            tvtStgQty = view.findViewById(R.id.tvtStgQty);
            btnShort = view.findViewById(R.id.btnShort);
            //chkSelect= view.findViewById(R.id.chkSelect);
        }
    }

    public interface ConfirmAdapterListener {

        void confirmShortClick(View v, int position);
    }
}
