package com.dcode.gacinventory.ui.view;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.data.model.INBOUND_HEADER;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SCAN;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SUMBIT;
import com.dcode.gacinventory.data.model.MENU;
import com.dcode.gacinventory.data.model.SECTION;
import com.dcode.gacinventory.network.model.GenericRetResponse;
import com.dcode.gacinventory.network.service.ServiceUtils;
import com.dcode.gacinventory.ui.adapter.ObjectsAdapter;
import com.dcode.gacinventory.ui.adapter.ObjectsSectionAdapter;
import com.example.gacinventory.R;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class SectionFragment extends BaseFragment implements View.OnClickListener {

    View root;
    ObjectsSectionAdapter objectsSectionAdapter;
    private MENU menu;
    TextInputEditText edSearch;
    INBOUND_HEADER inboundHeader;




    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                menu = (MENU) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
            }
        }



        root = inflater.inflate(R.layout.fragment_section, container, false);
        edSearch = root.findViewById(R.id.edsearch);
        edSearch.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_ENTER){
                if(event.getAction() == KeyEvent.ACTION_DOWN){
                    return onSearchEvent();
                }else{
                    return false;
                }
            }else{
                return false;
            }
        });
        //edSearch.requestFocus();
        RecyclerView recyclerView = root.findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        objectsSectionAdapter = new ObjectsSectionAdapter(getContext(),new ArrayList<>(),  this);
        recyclerView.setAdapter(objectsSectionAdapter);

        HashMap<Integer, SECTION> sectionMap = new HashMap();

        List<SECTION> sectionList = new ArrayList<SECTION>();
        List<SECTION> finalSectionList = new ArrayList<SECTION>();
        SECTION section;
        section = new SECTION();
        section.SECTION_ID = 1;
        section.PARENT_MENU_ID = 1;
        section.SECTION_ID_CODE = "INF";
        section.SECTION_MENU_NAME = "Inbound";
        section.SECTION_MENU_DESC = "Inbound Transactions";
        section.SECTION_ICON = R.drawable.ic_warehouse_full;
        sectionList.add(section);
        //sectionMap.put(1, section);

        section = new SECTION();
        section.PARENT_MENU_ID = 1;
        section.SECTION_ID = 9;
        section.SECTION_ID_CODE = "BS";
        section.SECTION_MENU_NAME = "Serial Tracker";
        section.SECTION_MENU_DESC = "Serial Scan Transactions";
        section.SECTION_ICON = R.drawable.ic_put_away;
        sectionList.add(section);

        section = new SECTION();
        section.PARENT_MENU_ID = 1;
        section.SECTION_ID = 7;
        section.SECTION_ID_CODE = "ST";
        section.SECTION_MENU_NAME = "Stagging";
        section.SECTION_MENU_DESC = "Stagging Transactions";
        section.SECTION_ICON = R.drawable.ic_put_away;
        sectionList.add(section);


        section = new SECTION();
        section.PARENT_MENU_ID = 1;
        section.SECTION_ID = 2;
        section.SECTION_ID_CODE = "INP";
        section.SECTION_MENU_NAME = "Inbound Partial";
        section.SECTION_MENU_DESC = "Inbound Partial Transactions";
        //sectionList.add(section);
        //sectionMap.put(1, section);

        section = new SECTION();
        section.PARENT_MENU_ID = 1;
        section.SECTION_ID = 3;
        section.SECTION_ID_CODE = "PT";
        section.SECTION_MENU_NAME = "Put Away";
        section.SECTION_MENU_DESC = "Put Away Transactions";
        section.SECTION_ICON = R.drawable.ic_put_away;
        sectionList.add(section);

        section = new SECTION();
        section.PARENT_MENU_ID = 1;
        section.SECTION_ID = 4;
        section.SECTION_ID_CODE = "DPT";
        section.SECTION_MENU_NAME = "Direct Put Away";
        section.SECTION_MENU_DESC = "Direct Put Away Transactions";
        section.SECTION_ICON = R.drawable.ic_put_away;
        sectionList.add(section);

        section = new SECTION();
        section.PARENT_MENU_ID = 1;
        section.SECTION_ID = 5;
        section.SECTION_ID_CODE = "IVN";
        section.SECTION_MENU_NAME = "Investigation";
        section.SECTION_MENU_DESC = "Investigation Transactions";
        section.SECTION_ICON = R.drawable.ic_put_away;
        sectionList.add(section);


        section = new SECTION();
        section.PARENT_MENU_ID = 1;
        section.SECTION_ID = 6;
        section.SECTION_ID_CODE = "JC";
        section.SECTION_MENU_NAME = "Job Closure";
        section.SECTION_MENU_DESC = "Job Closure Transactions";
        section.SECTION_ICON = R.drawable.ic_put_away;
        sectionList.add(section);



        section = new SECTION();
        section.PARENT_MENU_ID = 1;
        section.SECTION_ID = 8;
        section.SECTION_ID_CODE = "PU";
        section.SECTION_MENU_NAME = "Product Update";
        section.SECTION_MENU_DESC = "Product Update Transactions";
        section.SECTION_ICON = R.drawable.ic_put_away;
        sectionList.add(section);

        section = new SECTION();
        section.PARENT_MENU_ID = 1;
        section.SECTION_ID = 4;
        section.SECTION_ID_CODE = "FT";
        section.SECTION_MENU_NAME = "Focus";
        section.SECTION_MENU_DESC = "Focus Transactions";
        section.SECTION_ICON = R.drawable.ic_put_away;
        // sectionList.add(section);

        section = new SECTION();
        section.PARENT_MENU_ID = 2;
        section.SECTION_ID = 3;
        section.SECTION_ID_CODE = "OUP";
        section.SECTION_MENU_NAME = "Outbound Full";
        section.SECTION_MENU_DESC = "Outbound Full Transactions";
        sectionList.add(section);

        /* *//*Menu Pick and Pack*//*
        section = new SECTION();
        section.PARENT_MENU_ID = 2;
        section.SECTION_ID = 9;
        section.SECTION_ID_CODE = "OUPP"; // Outbound Pick and Pack
        section.SECTION_MENU_NAME = "Outbound Full - Pick and Pack";
        section.SECTION_MENU_DESC = "Outbound Full Transactions - Picking by Job(s)";
        sectionList.add(section);

        *//*Menu Pick and Pack*//*
        section = new SECTION();
        section.PARENT_MENU_ID = 2;
        section.SECTION_ID = 10;
        section.SECTION_ID_CODE = "OUPTID"; // Picking by truck ID
        section.SECTION_MENU_NAME = "Outbound Full";
        section.SECTION_MENU_DESC = "Picking by truck ID";
        sectionList.add(section);*/

        section = new SECTION();
        section.PARENT_MENU_ID = 2;
        section.SECTION_ID = 11;
        section.SECTION_ID_CODE = "OUTCON"; // Picking by truck ID
        section.SECTION_MENU_NAME = "Outbound Full";
        section.SECTION_MENU_DESC = "Pick Confirmation";
        sectionList.add(section);


        for (SECTION sec : sectionList) {
            if (sec.PARENT_MENU_ID == menu.MENU_ID) {
                finalSectionList.add(sec);
            }
        }

//        for (HashMap.Entry<Integer, SECTION> entry : sectionMap.entrySet()){
//            System.out.println("Key = " + entry.getKey() +
//                    ", Value = " + entry.getValue());
//            System.out.println("=menu.MENU_ID = " + String.valueOf(menu.MENU_ID));
//            if(entry.getKey()==menu.MENU_ID){
//                sectionList.add(entry.getValue());
//            }
//        }

        if (finalSectionList.size() > 0) {
            objectsSectionAdapter.addItems(finalSectionList);
        }


//        detAdapter = new ObjectsAdapter(getContext(),null,  this);
//        recyclerView.setAdapter(detAdapter);

        return root;
    }

    @Override
    public void onClick(View view) {
        SECTION section = (SECTION) view.getTag();

        if (section == null) {
            return;
        }


        if (section.SECTION_ID_CODE.equalsIgnoreCase("INF")) {
            OnModuleClick(R.id.action_nav_inbound);
        } else if (section.SECTION_ID_CODE.equalsIgnoreCase("DW")) {
            OnModuleClick(R.id.action_nav_sync);
        } else if (section.SECTION_ID_CODE.equalsIgnoreCase("SC")) {
            OnModuleClick(R.id.nav_logistic_label);
        } else if (section.SECTION_ID_CODE.equalsIgnoreCase("SPC")) {
            OnModuleClick(R.id.nav_partial_logistic_label);
        }else if(section.SECTION_ID_CODE.equalsIgnoreCase("INP")){
          //  OnModuleClick(R.id.action_nav_inbound_partial);
        }else if(section.SECTION_ID_CODE.equalsIgnoreCase("PT")){
            App.putawayMixtypeKey = false;
            App.putawayMixtypeJob = "";
            NavigateToPutAwayFull(R.id.nav_put_away_header);
        } else if (section.SECTION_ID_CODE.equalsIgnoreCase("INP")) {
            //  OnModuleClick(R.id.action_nav_inbound_partial);
        } else if (section.SECTION_ID_CODE.equalsIgnoreCase("PT")) {
            NavigateToPutAwayFull(R.id.nav_put_away_header);
        } else if (section.SECTION_ID_CODE.equalsIgnoreCase("FT")) {
            NavigateToPutAwayFull(R.id.nav_fragment_focustest);
        } else if (section.SECTION_ID_CODE.equalsIgnoreCase("IVN")) {
            OnModuleClick(R.id.action_nav_fragment_investigation_header);
        } else if (section.SECTION_ID_CODE.equalsIgnoreCase("DPT")) {
            NavigateToPutAwayFull(R.id.nav_direct_put_away_header);
        } else if (section.SECTION_ID_CODE.equalsIgnoreCase("JC")) {
            NavigateToClosure(R.id.nav_fragment_closure_header);
        } else if (section.SECTION_ID_CODE.equalsIgnoreCase("ST")) {
            NavigateToStagging(R.id.nav_stagging);
        } else if (section.SECTION_ID_CODE.equalsIgnoreCase("PU")) {
            NavigateToPutAwayFull(R.id.nav_product_update_header);
        }else if(section.SECTION_ID_CODE.equalsIgnoreCase("BS")) {
            NavigateToPutAwayFull(R.id.nav_box_scan);
        }
        /*else if (section.SECTION_ID_CODE.equalsIgnoreCase("OUPP")) {
            //NavigatePickAndPack(R.id.nav_pick_and_pack);
            NavigatePickAndPack(R.id.nav_site_location);
        }else if (section.SECTION_ID_CODE.equalsIgnoreCase("OUPTID")) {
            NavigatePickAndPack(R.id.nav_by_truck_id);
        }*/
        else if (section.SECTION_ID_CODE.equalsIgnoreCase("OUTCON")) {
            NavigatePickAndPack(R.id.nav_pick_confirm);
        }

    }

    private void NavigateToStagging(int fragmaneID) {
        Bundle bundle = new Bundle();
        //bundle.putString(AppConstants.SELECTED_ID, "P");
        ((MainActivity) requireActivity()).NavigateToFragment(fragmaneID, bundle);
    }

    private void OnModuleClick(int action_id) {
        Navigation.findNavController(root).navigate(action_id);
    }

    private void NavigateToClosure(int fragmaneID) {
        Bundle bundle = new Bundle();
        //bundle.putString(AppConstants.SELECTED_ID, "P");
        ((MainActivity) requireActivity()).NavigateToFragment(fragmaneID, bundle);
    }

    private void NavigateToPutAwayFull(int fragmaneID) {
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.SELECTED_ID, "P");
        ((MainActivity) requireActivity()).NavigateToFragment(fragmaneID, bundle);
    }

    private void NavigatePickAndPack(int fragmaneID) {
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.SELECTED_ID, "OUPP");
        ((MainActivity) requireActivity()).NavigateToFragment(fragmaneID, bundle);
    }

    private void NavigateToInbound(MENU menu) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, menu);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_inbound, bundle);
    }


    private boolean onSearchEvent() {
        String search = edSearch.getText().toString();

        if (search.isEmpty() || search.trim().length() == 0) {
            showToast("Invalid Barcode");
            edSearch.requestFocus();
            return false;
        } else {
            ParseBarCode(search, "");
        }
        return true;
    }

    private void ParseBarCode(String scanCode, String mode) {
        if (scanCode.length() <= 0) {
            return;
        }
        DownloadPutawayDocument(scanCode, mode);

    }

    private void DownloadPutawayDocument(String gacScan, String prodBarcode) {
        if (gacScan.length() <= 0) {
            return;
        }

        JsonObject requestObject = ServiceUtils.LogisticLabel.getPutAwayInfo(gacScan, prodBarcode, "P");

        showProgress(false);
        App.getNetworkClient().getAPIService().getRetGeneric(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("S")) {
                    String xmlDoc = genericRetResponse.getJsonString();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showAlert("Error", "No data received.");
                        return;
                    }
                    try {
                        LOGISTIC_LABEL_SUMBIT[] items_prints = new Gson().fromJson(xmlDoc, LOGISTIC_LABEL_SUMBIT[].class);
                        LOGISTIC_LABEL_SCAN[] logisticLabelScans = new Gson().fromJson(xmlDoc, LOGISTIC_LABEL_SCAN[].class);
                        if (items_prints.length == 0) {
                            showAlert("Error", "No data received.");
                            return;
                        }
                        edSearch.setText("");
                        //showAlert("items_prints[0].IS_STAGED", String.valueOf(items_prints[0].IS_STAGED));
                        if (items_prints[0].IS_STAGED.length() > 0 && items_prints[0].IS_STAGED.trim().equalsIgnoreCase("Y") &&
                                (items_prints[0].IS_PUTAWAY.length() > 0 && items_prints[0].IS_PUTAWAY.equalsIgnoreCase("N"))) {
                            //showAlert("OpenPutAway", "OpenPutAway");
                            INBOUND_HEADER inboundHeader = new INBOUND_HEADER();
                            inboundHeader.JOB_NO = items_prints[0].JOB_NO;
                            inboundHeader.PRIN_CODE = items_prints[0].PRIN_CODE;
                            inboundHeader.PRIN_NAME = items_prints[0].PRIN_NAME;
                            OpenMenuFragment(items_prints[0], inboundHeader, logisticLabelScans[0]);
                        } else {
                            //showAlert("staggine", "staggine");
                            INBOUND_HEADER inboundHeader = new INBOUND_HEADER();
                            inboundHeader.JOB_NO = items_prints[0].JOB_NO;
                            inboundHeader.PRIN_CODE = items_prints[0].PRIN_CODE;
                            inboundHeader.PRIN_NAME = items_prints[0].PRIN_NAME;
                            OpenLogisticLabel(inboundHeader, gacScan);
                        }

                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                        showAlert("Error", e.getMessage());
                        edSearch.setText("");
                        edSearch.requestFocus();
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showAlert("Error", msg);
                    edSearch.setText("");
                    edSearch.requestFocus();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showAlert("Error", msg);
                edSearch.setText("");
                edSearch.requestFocus();
            }
        });
    }

    private void OpenPutAway(LOGISTIC_LABEL_SUMBIT items_prints) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.SELECTED_OBJECT_NEW, items_prints);
        //bundle.putSerializable(AppConstants.SELECTED_OBJECT_NEW2, inbound_header);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_put_away, bundle);
    }

    private void OpenMenuFragment(LOGISTIC_LABEL_SUMBIT logisticLabelSumbit, INBOUND_HEADER inboundHeader, LOGISTIC_LABEL_SCAN logisticLabelScan) {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        //bundle.putString(AppConstants.SELECTED_ID, DocNo);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT_NEW, logisticLabelSumbit);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, logisticLabelScan);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT_NEW2, inboundHeader);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_menu_fragment, bundle);
    }

    private void OpenLogisticLabel(INBOUND_HEADER inboundHeader, String gacScan) {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putString(AppConstants.SELECTED_ID, gacScan);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, inboundHeader);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_logistic_label, bundle);
    }



    private void OpenHome() {
        Bundle bundle = new Bundle();
        ((MainActivity) requireActivity()).NavigateFullBackStack();
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_home, bundle);
    }



}