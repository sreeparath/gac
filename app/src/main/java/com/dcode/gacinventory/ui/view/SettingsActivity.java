package com.dcode.gacinventory.ui.view;

import android.content.Intent;
import android.os.Bundle;


import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppPreferences;
import com.example.gacinventory.R;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

public class SettingsActivity extends BaseActivity {
    private TextInputEditText edServerURL;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_settings;
    }

    @Override
    protected void onViewReady(Bundle savedInstanceState) {
        MaterialButton btnNext = findViewById(R.id.btnNext);
        edServerURL = findViewById(R.id.edUrl);
        //TextInputEditText tvDeviceID = findViewById(R.id.tvDeviceID);

        btnNext.setOnClickListener(v -> onSave());

        //tvDeviceID.setText(AppPreferences.getValue(SettingsActivity.this, AppPreferences.DEV_UNIQUE_ID, true));

        String ServiceURL = AppPreferences.getValue(SettingsActivity.this, AppPreferences.SERVICE_URL, false);
        if (ServiceURL.length() <= 0)
            ServiceURL = getString(R.string.default_url);
        edServerURL.setText(ServiceURL);
    }

    private void onSave() {
        showToast("Restart application for setting to apply");
        String ServiceURL = edServerURL.getText().toString();
        AppPreferences.SetValue(SettingsActivity.this, AppPreferences.SERVICE_URL, ServiceURL);
        App.appSettings().setBaseUrl(ServiceURL);
        App.getNetworkClient().destroyInstance();
        //finish();
        Intent intent = new Intent(SettingsActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
