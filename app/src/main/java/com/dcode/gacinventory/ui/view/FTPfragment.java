package com.dcode.gacinventory.ui.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.common.Utils;
import com.dcode.gacinventory.network.service.FtpService;
import com.dcode.gacinventory.ui.adapter.ImageViewAdapter;
import com.example.gacinventory.R;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.dcode.gacinventory.common.AppConstants.VISIT_IMAGE;

public class FTPfragment extends BaseFragment implements View.OnLongClickListener{
    private TextInputEditText edDocNo;
//    private AutoCompleteTextView tvDocTypes;
//    private ArrayAdapter<DocType> objectsAdapter;
    private int promptResource;
    private String DocNo;
    private long HDR_ID;
    private FloatingActionButton btnCamera;
    private Bitmap photo;
    private ImageViewAdapter imageViewAdapter;
    View root;
    private RecyclerView recyclerView;
    private Drawable icon;
    private final ColorDrawable background = new ColorDrawable(Color.RED);


    FtpService ftpService;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);
        }

        ftpService = new FtpService();



        root = inflater.inflate(R.layout.fragment_inbound_no, container, false);
        icon = getResources().getDrawable(R.drawable.ic_delete_black);
        //ImageView imgDocNo = root.findViewById(R.id.imgDocNo);
        //imgDocNo.setImageResource(R.drawable.ic_005);

        edDocNo = root.findViewById(R.id.edScanCode);
/*        edDocNo.setOnKeyListener((v, keyCode, event) -> {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    keyCode == KeyEvent.KEYCODE_ENTER) {
                return onDocNoKeyEvent();
            }
            return false;
        });*/

        MaterialButton btnPrint = root.findViewById(R.id.btnPrint);
        btnPrint.setOnClickListener(v -> {
//            new Thread(new Runnable() {
//                public void run() {
//                    ftpService.ftpDisconnect();
//                }
//            }).start();
            if (ftpService.isOnline(getContext())) {
                ftpService.connectToFTP();
            } else {
                showToast("Please check your internet connection!");
            }

        });

        MaterialButton btnNext = root.findViewById(R.id.btnNext);
        btnNext.setOnClickListener(v -> OnNextClick());

        btnCamera = root.findViewById(R.id.btnCamera);
        btnCamera.setOnClickListener(v -> onCameraClick());

/*        tvDocTypes = root.findViewById(R.id.acDocType);
        tvDocTypes.setOnItemClickListener((parent, view, position, id) -> {
            if (position > 0) {
                ordType = objectsAdapter.getItem(position);
            } else {
                ordType = null;
            }
        });*/

        recyclerView = root.findViewById(R.id.recycleView);

        imageViewAdapter = new ImageViewAdapter(getContext(), new ArrayList<>(),this);



        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(imageViewAdapter);



        return root;
    }


    private void fetchPhotos(){
//        List<String> photos = new ArrayList();
//        photos.add("/storage/emulated/0/DCIM/Camera/20210523170538.png");
//        photos.add("/storage/emulated/0/DCIM/Camera/20210523150152.png");
//        photos.add("/storage/emulated/0/DCIM/Camera/20210523133807.png");
//        imageViewAdapter.addItems(photos);
//        imageViewAdapter.notifyDataSetChanged();
    }

   @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        fetchPhotos();

//        ItemTouchHelper.SimpleCallback simpleCallback =
//               new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
//
//                   @Override
//                   public boolean onMove(@NonNull RecyclerView recyclerView,
//                                         @NonNull RecyclerView.ViewHolder viewHolder,
//                                         @NonNull RecyclerView.ViewHolder viewHolder1) {
//                       return false;
//                   }
//
//                   @Override
//                   public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
//                       if (direction == ItemTouchHelper.LEFT || direction == ItemTouchHelper.RIGHT) {
//                           int position = viewHolder.getAdapterPosition();
//                           String pl_item_rec = imageViewAdapter.getItemByPosition(position);
//                           if (pl_item_rec != null) {
//                               final AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
//                               alert.setTitle("Delete this photo?");
//                               alert.setPositiveButton("OK", (dialog, whichButton) -> {
//                                   recyclerView.removeViewAt(position);
//                                   imageViewAdapter.notifyItemRemoved(position);
//                                   imageViewAdapter.deleteBatch(position);
//                                   imageViewAdapter.notifyItemRangeChanged(position, imageViewAdapter.getItemCount());
//                               });
//                               alert.setNegativeButton("Cancel", (dialog, whichButton) -> {
//                                   //Put actions for CANCEL button here, or leave in blank
//                                   imageViewAdapter.notifyDataSetChanged();
//                               });
//                               alert.show();
//                           }
//                           fetchPhotos();
//                       }
//                   }
//
//                   @Override
//                   public void onChildDraw(@NonNull Canvas c,
//                                           @NonNull RecyclerView recyclerView,
//                                           @NonNull RecyclerView.ViewHolder viewHolder,
//                                           float dX, float dY, int actionState, boolean isCurrentlyActive) {
//                       super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
//                       View itemView = viewHolder.itemView;
//                       int backgroundCornerOffset = 20;
//
//                       int iconMargin = (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
//                       int iconTop = itemView.getTop() + (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
//                       int iconBottom = iconTop + icon.getIntrinsicHeight();
//
//                       if (dX > 0) {
//                           int iconLeft = itemView.getLeft() + iconMargin + icon.getIntrinsicWidth();
//                           int iconRight = itemView.getLeft() + iconMargin;
//                           icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);
//
//                           background.setBounds(itemView.getLeft(), itemView.getTop(),
//                                   itemView.getLeft() + ((int) dX) + backgroundCornerOffset, itemView.getBottom());
//                       } else if (dX < 0) {
//                           int iconLeft = itemView.getRight() - iconMargin - icon.getIntrinsicWidth();
//                           int iconRight = itemView.getRight() - iconMargin;
//                           icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);
//
//                           background.setBounds(itemView.getRight() + ((int) dX) - backgroundCornerOffset,
//                                   itemView.getTop(), itemView.getRight(), itemView.getBottom());
//                       } else {
//                           background.setBounds(0, 0, 0, 0);
//                       }
//                       background.draw(c);
//                       icon.draw(c);
//                   }
//               };
//       ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
//       itemTouchHelper.attachToRecyclerView(recyclerView);


    }

/*    private boolean onDocNoKeyEvent() {
        DocNo = edDocNo.getText().toString();

        if (DocNo.isEmpty() || DocNo.trim().length() == 0) {
            showToast(String.format("%s required", getString(promptResource)));
            return false;
        }
        return true;
    }*/

    private void OnNextClick() {
        //Validate();



        new Thread(new Runnable() {
            public void run() {
                boolean status = false;
                String TEMP_FILENAME = "20210523170538.png";
                status = ftpService.ftpUpload(
                        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath() + "/Camera/" + TEMP_FILENAME,
                        "test.png", "/", getContext());
//                if (status == true) {
//                    showToast("Upload success!");
//                } else {
//                    showToast("Upload failed!");
//                }
            }
        }).start();

    }

    private void Validate() {
//        String errMessage;
//        DocNo = edDocNo.getText().toString();
//        if (DocNo.length() == 0) {
//            errMessage = String.format("%s required", "PO/ASN/Job no");
//            showToast(errMessage);
//            edDocNo.setError(errMessage);
//            return;
//        }
        OpenLogisticLabel();

    }


    private void onCameraClick(){
//        if (location == null) {
//            showToast("Select Location");
//            return;
//        }

        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, VISIT_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (resultCode != Activity.RESULT_OK) return;

        if (requestCode == VISIT_IMAGE) {
            photo = (Bitmap) intent.getExtras().get("data");
            SavePhoto();
        }
    }

    private void SavePhoto() {
        String photoFileName = getPhotoFileName();
        Log.d("photoFileName##",photoFileName);
        if (photo != null) {
//            pngBase64 = Utils.getBase64EncodedPNG(photo);
            try (FileOutputStream out = new FileOutputStream(photoFileName)) {
                photo.compress(Bitmap.CompressFormat.PNG, 100, out);
                fetchPhotos();
                // PNG is a lossless format, the compression factor (100) is ignored
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String getPhotoFileName() {
        SimpleDateFormat dF = new SimpleDateFormat(Utils.DATETIME_NUM_FORMAT, Locale.getDefault());
        String fileName = dF.format(new Date()) + ".png";
        File outputFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath() + "/Camera/");
        try {
            if (!outputFile.exists() && !outputFile.isDirectory()) {
                outputFile.mkdir();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return outputFile.getAbsolutePath() + "/" + fileName;
    }

    private void OpenLogisticLabel() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putString(AppConstants.SELECTED_ID, DocNo);
        bundle.putSerializable(AppConstants.SELECTED_CODE, "ordType");
        bundle.putLong(AppConstants.PARENT_ID, HDR_ID);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_logistic_label, bundle);
    }

    @Override
    public boolean onLongClick(View view) {

        RecyclerView.ViewHolder holder = (RecyclerView.ViewHolder) view.getTag();
        if (view.getId() == holder.itemView.getId()) {

        }

        //Log.d("tag## ", String.valueOf(recyclerView.getChildAdapterPosition(view)));

        String photoSelected = (String) view.getTag();

//
//
//       Log.d("selected##",photoSelected);
////
//        if (photoSelected == null) {
//            return false;
//        }
//
//        Snackbar snackbar = Snackbar
//                .make(root, "Do you want to delete photo ?", Snackbar.LENGTH_LONG)
//                .setAction("YES", new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Snackbar mSnackbar = Snackbar.make(root, "Successfully deleted!", Snackbar.LENGTH_SHORT);
//                        mSnackbar.show();
//                        File file = new File(photoSelected);
//                        file.delete();
//                        if(file.exists()){
//                            try {
//                                file.getCanonicalFile().delete();
//                            } catch (IOException e) {
//                                e.printStackTrace();
//                            }
//                            if(file.exists()){
//                                getContext().deleteFile(file.getName());
//                            }
//                        }
//                        fetchPhotos();
//                    }
//
//                });
//
//        snackbar.show();

        return true;
    }


//    private void DownloadPO_HDR(final String docNo, final String docType) {
//        JsonObject requestObject = ServiceUtils.GoodsReceipt.getGoodsReceiptHdr(docNo, docType);
//
//
//        showProgress(false);
//        App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
//            @Override
//            public void success(GenericRetResponse genericRetResponse, Response response) {
//                dismissProgress();
//                if (genericRetResponse.getErrCode().equals("S")) {
//                    String xmlDoc = genericRetResponse.getXmlDoc();
//                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
//                        showToast(" Error: No data received.");
//                        return;
//                    }
//                    try {
//                        PO_HDR[] po_hdr = new Gson().fromJson(xmlDoc, PO_HDR[].class);
//                        if (po_hdr==null || po_hdr.length<=0) {
//                            showToast(" Error: No data received.");
//                            return;
//                        }
//                        if (po_hdr.length > 0) {
//                            ordType = new DocType();
//                            ordType.CODE = po_hdr[0]. ORTYP;
//                            ordType.NAME = po_hdr[0]. ORTYP;
//                        }
//                        App.getDatabaseClient().getAppDatabase().genericDao().deletePO_HDR(docNo, ordType.CODE);
//                        App.getDatabaseClient().getAppDatabase().genericDao().insertPO_HDR(po_hdr);
//                        DownloadPO_DET(docNo, ordType.CODE);
//                    } catch (Exception e) {
//                        Log.d(App.TAG, e.toString());
//                    }
//                } else {
//                    String msg = genericRetResponse.getErrMessage();
//                    Log.d(App.TAG, msg);
//                    showToast(msg);
//                }
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                dismissProgress();
//                String msg = error.getMessage();
//                Log.d(App.TAG, msg);
//                showToast(msg);
//            }
//        });
//    }
//
//    private void DownloadPO_DET(final String docNo, final String docType) {
//        JsonObject requestObject = ServiceUtils.GoodsReceipt.getGoodsReceiptDet(docNo, docType);
//
//        showProgress(false);
//        App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
//            @Override
//            public void success(GenericRetResponse genericRetResponse, Response response) {
//                dismissProgress();
//                if (genericRetResponse.getErrCode().equals("S")) {
//                    String xmlDoc = genericRetResponse.getXmlDoc();
//                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
//                        showToast(" Error: No data received.");
//                        return;
//                    }
//                    try {
//                        PO_DET[] poDet = new Gson().fromJson(xmlDoc, PO_DET[].class);
//                        App.getDatabaseClient().getAppDatabase().genericDao().deletePO_DET(docNo, docType);
//                        App.getDatabaseClient().getAppDatabase().genericDao().insertPO_DET(poDet);
//                        OpenDocHeader();
//                    } catch (Exception e) {
//                        Log.d(App.TAG, e.toString());
//                    }
//                } else {
//                    String msg = genericRetResponse.getErrMessage();
//                    Log.d(App.TAG, msg);
//                    showToast(msg);
//                }
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                dismissProgress();
//                String msg = error.getMessage();
//                Log.d(App.TAG, msg);
//                showToast(msg);
//            }
//        });
//    }
}
