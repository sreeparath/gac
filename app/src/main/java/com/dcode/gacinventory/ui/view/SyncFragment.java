package com.dcode.gacinventory.ui.view;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.network.service.ServiceUtils;
import com.example.gacinventory.R;



public class SyncFragment extends BaseFragment {
    private View root;
    private Switch swPrintFormat,swPrintList,swLotStatus,swRWO,swSTK221,swSTK221Q,swSTK222,swSTK222Q,swSTK411Q,swSTK415Q,swSTKcount,swInvestReason,swMasterList,swCountry;
//    private Switch swVisits;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);
        }

        root = inflater.inflate(R.layout.fragment_sync, container, false);
        setHasOptionsMenu(true);
        setupView();
        return root;
    }

    private void setupView() {
        Button btnBack = root.findViewById(R.id.btnBack);
        btnBack.setOnClickListener(v -> getActivity().onBackPressed());
        Button btnDownload = root.findViewById(R.id.btnDownload);
        btnDownload.setOnClickListener(v -> OnSynchroniseClick());

        swPrintList = root.findViewById(R.id.swPrintList);
        swPrintFormat = root.findViewById(R.id.swPrintFormat);
        swLotStatus  = root.findViewById(R.id.swLotStatus);
        swInvestReason = root.findViewById(R.id.swInvestReason);
        swCountry = root.findViewById(R.id.swCountry);
        swMasterList = root.findViewById(R.id.swMasterList);
        LinearLayout llprintlist = root.findViewById(R.id.llprintlist);
        llprintlist.setOnClickListener(v -> OnPrintListClick());
        LinearLayout llPrintFormat = root.findViewById(R.id.llPrintFormat);
        llPrintFormat.setOnClickListener(v -> {
            OnPrintFormatClick();
        });
        LinearLayout ilLotStatus = root.findViewById(R.id.ilLotStatus);
        ilLotStatus.setOnClickListener(v -> {
            OnLotStatusClick();
        });

        LinearLayout ilInvestReason = root.findViewById(R.id.ilInvestReason);
        ilInvestReason.setOnClickListener(v -> {
            OnInvestReasonClick();
        });

        LinearLayout ilCountry = root.findViewById(R.id.ilCountry);
        ilCountry.setOnClickListener(v -> {
            OnCountryClick();
        });

        LinearLayout ilMaster = root.findViewById(R.id.ilMaster);
        ilMaster.setOnClickListener(v -> {
            OnMasterClick();
        });

        if (ModuleID == AppConstants.UPLOADS) {

        } else if (ModuleID == AppConstants.DOWNLOADS) {
            llprintlist.setVisibility(View.VISIBLE);
            swPrintList.setChecked(false);
            llPrintFormat.setVisibility(View.VISIBLE);
            swPrintFormat.setChecked(false);
            ilLotStatus.setVisibility(View.VISIBLE);
            swLotStatus.setChecked(false);
            ilInvestReason.setVisibility(View.VISIBLE);
            swInvestReason.setChecked(false);
            ilCountry.setVisibility(View.VISIBLE);
            swCountry.setChecked(false);
            ilMaster.setVisibility(View.VISIBLE);
            swMasterList.setChecked(false);
        }

        TextView tvMessage = root.findViewById(R.id.tvMessage);
        tvMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // nothing to do.
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // nothing to do.
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().contains("failed")) {
                    shortVibration();
                }
            }
        });
    }

    private void OnSynchroniseClick() {
        ServiceUtils.SyncActivity syncActivity = new ServiceUtils.SyncActivity(getContext(), false);

        TextView textView = root.findViewById(R.id.tvMessage);
        textView.setText(R.string.empty_string);

        if (swPrintList.isChecked()) {
            syncActivity.DownloadPrinterList(AppConstants.DatabaseEntities.PRINTER_LIST_SERVICE);
        }

        if (swPrintFormat.isChecked()) {
            syncActivity.DownloadPrintFormats(AppConstants.DatabaseEntities.PRINT_FORMATS);
        }

        if (swLotStatus.isChecked()) {
            syncActivity.DownloadLotStatus(AppConstants.DatabaseEntities.LOT_STATUS);
        }
        if (swInvestReason.isChecked()) {
            syncActivity.DownloadInvestReason(AppConstants.DatabaseEntities.INVEST_REASON);
        }
        if (swCountry.isChecked()) {
            syncActivity.DownloadCountryList(AppConstants.DatabaseEntities.COUNTRY_LIST);
        }

        if (swMasterList.isChecked()) {
            syncActivity.DownloadMasterList(AppConstants.DatabaseEntities.MASTER_LIST,"STORAGE_TYPE","190");
        }

        if (ModuleID == AppConstants.UPLOADS) {
//            if (swGrn.isChecked()) {
//                syncActivity.updateProgressMessage("Uploading..."+AppConstants.DatabaseEntities.GOODS_RECEIPT_HDR.toString());
//                syncActivity.Uploads(AppConstants.DatabaseEntities.GOODS_RECEIPT_HDR,0);
//            }
//            if (swWO.isChecked()) {
//                syncActivity.updateProgressMessage("Uploading..."+AppConstants.DatabaseEntities.WORK_ORDER.toString());
//                syncActivity.Uploads(AppConstants.DatabaseEntities.WORK_ORDER,0);
//            }
//            if (swRWO.isChecked()) {
//                syncActivity.updateProgressMessage("Uploading..."+AppConstants.DatabaseEntities.REVERSE_WORD_ORDER.toString());
//                syncActivity.Uploads(AppConstants.DatabaseEntities.REVERSE_WORD_ORDER,0);
//            }
//            if (swSTK221.isChecked()) {
//                syncActivity.updateProgressMessage("Uploading..."+AppConstants.DatabaseEntities.STOCK_TRANSFER_221.toString());
//                syncActivity.Uploads(AppConstants.DatabaseEntities.STOCK_TRANSFER_221,0);
//            }
//            if (swSTK221Q.isChecked()) {
//                syncActivity.updateProgressMessage("Uploading..."+AppConstants.DatabaseEntities.STOCK_TRANSFER_221Q.toString());
//                syncActivity.Uploads(AppConstants.DatabaseEntities.STOCK_TRANSFER_221Q,0);
//            }
//            if (swSTK222.isChecked()) {
//                syncActivity.updateProgressMessage("Uploading..."+AppConstants.DatabaseEntities.STOCK_TRANSFER_222.toString());
//                syncActivity.Uploads(AppConstants.DatabaseEntities.STOCK_TRANSFER_222,0);
//            }
//            if (swSTK222Q.isChecked()) {
//                syncActivity.updateProgressMessage("Uploading..."+AppConstants.DatabaseEntities.STOCK_TRANSFER_222Q.toString());
//                syncActivity.Uploads(AppConstants.DatabaseEntities.STOCK_TRANSFER_222Q,0);
//            }
//            if (swSTK411Q.isChecked()) {
//                syncActivity.updateProgressMessage("Uploading..."+AppConstants.DatabaseEntities.STOCK_TRANSFER_411Q.toString());
//                syncActivity.Uploads(AppConstants.DatabaseEntities.STOCK_TRANSFER_411Q,0);
//            }
//            if (swSTK415Q.isChecked()) {
//                syncActivity.updateProgressMessage("Uploading..."+AppConstants.DatabaseEntities.STOCK_TRANSFER_415Q.toString());
//                syncActivity.Uploads(AppConstants.DatabaseEntities.STOCK_TRANSFER_415Q,0);
//            }
//            if (swSTKcount.isChecked()) {
//                syncActivity.updateProgressMessage("Uploading..."+AppConstants.DatabaseEntities.STOCK_COUNT.toString());
//                syncActivity.Uploads(AppConstants.DatabaseEntities.STOCK_COUNT,0);
//            }
        }

//        if (swVisits.isChecked()) {
//            syncActivity.Uploads(AppConstants.DatabaseEntities.VISITS_HDR, -1);
//        }
    }

    private void OnPrintFormatClick() {
        swPrintFormat.setChecked(!swPrintFormat.isChecked());
    }

    private void OnLotStatusClick() {
        swLotStatus.setChecked(!swLotStatus.isChecked());
    }

    private void OnPrintListClick() {
        swPrintList.setChecked(!swPrintList.isChecked());
    }

    private void OnInvestReasonClick() {
        swInvestReason.setChecked(!swInvestReason.isChecked());
    }

    private void OnCountryClick() {
        swCountry.setChecked(!swCountry.isChecked());
    }

    private void OnMasterClick() {
        swMasterList.setChecked(!swMasterList.isChecked());
    }

//    private void onVisitsClick() {
//        swVisits.setChecked(!swVisits.isChecked());
//    }
}
