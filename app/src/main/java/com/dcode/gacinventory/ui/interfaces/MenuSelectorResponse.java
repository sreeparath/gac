package com.dcode.gacinventory.ui.interfaces;

/**
 * Created byVijildhas on 05/08/2020.
 */
public interface MenuSelectorResponse {
    /**
     * @param selectedMenu
     */
    void onSelectedMenu(int selectedMenu);

    void onNavigateOptionASelector();

    void onNavigateOptionBSelector();

    /**
     * @param menuName
     */
    void onSelectedBottomMenu(String menuName);


}