package com.dcode.gacinventory.ui.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.data.model.INBOUND_HEADER;
import com.dcode.gacinventory.data.model.MENU;
import com.dcode.gacinventory.ui.adapter.ConfirmListTabAdapter;
import com.dcode.gacinventory.ui.adapter.ObjectsAdapter;
import com.example.gacinventory.R;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class ConfirmTabFragment extends BaseFragment implements View.OnClickListener {

    View root;
    ObjectsAdapter detAdapter;
    TabLayout tabLayout;
    ViewPager viewPager;
    INBOUND_HEADER inboundHeader;
    String jobNo;
    ConfirmListTabAdapter adapter;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if (this.getArguments() != null) {
            Bundle bundle = this.getArguments();
            //parentId = bundle.containsKey(AppConstants.PARENT_ID) ? bundle.getInt(AppConstants.PARENT_ID) : -1;
            jobNo = bundle.containsKey(AppConstants.SELECTED_ID) ? bundle.getString(AppConstants.SELECTED_ID, "") : "";
            inboundHeader  = (INBOUND_HEADER) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
            //logistic_label_sumbit = bundle.containsKey(AppConstants.SELECTED_OBJECT_NEW) ? (LOGISTIC_LABEL_SUMBIT) bundle.getSerializable(AppConstants.SELECTED_OBJECT_NEW) : null;
        }


        root = inflater.inflate(R.layout.fragment_confirmation_list_tab, container, false);



        setupView();

        return root;
    }

    @Override
    public void onClick(View view) {

    }

    void setupView(){
        setHeaderToolbar();
        tabLayout=(TabLayout) root.findViewById(R.id.tabLayout);
        viewPager=(ViewPager) root.findViewById(R.id.viewPager);
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.staging_status)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.put_away)));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        adapter = new ConfirmListTabAdapter(getContext(),getChildFragmentManager(), tabLayout.getTabCount(),this.getArguments());
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(2);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                viewPager.removeViewAt(tab.getPosition());
            }
        });

    }

    private void setHeaderToolbar() {
        if (inboundHeader != null) {
            Toolbar toolbar = root.findViewById(R.id.toolbar);
            TextView info1 = root.findViewById(R.id.info1);
            info1.setText(App.currentUser.USER_ID);
            TextView info2 = root.findViewById(R.id.info2);
            info2.setText(inboundHeader.JOB_NO + " - " + inboundHeader.PRIN_NAME );
        }
    }


}