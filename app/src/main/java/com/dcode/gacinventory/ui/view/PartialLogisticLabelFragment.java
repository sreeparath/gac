package com.dcode.gacinventory.ui.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.data.model.INBOUND_HEADER;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SCAN;
import com.dcode.gacinventory.network.model.GenericRetResponse;
import com.dcode.gacinventory.network.service.ServiceUtils;
import com.example.gacinventory.R;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PartialLogisticLabelFragment extends BaseFragment {
    private TextInputEditText edGacScan,edBarcode;
//    private AutoCompleteTextView tvDocTypes;
//    private ArrayAdapter<DocType> objectsAdapter;
    private int promptResource;
    private String DocNo;
    private long HDR_ID;
    private RadioGroup rdoGroup;
    private LOGISTIC_LABEL_SCAN[] items_prints;
    private INBOUND_HEADER inbound_header;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                //DocNo = bundle.getString(AppConstants.SELECTED_ID);
                //barcode = bundle.getString(AppConstants.MASTER_ID);
                inbound_header = (INBOUND_HEADER) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
               // Log.d("items_prints##",items_prints.toString());
            }
        }



        View root = inflater.inflate(R.layout.fragment_partial_logistic_label, container, false);
        //ImageView imgDocNo = root.findViewById(R.id.imgDocNo);
        //imgDocNo.setImageResource(R.drawable.ic_005);

        edBarcode = root.findViewById(R.id.edBarcode);
        edBarcode.requestFocus();
        edGacScan = root.findViewById(R.id.edBarcode);
//        edDocNo.setOnKeyListener((v, keyCode, event) -> {
//            if(keyCode == KeyEvent.KEYCODE_ENTER){
//                if(event.getAction() == KeyEvent.ACTION_DOWN){
//                    return onScanCodeKeyEvent();
//                }else{
//                    return true;
//                }
//            }else{
//                return false;
//            }
//        });

        MaterialButton btPhoto = root.findViewById(R.id.btnBack);
        btPhoto.setOnClickListener(v -> OnBackClick());

        MaterialButton btnPrint = root.findViewById(R.id.btNext);
        btnPrint.setOnClickListener(v -> OnNextClick());

/*        tvDocTypes = root.findViewById(R.id.acDocType);
        tvDocTypes.setOnItemClickListener((parent, view, position, id) -> {
            if (position > 0) {
                ordType = objectsAdapter.getItem(position);
            } else {
                ordType = null;
            }
        });*/

        return root;
    }

/*    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getDocTypes();
    }*/

/*    private boolean onDocNoKeyEvent() {
        DocNo = edDocNo.getText().toString();

        if (DocNo.isEmpty() || DocNo.trim().length() == 0) {
            showToast(String.format("%s required", getString(promptResource)));
            return false;
        }
        return true;
    }*/

    private void OnNextClick() {
        Validate();
    }

    private void OnBackClick() {
        getActivity().onBackPressed();
    }

    private void Validate() {
        String errMessage;

        String barcode = edBarcode.getText().toString();
        if (barcode.length() == 0) {
            errMessage = String.format("%s required", "Barcode");
            showToast(errMessage);
            edBarcode.setError(errMessage);
            return;
        }

        DownloadPartialLogistic_Label(barcode);
    }



/*    private void getDocTypes() {
        List<DocType> orTypList = App.getDatabaseClient().getAppDatabase().genericDao().getAllOrTyps();
        objectsAdapter = new ArrayAdapter<>(requireContext(),
                R.layout.dropdown_menu_popup_item, orTypList);
        tvDocTypes.setAdapter(objectsAdapter);
        tvDocTypes.setSelection(0);
        tvDocTypes.setThreshold(100);

        objectsAdapter.notifyDataSetChanged();
    }*/

    private void OpenPartialGACScan() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putString(AppConstants.SELECTED_ID, DocNo);
        bundle.putString(AppConstants.MASTER_ID, edBarcode.getText().toString());
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, items_prints[0]);
        bundle.putLong(AppConstants.PARENT_ID, HDR_ID);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_partial_gac_scan, bundle);
    }

    private void DownloadPartialLogistic_Label(String barcode) {
        if (barcode.length() <= 0)  {
            return;
        }

        JsonObject requestObject = ServiceUtils.LogisticLabel.getLogisticLabel(barcode,barcode,"","","M","","");

        showProgress(false);
        App.getNetworkClient().getAPIService().getRetGeneric(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (!genericRetResponse.getErrCode().equals("E")) {
                    String xmlDoc = genericRetResponse.getJsonString();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showAlert("Error", "No data received.");
                        return;
                    }
                    try {
                        items_prints = new Gson().fromJson(xmlDoc, LOGISTIC_LABEL_SCAN[].class);
                        if(items_prints.length==0){
                            showAlert("Error", "No data received.");
                            return;
                        }
                        OpenPartialGACScan();

                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                        showAlert("Error", e.getMessage());
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showAlert("Error", msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showAlert("Error", msg);
            }
        });
    }
}
