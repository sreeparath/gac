package com.dcode.gacinventory.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.gacinventory.data.model.SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS;
import com.dcode.gacinventory.data.model.SP_SCN_AN_OUT_PICK_PENDING;
import com.example.gacinventory.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

/**
 * @Created by: VIJIL DHAS A.S
 * @Date: 18-09-2021
 */
public class PickConfirmationAdapter extends RecyclerView.Adapter<PickConfirmationAdapter.RecycleViewHolder> {
    private List<SP_SCN_AN_OUT_PICK_PENDING> listItemDetails;
    public ArrayList<SP_SCN_AN_OUT_PICK_PENDING> privatearray;
    private Context context;

    public PickConfirmationAdapter(Context context,
                                   List<SP_SCN_AN_OUT_PICK_PENDING> listItemDetails) {
        this.listItemDetails = listItemDetails;
        privatearray = new ArrayList<>();
        this.context = context;
    }

    @NonNull
    @Override
    public PickConfirmationAdapter.RecycleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PickConfirmationAdapter.RecycleViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pick_confirmation_items, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull PickConfirmationAdapter.RecycleViewHolder holder, @SuppressLint("RecyclerView") int position) {
        ((RecycleViewHolder) holder).txtPrinciple.setText(listItemDetails.get(position).PRIN_CODE);
        ((RecycleViewHolder) holder).txtJobNumber.setText(listItemDetails.get(position).JOB_NO);
        ((RecycleViewHolder) holder).txtTruckId.setText(listItemDetails.get(position).TRUCK_ID);
        ((RecycleViewHolder) holder).checkBox.setChecked(listItemDetails.get(position).SELECTED == 1);

        ((RecycleViewHolder) holder).checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    listItemDetails.get(position).SELECTED = 1;
                } else {
                    listItemDetails.get(position).SELECTED = 0;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listItemDetails.size();
    }

    public List<SP_SCN_AN_OUT_PICK_PENDING> filter(String charText,
                                                   int filterMenuId) {
        charText = charText.toLowerCase(Locale.getDefault());
        listItemDetails.clear();
        if (charText.length() == 0) {
            listItemDetails.addAll(privatearray);
        } else {
            for (SP_SCN_AN_OUT_PICK_PENDING c : privatearray) {
                switch (filterMenuId) {
                    case 100:
                    case -1: // default selection
                        if (c.PRIN_CODE.toLowerCase(Locale.getDefault()).startsWith(charText) ||
                                c.JOB_NO.toLowerCase(Locale.getDefault()).startsWith(charText) ||
                                c.TRUCK_ID.toLowerCase(Locale.getDefault()).startsWith(charText)) {
                            listItemDetails.add(c);
                        }
                        break;
                    case 101:
                        if (c.PRIN_CODE.toLowerCase(Locale.getDefault()).startsWith(charText)) {
                            listItemDetails.add(c);
                        }
                        break;
                    case 102:
                        if (c.JOB_NO.toLowerCase(Locale.getDefault()).startsWith(charText)) {
                            listItemDetails.add(c);
                        }
                        break;
                    case 103:
                        if (c.TRUCK_ID.toLowerCase(Locale.getDefault()).startsWith(charText)) {
                            listItemDetails.add(c);
                        }
                        break;
                }

            }
        }
        notifyDataSetChanged();
        return listItemDetails;

    }

    /**
     * acceding or descending order
     */
    public void sortBySelected(String order) {
        // here selected is 1 so the order is revised
        if (order.equals("ASCENDING_ORDER")) {
            Collections.sort(listItemDetails, new Comparator<SP_SCN_AN_OUT_PICK_PENDING>() {
                @Override
                public int compare(SP_SCN_AN_OUT_PICK_PENDING p1, SP_SCN_AN_OUT_PICK_PENDING p2) {
                    return p2.SELECTED - p1.SELECTED; // Descending
                }
            });

        } else {
            Collections.sort(listItemDetails, new Comparator<SP_SCN_AN_OUT_PICK_PENDING>() {
                @Override
                public int compare(SP_SCN_AN_OUT_PICK_PENDING p1, SP_SCN_AN_OUT_PICK_PENDING p2) {
                    return p1.SELECTED - p2.SELECTED; // Ascending
                }
            });
        }

        notifyDataSetChanged();
    }

    /**
     * acceding or descending order
     */
    public void sortByPrinCode(String order) {
        if (order.equals("ASCENDING_ORDER")) {
            Collections.sort(listItemDetails, new Comparator<SP_SCN_AN_OUT_PICK_PENDING>() {
                public int compare(SP_SCN_AN_OUT_PICK_PENDING o1, SP_SCN_AN_OUT_PICK_PENDING o2) {
                    return o1.PRIN_CODE.toLowerCase().compareTo(o2.PRIN_CODE.toLowerCase());
                }
            });
        } else {
            Collections.reverse(listItemDetails);
        }

        notifyDataSetChanged();
    }

    public void sortByJobNumber(String order) {
        if (order.equals("ASCENDING_ORDER")) {
            Collections.sort(listItemDetails, new Comparator<SP_SCN_AN_OUT_PICK_PENDING>() {
                public int compare(SP_SCN_AN_OUT_PICK_PENDING o1, SP_SCN_AN_OUT_PICK_PENDING o2) {
                    return o1.JOB_NO.toLowerCase().compareTo(o2.JOB_NO.toLowerCase());
                }
            });
        } else {
            Collections.reverse(listItemDetails);
        }

        notifyDataSetChanged();
    }

    public void sortByTruckID(String order) {
        if (order.equals("ASCENDING_ORDER")) {
            Collections.sort(listItemDetails, new Comparator<SP_SCN_AN_OUT_PICK_PENDING>() {
                public int compare(SP_SCN_AN_OUT_PICK_PENDING o1, SP_SCN_AN_OUT_PICK_PENDING o2) {
                    return o1.TRUCK_ID.toLowerCase().compareTo(o2.TRUCK_ID.toLowerCase());
                }
            });
        } else {
            Collections.reverse(listItemDetails);
        }

        notifyDataSetChanged();
    }

    public List<SP_SCN_AN_OUT_PICK_PENDING> getListItems() {
        return listItemDetails;
    }

    public void addItems(List<SP_SCN_AN_OUT_PICK_PENDING> formattedList) {
        listItemDetails.addAll(formattedList);
        privatearray.addAll(listItemDetails);
        notifyDataSetChanged();
    }

    public void resetList(List<SP_SCN_AN_OUT_PICK_PENDING> baseFormattedList) {
        listItemDetails.clear();
        privatearray.clear();
        notifyDataSetChanged();
        addItems(baseFormattedList);
    }

    static class RecycleViewHolder extends RecyclerView.ViewHolder {
        private TextView txtPrinciple;
        private TextView txtJobNumber;
        private TextView txtTruckId;
        private CheckBox checkBox;

        public RecycleViewHolder(@NonNull View itemView) {
            super(itemView);
            txtPrinciple = itemView.findViewById(R.id.txt_principle);
            txtJobNumber = itemView.findViewById(R.id.txt_job_number);
            txtTruckId = itemView.findViewById(R.id.txt_truck_id);
            checkBox = itemView.findViewById(R.id.checkbox_select);
        }
    }


}
