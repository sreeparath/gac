package com.dcode.gacinventory.ui.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.data.model.CONFIRMATION_LIST;
import com.dcode.gacinventory.data.model.INBOUND_HEADER;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SCAN;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SUMBIT;
import com.dcode.gacinventory.data.model.PRINTER_LIST_SERVICE;
import com.dcode.gacinventory.data.model.PRINT_FORMAT;
import com.dcode.gacinventory.data.model.VIEW_IMAGE;
import com.dcode.gacinventory.network.model.GenericRetResponse;
import com.dcode.gacinventory.network.service.ServiceUtils;
import com.dcode.gacinventory.ui.adapter.ConfirmationListAdapter;
import com.dcode.gacinventory.ui.adapter.ImageGlideAdapter;
import com.dcode.gacinventory.ui.adapter.ItemsLabelsPrintGeneratedAdapter;
import com.example.gacinventory.R;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ViewImagesFragment
        extends BaseFragment
        implements View.OnClickListener {

    private String jobNo;
    private View root;
    private AutoCompleteTextView tvPrinter;
    private AutoCompleteTextView tvLabel;
    //private List<GENERATE_LABELS> itemsPrintList;
    //private List<GR_ITEM_PRINT_TYP> grItemPrintTyp;
    private ItemsLabelsPrintGeneratedAdapter labelsPrintGeneratedAdapter;
    private ArrayAdapter<PRINTER_LIST_SERVICE> printersAdapter;
    private ArrayAdapter<PRINT_FORMAT> labelsAdapter;
    private PRINTER_LIST_SERVICE barcode_printers;
    private PRINT_FORMAT label_formats;
    private RadioGroup rdoGroup;
    private TextInputEditText edMac;
    private TextInputLayout ilPrinter;
    private TextInputLayout ilmac;
    private TextView edInfo;
    private CheckBox chkSelect;
    private int noGacScan;
    private List<CONFIRMATION_LIST> itemsPrintList;
    private ImageGlideAdapter imageGlideAdapter;
    private INBOUND_HEADER inboundHeader;
    private LOGISTIC_LABEL_SCAN items_prints;
    int parentId = -1;
    //private LOGISTIC_LABEL_SUMBIT logistic_label_sumbit;
    //private PUTAWAY_SUMBIT putaway_sumbit;
    private ArrayAdapter<String> orderByAdapter;
    private AutoCompleteTextView orderBy;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);


        jobNo = "";
        if (this.getArguments() != null) {
            Bundle bundle = this.getArguments();
            //parentId = bundle.containsKey(AppConstants.PARENT_ID) ? bundle.getInt(AppConstants.PARENT_ID) : -1;
            jobNo = bundle.containsKey(AppConstants.SELECTED_ID) ? bundle.getString(AppConstants.SELECTED_ID, "") : "";
            //inboundHeader  = (INBOUND_HEADER) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
            inboundHeader = bundle.containsKey(AppConstants.SELECTED_OBJECT) ? (INBOUND_HEADER) bundle.getSerializable(AppConstants.SELECTED_OBJECT) : null;
        }

        root = inflater.inflate(R.layout.fragment_image_list, container, false);
        setupView();
        return root;
    }

    private void setupView(){

        RecyclerView recyclerView = root.findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));


        //labelsPrintGeneratedAdapter = new ItemsLabelsPrintGeneratedAdapter(new ArrayList<>(), this);
        //recyclerView.setAdapter(labelsPrintGeneratedAdapter);
        //FillGACPrintList();

        imageGlideAdapter = new ImageGlideAdapter(getContext(),new ArrayList<>(),this);
        recyclerView.setAdapter(imageGlideAdapter);
        //recyclerView.setNestedScrollingEnabled(false);



        MaterialButton btnPrint = root.findViewById(R.id.btnPrint);
        btnPrint.setOnClickListener(v -> {

        });

        MaterialButton btnBack = root.findViewById(R.id.btnBack);
        btnBack.setOnClickListener(v -> {
//            Bundle bundle = new Bundle();
//            bundle.putSerializable(AppConstants.SELECTED_OBJECT, inboundHeader);
//            ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_logistic_label, bundle);
                }
        );


        edInfo = root.findViewById(R.id.edInfo);
        //edInfo.setText("Total GACScan ID : "+noGacScan);

//        chkSelect = root.findViewById(R.id.chkSelect);
//        chkSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                //set your object's last status
//                if(itemsPrintList.size()>0){
//                    if(chkSelect.isChecked()){
//                        confirmationListAdapter.selectAll(true);
//                        confirmationListAdapter.notifyDataSetChanged();
//                    }else{
//                        confirmationListAdapter.selectAll(false);
//                        confirmationListAdapter.notifyDataSetChanged();
//                    }
//                }
//            }
//        });

    }

    private void OpenGACScan() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, items_prints);
        //bundle.putSerializable(AppConstants.SELECTED_OBJECT_NEW,inboundHeader);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_gac_scan, bundle);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //showToast("in this tab onViewCreated");
        DownloadImageContent();


    }


    @Override
    public void onClick(View view) {

    }




    private void DownloadImageContent() {
//        if (jobNo.length()<= 0 ) {
//            showAlert("Warning","Job shouldn't be empty!");
//            return;
//        }
//        if(inboundHeader==null){
//            showAlert("Warning","Job Header shouldn't be empty!");
//            return;
//        }

        JsonObject requestObject = ServiceUtils.LogisticLabel.getImageDetails("","");

        Log.d("requestObject##",requestObject.toString());

        showProgress(false);
        App.getNetworkClient().getAPIService().getRetGeneric(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("S")) {
                    String xmlDoc = genericRetResponse.getJsonString();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showAlert("Error", "No data received.");
                        return;
                    }
                    try {
                        VIEW_IMAGE[] viewImages = new Gson().fromJson(xmlDoc, VIEW_IMAGE[].class);
                        loadImages(Arrays.asList(viewImages));
                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                        showAlert("Error", e.getMessage());
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showAlert("Error", msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showAlert("Error", msg);
            }
        });
    }

    private void loadImages(List<VIEW_IMAGE> viewImage) {

        if (viewImage != null ) {
            imageGlideAdapter.addItems(viewImage);
            imageGlideAdapter.notifyDataSetChanged();
            //edInfo.setText("Total Items : "+confirmationListAdapter.getItemCount());
        }else {
            //showToast(" Alert : No Staging data found.");
            showAlert("Alert", "No Staging data found.");
        }
    }


}
