package com.dcode.gacinventory.ui.view;

import android.app.Service;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.common.AppVariables;
import com.dcode.gacinventory.data.model.MENU;
import com.dcode.gacinventory.ui.adapter.ObjectsAdapter;
import com.example.gacinventory.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;


public class HomeFragment extends BaseFragment implements View.OnClickListener {

    View root;
    ObjectsAdapter detAdapter;



    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);


        root = inflater.inflate(R.layout.fragment_home, container, false);

                RecyclerView recyclerView = root.findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));




        List menuList = new ArrayList<MENU>();
        MENU menu = new MENU();
        menu.MENU_ID = 1;
        menu.MENU_CODE = "IN";
        menu.MENU_NAME = "Inbound";
        menu.MENU_DESC = "Inbound Transactions";
        menu.MENU_ICON  = R.drawable.ic_inbound;
        menuList.add(menu);


        menu = new MENU();
        menu.MENU_ID = 2;
        menu.MENU_CODE = "SC";
        menu.MENU_NAME = "Full Staging";
        menu.MENU_DESC = "Full Staging Confirmation";
        //menuList.add(menu);

        menu = new MENU();
        menu.MENU_ID = 3;
        menu.MENU_CODE = "INP";
        menu.MENU_NAME = "Inbound Partial";
        menu.MENU_DESC = "Inbound Partial Transactions";
        //menuList.add(menu);

        menu = new MENU();
        menu.MENU_ID = 4;
        menu.MENU_CODE = "SPC";
        menu.MENU_NAME = "Partial Staging";
        menu.MENU_DESC = "Partial Staging Confirmation";
        //menuList.add(menu);


        menu = new MENU();
        menu.MENU_ID = 2;
        menu.MENU_CODE = "OUT";
        menu.MENU_NAME = "Outbound";
        menu.MENU_DESC = "Outbound Transactions";
        menu.MENU_ICON  = R.drawable.ic_warehouse_outbound;
        menuList.add(menu);

        menu = new MENU();
        menu.MENU_ID = 6;
        menu.MENU_CODE = "BT";
        menu.MENU_NAME = "Bin to Bin Transfer";
        menu.MENU_DESC = "Bin Transfer Transactions";
        menu.MENU_ICON  = R.drawable.ic_warehousein_transfer;
        menuList.add(menu);

        menu = new MENU();
        menu.MENU_ID = 7;
        menu.MENU_CODE = "ST";
        menu.MENU_NAME = "Stock Take";
        menu.MENU_DESC = "Stock Take Transactions";
        menu.MENU_ICON  = R.drawable.ic_warehouse_stock_take;
        menuList.add(menu);

        menu = new MENU();
        menu.MENU_ID = 8;
        menu.MENU_CODE = "DW";
        menu.MENU_NAME = "Download";
        menu.MENU_DESC = "Download masters for Transactions";
        menu.MENU_ICON  = R.drawable.ic_download;
        menuList.add(menu);

        menu = new MENU();
        menu.MENU_ID = 9;
        menu.MENU_CODE = "ST";
        menu.MENU_NAME = "Setting";
        menu.MENU_DESC = "Setting for application";
        menu.MENU_ICON  = R.drawable.ic_settings;
        menuList.add(menu);

        menu = new MENU();
        menu.MENU_ID = 10;
        menu.MENU_CODE = "LT";
        menu.MENU_NAME = "Logout";
        menu.MENU_DESC = "Exit application";

        menuList.add(menu);


        detAdapter = new ObjectsAdapter(getContext(),menuList,  this);
        recyclerView.setAdapter(detAdapter);

        return root;
    }

    @Override
    public void onClick(View view) {
        MENU menu = (MENU) view.getTag();

        if (menu == null) {
            return;
        }


        if(menu.MENU_CODE.equalsIgnoreCase("DW")){
            OnModuleClick(R.id.action_nav_sync);
        }else if(menu.MENU_CODE.equalsIgnoreCase("LT")){
            App.resetApp();
            getActivity().finish();
        }else if(menu.MENU_CODE.equalsIgnoreCase("ST")){
            OnModuleClick(R.id.action_nav_settings);
        }else{
            Bundle bundle = new Bundle();
            bundle.putInt(AppConstants.MODULE_ID, ModuleID);
            bundle.putString(AppConstants.MASTER_ID, menu.MENU_NAME);
            bundle.putSerializable(AppConstants.SELECTED_OBJECT, menu);
            ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_section, bundle);
        }



        //showToast("menu "+menu.MENU_NAME);

//        if(menu.MENU_CODE.equalsIgnoreCase("IN")){
//            OnModuleClick(R.id.action_nav_inbound);
//        }else if(menu.MENU_CODE.equalsIgnoreCase("DW")){
//            OnModuleClick(R.id.action_nav_sync);
//        }else if(menu.MENU_CODE.equalsIgnoreCase("ST")) {
//            OnModuleClick(R.id.action_nav_preference);
//        }else if(menu.MENU_CODE.equalsIgnoreCase("SC")) {
//            OnModuleClick(R.id.nav_logistic_label);
//        }else if(menu.MENU_CODE.equalsIgnoreCase("SPC")) {
//            OnModuleClick(R.id.nav_partial_logistic_label);
//        }else if(menu.MENU_CODE.equalsIgnoreCase("INP")){
//            OnModuleClick(R.id.action_nav_inbound_partial);
//        }

    }

    private void OnModuleClick(int action_id) {
        Navigation.findNavController(root).navigate(action_id);
    }

    private void NavigateToDownloadDetails(MENU menu) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, menu);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_sync, bundle);
    }

    private void NavigateToInbound(MENU menu) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, menu);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_inbound, bundle);
    }





}