package com.dcode.gacinventory.ui.view;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.navigation.Navigation;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.common.AppPreferences;
import com.dcode.gacinventory.common.AppVariables;
import com.dcode.gacinventory.common.Utils;
import com.example.gacinventory.R;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Set;

public class SettingsFragment extends BaseFragment {
    Button btn_save_setting,btnBack;
    private TextInputEditText edDeviceID;
    private TextInputEditText edMastersData;
    private TextInputEditText edIp;
    private TextInputEditText edPort;
    private TextInputEditText edMac;
    private TextInputEditText edUrl;
    private TextInputLayout lbUrl;
    private RadioGroup rdoGroup;
    private RadioGroup rdoGroupNet;
    private LinearLayout layoutIp;
    private LinearLayout layoutBlue;
    private ImageButton settings;
    private View root;
    private CheckBox chkSelect;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);
        }

        root = inflater.inflate(R.layout.fragment_settings, container, false);
        setHasOptionsMenu(true);
        setupView();
        return root;
    }


    private void setupView() {
        new AppVariables(getContext());

        btn_save_setting = root.findViewById(R.id.btnNext);
        btn_save_setting.setOnClickListener(v -> onSave());


        edUrl = root.findViewById(R.id.edUrl);


        String ServiceURL = AppPreferences.getValue(getContext(), AppPreferences.SERVICE_URL, false);
        if (ServiceURL.length() <= 0)
            ServiceURL = getString(R.string.default_url);
        edUrl.setText(ServiceURL);

        edUrl.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                edUrl.requestFocus();
                return false;
            }
        });

//        edUrl.requestFocus();
//        try {
//            Thread.sleep(1000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }


        chkSelect= root.findViewById(R.id.chkSelect);
        chkSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                }else{
                }

            }
        });
        String defaultPutaway = AppPreferences.getValue(getContext(), AppPreferences.DEFAULT_PUTAWAY, false);
        //if (defaultPutaway.length() <= 0){
        //    chkSelect.setChecked(true);
        //}

        if (defaultPutaway.length() > 0 && defaultPutaway.equalsIgnoreCase("true")){
            chkSelect.setChecked(true);
        }else{
            chkSelect.setChecked(false);
        }



    }





    private void onSave() {

        String url = edUrl.getText().toString().trim();
        if (url.isEmpty()) {
            showToast("URL is mandatory!");
            return;
        }

        if(chkSelect.isChecked()){
            AppPreferences.SetValue(getContext(), AppPreferences.DEFAULT_PUTAWAY, "true");
        }else{
            AppPreferences.SetValue(getContext(), AppPreferences.DEFAULT_PUTAWAY, "false");
        }

        showToastLong("Restart application for setting to apply");
        AppPreferences.SetValue(getContext(), AppPreferences.SERVICE_URL, url);
        App.appSettings().setBaseUrl(url);
        Log.d("BASE_URL##",App.appSettings().getBaseUrl());
        App.getNetworkClient().destroyInstance();
        Navigation.findNavController(root).navigate(R.id.nav_home);
    }
}
