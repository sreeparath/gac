package com.dcode.gacinventory.ui.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.data.model.CONFIRMATION_LIST;
import com.dcode.gacinventory.data.model.INBOUND_HEADER;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SCAN;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SUMBIT;
import com.dcode.gacinventory.data.model.PRINTER_LIST_SERVICE;
import com.dcode.gacinventory.data.model.PRINT_FORMAT;
import com.dcode.gacinventory.network.model.GenericRetResponse;
import com.dcode.gacinventory.network.service.ServiceUtils;
import com.dcode.gacinventory.ui.adapter.ConfirmationListAdapter;
import com.dcode.gacinventory.ui.adapter.ItemsLabelsPrintGeneratedAdapter;
import com.example.gacinventory.R;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ConfirmationListFragment
        extends BaseFragment
        implements View.OnClickListener {

    //private String jobNo;
    private View root;
    private AutoCompleteTextView tvPrinter;
    private AutoCompleteTextView tvLabel;
    //private List<GENERATE_LABELS> itemsPrintList;
    //private List<GR_ITEM_PRINT_TYP> grItemPrintTyp;
    private ItemsLabelsPrintGeneratedAdapter labelsPrintGeneratedAdapter;
    private ArrayAdapter<PRINTER_LIST_SERVICE> printersAdapter;
    private ArrayAdapter<PRINT_FORMAT> labelsAdapter;
    private PRINTER_LIST_SERVICE barcode_printers;
    private PRINT_FORMAT label_formats;
    private RadioGroup rdoGroup;
    private TextInputEditText edMac;
    private TextInputLayout ilPrinter;
    private TextInputLayout ilmac;
    private TextView edInfo;
    private CheckBox chkSelect;
    private int noGacScan;
    private List<CONFIRMATION_LIST> itemsPrintList;
    private ConfirmationListAdapter confirmationListAdapter;
    private INBOUND_HEADER inboundHeader;
    private LOGISTIC_LABEL_SCAN items_prints;
    int parentId = -1;
    //private LOGISTIC_LABEL_SUMBIT logistic_label_sumbit;
    //private PUTAWAY_SUMBIT putaway_sumbit;
    private ArrayAdapter<String> orderByAdapter;
    private AutoCompleteTextView orderBy;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);


        //jobNo = "";
        if (this.getArguments() != null) {
            Bundle bundle = this.getArguments();
            //parentId = bundle.containsKey(AppConstants.PARENT_ID) ? bundle.getInt(AppConstants.PARENT_ID) : -1;
            //jobNo = bundle.containsKey(AppConstants.SELECTED_ID) ? bundle.getString(AppConstants.SELECTED_ID) : "";
            //inboundHeader  = (INBOUND_HEADER) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
            inboundHeader = bundle.containsKey(AppConstants.SELECTED_OBJECT) ? (INBOUND_HEADER) bundle.getSerializable(AppConstants.SELECTED_OBJECT) : null;
        }

        root = inflater.inflate(R.layout.fragment_confirmation_list, container, false);
        setupView();
        return root;
    }

    private void setupView(){

        RecyclerView recyclerView = root.findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));


        //labelsPrintGeneratedAdapter = new ItemsLabelsPrintGeneratedAdapter(new ArrayList<>(), this);
        //recyclerView.setAdapter(labelsPrintGeneratedAdapter);
        //FillGACPrintList();

        confirmationListAdapter = new ConfirmationListAdapter(new ArrayList<>(),this);
        recyclerView.setAdapter(confirmationListAdapter);
        //recyclerView.setNestedScrollingEnabled(false);



        MaterialButton btnPrint = root.findViewById(R.id.btnPrint);
        btnPrint.setOnClickListener(v -> {

        });

        MaterialButton btnBack = root.findViewById(R.id.btnBack);
        btnBack.setOnClickListener(v -> {
//            Bundle bundle = new Bundle();
//            bundle.putSerializable(AppConstants.SELECTED_OBJECT, inboundHeader);
//            ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_logistic_label, bundle);
                }
        );


        edInfo = root.findViewById(R.id.edInfo);
        //edInfo.setText("Total GACScan ID : "+noGacScan);

//        chkSelect = root.findViewById(R.id.chkSelect);
//        chkSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                //set your object's last status
//                if(itemsPrintList.size()>0){
//                    if(chkSelect.isChecked()){
//                        confirmationListAdapter.selectAll(true);
//                        confirmationListAdapter.notifyDataSetChanged();
//                    }else{
//                        confirmationListAdapter.selectAll(false);
//                        confirmationListAdapter.notifyDataSetChanged();
//                    }
//                }
//            }
//        });

        orderBy = root.findViewById(R.id.edOrder);
        setOrderBy();
        orderBy.setOnItemClickListener((parent, view, position, id) -> {
            //showToast("position"+String.valueOf(position));
            if (position >= 0) {
                if (position == 0) {
                    loadData("ALL");
                } else if (position == 1) {
                    loadData("STAGE");
                } else if (position == 2) {loadData("CONFIRM");
                }
                edInfo.setText("Total Items : "+confirmationListAdapter.getItemCount());
            }

        });

    }

    private void setOrderBy() {
        ArrayList<String> orderList = new ArrayList<String>();
        orderList.add("All");
        orderList.add("Pending");
        orderList.add("Completed");

        orderByAdapter = new ArrayAdapter<>(requireContext(), R.layout.dropdown_menu_popup_item, orderList);
        orderBy.setAdapter(orderByAdapter);
        orderBy.setSelection(0);
        orderBy.setThreshold(100);
    }

    private void OpenGACScan() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, items_prints);
        //bundle.putSerializable(AppConstants.SELECTED_OBJECT_NEW,inboundHeader);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_gac_scan, bundle);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //showToast("in this tab onViewCreated");
        DownloadConfirmationList();


    }


    @Override
    public void onClick(View view) {
        CONFIRMATION_LIST confirmationList = (CONFIRMATION_LIST) view.getTag();

        if(confirmationList==null){
            showAlert("Warning","No Pending List!");
            return;
        }
        if (view.getId() == R.id.btnShort) {

            LOGISTIC_LABEL_SUMBIT logistic_label_sumbit = new LOGISTIC_LABEL_SUMBIT();
            logistic_label_sumbit.SRNO = confirmationList.SRNO;
            logistic_label_sumbit.JOB_NO = confirmationList.JOB_NO;
            logistic_label_sumbit.BARCODE = confirmationList.SKU;
            logistic_label_sumbit.PRIN_CODE = confirmationList.PRIN_CODE;
            logistic_label_sumbit.INPUT_LQTY = "0";
            logistic_label_sumbit.INPUT_MQTY = "0";
            logistic_label_sumbit.INPUT_PQTY = "0";

            logistic_label_sumbit.MFG_DATE = confirmationList.MFG_DATE;
            logistic_label_sumbit.EXP_DATE = confirmationList.EXP_DATE;
            logistic_label_sumbit.LOT_NO = confirmationList.LOT_NO;
            logistic_label_sumbit.LOT_STATUS =confirmationList.LOT_STATUS;
            logistic_label_sumbit.FORMULA_NO = confirmationList.FORMULA_NO;
            logistic_label_sumbit.FIL_CODE = confirmationList.FIL_CODE;
            logistic_label_sumbit.LOC_CODE = confirmationList.LOC_CODE;
            logistic_label_sumbit.SITE = confirmationList.SITE;
            logistic_label_sumbit.UPDATE_FROM = confirmationList.UPDATE_FROM;
            logistic_label_sumbit.PALLET_ID = confirmationList.PALLET_ID;
            logistic_label_sumbit.SCAN_PUT_LOC_CODE = confirmationList.SCAN_PUT_LOC_CODE;
            logistic_label_sumbit.DESCRIPTION = confirmationList.DESCRIPTION;
            uploadConfirmShortGACScanData(logistic_label_sumbit);
        }

        /*ITEMS_PRINT items_print = (ITEMS_PRINT) view.getTag();
        if (items_print != null) {
            final AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
            alert.setTitle("Please Enter Labels count");
            final EditText input = new EditText(getContext());
            input.setInputType(InputType.TYPE_CLASS_NUMBER);
            input.setRawInputType(Configuration.KEYBOARD_12KEY);
            alert.setView(input);
            alert.setPositiveButton("Next", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    //Put actions for OK button here
                    String inputValue = input.getText().toString().trim();
                    if (inputValue.length() <= 0) {
                        showToast("Invalid Labels count");
                    } else {
                        int count = Utils.convertToInt(inputValue, items_print.QTY);
                        //if (count > items_print.ITQTY) {
                        //    showToast("Cannot exceed receipt");
                        //    count = items_print.ITQTY;
                       //}
                        items_print.QTY = count;
                        itemsLabelsPrintAdapter.notifyDataSetChanged();
                    }
                }
            });
            alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    //Put actions for CANCEL button here, or leave in blank
                }
            });
            alert.show();
        }*/

    }

    private void uploadConfirmShortGACScanData(LOGISTIC_LABEL_SUMBIT logistic_label_sumbit) {
        if(logistic_label_sumbit==null){
            return;
        }
        showToast(getString(R.string.send_data_to_server));
        JsonObject requestObject = ServiceUtils.LogisticLabel.getLOGISTIC_LABEL_SUMBIT_FULL(logistic_label_sumbit,"N");
        showProgress(false);
        App.getNetworkClient().getAPIService().submitWithLinesData(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericSubmissionResponse, Response response) {
                dismissProgress();
                if (genericSubmissionResponse.getErrCode().equals("S")) {
                    if (genericSubmissionResponse.getErrCode().equals("S")) {
                        String msg = genericSubmissionResponse.getErrMessage();
                        showAlert("Success",msg);
                        App.getDatabaseClient().getAppDatabase().genericDao().updateConfirmedSerialNo(logistic_label_sumbit.SRNO,logistic_label_sumbit.PRIN_CODE,
                                logistic_label_sumbit.BARCODE);
                        confirmationListAdapter.notifyDataSetChanged();
                    } else {
                        showAlert("Error",genericSubmissionResponse.getErrMessage());
                        //showToast(genericSubmissionResponse.getErrMessage());
                    }
                } else {
                    Log.d(App.TAG, genericSubmissionResponse.getErrMessage());
                    showAlert("Error",genericSubmissionResponse.getErrMessage());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(App.TAG, error.getMessage());
                showToast(error.getMessage());
                dismissProgress();
            }
        });
    }




    private void DownloadConfirmationList() {
//        if (jobNo.length()<= 0 ) {
//            showAlert("Warning","Job shouldn't be empty!");
//            return;
//        }
        if(inboundHeader==null){
            showAlert("Warning","Job Header shouldn't be empty!");
            return;
        }

        JsonObject requestObject = ServiceUtils.LogisticLabel.getConfirmationList(inboundHeader.JOB_NO,inboundHeader.PRIN_CODE);

        Log.d("requestObject##",requestObject.toString());

        showProgress(false);
        App.getNetworkClient().getAPIService().getRetGeneric(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("S")) {
                    String xmlDoc = genericRetResponse.getJsonString();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showAlert("Error - Staging", "No data received.");
                        return;
                    }
                    try {
//                        String lastId = genericRetResponse.getErrMessage();
//                        if(lastId==null&& lastId.length()==0){
//                            showAlert("Error", "No lastId received.");
//                            return;
//                        }
                        CONFIRMATION_LIST[] items_prints = new Gson().fromJson(xmlDoc, CONFIRMATION_LIST[].class);
                        App.getDatabaseClient().getAppDatabase().genericDao().deleteAllCONFIRMATION_LIST();
                        App.getDatabaseClient().getAppDatabase().genericDao().insertCONFIRMATION_LIST(items_prints);
                        loadData("ALL");

                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                        showAlert("Error - Staging", e.getMessage());
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showAlert("Error - Staging", msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showAlert("Error - Staging", msg);
            }
        });
    }

    private void loadData(String filter) {

        //pl_itemsList = App.getDatabaseClient().getAppDatabase().genericDao().GetLOAD_SLIP_ITEMS_NEW(pl_hdr.LSNO);// summary list
        itemsPrintList = App.getDatabaseClient().getAppDatabase().genericDao().getCONFIRMATION_LIST(filter);
        if (itemsPrintList != null && itemsPrintList.size()>0 ) {
            confirmationListAdapter.addItems(itemsPrintList);
            confirmationListAdapter.notifyDataSetChanged();
            edInfo.setText("Total Items : "+confirmationListAdapter.getItemCount());
        }else {
            //showToast(" Alert : No Staging data found.");
            showAlert("Alert - Staging", "No Staging data found.");
        }
    }


}
