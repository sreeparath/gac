package com.dcode.gacinventory.ui.view.outbound;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SCAN;
import com.dcode.gacinventory.data.model.SP_SCN_AN_OUT_LOC_BY_JOB;
import com.dcode.gacinventory.data.model.SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS;
import com.dcode.gacinventory.network.model.GenericRetResponse;
import com.dcode.gacinventory.network.service.ServiceUtils;
import com.dcode.gacinventory.ui.adapter.ImageViewAdapter;
import com.dcode.gacinventory.ui.adapter.PickAndPackListAdapter;
import com.dcode.gacinventory.ui.adapter.PickAndPackListDetailedAdapter;
import com.dcode.gacinventory.ui.view.BaseFragment;
import com.dcode.gacinventory.ui.view.MainActivity;
import com.example.gacinventory.R;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * @Created by: VIJIL DHAS A.S
 * @Date: 07-09-2021
 * <p>
 * Retrieves the list of jobs and locations assigned to a scan user.
 * Applicable to both process flows Normal picking and Pick/Pack.
 */
public class PickAndPackFragment extends BaseFragment implements View.OnClickListener {
    private String putawayType;
    private View root;

    private RecyclerView mItemList;
    private RecyclerView mItemDetailedList;

    private PickAndPackListAdapter packListAdapter;
    private PickAndPackListDetailedAdapter packListDetailedAdapter;

    private List<SP_SCN_AN_OUT_LOC_BY_JOB> listLocByJob;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);
            putawayType = this.getArguments().getString("PutAwayType", "");
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                if (bundle.containsKey(AppConstants.SELECTED_ID)) {
                    putawayType = bundle.getString(AppConstants.SELECTED_ID);
                }

                if (bundle.containsKey(AppConstants.SELECTED_SP_SCN_AN_OUT_LOC_BY_JOB)) {
                    listLocByJob = (List<SP_SCN_AN_OUT_LOC_BY_JOB>) bundle.getSerializable(AppConstants.SELECTED_SP_SCN_AN_OUT_LOC_BY_JOB);
                }
            }
        }
        root = inflater.inflate(R.layout.fragment_pick_and_pack, container, false);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupView();
        setHeaderToolbar();
    }

    private void setupView() {
        mItemList = root.findViewById(R.id.rcv_item_list);
        mItemDetailedList = root.findViewById(R.id.rcv_item_short_list);
        initRecyclerView();
        //  apiFetchLocByJob();
    }

    private void initRecyclerView() {
        packListAdapter = new PickAndPackListAdapter(getContext(), new ArrayList<>(), this,
                new PickAndPackListAdapter.OnNavigateFragment() {
                    @Override
                    public void onNavigateToFragment(SP_SCN_AN_OUT_LOC_BY_JOB spScnAnOutLocByJob) {
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(AppConstants.SELECTED_SP_SCN_AN_OUT_LOC_BY_JOB, spScnAnOutLocByJob);
                        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_pick_and_pack_details, bundle);
                    }
                });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mItemList.setLayoutManager(linearLayoutManager);
        mItemList.setAdapter(packListAdapter);


        packListDetailedAdapter = new PickAndPackListDetailedAdapter(getContext(), new ArrayList<>(), this);
        mItemDetailedList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mItemDetailedList.setAdapter(packListDetailedAdapter);

        setupListView(listLocByJob);
    }

    /**
     * ProcName":"SP_SCN_AN_OUT_LOC_BY_JOB"
     */
    private void apiFetchLocByJob() {
        JsonObject requestObject = ServiceUtils.OutBound.getSP_SCN_AN_OUT_LOC_BY_JOB();
        showProgress(false);

        App.getNetworkClient().getAPIService().getSpScnOutLocByJob(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("E")) {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showAlert("Error", msg);
                } else {
                    String xmlDoc = genericRetResponse.getJsonString();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showAlert("Error", "No data received.");
                        return;
                    }
                    try {
                        SP_SCN_AN_OUT_LOC_BY_JOB[] spScnAnOutLocByJobs = new Gson().fromJson(xmlDoc, SP_SCN_AN_OUT_LOC_BY_JOB[].class);
                        if (spScnAnOutLocByJobs.length == 0) {
                            showAlert("Error", "No data received.");
                            return;
                        }
                        List<SP_SCN_AN_OUT_LOC_BY_JOB> formattedList = new ArrayList<>();
                        for (int i = 0; i < spScnAnOutLocByJobs.length; i++) {
                            SP_SCN_AN_OUT_LOC_BY_JOB spScnAnOutLocByJob = new SP_SCN_AN_OUT_LOC_BY_JOB();
                            spScnAnOutLocByJob = spScnAnOutLocByJobs[i];
                            formattedList.add(spScnAnOutLocByJob);
                        }
                        setupListView(listLocByJob);

                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                        showAlert("Error", e.getMessage());
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showAlert("Error", msg);
            }
        });
    }


    private void setupListView(List<SP_SCN_AN_OUT_LOC_BY_JOB> spScnAnOutLocByJob) {
        if (spScnAnOutLocByJob != null && !spScnAnOutLocByJob.isEmpty()) {
            packListAdapter.addItems(spScnAnOutLocByJob);
            TextView info2 = root.findViewById(R.id.info2);
            info2.setText(spScnAnOutLocByJob.get(0).SRNO);
        }
    }

    @Override
    public void onClick(View v) {
        SP_SCN_AN_OUT_LOC_BY_JOB spScnAnOutLocByJob = (SP_SCN_AN_OUT_LOC_BY_JOB) v.getTag();
        // NavigatePickAndPackDetails(R.id.nav_pick_and_pack_details, spScnAnOutLocByJob);
        showProgress(true);
        List<SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS> itemDetails = new ArrayList<>();

        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS itemName = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        itemName.itemLabel = "GACScan ID";
        itemName.itemValue = spScnAnOutLocByJob.SRNO;
        itemDetails.add(itemName);

        SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS itemCode = new SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS();
        itemCode.itemLabel = "Picking Sequence";
        itemCode.itemValue = String.valueOf(spScnAnOutLocByJob.PICK_SEQNO);
        itemDetails.add(itemCode);

        packListDetailedAdapter.addItems(itemDetails);
        packListDetailedAdapter.notifyDataSetChanged();

        TextView info2 = root.findViewById(R.id.info2);
        info2.setText(spScnAnOutLocByJob.SRNO);

        dismissProgress();
    }


    private void setHeaderToolbar() {
        TextView info1 = root.findViewById(R.id.info1);
        info1.setText(App.currentUser.USER_ID);
    }


}
