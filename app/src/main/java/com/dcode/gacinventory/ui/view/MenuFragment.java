package com.dcode.gacinventory.ui.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.common.AppVariables;
import com.dcode.gacinventory.data.model.INBOUND_HEADER;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SCAN;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SUMBIT;
import com.dcode.gacinventory.data.model.MENU;
import com.dcode.gacinventory.ui.adapter.MenuObjectsAdapter;
import com.dcode.gacinventory.ui.adapter.ObjectsAdapter;
import com.example.gacinventory.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class MenuFragment extends BaseFragment implements View.OnClickListener {

    View root;
    MenuObjectsAdapter detAdapter;
    LOGISTIC_LABEL_SUMBIT logistic_label_sumbit;
    LOGISTIC_LABEL_SCAN items_prints;
    INBOUND_HEADER inbound_header;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                items_prints = (LOGISTIC_LABEL_SCAN) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
                inbound_header = (INBOUND_HEADER) bundle.getSerializable(AppConstants.SELECTED_OBJECT_NEW2);
                logistic_label_sumbit = (LOGISTIC_LABEL_SUMBIT) bundle.getSerializable(AppConstants.SELECTED_OBJECT_NEW);
            }
        }


        root = inflater.inflate(R.layout.fragment_menu, container, false);

        setupView();

        return root;
    }


    private  void setupView(){

        setHeaderToolbar();

        RecyclerView recyclerView = root.findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


        List menuList = new ArrayList<MENU>();
        MENU menu = new MENU();
        menu.MENU_ID = 1;
        menu.MENU_CODE = "PT";
        menu.MENU_NAME = "Put Away";
        menu.MENU_DESC = "Put Away Transactions";
        menu.MENU_ICON  = R.drawable.ic_put_away;
        menuList.add(menu);


        menu = new MENU();
        menu.MENU_ID = 2;
        menu.MENU_CODE = "SN";
        menu.MENU_NAME = "Scan Next";
        menu.MENU_DESC = "";
        menu.MENU_ICON  = R.drawable.ic_scan_next;
        menuList.add(menu);

        menu = new MENU();
        menu.MENU_ID = 3;
        menu.MENU_CODE = "DM";
        menu.MENU_NAME = "Damage/Split ";
        menu.MENU_DESC = "Repor damage/Split Transactions";
        menu.MENU_ICON  = R.drawable.ic_damage_split;
        menuList.add(menu);

        menu = new MENU();
        menu.MENU_ID = 4;
        menu.MENU_CODE = "CS";
        menu.MENU_NAME = "Confirmation Staging";
        menu.MENU_DESC = "Stage Confirmation Transaction";
        menu.MENU_ICON  = R.drawable.ic_confirmation_staging;
        menuList.add(menu);

        menu = new MENU();
        menu.MENU_ID = 5;
        menu.MENU_CODE = "UP";
        menu.MENU_NAME = "Update Product Info";
        menu.MENU_DESC = "Update Product Information";
        menu.MENU_ICON  = R.drawable.ic_updatedinformation;
        menuList.add(menu);

        menu = new MENU();
        menu.MENU_ID = 6;
        menu.MENU_CODE = "ED";
        menu.MENU_NAME = "Edit";
        menu.MENU_DESC = "Edit Staging Confirmation";
        menu.MENU_ICON  = R.drawable.ic_edit;
        menuList.add(menu);

        detAdapter = new MenuObjectsAdapter(getContext(),menuList,  this);
        recyclerView.setAdapter(detAdapter);
    }

    private void setHeaderToolbar() {
        if (inbound_header != null) {
            Toolbar toolbar = root.findViewById(R.id.toolbar);
            TextView info1 = root.findViewById(R.id.info1);
            info1.setText(App.currentUser.USER_ID);
            TextView info2 = root.findViewById(R.id.info2);
            info2.setText(inbound_header.JOB_NO + " - "+ inbound_header.PRIN_NAME);
        }
    }

    @Override
    public void onClick(View view) {
        MENU menu = (MENU) view.getTag();

        if (menu == null) {
            return;
        }

        if(logistic_label_sumbit==null){
            return;
        }

//        Bundle bundle = new Bundle();
//        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
//        bundle.putSerializable(AppConstants.SELECTED_OBJECT, menu);
//        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_section, bundle);

        //showToast("menu "+menu.MENU_NAME);

        if(menu.MENU_CODE.equalsIgnoreCase("PT")){
            OnModuleClickFragement(R.id.nav_put_away);
        }else if(menu.MENU_CODE.equalsIgnoreCase("DM")){
            OnModuleDamageClickFragement(R.id.nav_gac_scan);
        }else if(menu.MENU_CODE.equalsIgnoreCase("SN")) {
            onModuleLogisticClickFragment(R.id.nav_logistic_label);
        }else if(menu.MENU_CODE.equalsIgnoreCase("CS")){
            NavigateToConfirmationList(R.id.nav_confirmation_list);
        }else if(menu.MENU_CODE.equalsIgnoreCase("ED")){
            OnEditClickFragement(R.id.nav_gac_scan);
        }

    }

    private void onModuleLogisticClickFragment(int action_id) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, inbound_header);
        ((MainActivity) requireActivity()).NavigateToFragment(action_id, bundle);
    }

    private void OnModuleClickFragement(int action_id) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.SELECTED_OBJECT_NEW, logistic_label_sumbit);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, items_prints);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT_NEW2, inbound_header);
        ((MainActivity) requireActivity()).NavigateToFragment(action_id, bundle);
    }

    private void OnModuleDamageClickFragement(int action_id) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.SELECTED_OBJECT_NEW, inbound_header);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, items_prints);
        ((MainActivity) requireActivity()).NavigateToFragment(action_id, bundle);
    }

    private void OnEditClickFragement(int action_id) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.SELECTED_OBJECT_NEW, inbound_header);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, items_prints);
        bundle.putBoolean(AppConstants.SELECTED_ID, true);
        ((MainActivity) requireActivity()).NavigateToFragment(action_id, bundle);
    }

    private void OnModuleClick(int action_id) {
        Navigation.findNavController(root).navigate(action_id);
    }


    private void NavigateToConfirmationList(int fragmentId) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, inbound_header);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT_NEW, logistic_label_sumbit);
        ((MainActivity) requireActivity()).NavigateToFragment(fragmentId, bundle);
    }

    private void NavigateToInbound(MENU menu) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, menu);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_inbound, bundle);
    }





}