package com.dcode.gacinventory.ui.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.data.model.INBOUND_HEADER;
import com.dcode.gacinventory.data.model.PRN_CODE_LIST;
import com.example.gacinventory.R;

import java.util.ArrayList;
import java.util.List;

public class SearchPRNAdapter
        extends RecyclerView.Adapter<SearchPRNAdapter.RecyclerViewHolder>
        implements Filterable {

    private Context context;
    private View.OnClickListener shortClickListener;
    private List<PRN_CODE_LIST> dataList;
    private ArrayList<PRN_CODE_LIST> dataListFull;
    private String CodeTitle;
    private String NameTitle;
    private String DescTitle;


    private Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<PRN_CODE_LIST> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(dataListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (PRN_CODE_LIST item : dataListFull) {
                    if (item.PRIN_NAME.toLowerCase().contains(filterPattern.toLowerCase()) ||
                            item.PRIN_CODE.toLowerCase().contains(filterPattern.toLowerCase()) ) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            dataList.clear();
            dataList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public SearchPRNAdapter(Context context, List<PRN_CODE_LIST> objects, View.OnClickListener shortClickListener, @Nullable String... sArgs) {
        try {
            this.context = context;
            this.shortClickListener = shortClickListener;
            this.dataList = objects;
            this.dataListFull = new ArrayList<>(objects);
            if (sArgs != null && sArgs.length > 0) {
                CodeTitle = sArgs.length >= 1 && sArgs[0] != null ? sArgs[0] : AppConstants.CODE_TITLE;
                NameTitle = sArgs.length >= 2 && sArgs[1] != null ? sArgs[1] : AppConstants.NAME_TITLE;
                DescTitle = sArgs.length >= 3 && sArgs[2] != null ? sArgs[2] : AppConstants.DESC_TITLE;
            }

            setHasStableIds(true);
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
        }
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_search, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        final PRN_CODE_LIST searchTypeItem = this.dataList.get(position);

        //holder.tvCodeTitle.setText(CodeTitle);
        holder.tvNameTitle.setText(NameTitle);
        holder.tvDescTitle.setText(DescTitle);
        //holder.tvCode.setText(searchTypeItem.PRIN_CODE);
        holder.tvName.setText(searchTypeItem.PRIN_CODE);
        holder.tvDesc.setText(searchTypeItem.PRIN_NAME);
        holder.itemView.setTag(searchTypeItem);

        holder.itemView.setTag(searchTypeItem);
        holder.itemView.setOnClickListener(shortClickListener);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public void addItems(List<PRN_CODE_LIST> list) {
        this.dataList = list;
        this.dataListFull = new ArrayList<>(list);
        notifyDataSetChanged();
    }

    public List<PRN_CODE_LIST> getItems() {
        return this.dataList;
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView tvCodeTitle;
        TextView tvNameTitle;
        TextView tvDescTitle;
        TextView tvCode;
        TextView tvName;
        TextView tvDesc;

        RecyclerViewHolder(View view) {
            super(view);
            //tvCodeTitle = view.findViewById(R.id.tvCodeTitle);
            tvNameTitle = view.findViewById(R.id.tvNameTitle);
            tvDescTitle = view.findViewById(R.id.tvDescTitle);
            //tvCode = view.findViewById(R.id.tvCode);
            tvName = view.findViewById(R.id.tvName);
            tvDesc = view.findViewById(R.id.tvDesc);
        }
    }
}
