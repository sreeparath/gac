package com.dcode.gacinventory.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.gacinventory.data.model.SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS;
import com.example.gacinventory.R;

import java.util.List;

/**
 * @Created by: VIJIL DHAS A.S
 * @Date: 08-09-2021
 */
public class PickAndPackListDetailedAdapter extends RecyclerView.Adapter<PickAndPackListDetailedAdapter.RecycleViewHolder> {
    private List<SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS> listItemDetails;
    private Context context;
    private View.OnClickListener itemClickListener;

    public PickAndPackListDetailedAdapter(Context context,
                                          List<SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS> anOutLocByJobs,
                                          View.OnClickListener itemClickListener) {
        this.listItemDetails = anOutLocByJobs;
        this.context = context;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public RecycleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PickAndPackListDetailedAdapter.RecycleViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pick_and_pack_item_detailed, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecycleViewHolder holder, int position) {
        ((RecycleViewHolder) holder).txtItemLabel.setText(listItemDetails.get(position).itemLabel);
        ((RecycleViewHolder) holder).txtItemValue.setText(listItemDetails.get(position).itemValue);

    }

    public void addItems(List<SP_SCN_AN_OUT_LOC_BY_JOB_DETAILS> detList) {
        listItemDetails = detList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return listItemDetails.size();
    }

    static class RecycleViewHolder extends RecyclerView.ViewHolder {
        private TextView txtItemLabel;
        private TextView txtItemValue;

        public RecycleViewHolder(@NonNull View itemView) {
            super(itemView);
            txtItemLabel = itemView.findViewById(R.id.txt_label);
            txtItemValue = itemView.findViewById(R.id.txt_label_details);
        }
    }
}
