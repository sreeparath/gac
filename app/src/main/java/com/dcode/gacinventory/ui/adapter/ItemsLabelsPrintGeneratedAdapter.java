package com.dcode.gacinventory.ui.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.gacinventory.data.model.GENERATE_LABELS;
import com.dcode.gacinventory.data.model.LABEL_ITEM_LIST;
import com.example.gacinventory.R;

import java.util.List;

public class ItemsLabelsPrintGeneratedAdapter
        extends RecyclerView.Adapter<ItemsLabelsPrintGeneratedAdapter.RecyclerViewHolder> {

    private GENERATE_LABELS editedRow;
    private List<GENERATE_LABELS> itemsPrintList;
    private View.OnClickListener shortClickListener;
    private boolean isSelectedAll = false;

    public ItemsLabelsPrintGeneratedAdapter(List<GENERATE_LABELS> itemsPrints,
                                            View.OnClickListener shortClickListener) {
        this.itemsPrintList = itemsPrints;
        this.shortClickListener = shortClickListener;
        setHasStableIds(true);
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_card_items_print, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        final GENERATE_LABELS items_print = itemsPrintList.get(position);

        holder.tvName.setText(items_print.LABEL);
        //holder.tvCode.setText(items_print.GACSCANID);
        //holder.tvUnits.setText(String.valueOf(items_print.QTY1));
        //holder.tvBatchNo.setText(items_print.UNIT_CODE);

        //in some cases, it will prevent unwanted situations
        holder.chkSelect.setOnCheckedChangeListener(null);

        //if true, your checkbox will be selected, else unselected
        //items_print.IS_SELECT = Boolean.TRUE;
        //holder.chkSelect.setChecked(true);

        if (!isSelectedAll) {
            holder.chkSelect.setChecked(false);
        }
        else{
            holder.chkSelect.setChecked(true);
        }

        holder.chkSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //set your object's last status
//                if (isChecked){
//                    holder.chkSelect.setChecked(false);
//                    items_print.IS_SELECT = false;
//                }else{
//                    holder.chkSelect.setChecked(true);
//                    items_print.IS_SELECT = true;
//                }
                if(items_print.IS_SELECT){
                    items_print.IS_SELECT = false ;
                }else{
                    items_print.IS_SELECT = true;
                }

            }
        });

        holder.itemView.setTag(items_print);
        holder.itemView.setOnClickListener(shortClickListener);
        editedRow = items_print;
    }

    public void selectAll(boolean select){
        Log.e("onClickSelectAll",String.valueOf(select));
        isSelectedAll=select;
        notifyDataSetChanged();
    }

    public void selectAllItem(boolean isSelectedAll) {
        try {

            if (itemsPrintList != null) {
                for (int i = 0; i < itemsPrintList.size(); i++) {
                    itemsPrintList.get(i).IS_SELECT = isSelectedAll;
                    Log.d("isSelectedAll",String.valueOf(itemsPrintList.get(i).IS_SELECT));
                }
            }
            notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        if (itemsPrintList == null) {
            return 0;
        }
        return itemsPrintList.size();
    }

    public List<GENERATE_LABELS> getItems() {
        return this.itemsPrintList;
    }

    public void addItems(List<GENERATE_LABELS> detList) {
        this.itemsPrintList = detList;
        notifyDataSetChanged();
    }

    public void addItems(GENERATE_LABELS items_print) {
        this.itemsPrintList.add(items_print);
        notifyDataSetChanged();
    }

    public GENERATE_LABELS getItemByPosition(int position) {
        return this.itemsPrintList.get(position);
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName;
        private TextView tvCode;
        private TextView tvUnits;
        private TextView tvBatchNo;
        private TextView tvReceiptQty;
        private TextView tvPrintQty;
        private CheckBox chkSelect;

        RecyclerViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.tvName);
//            tvCode = view.findViewById(R.id.tvCode);
//            tvUnits = view.findViewById(R.id.tvUnits);
//            tvBatchNo = view.findViewById(R.id.tvBatchNo);
//            tvReceiptQty = view.findViewById(R.id.tvReceiptQty);
//            tvPrintQty = view.findViewById(R.id.tvPrintQty);
            chkSelect= view.findViewById(R.id.chkSelect);
        }
    }
}
