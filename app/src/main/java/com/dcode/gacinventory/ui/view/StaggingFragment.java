package com.dcode.gacinventory.ui.view;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.data.model.INBOUND_HEADER;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SCAN;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SUMBIT;
import com.dcode.gacinventory.data.model.MENU;
import com.dcode.gacinventory.data.model.SECTION;
import com.dcode.gacinventory.network.model.GenericRetResponse;
import com.dcode.gacinventory.network.service.ServiceUtils;
import com.dcode.gacinventory.ui.adapter.ObjectsSectionAdapter;
import com.example.gacinventory.R;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class StaggingFragment extends BaseFragment implements View.OnClickListener {

    View root;
    ObjectsSectionAdapter objectsSectionAdapter;
    private MENU menu;
    TextInputEditText edSearch;
    INBOUND_HEADER inboundHeader;
    TextInputEditText edLastScan;



    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true);
        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                menu = bundle.containsKey(AppConstants.SELECTED_OBJECT) ? (MENU) bundle.getSerializable(AppConstants.SELECTED_OBJECT) : null;
            }
        }



        root = inflater.inflate(R.layout.fragment_stagging, container, false);
        edSearch = root.findViewById(R.id.edsearch);
        edSearch.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_ENTER){
                if(event.getAction() == KeyEvent.ACTION_DOWN){
                    return onSearchEvent();
                }else{
                    return false;
                }
            }else{
                return false;
            }
        });

        return root;
    }

    @Override
    public void onClick(View view) {

    }

    private void OnModuleClick(int action_id) {
        Navigation.findNavController(root).navigate(action_id);
    }

    private void NavigateToClosure(int fragmaneID) {
        Bundle bundle = new Bundle();
        //bundle.putString(AppConstants.SELECTED_ID, "P");
        ((MainActivity) requireActivity()).NavigateToFragment(fragmaneID, bundle);
    }

    private void NavigateToPutAwayFull(int fragmaneID) {
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.SELECTED_ID, "P");
        ((MainActivity) requireActivity()).NavigateToFragment(fragmaneID, bundle);
    }

    private void NavigateToInbound(MENU menu) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, menu);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_inbound, bundle);
    }


    private boolean onSearchEvent() {
        String search = edSearch.getText().toString();

        if (search.isEmpty() || search.trim().length() == 0) {
            showToast("Invalid Barcode");
            edSearch.requestFocus();
            return false;
        } else {
            ParseBarCode(search,"");
        }
        return true;
    }

    private void ParseBarCode(String scanCode,String mode) {
        if (scanCode.length() <= 0 ) {
            return;
        }
        DownloadPutawayDocument(scanCode,mode);

    }

    private void DownloadPutawayDocument(String gacScan,String prodBarcode) {
        if (gacScan.length() <= 0)  {
            return;
        }

        JsonObject requestObject = ServiceUtils.LogisticLabel.getInfoBySrNo(gacScan,prodBarcode,"P");

        showProgress(false);
        App.getNetworkClient().getAPIService().getRetGeneric(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("S")) {
                    String xmlDoc = genericRetResponse.getJsonString();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showAlert("Error", "No data received.");
                        return;
                    }
                    try {
                        LOGISTIC_LABEL_SUMBIT[] items_prints = new Gson().fromJson(xmlDoc, LOGISTIC_LABEL_SUMBIT[].class);
                        LOGISTIC_LABEL_SCAN[] logisticLabelScans = new Gson().fromJson(xmlDoc, LOGISTIC_LABEL_SCAN[].class);
                        if(logisticLabelScans.length==0){
                            showAlert("Error", "No data received.");
                            return;
                        }
                        edSearch.setText("");
                        if(logisticLabelScans[0].IS_STAGED.length()>0 && logisticLabelScans[0].IS_STAGED.trim().equalsIgnoreCase("N")){
                            INBOUND_HEADER    inboundHeader = new INBOUND_HEADER();
                            inboundHeader.JOB_NO = items_prints[0].JOB_NO;
                            inboundHeader.PRIN_CODE = items_prints[0].PRIN_CODE;
                            inboundHeader.PRIN_NAME = items_prints[0].PRIN_NAME;
                            OpenGACScan(logisticLabelScans[0],inboundHeader);
                        }else{
                            showAlert("Waring", "Stagging already completed!.");
                        }

                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                        showAlert("Error", e.getMessage());
                        //edSearch.setText("");
                        //edSearch.requestFocus();
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showAlert("Error", msg);
                    //edSearch.setText("");
                    //edSearch.requestFocus();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showAlert("Error", msg);
                edSearch.setText("");
                edSearch.requestFocus();
            }
        });
    }

    private void OpenPutAway(LOGISTIC_LABEL_SUMBIT items_prints) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.SELECTED_OBJECT_NEW, items_prints);
        //bundle.putSerializable(AppConstants.SELECTED_OBJECT_NEW2, inbound_header);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_put_away, bundle);
    }

    private void OpenMenuFragment(LOGISTIC_LABEL_SUMBIT logisticLabelSumbit,INBOUND_HEADER inboundHeader,LOGISTIC_LABEL_SCAN logisticLabelScan) {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        //bundle.putString(AppConstants.SELECTED_ID, DocNo);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT_NEW,  logisticLabelSumbit);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, logisticLabelScan);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT_NEW2,  inboundHeader);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_menu_fragment, bundle);
    }

    private void OpenGACScan(LOGISTIC_LABEL_SCAN logisticLabelScan,INBOUND_HEADER inboundHeader) {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putString(AppConstants.MASTER_ID, "");
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, logisticLabelScan);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT_NEW,inboundHeader);
        bundle.putLong(AppConstants.SOURCE_ID , AppConstants.DIRECT_STAG);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_stagging_list_tablayout, bundle);
    }


    private void OpenLogisticLabel(INBOUND_HEADER inboundHeader,String gacScan) {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putString(AppConstants.SELECTED_ID, gacScan);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, inboundHeader);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_logistic_label, bundle);
    }



}