package com.dcode.gacinventory.ui.view;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.common.AppVariables;
import com.dcode.gacinventory.data.model.INBOUND_HEADER;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SCAN;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SUMBIT;
import com.dcode.gacinventory.network.model.GenericRetResponse;
import com.dcode.gacinventory.network.service.ServiceUtils;
import com.example.gacinventory.R;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PutAwayHeaderFragment extends BaseFragment {
    private TextInputEditText edJobNo,edBarcode,edGacScan;
    private TextInputLayout ilLotNo;
//    private AutoCompleteTextView tvDocTypes;
//    private ArrayAdapter<DocType> objectsAdapter;
    private int promptResource;
    private String putawayType;
    private long HDR_ID;
    private CheckBox chkSelect;
    private LOGISTIC_LABEL_SUMBIT[] items_prints;
    private View root;
    private INBOUND_HEADER inbound_header;
    //private RadioGroup rdoGroup;
    boolean scanIdMandatory = true;
    boolean jobMandatory = false;
    boolean prodMandatory = false;
    private TextInputLayout ilJobNo,ilBarcode,ilGacScan;
    private Switch swFull;
    private TextView swlabel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true);
        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);
            putawayType = this.getArguments().getString("PutAwayType", "");
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                //DocNo = bundle.getString(AppConstants.SELECTED_ID);
                //barcode = bundle.getString(AppConstants.MASTER_ID);
                if(bundle.containsKey(AppConstants.SELECTED_ID)){
                    putawayType = bundle.getString(AppConstants.SELECTED_ID);
                }
            }
        }
        root = inflater.inflate(R.layout.fragment_putaway_header, container, false);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupView();
    }

    private void setupView(){
        //ImageView imgDocNo = root.findViewById(R.id.imgDocNo);
        //imgDocNo.setImageResource(R.drawable.ic_005);

        //setHeaderToolbar();




        edBarcode = root.findViewById(R.id.edBarcode);
        //edGacScan = root.findViewById(R.id.edGacScan);
        edJobNo = root.findViewById(R.id.edJobNo);
        //edGacScan.requestFocus();
        edGacScan = root.findViewById(R.id.edGacScan);
        edGacScan.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_ENTER){
                if(event.getAction() == KeyEvent.ACTION_DOWN){
                    return onGacScanEvent();
                }else{
                    return false;
                }
            }else{
                return false;
            }
        });


        MaterialButton btPhoto = root.findViewById(R.id.btnBack);
        btPhoto.setOnClickListener(v -> OnBackClick());

        MaterialButton btnPrint = root.findViewById(R.id.btNext);
        btnPrint.setOnClickListener(v -> OnNextClick());

/*        tvDocTypes = root.findViewById(R.id.acDocType);
        tvDocTypes.setOnItemClickListener((parent, view, position, id) -> {
            if (position > 0) {
                ordType = objectsAdapter.getItem(position);
            } else {
                ordType = null;
            }
        });*/
        ilGacScan = root.findViewById(R.id.ilGacScan);
        ilJobNo = root.findViewById(R.id.ilJobNo);
        ilBarcode = root.findViewById(R.id.ilBarcode);
        //rdoGroup = root.findViewById(R.id.radioGroup);
//        int check = rdoGroup.getCheckedRadioButtonId();
//        if (check==-1) {
//            showToast("Logistic Type is Mandatory!");
//            return;
//        }
        /*rdoGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int check = group.getCheckedRadioButtonId();
                RadioButton rdoButton = (RadioButton) group.findViewById(check);
                showToast(rdoButton.getText().toString());
                if((rdoButton.getText().toString().equalsIgnoreCase("FULL"))){
                    scanIdMandatory = true;
                    jobMandatory = false;
                    prodMandatory = false;
                    ilGacScan.setHint(getString(R.string.new_id));
                    ilJobNo.setHint(getString(R.string.job_no));
                    ilBarcode.setHint(getString(R.string.prod_barcode_optional));
                    putawayType = "P";
                }else{
                    scanIdMandatory = false;
                    jobMandatory = true;
                    prodMandatory = true;
                    ilGacScan.setHint(getString(R.string.new_id_optional));
                    ilJobNo.setHint(getString(R.string.job_no_mandatory));
                    ilBarcode.setHint(getString(R.string.prod_barcode));
                    putawayType = "M";
                }
            }
        });*/

        swFull= root.findViewById(R.id.swFull);
        swFull.setChecked(true);
        swlabel = root.findViewById(R.id.swlabel);
        swlabel.setText(getString(R.string.full));

        if(App.putawayMixtypeKey){
            scanIdMandatory = false;
            jobMandatory = true;
            prodMandatory = true;
            ilGacScan.setHint(getString(R.string.new_id_optional));
            ilJobNo.setHint(getString(R.string.job_no_mandatory));
            ilBarcode.setHint(getString(R.string.prod_barcode));
            putawayType = "M";
            swFull.setChecked(false);
            swlabel.setText(getString(R.string.mixed));
            edJobNo.setText(App.putawayMixtypeJob);
        }

        swFull.setOnClickListener(new View.OnClickListener() {   // inside oncreate
            @Override
            public void onClick(View view) {

                if( swFull.isChecked() ){
                    scanIdMandatory = true;
                    jobMandatory = false;
                    prodMandatory = false;
                    ilGacScan.setHint(getString(R.string.new_id));
                    ilJobNo.setHint(getString(R.string.job_no));
                    ilBarcode.setHint(getString(R.string.prod_barcode_optional));
                    putawayType = "P";
                    swlabel.setText(getString(R.string.full));
                    App.putawayMixtypeKey = false;
                }else{
                    scanIdMandatory = false;
                    jobMandatory = true;
                    prodMandatory = true;
                    ilGacScan.setHint(getString(R.string.new_id_optional));
                    ilJobNo.setHint(getString(R.string.job_no_mandatory));
                    ilBarcode.setHint(getString(R.string.prod_barcode));
                    putawayType = "M";
                    swlabel.setText(getString(R.string.mixed));
                    App.putawayMixtypeKey = true;
                }

            }

        });

    }

    public String getColoredString(String start) {
        int color = Color.RED;
        Spannable wordToSpan = new SpannableString(start);
        wordToSpan.setSpan(new ForegroundColorSpan(color), 0, start.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return wordToSpan.toString();
    }

    private boolean onGacScanEvent() {
        String gac = edGacScan.getText().toString();

        if (gac.isEmpty() || gac.trim().length() == 0) {
            showToast("Invalid GacScan");
            edGacScan.requestFocus();
            return false;
        } else {
            if(swFull.isChecked()){
                OnNextClick();
            }

        }
        return true;
    }

    private void setHeaderToolbar() {
        if (inbound_header != null) {
            Toolbar toolbar = root.findViewById(R.id.toolbar);
            TextView info1 = root.findViewById(R.id.info1);
            info1.setText(App.currentUser.USER_ID);
            TextView info2 = root.findViewById(R.id.info2);
            info2.setText(inbound_header.PRIN_CODE);
        }
    }

    private void OnNextClick() {
        if(Validate()){
            DownloadPutawayDocument(edGacScan.getText().toString(),edBarcode.getText().toString(),edJobNo.getText().toString());
        }
    }

    private void OnBackClick() {
        getActivity().onBackPressed();
    }

    private boolean Validate() {
        String errMessage;
        String gac = edGacScan.getText().toString();
        if (gac.length() == 0 && scanIdMandatory) {
            errMessage = String.format("%s required", "GAC Scan Id");
            showToast(errMessage);
            edGacScan.setError(errMessage);
            return false;
        }

        String job = edJobNo.getText().toString();
        if (job.length() == 0 && jobMandatory) {
            errMessage = String.format("%s required", "Job No");
            showToast(errMessage);
            edJobNo.setError(errMessage);
            return false;
        }

        String prod = edBarcode.getText().toString();
        if (prod.length() == 0 && prodMandatory) {
            errMessage = String.format("%s required", "Product Barcode");
            showToast(errMessage);
            edBarcode.setError(errMessage);
            return false;
        }

        return true;

    }

    private void DownloadPutawayDocument(String gacScan,String prodBarcode,String jobNo) {
//        if (gacScan.length() <= 0)  {
//            return;
//        }

        JsonObject requestObject = ServiceUtils.LogisticLabel.getPutAwayInfoByJobNo(jobNo,gacScan,prodBarcode,putawayType);

        showProgress(false);
        App.getNetworkClient().getAPIService().getRetGeneric(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("S")) {
                    String xmlDoc = genericRetResponse.getJsonString();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showAlert("Error", "No data received.");
                        return;
                    }
                    try {
                        items_prints = new Gson().fromJson(xmlDoc, LOGISTIC_LABEL_SUMBIT[].class);
                        if(items_prints.length==0){
                            showAlert("Error", "No data received.");
                            return;
                        }
                        App.putawayMixtypeJob = jobNo;
                        OpenPutAway();

                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                        showAlert("Error", e.getMessage());
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showAlert("Error", msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showAlert("Error", msg);
            }
        });
    }

/*    private void getDocTypes() {
        List<DocType> orTypList = App.getDatabaseClient().getAppDatabase().genericDao().getAllOrTyps();
        objectsAdapter = new ArrayAdapter<>(requireContext(),
                R.layout.dropdown_menu_popup_item, orTypList);
        tvDocTypes.setAdapter(objectsAdapter);
        tvDocTypes.setSelection(0);
        tvDocTypes.setThreshold(100);

        objectsAdapter.notifyDataSetChanged();
    }*/

    private void OpenGACScan() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putString(AppConstants.SELECTED_ID, "");
        bundle.putString(AppConstants.MASTER_ID, edBarcode.getText().toString());
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, items_prints[0]);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT_NEW,inbound_header);
        bundle.putLong(AppConstants.PARENT_ID, HDR_ID);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_gac_scan, bundle);
    }

    private void OpenPutAway() {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.SELECTED_OBJECT_NEW, items_prints[0]);
        bundle.putBoolean(AppConstants.SELECTED_ID, true);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_put_away, bundle);
    }

//    private void DownloadLogisticLabel(final String docNo, final String docType) {
//        JsonObject requestObject = ServiceUtils.GoodsReceipt.getGoodsReceiptHdr(docNo, docType);
//
//
//        showProgress(false);
//        App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
//            @Override
//            public void success(GenericRetResponse genericRetResponse, Response response) {
//                dismissProgress();
//                if (genericRetResponse.getErrCode().equals("S")) {
//                    String xmlDoc = genericRetResponse.getXmlDoc();
//                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
//                        showToast(" Error: No data received.");
//                        return;
//                    }
//                    try {
//                        PO_HDR[] po_hdr = new Gson().fromJson(xmlDoc, PO_HDR[].class);
//                        if (po_hdr==null || po_hdr.length<=0) {
//                            showToast(" Error: No data received.");
//                            return;
//                        }
//                        if (po_hdr.length > 0) {
//                            ordType = new DocType();
//                            ordType.CODE = po_hdr[0]. ORTYP;
//                            ordType.NAME = po_hdr[0]. ORTYP;
//                        }
//                        App.getDatabaseClient().getAppDatabase().genericDao().deletePO_HDR(docNo, ordType.CODE);
//                        App.getDatabaseClient().getAppDatabase().genericDao().insertPO_HDR(po_hdr);
//                        DownloadPO_DET(docNo, ordType.CODE);
//                    } catch (Exception e) {
//                        Log.d(App.TAG, e.toString());
//                    }
//                } else {
//                    String msg = genericRetResponse.getErrMessage();
//                    Log.d(App.TAG, msg);
//                    showToast(msg);
//                }
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                dismissProgress();
//                String msg = error.getMessage();
//                Log.d(App.TAG, msg);
//                showToast(msg);
//            }
//        });
//    }
//
//    private void DownloadPO_DET(final String docNo, final String docType) {
//        JsonObject requestObject = ServiceUtils.GoodsReceipt.getGoodsReceiptDet(docNo, docType);
//
//        showProgress(false);
//        App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
//            @Override
//            public void success(GenericRetResponse genericRetResponse, Response response) {
//                dismissProgress();
//                if (genericRetResponse.getErrCode().equals("S")) {
//                    String xmlDoc = genericRetResponse.getXmlDoc();
//                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
//                        showToast(" Error: No data received.");
//                        return;
//                    }
//                    try {
//                        PO_DET[] poDet = new Gson().fromJson(xmlDoc, PO_DET[].class);
//                        App.getDatabaseClient().getAppDatabase().genericDao().deletePO_DET(docNo, docType);
//                        App.getDatabaseClient().getAppDatabase().genericDao().insertPO_DET(poDet);
//                        OpenDocHeader();
//                    } catch (Exception e) {
//                        Log.d(App.TAG, e.toString());
//                    }
//                } else {
//                    String msg = genericRetResponse.getErrMessage();
//                    Log.d(App.TAG, msg);
//                    showToast(msg);
//                }
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                dismissProgress();
//                String msg = error.getMessage();
//                Log.d(App.TAG, msg);
//                showToast(msg);
//            }
//        });
//    }
}
