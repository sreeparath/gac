package com.dcode.gacinventory.ui.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.dcode.gacinventory.common.Utils;
import com.dcode.gacinventory.data.model.PHOTO_MASTER;
import com.example.gacinventory.R;

import java.util.List;

public class ImageViewAdapter
        extends RecyclerView.Adapter<ImageViewAdapter.RecycleViewHolder> {
    private List<PHOTO_MASTER> trackDetList;
    private Context context;
    private View.OnLongClickListener longClickListener;

    public ImageViewAdapter(Context context, List<PHOTO_MASTER> detList, View.OnLongClickListener longClickListener) {
        trackDetList = detList;
        this.context = context;
        this.longClickListener = longClickListener;
    }

    @NonNull
    @Override
    public RecycleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecycleViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_image, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecycleViewHolder holder, int position) {
        try {
            final PHOTO_MASTER track_det = trackDetList.get(position);

//            if (track_det == null || track_det.OBJECT_IMAGE.length() <= 0) {
//                return;
//            }

            Bitmap img = Utils.getBitmap(track_det.PHOTO_PATH);
            if (img == null) {
                return;
            }

            holder.imageView.setImageBitmap(img);
            holder.imageView.setTag(track_det);
//            holder.imageView.setTag(position,track_det);
            holder.imageView.setOnLongClickListener(longClickListener);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public PHOTO_MASTER getItemByPosition(int position) {
        return this.trackDetList.get(position);
    }

    public void deleteBatch(int position) {
        try {
//            grDetListFull.remove(position);
            trackDetList.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, trackDetList.size());
            notifyDataSetChanged();
        } catch (Exception e) {
            //Log.d(App.TAG, e.toString());
        }
    }

    @Override
    public int getItemCount() {
        return trackDetList.size();
    }

    public void addItems(List<PHOTO_MASTER> detList) {
        trackDetList = detList;
        notifyDataSetChanged();
    }

    static class RecycleViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;

        public RecycleViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.imageView);
        }
    }
}
