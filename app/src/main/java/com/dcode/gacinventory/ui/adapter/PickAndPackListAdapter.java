package com.dcode.gacinventory.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.data.model.SP_SCN_AN_OUT_LOC_BY_JOB;
import com.dcode.gacinventory.ui.view.MainActivity;
import com.example.gacinventory.R;
import com.google.gson.Gson;

import java.text.MessageFormat;
import java.util.List;

/**
 * @Created by: VIJIL DHAS A.S
 * @Date: 08-09-2021
 */
public class PickAndPackListAdapter extends RecyclerView.Adapter<PickAndPackListAdapter.RecycleViewHolder> {
    private List<SP_SCN_AN_OUT_LOC_BY_JOB> anOutLocByJobs;
    private Context context;
    private View.OnClickListener itemClickListener;
    private OnNavigateFragment navigateFragment;

    public PickAndPackListAdapter(Context context,
                                  List<SP_SCN_AN_OUT_LOC_BY_JOB> anOutLocByJobs,
                                  View.OnClickListener itemClickListener,
                                  OnNavigateFragment navigateFragment) {
        this.anOutLocByJobs = anOutLocByJobs;
        this.context = context;
        this.itemClickListener = itemClickListener;
        this.navigateFragment = navigateFragment;
    }

    @NonNull
    @Override
    public RecycleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PickAndPackListAdapter.RecycleViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pick_and_pack_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecycleViewHolder holder, @SuppressLint("RecyclerView") int position) {
        ((RecycleViewHolder) holder).txtSite.setText(anOutLocByJobs.get(position).SITE_CODE);
        ((RecycleViewHolder) holder).txtLoc.setText(anOutLocByJobs.get(position).LOC_CODE);
        ((RecycleViewHolder) holder).txtProduct.setText(anOutLocByJobs.get(position).PROD_CODE);
        ((RecycleViewHolder) holder).txtPqty.setText(MessageFormat.format("{0}({1})",
                anOutLocByJobs.get(position).PQTY, anOutLocByJobs.get(position).PUOM));

        ((RecycleViewHolder) holder).labelContainer.setTag(anOutLocByJobs.get(position));
        ((RecycleViewHolder) holder).labelContainer.setOnClickListener(itemClickListener);

        ((RecycleViewHolder) holder).txtButtonDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (navigateFragment != null) {
                    navigateFragment.onNavigateToFragment(anOutLocByJobs.get(position));
                }

            }
        });
    }

    public void addItems(List<SP_SCN_AN_OUT_LOC_BY_JOB> detList) {
        anOutLocByJobs = detList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return anOutLocByJobs.size();
    }

    static class RecycleViewHolder extends RecyclerView.ViewHolder {
        private TextView txtSite;
        private TextView txtLoc;
        private TextView txtProduct;
        private TextView txtPqty;
        private TextView txtButtonDetails;
        private LinearLayout labelContainer;

        public RecycleViewHolder(@NonNull View itemView) {
            super(itemView);
            txtSite = itemView.findViewById(R.id.txt_site);
            txtLoc = itemView.findViewById(R.id.txt_loc);
            txtProduct = itemView.findViewById(R.id.txt_product);
            txtPqty = itemView.findViewById(R.id.txt_pqty);
            txtButtonDetails = itemView.findViewById(R.id.txt_details_button);
            labelContainer = itemView.findViewById(R.id.top_label_container);
        }
    }

    public interface OnNavigateFragment {
        void onNavigateToFragment(SP_SCN_AN_OUT_LOC_BY_JOB spScnAnOutLocByJob);
    }
}
