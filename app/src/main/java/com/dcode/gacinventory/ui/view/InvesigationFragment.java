package com.dcode.gacinventory.ui.view;

import android.app.AlertDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.common.AppPreferences;
import com.dcode.gacinventory.common.StagingValidation;
import com.dcode.gacinventory.common.ValidRespSet;
import com.dcode.gacinventory.data.model.INBOUND_HEADER;
import com.dcode.gacinventory.data.model.INVESIGATE_REGISTER;
import com.dcode.gacinventory.data.model.INVEST_REASONS;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SUMBIT;
import com.dcode.gacinventory.data.model.LOT_STATUS;
import com.dcode.gacinventory.data.model.LOT_STATUS_LIST;
import com.dcode.gacinventory.data.model.PUTAWAY_SUMBIT;
import com.dcode.gacinventory.network.model.GenericRetResponse;
import com.dcode.gacinventory.network.service.ServiceUtils;
import com.example.gacinventory.R;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.JsonObject;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.dcode.gacinventory.common.AppConstants.INVEST_HEADER;
import static com.dcode.gacinventory.common.AppConstants.INVEST_PUT;
import static com.dcode.gacinventory.common.AppConstants.INVEST_STAG;

public class InvesigationFragment extends BaseFragment {
    private TextView edInfoNew ;
//    private AutoCompleteTextView tvDocTypes;
//    private ArrayAdapter<DocType> objectsAdapter;
    private int promptResource;
    private String DocNo;
    private long HDR_ID;
    private RadioGroup rdoGroup;
    private LOGISTIC_LABEL_SUMBIT logistic_label_sumbit;
    private TextInputEditText edJobCode;
    private TextInputEditText edInvestNo;
    private TextInputEditText edRemarks;
    private View root;
    private INVESIGATE_REGISTER invesigateRegister;
    private INBOUND_HEADER inboundHeader;
    private AutoCompleteTextView edReason;
    private ArrayAdapter<INVEST_REASONS> investReasonsArrayAdapter;
    private INVEST_REASONS investReasons;

    StagingValidation stagingValidation;
    private boolean mResult = false;
    private long fromModule = -1;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

            ModuleID = this.getArguments().getInt("ModuleID", 0);
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                logistic_label_sumbit = (LOGISTIC_LABEL_SUMBIT) bundle.getSerializable(AppConstants.SELECTED_OBJECT_NEW);
                if(bundle.containsKey(AppConstants.SELECTED_OBJECT)){
                    inboundHeader = (INBOUND_HEADER) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
                }
                fromModule = bundle.containsKey(AppConstants.SELECTED_ID) ? bundle.getLong(AppConstants.SELECTED_ID) : -1;
            }

        root = inflater.inflate(R.layout.fragment_investogation, container, false);

        setupView();
        return root;
    }


    private void setupView(){
        setHeaderToolbar();

        edJobCode= root.findViewById(R.id.edJobCode);
        edInvestNo= root.findViewById(R.id.edInvestNo);
        edRemarks= root.findViewById(R.id.edRemarks);
        edReason = root.findViewById(R.id.edReason);

        MaterialButton btnBack = root.findViewById(R.id.btnBack);
        btnBack.setOnClickListener(v -> getActivity().onBackPressed());

        MaterialButton btnNext = root.findViewById(R.id.btnNext);
        btnNext.setOnClickListener(v -> OnNextClick());
    }

    private void setHeaderToolbar() {
        if (inboundHeader != null) {
            Toolbar toolbar = root.findViewById(R.id.toolbar);
            TextView info1 = root.findViewById(R.id.info1);
            info1.setText(App.currentUser.USER_ID);
            TextView info2 = root.findViewById(R.id.info2);
            info2.setText(inboundHeader.JOB_NO + " - "+ inboundHeader.PRIN_NAME);
        }
    }


   @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       fillInvestHeader();
       FillReason();
    }

    private void fillInvestHeader(){
        if(inboundHeader==null){
            return;
        }
        edJobCode.setText(inboundHeader.JOB_NO);

    }

/*    private boolean onDocNoKeyEvent() {
        DocNo = edDocNo.getText().toString();

        if (DocNo.isEmpty() || DocNo.trim().length() == 0) {
            showToast(String.format("%s required", getString(promptResource)));
            return false;
        }
        return true;
    }*/



    private void OnNextClick() {
        if(Validate()){
            invesigateRegister = new INVESIGATE_REGISTER();
            invesigateRegister.JOB_NO = edJobCode.getText().toString();
            invesigateRegister.INVESTIGATION_NO = edInvestNo.getText().toString();
            invesigateRegister.INVESTIGATION_REMARKS = edRemarks.getText().toString();
            invesigateRegister.INVESTIGATION_REASON = investReasons.REASON_CODE;

            uploadInvestigationRegister();
        }
    }






    private void backToHome() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        ((MainActivity) requireActivity()).NavigateFullBackStack();
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_home, bundle);
    }

    private void backToDirectPutaway() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        ((MainActivity) requireActivity()).NavigateFullBackStack();
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_put_away_header, bundle);
    }

    private void backToHeader() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, inboundHeader);
        ((MainActivity) requireActivity()).NavigateFullBackStack();
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_fragment_investigation_header, bundle);
    }

    private boolean Validate() {
        String errMessage;
//        String scanLoc = edInvestNo.getText().toString();
//        if (scanLoc.length() == 0) {
//            errMessage = String.format("%s required", "Investigation No");
//            showToast(errMessage);
//            edInvestNo.setError(errMessage);
//            return false;
//        }
        return true;

    }



    private void OpenGACScan() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putString(AppConstants.SELECTED_ID, DocNo);
        bundle.putSerializable(AppConstants.SELECTED_CODE, "");
        bundle.putLong(AppConstants.PARENT_ID, HDR_ID);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_gac_scan, bundle);
    }


    private void uploadInvestigationRegister() {
        if(invesigateRegister==null){
            return;
        }

        showToast(getString(R.string.send_data_to_server));
        JsonObject requestObject = ServiceUtils.LogisticLabel.getInvestigationData(invesigateRegister);
        showProgress(false);
        App.getNetworkClient().getAPIService().submitWithLinesData(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericSubmissionResponse, Response response) {
                dismissProgress();
                if (genericSubmissionResponse.getErrCode().equals("S")) {
                        String msg = genericSubmissionResponse.getErrMessage();
                        resetUI();
                        showAlert("Success",msg);
                        if(fromModule==INVEST_HEADER){
                            backToHeader();
                        }else if(fromModule==INVEST_STAG){
                            backToLogistic();
                        }else if(fromModule==INVEST_PUT){
                            backToPutaway();
                        }
                } else {
                    Log.d(App.TAG, genericSubmissionResponse.getErrMessage());
                    showAlert("Error",genericSubmissionResponse.getErrMessage());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(App.TAG, error.getMessage());
                showToast(error.getMessage());
                dismissProgress();
            }
        });
    }

    private void FillReason() {
        List<INVEST_REASONS> lot_status_lists = App.getDatabaseClient().getAppDatabase().genericDao().getInvestReason();
        if(lot_status_lists!=null&& lot_status_lists.size()>0){
            investReasonsArrayAdapter = new ArrayAdapter<>(getContext(), R.layout.dropdown_menu_popup_item, lot_status_lists);
            edReason.setAdapter(investReasonsArrayAdapter);
            edReason.setThreshold(100);
            edReason.setOnItemClickListener((parent, view, position, id) -> {
                if (position >= 0) {
                    investReasons = investReasonsArrayAdapter.getItem(position);
                } else {
                    investReasons = null;
                }

            });
        }


    }

    private void backToPutaway() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        ((MainActivity) requireActivity()).NavigateFullBackStack();
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_put_away_header, bundle);
    }

    private void backToLogistic() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, inboundHeader);
        ((MainActivity) requireActivity()).NavigateFullBackStack();
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_logistic_label, bundle);
    }

    private void resetUI(){
        edJobCode.setText("");
        inboundHeader = null;
    }

}
