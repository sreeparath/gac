package com.dcode.gacinventory.ui.menus;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.dcode.gacinventory.ui.adapter.ImageGlideAdapter;
import com.dcode.gacinventory.ui.interfaces.DebouncedOnClickListener;
import com.dcode.gacinventory.ui.interfaces.MenuSelectorResponse;
import com.example.gacinventory.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Vijildhas
 */
public class MenuAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context mContext;
    private List<MenuItems> mMenuItemsList;
    private final MenuSelectorResponse mMenuSelectorResponse;
    private Integer selectedMenuId;

    public MenuAdapter(Context context,
                       List<MenuItems> menuItemsList,
                       Integer selectedMenuId,
                       MenuSelectorResponse selectorResponse) {
        this.mContext = context;
        mMenuItemsList = new ArrayList<>();
        this.mMenuItemsList = menuItemsList;
        this.selectedMenuId = selectedMenuId;
        this.mMenuSelectorResponse = selectorResponse;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MenuViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_menu_items_row, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        //holder.onBind(position);
        ((MenuViewHolder) holder).onBind(position);
    }

    @Override
    public int getItemCount() {
        return mMenuItemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    private class MenuViewHolder extends RecyclerView.ViewHolder {
        private final ImageView mImageMenu;
        private final ImageView mImageSelected;
        private final TextView mTextMenuName;
        private final View rootView;
        private final View bottomView;

        MenuViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            mImageMenu = rootView.findViewById(R.id.image_menu);
            mTextMenuName = rootView.findViewById(R.id.txt_menu_name);
            bottomView = rootView.findViewById(R.id.bottom_view);
            mImageSelected = rootView.findViewById(R.id.image_selected);
            setFont();
        }

        /**
         * font setup
         */
        public void setFont() {
        }

        public void onBind(int position) {

            if (position == mMenuItemsList.size() - 1) {
                bottomView.setVisibility(View.GONE);
            } else {
                bottomView.setVisibility(View.VISIBLE);
            }
            if (selectedMenuId != null &&
                    mMenuItemsList.get(position).getMenuID() == selectedMenuId) {
                mImageSelected.setVisibility(View.VISIBLE);
            } else {
                mImageSelected.setVisibility(View.GONE);
            }

            mTextMenuName.setText(mMenuItemsList.get(position).getMenuName());
            if (mMenuItemsList.get(position).getMenuImage() != -1) {
                mImageMenu.setVisibility(View.VISIBLE);
                mImageMenu.setImageResource(mMenuItemsList.get(position).getMenuImage());
            } else {
                mImageMenu.setVisibility(View.INVISIBLE);
            }

            rootView.setOnClickListener(new DebouncedOnClickListener() {
                @Override
                protected void onDebouncedClick(View v) {
                    mMenuSelectorResponse.onSelectedMenu(mMenuItemsList.get(position).getMenuID());
                }
            });
        }


    }
}