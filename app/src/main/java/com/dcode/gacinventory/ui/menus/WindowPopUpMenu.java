package com.dcode.gacinventory.ui.menus;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.dcode.gacinventory.ui.interfaces.DebouncedOnClickListener;
import com.dcode.gacinventory.ui.interfaces.MenuSelectorResponse;
import com.example.gacinventory.R;
import com.google.android.material.button.MaterialButton;

import java.util.List;
import java.util.Objects;

/**
 *
 */
public class WindowPopUpMenu {

    public static WindowPopUpMenu getInstance() {
        return new WindowPopUpMenu();
    }

    @SuppressLint("InflateParams")
    public PopupWindow popupDisplay(Context context,
                                    List<MenuItems> menuItemsList,
                                    Integer selectedMenuId,
                                    MenuSelectorResponse response) {
        MenuAdapter menuAdapter;
        PopupWindow popupWindow = new PopupWindow(context);
        // inflate your layout or dynamically add view
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = Objects.requireNonNull(inflater).inflate(R.layout.layout_menu_container, null);
        ConstraintLayout menuContainer = view.findViewById(R.id.root_menu_container);


        RecyclerView rcvMenuList = view.findViewById(R.id.rcv_menu);

        menuAdapter = new MenuAdapter(context, menuItemsList,
                selectedMenuId, new MenuSelectorResponse() {
            @Override
            public void onSelectedMenu(int selectedMenu) {
                popupWindow.dismiss();
                response.onSelectedMenu(selectedMenu);
            }

            @Override
            public void onNavigateOptionASelector() {

            }

            @Override
            public void onNavigateOptionBSelector() {

            }

            @Override
            public void onSelectedBottomMenu(String menuName) {

            }
        });
        rcvMenuList.setLayoutManager(new LinearLayoutManager(context));
        rcvMenuList.setAdapter(menuAdapter);
        menuContainer.setOnClickListener(new DebouncedOnClickListener() {
            @Override
            protected void onDebouncedClick(View v) {
                if (popupWindow.isShowing()) popupWindow.dismiss();
            }
        });


        popupWindow.setBackgroundDrawable(null);
        popupWindow.setFocusable(true);
//        popupWindow.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
//        popupWindow.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popupWindow.setContentView(view);

        return popupWindow;
    }
}