package com.dcode.gacinventory.ui.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.common.GS1Identifiers;
import com.dcode.gacinventory.common.StagingValidation;
import com.dcode.gacinventory.common.ValidRespSet;
import com.dcode.gacinventory.data.model.INBOUND_HEADER;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SUMBIT;
import com.dcode.gacinventory.network.model.GenericRetResponse;
import com.dcode.gacinventory.network.service.ServiceUtils;
import com.example.gacinventory.R;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.Serializable;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class DirectPutAwayHeaderFragment extends BaseFragment {
    private TextInputEditText edDocNo,edBarcode,edGacScan,edLotNo;
    private TextInputLayout ilLotNo;
//    private AutoCompleteTextView tvDocTypes;
//    private ArrayAdapter<DocType> objectsAdapter;
    private int promptResource;
    private String putawayType,newScanId;
    private long HDR_ID;
    private CheckBox chkSelect;
    private LOGISTIC_LABEL_SUMBIT[] items_prints;
    private View root;
    private INBOUND_HEADER inbound_header;
    private StagingValidation stagingValidation;
    Map<String, String> gs1Map;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true);
        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);
            putawayType = this.getArguments().getString("PutAwayType", "");
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                //DocNo = bundle.getString(AppConstants.SELECTED_ID);
                //barcode = bundle.getString(AppConstants.MASTER_ID);
                if(bundle.containsKey(AppConstants.SELECTED_ID)){
                    putawayType =bundle.getString(AppConstants.SELECTED_ID);
                }
                //newScanId = bundle.containsKey(AppConstants.SELECTED_ID) ? bundle.getString(AppConstants.MASTER_ID) : "";
            }
        }
        root = inflater.inflate(R.layout.fragment_direct_putaway_header, container, false);
        setupView();
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(newScanId!=null && newScanId.length()>0){
            edGacScan.setText(newScanId);
        }
    }

    private void setupView(){
        //ImageView imgDocNo = root.findViewById(R.id.imgDocNo);
        //imgDocNo.setImageResource(R.drawable.ic_005);

        //setHeaderToolbar();

        stagingValidation = new StagingValidation();


        edBarcode = root.findViewById(R.id.edBarcode);
//        edBarcode.setOnKeyListener((v, keyCode, event) -> {
//            if(keyCode == KeyEvent.KEYCODE_ENTER){
//                if(event.getAction() == KeyEvent.ACTION_DOWN){
//                    return onBarcodeKeyEvent();
//                }else{
//                    return false;
//                }
//            }else{
//                return false;
//            }
//        });
        edGacScan = root.findViewById(R.id.edGacScan);
        //edGacScan.requestFocus();



        MaterialButton btPhoto = root.findViewById(R.id.btnBack);
        btPhoto.setOnClickListener(v -> OnBackClick());

        MaterialButton btnPrint = root.findViewById(R.id.btNext);
        btnPrint.setOnClickListener(v -> OnNextClick());

/*        tvDocTypes = root.findViewById(R.id.acDocType);
        tvDocTypes.setOnItemClickListener((parent, view, position, id) -> {
            if (position > 0) {
                ordType = objectsAdapter.getItem(position);
            } else {
                ordType = null;
            }
        });*/
    }

/*    private boolean onDocNoKeyEvent() {
        DocNo = edDocNo.getText().toString();

        if (DocNo.isEmpty() || DocNo.trim().length() == 0) {
            showToast(String.format("%s required", getString(promptResource)));
            return false;
        }
        return true;
    }*/

    private boolean onBarcodeKeyEvent() {
        String ScanCode = edBarcode.getText().toString();

        if (ScanCode.isEmpty() || ScanCode.trim().length() == 0) {
            showToast("Invalid Barcode no");
            edBarcode.requestFocus();
            return false;
        } else {
            ParseBarCode(ScanCode);
        }
        return true;
    }

    private void ParseBarCode(String scanCode) {
        if (scanCode.length() <= 0 ) {
            return;
        }
        ValidRespSet validObject = stagingValidation.ValidateGS1Barcode(App.dynFuncs, "473");
        if (validObject.getErrCode().length() > 0 && validObject.getErrCode().equals("S")) {
            showToast(validObject.getErrMSG());
            if (validObject.getErrMSG().length() > 0) {
                gs1Map  = GS1Identifiers.decodeBarcodeGS1_128(scanCode,",",true,validObject.getErrMSG());
            }
        }

    }

    private void setHeaderToolbar() {
        if (inbound_header != null) {
            Toolbar toolbar = root.findViewById(R.id.toolbar);
            TextView info1 = root.findViewById(R.id.info1);
            info1.setText(App.currentUser.USER_ID);
            TextView info2 = root.findViewById(R.id.info2);
            info2.setText(inbound_header.PRIN_CODE);
        }
    }

    private void OnNextClick() {
        if(Validate()){
            DownloadPutawayDocument(edGacScan.getText().toString(),edBarcode.getText().toString());
        }
    }

    private void OnBackClick() {
        getActivity().onBackPressed();
    }

    private boolean Validate() {
        String errMessage;
//        String DocNo = edDocNo.getText().toString();
//        if (chkSelect.isChecked() && DocNo.length() == 0) {
//            errMessage = String.format("%s required", "Logistic Label");
//            showToast(errMessage);
//            edDocNo.setError(errMessage);
//            return false;
//        }
        String gac = edGacScan.getText().toString();
        if (gac.length() == 0) {
            errMessage = String.format("%s required", "GAC Scan Id");
            showToast(errMessage);
            edGacScan.setError(errMessage);
            return false;
        }
//        String barcode = edBarcode.getText().toString();
//        if (barcode.length() == 0) {
//            errMessage = String.format("%s required", "Product Barcode");
//            showToast(errMessage);
//            edBarcode.setError(errMessage);
//            return false;
//        }

//        if(inbound_header==null){
//            errMessage = String.format("%s required", "Inbound Header");
//            showToast(errMessage);
//            return false;
//        }

        return true;

    }

    private void DownloadPutawayDocument(String gacScan,String prodBarcode) {
        if (gacScan.length() <= 0)  {
            return;
        }

        JsonObject requestObject = ServiceUtils.LogisticLabel.getPutAwayInfo(gacScan,prodBarcode,putawayType);

        showProgress(false);
        App.getNetworkClient().getAPIService().getRetGeneric(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("S")) {
                    String xmlDoc = genericRetResponse.getJsonString();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showAlert("Error", "No data received.");
                        return;
                    }
                    try {
                        items_prints = new Gson().fromJson(xmlDoc, LOGISTIC_LABEL_SUMBIT[].class);
                        if(items_prints.length==0){
                            showAlert("Error", "No data received.");
                            return;
                        }
                        OpenDirectPutAway();

                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                        showAlert("Error", e.getMessage());
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showAlert("Error", msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showAlert("Error", msg);
            }
        });
    }

/*    private void getDocTypes() {
        List<DocType> orTypList = App.getDatabaseClient().getAppDatabase().genericDao().getAllOrTyps();
        objectsAdapter = new ArrayAdapter<>(requireContext(),
                R.layout.dropdown_menu_popup_item, orTypList);
        tvDocTypes.setAdapter(objectsAdapter);
        tvDocTypes.setSelection(0);
        tvDocTypes.setThreshold(100);

        objectsAdapter.notifyDataSetChanged();
    }*/

    private void OpenGACScan() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putString(AppConstants.SELECTED_ID, "");
        bundle.putString(AppConstants.MASTER_ID, edBarcode.getText().toString());
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, items_prints[0]);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT_NEW,inbound_header);
        bundle.putLong(AppConstants.PARENT_ID, HDR_ID);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_gac_scan, bundle);
    }

    private void OpenDirectPutAway() {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.SELECTED_OBJECT_NEW, items_prints[0]);
        bundle.putBoolean(AppConstants.SELECTED_ID, true);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT_NEW2, (Serializable) gs1Map);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_direct_put_away, bundle);
    }

//    private void DownloadLogisticLabel(final String docNo, final String docType) {
//        JsonObject requestObject = ServiceUtils.GoodsReceipt.getGoodsReceiptHdr(docNo, docType);
//
//
//        showProgress(false);
//        App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
//            @Override
//            public void success(GenericRetResponse genericRetResponse, Response response) {
//                dismissProgress();
//                if (genericRetResponse.getErrCode().equals("S")) {
//                    String xmlDoc = genericRetResponse.getXmlDoc();
//                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
//                        showToast(" Error: No data received.");
//                        return;
//                    }
//                    try {
//                        PO_HDR[] po_hdr = new Gson().fromJson(xmlDoc, PO_HDR[].class);
//                        if (po_hdr==null || po_hdr.length<=0) {
//                            showToast(" Error: No data received.");
//                            return;
//                        }
//                        if (po_hdr.length > 0) {
//                            ordType = new DocType();
//                            ordType.CODE = po_hdr[0]. ORTYP;
//                            ordType.NAME = po_hdr[0]. ORTYP;
//                        }
//                        App.getDatabaseClient().getAppDatabase().genericDao().deletePO_HDR(docNo, ordType.CODE);
//                        App.getDatabaseClient().getAppDatabase().genericDao().insertPO_HDR(po_hdr);
//                        DownloadPO_DET(docNo, ordType.CODE);
//                    } catch (Exception e) {
//                        Log.d(App.TAG, e.toString());
//                    }
//                } else {
//                    String msg = genericRetResponse.getErrMessage();
//                    Log.d(App.TAG, msg);
//                    showToast(msg);
//                }
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                dismissProgress();
//                String msg = error.getMessage();
//                Log.d(App.TAG, msg);
//                showToast(msg);
//            }
//        });
//    }
//
//    private void DownloadPO_DET(final String docNo, final String docType) {
//        JsonObject requestObject = ServiceUtils.GoodsReceipt.getGoodsReceiptDet(docNo, docType);
//
//        showProgress(false);
//        App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
//            @Override
//            public void success(GenericRetResponse genericRetResponse, Response response) {
//                dismissProgress();
//                if (genericRetResponse.getErrCode().equals("S")) {
//                    String xmlDoc = genericRetResponse.getXmlDoc();
//                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
//                        showToast(" Error: No data received.");
//                        return;
//                    }
//                    try {
//                        PO_DET[] poDet = new Gson().fromJson(xmlDoc, PO_DET[].class);
//                        App.getDatabaseClient().getAppDatabase().genericDao().deletePO_DET(docNo, docType);
//                        App.getDatabaseClient().getAppDatabase().genericDao().insertPO_DET(poDet);
//                        OpenDocHeader();
//                    } catch (Exception e) {
//                        Log.d(App.TAG, e.toString());
//                    }
//                } else {
//                    String msg = genericRetResponse.getErrMessage();
//                    Log.d(App.TAG, msg);
//                    showToast(msg);
//                }
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                dismissProgress();
//                String msg = error.getMessage();
//                Log.d(App.TAG, msg);
//                showToast(msg);
//            }
//        });
//    }
}
