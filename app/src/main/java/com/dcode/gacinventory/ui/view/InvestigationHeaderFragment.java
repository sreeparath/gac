package com.dcode.gacinventory.ui.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.common.AppVariables;
import com.dcode.gacinventory.common.Utils;
import com.dcode.gacinventory.data.model.GENERATE_LABELS;
import com.dcode.gacinventory.data.model.INBOUND_HEADER;
import com.dcode.gacinventory.data.model.PHOTO_MASTER;
import com.dcode.gacinventory.network.model.GenericRetResponse;
import com.dcode.gacinventory.network.model.OAuthTokenResponse;
import com.dcode.gacinventory.network.service.ServiceUtils;
import com.dcode.gacinventory.ui.adapter.ImageViewAdapter;
import com.example.gacinventory.R;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.dcode.gacinventory.common.AppConstants.INVEST_HEADER;
import static com.dcode.gacinventory.common.AppConstants.VISIT_IMAGE;

public class InvestigationHeaderFragment extends BaseFragment implements View.OnLongClickListener{
    private TextInputEditText edContainer,edTruck,edGscan,edJobNo;
//    private AutoCompleteTextView tvDocTypes;
//    private ArrayAdapter<DocType> objectsAdapter;
    private int promptResource;
    //private String DocNo;
    private long HDR_ID=-1;
    private FloatingActionButton btnCamera;
    private Bitmap photo;
    private ImageViewAdapter imageViewAdapter;
    View root;
    private RecyclerView recyclerView;
    private Drawable icon;
    private final ColorDrawable background = new ColorDrawable(Color.RED);
    //private MENU menu;
    private INBOUND_HEADER inbound_header;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        //setHasOptionsMenu(true);

        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);
            Bundle bundle = this.getArguments();
//            if (bundle != null) {
//                menu = (MENU) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
//            }

        }

        root = inflater.inflate(R.layout.fragment_investigation_header, container, false);
        icon = getResources().getDrawable(R.drawable.ic_delete_black);



        return root;
    }

    private void setupView(){

        edJobNo = root.findViewById(R.id.edJobNo);
        edJobNo.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_ENTER){
                if(event.getAction() == KeyEvent.ACTION_DOWN){
                    return onJOBKeyEvent();
                }else{
                    return false;
                }
            }else{
                return false;
            }
        });


        edContainer = root.findViewById(R.id.edContainer);
        edTruck = root.findViewById(R.id.edTruck);
        edGscan = root.findViewById(R.id.edGscan);


        MaterialButton btnNext = root.findViewById(R.id.btnNext);
        btnNext.setOnClickListener(v -> {
            OnNextClick();
        });

        MaterialButton btnBack = root.findViewById(R.id.btnBack);
        btnBack.setOnClickListener(v -> getActivity().onBackPressed());



    }



//    @Override
//    public void onResume() {
//        super.onResume();
////        edJobNo.clearFocus();
////        edJobNo.setFocusableInTouchMode(true);
////        edJobNo.requestFocus();
////        edJobNo.requestFocusFromTouch();
//    }

    private void DownloadOauthToken(){
        App.getNetworkClientOAuth().getAPIService().getOauthToken("client_credentials",new Callback<OAuthTokenResponse>() {
            @Override
            public void success(OAuthTokenResponse response, Response response2) {
                Log.d("success##", response.toString());
                if(response.getAccessToken().length()==0){
                    showAlert("Warning","Authorization Token Not Found!");
                    return;
                }
                AppVariables.setAccessToken(response.getAccessToken());
            }
            @Override
            public void failure(RetrofitError error) {
                Log.d("failure##", error.toString());
            }
        }) ;
    }


private void openConfirmationList(){
       // String jobNo = edJobNo.getText().toString();
    if (inbound_header==null) {
        showToast("Enter Job Id");
        return;
    }

//    if(inbound_header==null){
//        String errMessage = "Inbound header data not found!";
//        showToast(errMessage);
//        return;
//    }

    Bundle bundle = new Bundle();
    bundle.putInt(AppConstants.MODULE_ID, ModuleID);
    bundle.putString(AppConstants.SELECTED_ID, inbound_header.JOB_NO);
    bundle.putSerializable(AppConstants.SELECTED_OBJECT, inbound_header);
    ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_confirmation_list_tablayout, bundle);

}






    private boolean onJOBKeyEvent() {
        String ScanCode = edJobNo.getText().toString();

        if (ScanCode.isEmpty() || ScanCode.trim().length() == 0) {
            showToast("Invalid Job No");
            edJobNo.requestFocus();
            return false;
        } else {
            ParseBarCode(ScanCode,"JOB");
        }
        return true;
    }

    private void ParseBarCode(String scanCode,String mode) {
        if (scanCode.length() <= 0 ) {
            return;
        }
        DownloadInboundHeader(scanCode,mode);
    }

    private void fetchPhotos(){
        List<PHOTO_MASTER> photo_masters = App.getDatabaseClient().getAppDatabase().genericDao().getPhotoMasterList(ModuleID,edJobNo.getText().toString());
        imageViewAdapter.addItems(photo_masters);
        imageViewAdapter.notifyDataSetChanged();
    }

   @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

       setupView();



   }

/*    private boolean onDocNoKeyEvent() {
        DocNo = edDocNo.getText().toString();

        if (DocNo.isEmpty() || DocNo.trim().length() == 0) {
            showToast(String.format("%s required", getString(promptResource)));
            return false;
        }
        return true;
    }*/

    private void OnNextClick() {
        Validate();
    }

    private void Validate() {
        String errMessage;
        String jobNo = edJobNo.getText().toString();
        if (jobNo.length() == 0) {
            errMessage = String.format("%s required", "Job no");
            showToast(errMessage);
            edJobNo.setError(errMessage);
            return;
        }
        if(inbound_header==null){
            errMessage = "Inbound header data not found!";
            showToast(errMessage);
            return;
        }
        OpenInvestigation();
    }


    private void onCameraClick(){
        if (edJobNo.getText().toString().length()==0) {
            showToast("Enter Job Id");
            return;
        }

        if(inbound_header==null){
            String errMessage = "Inbound header data not found!";
            showToast(errMessage);
            return;
        }

        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, VISIT_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (resultCode != Activity.RESULT_OK) return;

        if (requestCode == VISIT_IMAGE) {
            photo = (Bitmap) intent.getExtras().get("data");
            SavePhoto();
        }


        if (requestCode == AppConstants.JOB_ID) {
            inbound_header = intent.hasExtra(AppConstants.SELECTED_OBJECT) ? (INBOUND_HEADER) intent.getSerializableExtra(AppConstants.SELECTED_OBJECT) : null;
            if (inbound_header != null) {
                fillHeader(inbound_header);
            }
        }
    }

    private void SavePhoto() {
        String photoFileName = getPhotoFileName();
        Log.d("photoFileName##",photoFileName);
        if (photo != null) {
//            pngBase64 = Utils.getBase64EncodedPNG(photo);
            try (FileOutputStream out = new FileOutputStream(photoFileName)) {
                photo.compress(Bitmap.CompressFormat.PNG, 100, out);
                fetchPhotos();
                // PNG is a lossless format, the compression factor (100) is ignored
            } catch (IOException e) {
                e.printStackTrace();
            }

            PHOTO_MASTER photo_master = new PHOTO_MASTER();
            photo_master.MODULE_ID = ModuleID;
            long seqNo = Utils.GetMaxValue("PHOTO_MASTER", "SLNO", false,null , null);
            photo_master.SLNO = seqNo;
            photo_master.JOB_ID = edJobNo.getText().toString();
            photo_master.IS_UPLOAD = 0;
            photo_master.PHOTO_PATH = photoFileName;
            App.getDatabaseClient().getAppDatabase().genericDao().insertPhotoMaster(photo_master);
            fetchPhotos();
        }
    }

    public String getPhotoFileName() {
        SimpleDateFormat dF = new SimpleDateFormat(Utils.DATETIME_NUM_FORMAT, Locale.getDefault());
        String fileName = dF.format(new Date()) + ".png";
        File outputFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath() + "/Camera/");
        try {
            if (!outputFile.exists() && !outputFile.isDirectory()) {
                outputFile.mkdir();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return outputFile.getAbsolutePath() + "/" + fileName;
    }

    private void OpenInvestigation() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, inbound_header);
        bundle.putLong(AppConstants.SELECTED_ID, INVEST_HEADER);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_fragment_investigation, bundle);
    }
    @Override
    public boolean onLongClick(View view) {


        return true;
    }



    private void DownloadInboundHeader(String docNo,String mode) {
        if (docNo.length() <= 0) {
            return;
        }

        JsonObject requestObject = ServiceUtils.LogisticLabel.getInboundHeader(docNo,mode);

        showProgress(false);
        App.getNetworkClient().getAPIService().getRetGeneric(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("S")) {
                    String xmlDoc = genericRetResponse.getJsonString();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showAlert("Error", "No data received.");
                        return;
                    }
                    try {
                        INBOUND_HEADER[] items_prints = new Gson().fromJson(xmlDoc, INBOUND_HEADER[].class);
                        if(items_prints.length==0){
                            showAlert("Error", "No data received.");
                            return;
                        }
                        if(items_prints.length>1){
                            try {
                                OnJobNoLookupClick(AppConstants.JOB_KEY, AppConstants.JOB_ID, -1,items_prints);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }else {
                            inbound_header = items_prints[0];
                            fillHeader(inbound_header);
                        }

                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                        showAlert("Error", e.getMessage());
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showAlert("Error", msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showAlert("Error", msg);
            }
        });
    }

    private void fillHeader(INBOUND_HEADER inbound_header){
        edJobNo.setText(inbound_header.JOB_NO);
        edContainer.setText(String.valueOf(inbound_header.SEAL_NO));
        edTruck.setText(String.valueOf(inbound_header.TRUCK_NO));
        if(inbound_header.GACSCANID_COUNT==0 ){
            inbound_header.GACSCANID_COUNT = 5;
        }
        edGscan.setText(String.valueOf(inbound_header.GACSCANID_COUNT));
    }

    private void resetUI() {
        edJobNo.setText("");
        edContainer.setText("");
        edTruck.setText("");
        edGscan.setText("");
        inbound_header = new INBOUND_HEADER();
    }





    @Override
    public void onCreateOptionsMenu(@NonNull @NotNull Menu menu, @NonNull @NotNull MenuInflater inflater) {
        menu.clear();

        // Add the new menu items
        inflater.inflate(R.menu.main, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.actionJobStatus:
                openConfirmationList();
                return true;
            case R.id.actionImages:
                //openImages();
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
