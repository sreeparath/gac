package com.dcode.gacinventory.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.gacinventory.data.model.BOX_SCAN_GACID;
import com.dcode.gacinventory.data.model.JOB_CLOSURE_INFO;
import com.example.gacinventory.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BarcodeAdapterList
        extends RecyclerView.Adapter<BarcodeAdapterList.RecyclerViewHolder>
        implements Filterable {

    private Context context;
    private View.OnClickListener shortClickListener;
    private List<BOX_SCAN_GACID> dataList;
    private List<BOX_SCAN_GACID> dataListFull;
    private List<String> dataArray;

    private Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<BOX_SCAN_GACID> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(dataListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

//                for (BARCODE item : dataListFull) {
//                    if (item.MENU_NAME.toLowerCase().contains(filterPattern.toLowerCase()) ||
//                            item.MENU_DESC.toLowerCase().contains(filterPattern.toLowerCase())) {
//                        filteredList.add(item);
//                    }
//                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
//            dataList.clear();
//            dataList.putAll((List) results.values);
//            notifyDataSetChanged();
        }
    };

    public BarcodeAdapterList(List<BOX_SCAN_GACID> detList) {
        this.dataList = detList;
        //this.dataArray = dataArray;
        setHasStableIds(true);
//        grDetListFull = new ArrayList<>(detList);
    }

    public BarcodeAdapterList(Context context, List<BOX_SCAN_GACID> objects, View.OnClickListener shortClickListener) {
        this.context = context;
        this.shortClickListener = shortClickListener;
        this.dataList = objects;
        this.dataListFull = objects;
        setHasStableIds(true);
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_box_scan, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        final BOX_SCAN_GACID value = dataList.get(position);
        holder.tvRowId.setText(String.valueOf(position+1));
        holder.tvGTIN.setText(value.GTIN);
        holder.tvLotNo.setText(value.LOT_NO);
        holder.tvManf.setText(value.MANF_DT_DISPLAY);
        holder.tvExp.setText(value.EXP_DT_DISPLAY);
        holder.tvSlno.setText(value.SLNO);
        holder.tvSSCC.setText(String.valueOf(value.SSCC));
        holder.itemView.setTag(value);
        holder.itemView.setOnClickListener(shortClickListener);


    }

    public int getTotalScannedQty(){
        int totalQty = 0;
        for (BOX_SCAN_GACID entry:dataList) {
            totalQty = totalQty+entry.QTY;
        }
        return totalQty;
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void addItems(List<BOX_SCAN_GACID> detList) {
        this.dataList = detList;
        this.dataListFull = detList;
        //this.dataArray = dataArray;
        notifyDataSetChanged();
        //notifyItemRangeChanged(0, dataList.size());
    }

    public void clearList() {
        this.dataList = new ArrayList<>();
        this.dataListFull =new ArrayList<>();
        notifyDataSetChanged();
    }

    public List<BOX_SCAN_GACID>  getItems() {
        return this.dataList;
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView tvExp,tvManf,tvSlno,tvLotNo,tvGTIN,tvSSCC,tvRowId;
        //TextView slno;
        LinearLayout borderPanel;

        RecyclerViewHolder(View view) {
            super(view);
            tvRowId= view.findViewById(R.id.tvRowId);
            tvExp = view.findViewById(R.id.tvExp);
            tvManf = view.findViewById(R.id.tvManf);
            tvSlno = view.findViewById(R.id.tvSlno);
            tvLotNo = view.findViewById(R.id.tvLotNo);
            tvGTIN = view.findViewById(R.id.tvGTIN);
            tvSSCC= view.findViewById(R.id.tvSSCC);
        }
    }

}
