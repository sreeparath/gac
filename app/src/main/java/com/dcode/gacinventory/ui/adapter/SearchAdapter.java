package com.dcode.gacinventory.ui.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;


import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.data.model.SEARCH_TYPE;
import com.example.gacinventory.R;

import java.util.ArrayList;
import java.util.List;

public class SearchAdapter
        extends RecyclerView.Adapter<SearchAdapter.RecyclerViewHolder>
        implements Filterable {

    private Context context;
    private View.OnClickListener shortClickListener;
    private List<SEARCH_TYPE> dataList;
    private ArrayList<SEARCH_TYPE> dataListFull;
    private String CodeTitle;
    private String NameTitle;

    private Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<SEARCH_TYPE> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(dataListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (SEARCH_TYPE item : dataListFull) {
                    if (item.NAME.toLowerCase().contains(filterPattern.toLowerCase()) ||
                            item.CODE.toLowerCase().contains(filterPattern.toLowerCase())) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            dataList.clear();
            dataList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public SearchAdapter(Context context, List<SEARCH_TYPE> objects, View.OnClickListener shortClickListener, @Nullable String... sArgs) {
        try {
            this.context = context;
            this.shortClickListener = shortClickListener;
            this.dataList = objects;
            this.dataListFull = new ArrayList<>(objects);
            if (sArgs != null && sArgs.length > 0) {
                CodeTitle = sArgs.length >= 1 && sArgs[0] != null ? sArgs[0] : AppConstants.CODE_TITLE;
                NameTitle = sArgs.length >= 2 && sArgs[1] != null ? sArgs[1] : AppConstants.NAME_TITLE;
            }

            setHasStableIds(true);
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
        }
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_card_search, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        final SEARCH_TYPE searchTypeItem = this.dataList.get(position);

        holder.tvCodeTitle.setText(CodeTitle);
        holder.tvNameTitle.setText(NameTitle);
        holder.tvCode.setText(searchTypeItem.CODE);
        holder.tvName.setText(searchTypeItem.NAME);
        holder.itemView.setTag(searchTypeItem);

        holder.itemView.setTag(searchTypeItem);
        holder.itemView.setOnClickListener(shortClickListener);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public void addItems(List<SEARCH_TYPE> list) {
        this.dataList = list;
        this.dataListFull = new ArrayList<>(list);
        notifyDataSetChanged();
    }

    public List<SEARCH_TYPE> getItems() {
        return this.dataList;
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView tvCodeTitle;
        TextView tvNameTitle;
        TextView tvCode;
        TextView tvName;

        RecyclerViewHolder(View view) {
            super(view);
            tvCodeTitle = view.findViewById(R.id.tvCodeTitle);
            tvNameTitle = view.findViewById(R.id.tvNameTitle);
            tvCode = view.findViewById(R.id.tvCode);
            tvName = view.findViewById(R.id.tvName);
        }
    }
}
