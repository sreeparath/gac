package com.dcode.gacinventory.ui.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.data.model.INBOUND_HEADER;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SCAN;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SUMBIT;
import com.dcode.gacinventory.network.model.GenericRetResponse;
import com.dcode.gacinventory.network.service.ServiceUtils;
import com.example.gacinventory.R;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ProductUpdateHeaderFragment extends BaseFragment {
    private TextInputEditText edDocNo,edBarcode,edGacScan,edLotNo;
    private TextInputLayout ilLotNo;
//    private AutoCompleteTextView tvDocTypes;
//    private ArrayAdapter<DocType> objectsAdapter;
    private int promptResource;
    private String putawayType;
    private long HDR_ID;
    private CheckBox chkSelect;
    //private LOGISTIC_LABEL_SCAN[] logisticLabelScans;
    private View root;
    private INBOUND_HEADER inbound_header;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);
            putawayType = this.getArguments().getString("PutAwayType", "");
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                //DocNo = bundle.getString(AppConstants.SELECTED_ID);
                //barcode = bundle.getString(AppConstants.MASTER_ID);
                if(bundle.containsKey(AppConstants.SELECTED_ID)){
                    putawayType =bundle.getString(AppConstants.SELECTED_ID);
                }
            }
        }
        root = inflater.inflate(R.layout.fragment_product_update_header, container, false);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupView();
    }

    private void setupView(){
        //ImageView imgDocNo = root.findViewById(R.id.imgDocNo);
        //imgDocNo.setImageResource(R.drawable.ic_005);

        //setHeaderToolbar();




        edBarcode = root.findViewById(R.id.edBarcode);
        edGacScan = root.findViewById(R.id.edGacScan);
        //edGacScan.requestFocus();



        MaterialButton btPhoto = root.findViewById(R.id.btnBack);
        btPhoto.setOnClickListener(v -> OnBackClick());

        MaterialButton btnPrint = root.findViewById(R.id.btNext);
        btnPrint.setOnClickListener(v -> OnNextClick());

/*        tvDocTypes = root.findViewById(R.id.acDocType);
        tvDocTypes.setOnItemClickListener((parent, view, position, id) -> {
            if (position > 0) {
                ordType = objectsAdapter.getItem(position);
            } else {
                ordType = null;
            }
        });*/
    }

/*    private boolean onDocNoKeyEvent() {
        DocNo = edDocNo.getText().toString();

        if (DocNo.isEmpty() || DocNo.trim().length() == 0) {
            showToast(String.format("%s required", getString(promptResource)));
            return false;
        }
        return true;
    }*/

    private void setHeaderToolbar() {
        if (inbound_header != null) {
            Toolbar toolbar = root.findViewById(R.id.toolbar);
            TextView info1 = root.findViewById(R.id.info1);
            info1.setText(App.currentUser.USER_ID);
            TextView info2 = root.findViewById(R.id.info2);
            info2.setText(inbound_header.PRIN_CODE);
        }
    }

    private void OnNextClick() {
        if(Validate()){
            DownloadProdUpdateDocument(edGacScan.getText().toString(),edBarcode.getText().toString());
        }
    }

    private void OnBackClick() {
        getActivity().onBackPressed();
    }

    private boolean Validate() {
        String errMessage;
//        String DocNo = edDocNo.getText().toString();
//        if (chkSelect.isChecked() && DocNo.length() == 0) {
//            errMessage = String.format("%s required", "Logistic Label");
//            showToast(errMessage);
//            edDocNo.setError(errMessage);
//            return false;
//        }
        String gac = edGacScan.getText().toString();
        if (gac.length() == 0) {
            errMessage = String.format("%s required", "GAC Scan Id");
            showToast(errMessage);
            edGacScan.setError(errMessage);
            return false;
        }
//        String barcode = edBarcode.getText().toString();
//        if (barcode.length() == 0) {
//            errMessage = String.format("%s required", "Product Barcode");
//            showToast(errMessage);
//            edBarcode.setError(errMessage);
//            return false;
//        }

//        if(inbound_header==null){
//            errMessage = String.format("%s required", "Inbound Header");
//            showToast(errMessage);
//            return false;
//        }

        return true;

    }

    private void DownloadProdUpdateDocument(String gacScan,String prodBarcode) {
        if (gacScan.length() <= 0)  {
            return;
        }

        JsonObject requestObject = ServiceUtils.LogisticLabel.getInfoBySrNo(gacScan,prodBarcode,putawayType);

        showProgress(false);
        App.getNetworkClient().getAPIService().getRetGeneric(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("S")) {
                    String xmlDoc = genericRetResponse.getJsonString();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showAlert("Error", "No data received.");
                        return;
                    }
                    try {
                        LOGISTIC_LABEL_SCAN[]  logisticLabelScans = new Gson().fromJson(xmlDoc, LOGISTIC_LABEL_SCAN[].class);
                        if(logisticLabelScans.length==0){
                            showAlert("Error", "No data received.");
                            return;
                        }
                        OpenProductUpdateAway(logisticLabelScans[0]);

                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                        showAlert("Error", e.getMessage());
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showAlert("Error", msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showAlert("Error", msg);
            }
        });
    }


    private void OpenProductUpdateAway(LOGISTIC_LABEL_SCAN logisticLabelScans) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, logisticLabelScans);
        bundle.putLong(AppConstants.SELECTED_ID, AppConstants.PROD_UPDATE_HEADER);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_product_update, bundle);
    }

}
