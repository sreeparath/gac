package com.dcode.gacinventory.ui.view.outbound;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.data.model.SP_SCN_AN_OUT_PICK_PENDING;
import com.dcode.gacinventory.network.model.GenericRetResponse;
import com.dcode.gacinventory.network.service.ServiceUtils;
import com.dcode.gacinventory.ui.adapter.PickConfirmationAdapter;
import com.dcode.gacinventory.ui.interfaces.MenuSelectorResponse;
import com.dcode.gacinventory.ui.menus.MenuItems;
import com.dcode.gacinventory.ui.menus.MenuItemsBuilder;
import com.dcode.gacinventory.ui.menus.WindowPopUpMenu;
import com.dcode.gacinventory.ui.view.BaseFragment;
import com.example.gacinventory.R;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * @Created by: VIJIL DHAS A.S
 * @Date: 17-09-2021
 */
public class PickConfirmationFragment extends BaseFragment implements View.OnClickListener {
    private View root;
    private RecyclerView mRcvItems;
    private TextView mTxtSelect;
    private TextView mTxtPrin;
    private TextView mTxtJobNo;
    private TextView mTxtTruckId;
    private TextInputEditText mTxtSearch;
    private ImageView mImageFilter;

    private String ASCENDING_ORDER = "ASCENDING_ORDER";
    private String DESCENDING_ORDER = "DESCENDING_ORDER";

    private boolean isSortBySelected = false;
    private boolean isSortByPrinCode = false;
    private boolean isSortByJobNumber = false;
    private boolean isSortByTruckId = false;

    private PickConfirmationAdapter confirmationAdapter;
    private List<SP_SCN_AN_OUT_PICK_PENDING> baseFormattedList;
    private int filterMenuId = -1; // default value is all

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        root = inflater.inflate(R.layout.fragment_pick_confrimation, container, false);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHeaderToolbar();
        initViews();
    }

    private void setHeaderToolbar() {
        TextView info1 = root.findViewById(R.id.info1);
        info1.setText(App.currentUser.USER_ID);
    }

    private void initViews() {
        mRcvItems = root.findViewById(R.id.rcv_items);
        mTxtSelect = root.findViewById(R.id.txt_select);
        mTxtPrin = root.findViewById(R.id.txt_principle);
        mTxtJobNo = root.findViewById(R.id.txt_job_number);
        mTxtTruckId = root.findViewById(R.id.txt_truck_id);
        mTxtSearch = root.findViewById(R.id.edsearch);
        mImageFilter = root.findViewById(R.id.image_filter);
        setupClickListener();


        initAdapter();
        apiPickConfirmation();
    }


    private void initAdapter() {
        confirmationAdapter = new PickConfirmationAdapter(getContext(), new ArrayList<>());
        mRcvItems.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mRcvItems.setAdapter(confirmationAdapter);
    }

    private void apiPickConfirmation() {
        JsonObject requestObject = ServiceUtils.OutBound.getSP_SCN_AN_OUT_PICK_PENDING();
        showProgress(false);

        App.getNetworkClient().getAPIService().getSpScnOutLocByJob(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("E")) {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showAlert("Error", msg);
                } else {
                    String xmlDoc = genericRetResponse.getJsonString();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showAlert("Error", "No data received.");
                        return;
                    }
                    try {
                        SP_SCN_AN_OUT_PICK_PENDING[] spScnAnOutAssignedTrucks = new Gson().fromJson(xmlDoc, SP_SCN_AN_OUT_PICK_PENDING[].class);
                        if (spScnAnOutAssignedTrucks.length == 0) {
                            showAlert("Error", "No data received.");
                            return;
                        }
                        List<SP_SCN_AN_OUT_PICK_PENDING> formattedList = new ArrayList<>();
                        for (int i = 0; i < spScnAnOutAssignedTrucks.length; i++) {
                            SP_SCN_AN_OUT_PICK_PENDING spScnAnOutLocByJob = new SP_SCN_AN_OUT_PICK_PENDING();
                            spScnAnOutLocByJob = spScnAnOutAssignedTrucks[i];
                            formattedList.add(spScnAnOutLocByJob);
                        }

                       /* SP_SCN_AN_OUT_PICK_PENDING i1 = new SP_SCN_AN_OUT_PICK_PENDING();
                        i1.SELECTED = 0;
                        i1.ERR_CODE = "S";
                        i1.JOB_NO = "JOB_NO 1";
                        i1.PRIN_CODE = "PRIN_CODE 1";
                        i1.TRUCK_ID = "TRUCK_ID 1";
                        formattedList.add(i1);

                        SP_SCN_AN_OUT_PICK_PENDING i2 = new SP_SCN_AN_OUT_PICK_PENDING();
                        i2.SELECTED = 0;
                        i2.ERR_CODE = "S";
                        i2.JOB_NO = "JOB_NO 2";
                        i2.PRIN_CODE = "PRIN_CODE 2";
                        i2.TRUCK_ID = "TRUCK_ID 2";
                        formattedList.add(i2);

                        SP_SCN_AN_OUT_PICK_PENDING i3 = new SP_SCN_AN_OUT_PICK_PENDING();
                        i3.SELECTED = 0;
                        i3.ERR_CODE = "S";
                        i3.JOB_NO = "JOB_NO 3";
                        i3.PRIN_CODE = "PRIN_CODE 3";
                        i3.TRUCK_ID = "TRUCK_ID 3";
                        formattedList.add(i3);

                        SP_SCN_AN_OUT_PICK_PENDING i4 = new SP_SCN_AN_OUT_PICK_PENDING();
                        i4.SELECTED = 0;
                        i4.ERR_CODE = "S";
                        i4.JOB_NO = "JOB_NO 4";
                        i4.PRIN_CODE = "PRIN_CODE 4";
                        i4.TRUCK_ID = "TRUCK_ID 4";
                        formattedList.add(i4);

                        SP_SCN_AN_OUT_PICK_PENDING i5 = new SP_SCN_AN_OUT_PICK_PENDING();
                        i5.SELECTED = 0;
                        i5.ERR_CODE = "S";
                        i5.JOB_NO = "JOB_NO 5";
                        i5.PRIN_CODE = "PRIN_CODE 5";
                        i5.TRUCK_ID = "TRUCK_ID 5";
                        formattedList.add(i5);


                        setBaseFormattedList(formattedList);*/
                        setupListView(formattedList);

                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                        showAlert("Error", e.getMessage());
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showAlert("Error", msg);
            }
        });
    }

    private void setupListView(List<SP_SCN_AN_OUT_PICK_PENDING> formattedList) {
        confirmationAdapter.addItems(formattedList);
    }

    private void setupClickListener() {
        mTxtSelect.setOnClickListener(this);
        mTxtPrin.setOnClickListener(this);
        mTxtJobNo.setOnClickListener(this);
        mTxtTruckId.setOnClickListener(this);
        mImageFilter.setOnClickListener(this);

        mTxtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                confirmationAdapter.filter(Objects.requireNonNull(mTxtSearch.getText()).toString(),
                        getFilterMenuId());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.txt_select) {
            if (isSortByPrinCode || isSortByJobNumber || isSortByTruckId) {
                mTxtPrin.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                mTxtJobNo.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                mTxtTruckId.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                confirmationAdapter.resetList(getBaseFormattedList());
            }
            sortBySelected(v);
            isSortBySelected = true;
        } else if (v.getId() == R.id.txt_principle) {
            if (isSortBySelected || isSortByJobNumber || isSortByTruckId) {
                mTxtSelect.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                mTxtJobNo.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                mTxtTruckId.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                confirmationAdapter.resetList(getBaseFormattedList());
            }
            sortByPrinCode(v);
            isSortByPrinCode = true;
        } else if (v.getId() == R.id.txt_job_number) {
            if (isSortBySelected || isSortByPrinCode || isSortByTruckId) {
                mTxtSelect.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                mTxtPrin.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                mTxtTruckId.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                confirmationAdapter.resetList(getBaseFormattedList());
            }
            sortByJobNumber(v);
            isSortByJobNumber = true;
        } else if (v.getId() == R.id.txt_truck_id) {
            if (isSortBySelected || isSortByPrinCode || isSortByJobNumber) {
                mTxtSelect.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                mTxtPrin.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                mTxtJobNo.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                confirmationAdapter.resetList(getBaseFormattedList());
            }
            sortByTruckId(v);
            isSortByTruckId = true;
        } else if (v.getId() == R.id.image_filter) {
            showPopupMenu(v);
        }
    }

    /**
     * method responsible to show popup menu
     */
    private void showPopupMenu(View view) {
        WindowPopUpMenu windowPopUpMenu = WindowPopUpMenu.getInstance();
        PopupWindow popupWindow = windowPopUpMenu.popupDisplay(getContext(),
                MenuItemsBuilder.getInstance().getFilterItems(),
                getFilterMenuId(),
                new MenuSelectorResponse() {
                    @Override
                    public void onSelectedMenu(int selectedMenu) {
                        setFilterMenuId(selectedMenu);
                        switch (selectedMenu) {
                            case 100:
                            case -1:
                                // filter all
                                mImageFilter.setImageResource(R.drawable.ic_filter);
                                break;
                            case 101:
                                // prin code
                                mImageFilter.setImageResource(R.drawable.ic_person_black_24dp);
                                break;
                            case 102:
                                // job number
                                mImageFilter.setImageResource(R.drawable.ic_job);
                                break;
                            case 103:
                                // truck id
                                mImageFilter.setImageResource(R.drawable.ic_truck);
                                break;
                        }
                    }

                    @Override
                    public void onNavigateOptionASelector() {

                    }

                    @Override
                    public void onNavigateOptionBSelector() {

                    }

                    @Override
                    public void onSelectedBottomMenu(String menuName) {

                    }
                });
//        popupWindow.showAsDropDown(view);
        popupWindow.showAsDropDown(view, 0, 0, Gravity.BOTTOM);

    }

    private void sortByTruckId(View v) {
        if (v.getTag() == null) {
            mTxtTruckId.setTag(ASCENDING_ORDER);
            mTxtTruckId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_sort_up, 0);
        } else if (v.getTag().equals(ASCENDING_ORDER)) {
            mTxtTruckId.setTag(DESCENDING_ORDER);
            mTxtTruckId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_sort_down, 0);
        } else if (v.getTag().equals(DESCENDING_ORDER)) {
            mTxtTruckId.setTag(ASCENDING_ORDER);
            mTxtTruckId.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_sort_up, 0);
        }
        confirmationAdapter.sortByTruckID((String) mTxtTruckId.getTag());
    }

    private void sortByJobNumber(View v) {
        if (v.getTag() == null) {
            mTxtJobNo.setTag(ASCENDING_ORDER);
            mTxtJobNo.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_sort_up, 0);
        } else if (v.getTag().equals(ASCENDING_ORDER)) {
            mTxtJobNo.setTag(DESCENDING_ORDER);
            mTxtJobNo.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_sort_down, 0);
        } else if (v.getTag().equals(DESCENDING_ORDER)) {
            mTxtJobNo.setTag(ASCENDING_ORDER);
            mTxtJobNo.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_sort_up, 0);
        }
        confirmationAdapter.sortByJobNumber((String) mTxtJobNo.getTag());
    }

    private void sortByPrinCode(View v) {
        if (v.getTag() == null) {
            mTxtPrin.setTag(ASCENDING_ORDER);
            mTxtPrin.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_sort_up, 0);
        } else if (v.getTag().equals(ASCENDING_ORDER)) {
            mTxtPrin.setTag(DESCENDING_ORDER);
            mTxtPrin.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_sort_down, 0);
        } else if (v.getTag().equals(DESCENDING_ORDER)) {
            mTxtPrin.setTag(ASCENDING_ORDER);
            mTxtPrin.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_sort_up, 0);
        }
        confirmationAdapter.sortByPrinCode((String) mTxtPrin.getTag());
    }

    /**
     * here selected is 1 so the order is revised
     *
     * @param v
     */
    private void sortBySelected(View v) {
        if (containsSelected(confirmationAdapter.getListItems())) {
            if (v.getTag() == null) {
                mTxtSelect.setTag(ASCENDING_ORDER);
                mTxtSelect.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_sort_down, 0);
            } else if (v.getTag().equals(ASCENDING_ORDER)) {
                mTxtSelect.setTag(DESCENDING_ORDER);
                mTxtSelect.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_sort_up, 0);
            } else if (v.getTag().equals(DESCENDING_ORDER)) {
                mTxtSelect.setTag(ASCENDING_ORDER);
                mTxtSelect.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_sort_up, 0);
            }
            confirmationAdapter.sortBySelected((String) mTxtSelect.getTag());
        }
    }

    public boolean containsSelected(final List<SP_SCN_AN_OUT_PICK_PENDING> list) {
        return list.stream().filter(o -> o.SELECTED == 1).findFirst().isPresent();
    }

    public List<SP_SCN_AN_OUT_PICK_PENDING> getBaseFormattedList() {
        return baseFormattedList;
    }

    public void setBaseFormattedList(List<SP_SCN_AN_OUT_PICK_PENDING> baseFormattedList) {
        this.baseFormattedList = baseFormattedList;
    }

    public int getFilterMenuId() {
        return filterMenuId;
    }

    public void setFilterMenuId(int filterMenuId) {
        this.filterMenuId = filterMenuId;
    }
}
