package com.dcode.gacinventory.ui.view;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.common.AppVariables;
import com.dcode.gacinventory.common.PrintManager;
import com.dcode.gacinventory.common.Utils;
import com.dcode.gacinventory.data.model.INBOUND_HEADER;
import com.dcode.gacinventory.data.model.LABEL_ITEM_LIST;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SCAN;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SUMBIT;
import com.dcode.gacinventory.data.model.PRINTER_LIST_SERVICE;
import com.dcode.gacinventory.data.model.PRINT_FORMAT;
import com.dcode.gacinventory.network.model.GenericRetResponse;
import com.dcode.gacinventory.network.service.ServiceUtils;
import com.dcode.gacinventory.ui.adapter.ItemsLabelsPrintGeneratedAdapter;
import com.dcode.gacinventory.ui.adapter.MultiAdapter;
import com.example.gacinventory.R;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.JsonObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class LabelsPrintFragment
        extends BaseFragment
        implements View.OnClickListener {

    private String ReceiptNo;
    private View root;
    private AutoCompleteTextView tvPrinter;
    private AutoCompleteTextView tvLabel;
    //private List<GENERATE_LABELS> itemsPrintList;
    //private List<GR_ITEM_PRINT_TYP> grItemPrintTyp;
    private ItemsLabelsPrintGeneratedAdapter labelsPrintGeneratedAdapter;
    private ArrayAdapter<PRINTER_LIST_SERVICE> printersAdapter;
    private ArrayAdapter<PRINT_FORMAT> labelsAdapter;
    private PRINTER_LIST_SERVICE barcode_printers;
    private PRINT_FORMAT label_formats;
    private RadioGroup rdoGroup;
    private TextInputEditText edMac;
    private TextInputLayout ilPrinter;
    private TextInputLayout ilmac;
    private TextView edInfo;
    private CheckBox chkSelect;
    private int noGacScan;
    private ArrayList<LABEL_ITEM_LIST> itemsPrintList;
    //private lablePrintAdapter itemsLabelsPrintAdapter;
    private MultiAdapter itemsLabelsPrintAdapter;
    private INBOUND_HEADER inboundHeader;
    private LOGISTIC_LABEL_SCAN items_prints;
    private LOGISTIC_LABEL_SUMBIT logisticLabelSumbit;
    //int parentId = -1;
    private long fromSource = -1;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        ReceiptNo = "";
        if (this.getArguments() != null) {
            Bundle bundle = this.getArguments();
            //parentId = bundle.containsKey(AppConstants.PARENT_ID) ? bundle.getInt(AppConstants.PARENT_ID) : -1;
            ReceiptNo = bundle.containsKey(AppConstants.SELECTED_ID) ? bundle.getString(AppConstants.SELECTED_ID) : "";
            inboundHeader  = (INBOUND_HEADER) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
            fromSource = bundle.containsKey(AppConstants.SOURCE_ID) ? bundle.getLong(AppConstants.SOURCE_ID) : -1;
            items_prints = bundle.containsKey(AppConstants.SELECTED_OBJECT_NEW) ? (LOGISTIC_LABEL_SCAN) bundle.getSerializable(AppConstants.SELECTED_OBJECT_NEW) : null;
            logisticLabelSumbit = bundle.containsKey(AppConstants.SELECTED_OBJECT_NEW2) ? (LOGISTIC_LABEL_SUMBIT) bundle.getSerializable(AppConstants.SELECTED_OBJECT_NEW2) : null;

        }

        root = inflater.inflate(R.layout.fragment_labels_print, container, false);
        setupView();
        return root;
    }

    private void setupView(){
        setHeaderToolbar();

        List<PRINTER_LIST_SERVICE> printersList = App.getDatabaseClient().getAppDatabase().genericDao().getPrintServiceList(App.currentWarehouse.WH_CODE);
        printersAdapter = new ArrayAdapter<>(requireContext(),
                R.layout.dropdown_menu_popup_item, printersList);
        tvPrinter = root.findViewById(R.id.tvPrinter);
        tvPrinter.setAdapter(printersAdapter);
        tvPrinter.setSelection(0);
        tvPrinter.setThreshold(100);
        tvPrinter.setOnItemClickListener((parent, dropdownView, position, id) -> {
            if (position >= 0) {
                barcode_printers = printersAdapter.getItem(position);
            } else {
                barcode_printers = null;
            }
        });
        List<PRINT_FORMAT> print_formats = App.getDatabaseClient().getAppDatabase().genericDao().getPrintFormats();
        labelsAdapter = new ArrayAdapter<>(requireContext(),
                R.layout.dropdown_menu_popup_item, print_formats);
        tvLabel = root.findViewById(R.id.tvLabel);
        tvLabel.setAdapter(labelsAdapter);
        tvLabel.setSelection(0);
        tvLabel.setThreshold(100);
        tvLabel.setOnItemClickListener((parent, dropdownView, position, id) -> {
            if (position >= 0) {
                label_formats = labelsAdapter.getItem(position);
            } else {
                label_formats = null;
            }
        });



        RecyclerView recyclerView = root.findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));

        itemsLabelsPrintAdapter = new MultiAdapter(getContext(),new ArrayList<>());
        recyclerView.setAdapter(itemsLabelsPrintAdapter);



        MaterialButton btnPrint = root.findViewById(R.id.btnPrint);
        btnPrint.setOnClickListener(v -> {
            onPrintClick();
//            if (itemsLabelsPrintAdapter.getSelected().size() > 0) {
//                showToast(String.valueOf(itemsLabelsPrintAdapter.getSelected().size() ));
//                //StringBuilder stringBuilder = new StringBuilder();
//                for (int i = 0; i < itemsLabelsPrintAdapter.getSelected().size(); i++) {
////                    stringBuilder.append(itemsLabelsPrintAdapter.getSelected().get(i).GACSCANID);
////                    stringBuilder.append("\n");
//                    Log.d("selected##",itemsLabelsPrintAdapter.getSelected().get(i).GACSCANID);
//                }
//                //showToast(stringBuilder.toString().trim());
//            } else {
//                showToast("No Selection");
//            }
        });

        MaterialButton btnBack = root.findViewById(R.id.btnBack);
        btnBack.setOnClickListener(v -> {
                    if(fromSource==-1){
                        getActivity().onBackPressed();
                    }else if(fromSource==AppConstants.DIRECT_PUT){
                        OpenDirectPutaway();
                    }else if(fromSource==AppConstants.LOGISTIC_LABEL){
                        OpenGACScan();
                    }
                }
        );

        ilmac = root.findViewById(R.id.ilmac);
        ilPrinter = root.findViewById(R.id.ilPrinter);
        edMac = root.findViewById(R.id.edMac);
        edMac.setVisibility(View.GONE);
        tvPrinter.setVisibility(View.GONE);
        ilmac.setVisibility(View.GONE);
        ilPrinter.setVisibility(View.GONE);
        edInfo = root.findViewById(R.id.edInfo);
        //edInfo.setText("Total GACScan ID : "+noGacScan);

        chkSelect = root.findViewById(R.id.chkSelect);
        //chkSelect.setChecked(true);
        chkSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                itemsLabelsPrintAdapter.setSelectedAll(isChecked);
                //set your object's last status
                //showToast(String.valueOf(isChecked));
                //itemsLabelsPrintAdapter.selectAll(isChecked);
                //itemsLabelsPrintAdapter.notifyDataSetChanged();
            }
        });



        rdoGroup = root.findViewById(R.id.radioGroup);
        rdoGroup.clearCheck();
        String printType = AppVariables.getPrinterType();
        Log.d("printType##",printType);
        if(printType.equalsIgnoreCase(getString(R.string.ip))){
            RadioButton b = (RadioButton) root.findViewById(R.id.ipPrinter);
            b.setChecked(true);
            tvPrinter.setVisibility(View.VISIBLE);
            ilPrinter.setVisibility(View.VISIBLE);
        }else if(printType.equalsIgnoreCase(getString(R.string.blue))){
            RadioButton b = (RadioButton) root.findViewById(R.id.bluetoothPrinter);
            b.setChecked(true);
            edMac.setVisibility(View.VISIBLE);
            ilmac.setVisibility(View.VISIBLE);
            String mac = AppVariables.getMacAddress();
            Log.d("mac##",mac);
            edMac.setText(mac);
        }

        rdoGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int check = group.getCheckedRadioButtonId();
                RadioButton rdoButton = (RadioButton) group.findViewById(check);
                if(rdoButton.getText().toString().equalsIgnoreCase(getString(R.string.ip))){
                    tvPrinter.setVisibility(View.VISIBLE);
                    ilPrinter.setVisibility(View.VISIBLE);
                    edMac.setVisibility(View.GONE);
                    ilmac.setVisibility(View.GONE);
                }else if(rdoButton.getText().toString().equals(getString(R.string.blue))){
                    tvPrinter.setVisibility(View.GONE);
                    ilPrinter.setVisibility(View.GONE);
                    edMac.setVisibility(View.VISIBLE);
                    ilmac.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    private void setHeaderToolbar() {
        if (inboundHeader != null) {
            Toolbar toolbar = root.findViewById(R.id.toolbar);
            TextView info1 = root.findViewById(R.id.info1);
            info1.setText(App.currentUser.USER_ID);
            TextView info2 = root.findViewById(R.id.info2);
            info2.setText(inboundHeader.JOB_NO + " - " + inboundHeader.PRIN_NAME );
        }
    }

    private void OpenGACScan() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, items_prints);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT_NEW,inboundHeader);
        ((MainActivity) requireActivity()).NavigateFullBackStack();
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_gac_scan, bundle);
    }

    private void OpenDirectPutaway() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT_NEW,logisticLabelSumbit);
        ((MainActivity) requireActivity()).NavigateFullBackStack();
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_direct_put_away, bundle);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(fromSource==-1){
            DownloadGACPrintList();
        }else{
            if(ReceiptNo.length()==0){
                showAlert("Error","New Scan Id is null.");
                return;
            }
            setNewIdPrintList();
        }


    }

    private void setNewIdPrintList(){
        LABEL_ITEM_LIST itemList =  new LABEL_ITEM_LIST();
        if(itemsPrintList==null){
            itemsPrintList = new ArrayList<>();
        }
        Log.d("GACSCANID##",ReceiptNo);
        itemList.GACSCANID = ReceiptNo;
        itemList.PROD_NAME = "";
        itemList.QTY1 = 1;
        itemList.UNIT_CODE = "";
        itemList.TRUCK_NO=inboundHeader.TRUCK_NO;
        itemList.SEAL_NO=inboundHeader.SEAL_NO;
        itemList.AWB_NO= inboundHeader.AWB_NO;
        itemsPrintList.add(itemList);
        itemsLabelsPrintAdapter.setLableItemList(itemsPrintList);
        itemsLabelsPrintAdapter.notifyDataSetChanged();
        edInfo.setText("Total GACScan ID : 1");
    }

    @Override
    public void onClick(View view) {
        LABEL_ITEM_LIST items_print = (LABEL_ITEM_LIST) view.getTag();
        //showToast(String.valueOf(items_print.IS_SELECT));

        /*ITEMS_PRINT items_print = (ITEMS_PRINT) view.getTag();
        if (items_print != null) {
            final AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
            alert.setTitle("Please Enter Labels count");
            final EditText input = new EditText(getContext());
            input.setInputType(InputType.TYPE_CLASS_NUMBER);
            input.setRawInputType(Configuration.KEYBOARD_12KEY);
            alert.setView(input);
            alert.setPositiveButton("Next", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    //Put actions for OK button here
                    String inputValue = input.getText().toString().trim();
                    if (inputValue.length() <= 0) {
                        showToast("Invalid Labels count");
                    } else {
                        int count = Utils.convertToInt(inputValue, items_print.QTY);
                        //if (count > items_print.ITQTY) {
                        //    showToast("Cannot exceed receipt");
                        //    count = items_print.ITQTY;
                       //}
                        items_print.QTY = count;
                        itemsLabelsPrintAdapter.notifyDataSetChanged();
                    }
                }
            });
            alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    //Put actions for CANCEL button here, or leave in blank
                }
            });
            alert.show();
        }*/
    }

    private void generateGACscanLabel(int lastId,int count){
        String jobCode = Utils.PadLeft(inboundHeader.JOB_NO, 10, '0'); // pad to 5;
        String keyType = "I";// AS per GAC processflow
        //String userStamp = "123"; // THREE DIGIT
        //String keyFromTypeStamp="";
        String prefix  =  keyType+inboundHeader.JOB_NO;//keyType+jobCode.substring(jobCode.length() - 5);
        if(itemsPrintList==null){
            itemsPrintList = new ArrayList<>();
        }
        if(count==0){
            return;
        }
        LABEL_ITEM_LIST itemList;
        for(int i=0;i<count;i++){
            Log.d("generateNewGACIds##",prefix+(lastId-i));
            itemList= new LABEL_ITEM_LIST();
            itemList.GACSCANID = prefix+(lastId-i);
            itemList.PROD_NAME = "";
            itemList.QTY1 = 1;
            itemList.UNIT_CODE = "";
            itemList.TRUCK_NO=inboundHeader.TRUCK_NO;
            itemList.SEAL_NO=inboundHeader.SEAL_NO;
            itemList.AWB_NO= inboundHeader.AWB_NO;
            itemsPrintList.add(itemList);
        }
        if(itemsPrintList.size()==0){
            return;
        }
        //itemsLabelsPrintAdapter.addItems(itemsPrintList);
        itemsLabelsPrintAdapter.setLableItemList(itemsPrintList);
        itemsLabelsPrintAdapter.notifyDataSetChanged();

        edInfo.setText("Total GACScan ID : "+count);
    }



    private void DownloadGACPrintList() {
        if (inboundHeader==null) {
            return;
        }

        JsonObject requestObject = ServiceUtils.LabelPrint.getPrintList(inboundHeader.JOB_NO,inboundHeader.GACSCANID_COUNT);

        Log.d("requestObject##",requestObject.toString());

        showProgress(false);
        App.getNetworkClient().getAPIService().getRetGeneric(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("S")) {
                    String xmlDoc = genericRetResponse.getJsonString();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showAlert("Error", "No data received.");
                        return;
                    }
                    try {
                        String lastId = genericRetResponse.getErrMessage();
                        if(lastId==null&& lastId.length()==0){
                            showAlert("Error", "No lastId received.");
                            return;
                        }
                        generateGACscanLabel(Integer.parseInt(lastId),inboundHeader.GACSCANID_COUNT);
//                        LABEL_ITEM_LIST[] items_prints = new Gson().fromJson(xmlDoc, LABEL_ITEM_LIST[].class);
//                        itemsPrintList = Arrays.asList(items_prints);
//                        itemsLabelsPrintAdapter.addItems(itemsPrintList);
//                        itemsLabelsPrintAdapter.notifyDataSetChanged();
                        //edInfo.setText("Total GACScan ID : "+itemsLabelsPrintAdapter.getItemCount());
//                        if(itemsPrintList.size()>0){
//
//                        }

                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                        showAlert("Error", e.getMessage());
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showAlert("Error", msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showAlert("Error", msg);
            }
        });
    }

    private void bluetoothPrint(String printLabel,String mac){
        PrintManager.PrintMacID = mac;// getResources().getString(R.string.deviceMac);
        try {
            String result = "";
            if (printLabel.length() > 0) {
                result = PrintManager.TestPrint(printLabel);
            }else{
                result = "No Label format found!";
            }
//            if (result.length() > 0) {
//                showToast(result);
//                return;
//            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void onPrintClick() {
        if (IsValid()) {
            Log.d("HEREEEE## ","");
            //ipPrint();
            String mac = edMac.getText().toString();
            int check = rdoGroup.getCheckedRadioButtonId();
            RadioButton rdoButton = (RadioButton) rdoGroup.findViewById(check);
            int printer = -1;
            if(rdoButton.getText().toString().equalsIgnoreCase(getString(R.string.ip))){
                printer = 0;
            }else if(rdoButton.getText().toString().equals(getString(R.string.blue))){
                printer = 1;
                if (mac != null && mac.length() != 12 /* && !AppVariables.getMacAddress().contains(":")*/) {
                    showToast("Provide a valid printer mac address!");
                    return;
                }
            }

            if(printer==0){
                ipPrint();
                AppVariables.setPrinterType(rdoButton.getText().toString());
            }else if(printer==1){
                showProgress(false);
                AppVariables.setPrinterType(rdoButton.getText().toString());
                AppVariables.setMacAddress(mac);
                //itemsPrintList = itemsLabelsPrintAdapter.getItems();
                if(itemsPrintList!=null) {
                    if(PrintManager.findBT()) {
                        for (LABEL_ITEM_LIST items : itemsPrintList) {
                            bluetoothPrint(getPrintLabelData(items), mac);
                        }
                    }
                    showToast(PrintManager.StatusMessage);
                }
                dismissProgress();

            }


        }
    }

    private String getPrintLabelData(LABEL_ITEM_LIST items){
        String printCode = label_formats.PRINT_BODY;
        if (items.isIS_SELECT()) {
            Log.d("items.BARCODE## ",items.GACSCANID);
            printCode = printCode.replace("{BARCODE}", items.GACSCANID);
            printCode = printCode.replace("{NCOPY}", "1");
            printCode = printCode.replace("{JOBNO}", inboundHeader.JOB_NO);
            printCode = printCode.replace("{PONO}", inboundHeader.PO_NO==null?" ":inboundHeader.PO_NO);
            printCode = printCode.replace("{SEALNO}", inboundHeader.SEAL_NO.trim());
            printCode = printCode.replace("{PRINCODE}", inboundHeader.PRIN_CODE);
            printCode = printCode.replace("{PRINNAME}", inboundHeader.PRIN_NAME.trim());
            printCode = printCode.replace("{AWBNO}", inboundHeader.AWB_NO.trim());
            printCode = printCode.replace("{TRUCKNO}", inboundHeader.TRUCK_NO.trim());
        }
        return printCode;
    }

    private void ipPrint(){
        try {
            //showProgress(false);
//            AsyncTask.execute(new Runnable() {
//                @Override
//                public void run() {
//                    DoPrint();
//                }
//            });
            new TestAsync().execute();

        }  catch (Exception e) {
            showAlert("Error",e.getMessage());
        }
    }

    class TestAsync extends AsyncTask<Void, Integer, String> {
        String TAG = getClass().getSimpleName();

        protected void onPreExecute() {
            super.onPreExecute();
            Log.d(TAG + " PreExceute","On pre Exceute......");
            getActivity().runOnUiThread(() -> showProgress(false));
        }

        protected String doInBackground(Void...arg0) {
            Log.d(TAG + " DoINBackGround", "On doInBackground...");
            try {
                String printerIP = barcode_printers.PRINTER_IP;
                int printerPort = Integer.parseInt(barcode_printers.PRINTER_PORT);
                //Log.d("printLabel## ",printLabel);
                String response = null;
                Socket socket = new Socket(printerIP, printerPort);
                if (socket.isConnected()) {
                    PrintWriter out = new PrintWriter(new BufferedWriter(
                            new OutputStreamWriter(socket.getOutputStream())), true);
                    BufferedReader in = new BufferedReader(
                            new InputStreamReader(socket.getInputStream()));
                    //itemsPrintList = itemsLabelsPrintAdapter.getItems();
                    if (itemsLabelsPrintAdapter.getSelected().size() > 0) {
                        for (LABEL_ITEM_LIST items : itemsLabelsPrintAdapter.getSelected()) {
                            out.println(getPrintLabelData(items));
                        }
                    } else {
                        showToast("No Selection");
                    }
//                    //out.println(printLabel);
//                    String resposeFromServer = in.readLine();
                    in.close();
                    out.close();
//                    response = resposeFromServer;
                    socket.close();
                } else {
                    Log.d("Soket issue##", "Socket is not connected");
                    //showToast("Socket is not connected");

                    getActivity().runOnUiThread(() ->   showAlert("Error","Socket is not connected"));
                }

            } catch (Exception e) {
                getActivity().runOnUiThread(() ->  showAlert("Error",e.getMessage()));
                e.printStackTrace();
            }

//            for (int i=0; i<10; i++){
//                Integer in = new Integer(i);
//                publishProgress(i);
//            }
            return "You are at PostExecute";
        }

        protected void onProgressUpdate(Integer...a) {
            super.onProgressUpdate(a);
            Log.d(TAG + " onProgressUpdate", "You are in progress update ... " + a[0]);
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.d(TAG + " onPostExecute", "" + result);
            dismissProgress();
        }
    }

    private void DoPrint() {
        try {
            String printerIP = barcode_printers.PRINTER_IP;
            int printerPort = Integer.parseInt(barcode_printers.PRINTER_PORT);
            //Log.d("printLabel## ",printLabel);
            String response = null;
            Socket socket = new Socket(printerIP, printerPort);
            if (socket.isConnected()) {
                PrintWriter out = new PrintWriter(new BufferedWriter(
                        new OutputStreamWriter(socket.getOutputStream())), true);
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(socket.getInputStream()));
                //itemsPrintList = itemsLabelsPrintAdapter.getItems();
                if(itemsPrintList!=null) {
                    for (LABEL_ITEM_LIST items : itemsPrintList) {
                        if (items.IS_SELECT) {
                           //code here
                        }
                    }
                }
                //out.println(printLabel);
                String resposeFromServer = in.readLine();
                out.close();
                in.close();
                response = resposeFromServer;
                socket.close();
            } else {
                Log.d("Soket issue##", "Socket is not connected");
            }

        } catch (Exception e) {
            //showAlert("Error",e.getMessage());
            e.printStackTrace();
        }
    }



    private boolean IsValid() {
        if (tvPrinter.getVisibility()==View.VISIBLE && (barcode_printers == null || barcode_printers.PRIN_CODE.length()<=0)) {
            showToast("Please select a printer");
            tvPrinter.setError("Select a printer");
            return false;
        }

        if (edMac.getVisibility()==View.VISIBLE && (edMac.getText().toString() == null ||edMac.getText().toString().length() == 0)) {
            showToast("Please enter mac address");
            tvPrinter.setError("Please enter mac address");
            return false;
        }

        if (label_formats == null || label_formats.REPORT_ID.length()<=0) {
            showToast("Please select a Label Format");
            tvLabel.setError("Select a Label");
            return false;
        }

        if (!(itemsLabelsPrintAdapter.getSelected().size()>0)) {
            showToast("Please select any item");
            return false;
        }

        return true;
    }
}
