package com.dcode.gacinventory.ui.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.common.Utils;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SCAN;
import com.dcode.gacinventory.data.model.PHOTO_MASTER;
import com.dcode.gacinventory.ui.adapter.ImageViewAdapter;
import com.example.gacinventory.R;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.dcode.gacinventory.common.AppConstants.VISIT_IMAGE;

public class DamageFragment extends BaseFragment implements  View.OnLongClickListener {
    private TextView edInfoNew ;
//    private AutoCompleteTextView tvDocTypes;
//    private ArrayAdapter<DocType> objectsAdapter;
    private int promptResource;
    private String DocNo;
    private long HDR_ID;
    private RadioGroup rdoGroup;
    private LOGISTIC_LABEL_SCAN items_prints;
    private TextInputEditText edPunitActual,edMUnitActual,edLUnitActual;
    private FloatingActionButton btnCamera;
    private Bitmap photo;
    private ImageViewAdapter imageViewAdapter;
    private RecyclerView recyclerView;
    private View root;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

//
//
//        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
//            @Override
//            public void handleOnBackPressed() {
//                // Handle the back button even
//                Log.d("BACKBUTTON", "Back button clicks");
//            }
//        };
//
//        ((MainActivity) requireActivity()).getOnBackPressedDispatcher().addCallback(callback);

        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                items_prints = (LOGISTIC_LABEL_SCAN) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
                Log.d("items_prints##",items_prints.toString());
            }
        }



        root = inflater.inflate(R.layout.fragment_damage, container, false);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupView();

    }

    private void setupView(){
        LinearLayout lyActualUnit= root.findViewById(R.id.unitpanel);
        lyActualUnit.setVisibility(View.GONE);

        edPunitActual = root.findViewById(R.id.edPUnitActual);
        edMUnitActual = root.findViewById(R.id.edMunitActual);
        edLUnitActual = root.findViewById(R.id.edLUnitActual);

        if(items_prints!=null){
            edPunitActual.setText(String.valueOf(items_prints.Pqty));
            edMUnitActual.setText(String.valueOf(items_prints.Mqty));
            edLUnitActual.setText(String.valueOf(items_prints.Lqty));
        }


        edInfoNew = root.findViewById(R.id.edInfoNew);
        edInfoNew.setVisibility(View.GONE);

        rdoGroup = root.findViewById(R.id.radioGroup);
        rdoGroup.clearCheck();
        rdoGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int check = group.getCheckedRadioButtonId();
                RadioButton rdoButton = (RadioButton) group.findViewById(check);
                if(rdoButton.getText().toString().equalsIgnoreCase(getString(R.string.full))){
                    lyActualUnit.setVisibility(View.GONE);
                    edInfoNew.setVisibility(View.GONE);
                }else if(rdoButton.getText().toString().equals(getString(R.string.partial))){
                    lyActualUnit.setVisibility(View.VISIBLE);
                    edInfoNew.setVisibility(View.VISIBLE);
                }
            }
        });


        MaterialButton btPhoto = root.findViewById(R.id.btnBack);
        btPhoto.setOnClickListener(v -> onPutAwayClick());

        MaterialButton btnPrint = root.findViewById(R.id.btNext);
        btnPrint.setOnClickListener(v -> OnNextClick());

        btnCamera = root.findViewById(R.id.btnCamera);
        btnCamera.setOnClickListener(v -> onCameraClick());

        recyclerView = root.findViewById(R.id.recycleView);
        imageViewAdapter = new ImageViewAdapter(getContext(), new ArrayList<>(),this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(imageViewAdapter);

/*        tvDocTypes = root.findViewById(R.id.acDocType);
        tvDocTypes.setOnItemClickListener((parent, view, position, id) -> {
            if (position > 0) {
                ordType = objectsAdapter.getItem(position);
            } else {
                ordType = null;
            }
        });*/
    }
    private void onCameraClick(){
        if (items_prints==null) {
            showToast("Data not found!");
            return;
        }

        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, VISIT_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (resultCode != Activity.RESULT_OK) return;

        if (requestCode == VISIT_IMAGE) {
            photo = (Bitmap) intent.getExtras().get("data");
            SavePhoto();
        }
    }

    private void SavePhoto() {
        String photoFileName = getPhotoFileName();
        Log.d("photoFileName##",photoFileName);
        if (photo != null) {
//            pngBase64 = Utils.getBase64EncodedPNG(photo);
            try (FileOutputStream out = new FileOutputStream(photoFileName)) {
                photo.compress(Bitmap.CompressFormat.PNG, 100, out);
                fetchPhotos();
                // PNG is a lossless format, the compression factor (100) is ignored
            } catch (IOException e) {
                e.printStackTrace();
            }

            PHOTO_MASTER photo_master = new PHOTO_MASTER();
            photo_master.MODULE_ID = ModuleID;
            long seqNo = Utils.GetMaxValue("PHOTO_MASTER", "SLNO", false,null , null);
            photo_master.SLNO = seqNo;
            photo_master.JOB_ID = items_prints.JOB_NO;
            photo_master.IS_UPLOAD = 0;
            photo_master.PHOTO_PATH = photoFileName;
            App.getDatabaseClient().getAppDatabase().genericDao().insertPhotoMaster(photo_master);
            fetchPhotos();
        }
    }

    private void fetchPhotos(){
        List<PHOTO_MASTER> photo_masters = App.getDatabaseClient().getAppDatabase().genericDao().getPhotoMasterList(ModuleID,items_prints.JOB_NO);
        if(photo_masters!=null && photo_masters.size()>0 ){
            imageViewAdapter.addItems(photo_masters);
            imageViewAdapter.notifyDataSetChanged();
        }
    }

    public String getPhotoFileName() {
        SimpleDateFormat dF = new SimpleDateFormat(Utils.DATETIME_NUM_FORMAT, Locale.getDefault());
        String fileName = dF.format(new Date()) + ".png";
        File outputFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath() + "/Camera/");
        try {
            if (!outputFile.exists() && !outputFile.isDirectory()) {
                outputFile.mkdir();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return outputFile.getAbsolutePath() + "/" + fileName;
    }

/*    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getDocTypes();
    }*/

/*    private boolean onDocNoKeyEvent() {
        DocNo = edDocNo.getText().toString();

        if (DocNo.isEmpty() || DocNo.trim().length() == 0) {
            showToast(String.format("%s required", getString(promptResource)));
            return false;
        }
        return true;
    }*/

    private void OnNextClick() {
        Validate();
    }

    private void onPutAwayClick() {
        OpenPutAway();
    }

    private void OpenPutAway() {
        if(items_prints==null){
            showAlert("Warning","Data not found!");
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putString(AppConstants.SELECTED_ID, DocNo);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT,  items_prints);
        bundle.putLong(AppConstants.PARENT_ID, HDR_ID);
        ((MainActivity) requireActivity()).NavigateBackStack(null, false);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_put_away, bundle);

    }

    private void Validate() {
//        String errMessage;
//        DocNo = edDocNo.getText().toString();
//        if (DocNo.length() == 0) {
//            errMessage = String.format("%s required", "PO/ASN/Job no");
//            showToast(errMessage);
//            edDocNo.setError(errMessage);
//            return;
//        }
        OpenLogisticLabel();

    }

    private void OpenLogisticLabel() {
        if(items_prints==null){
            showAlert("Warning","Data not found!");
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putString(AppConstants.SELECTED_ID, DocNo);
        //bundle.putSerializable(AppConstants.SELECTED_OBJECT,  items_prints);
        bundle.putLong(AppConstants.PARENT_ID, HDR_ID);
        ((MainActivity) requireActivity()).NavigateBackStack(null, false);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_inbound, bundle);

    }

    @Override
    public boolean onLongClick(View view) {
        return false;
    }

/*    private void getDocTypes() {
        List<DocType> orTypList = App.getDatabaseClient().getAppDatabase().genericDao().getAllOrTyps();
        objectsAdapter = new ArrayAdapter<>(requireContext(),
                R.layout.dropdown_menu_popup_item, orTypList);
        tvDocTypes.setAdapter(objectsAdapter);
        tvDocTypes.setSelection(0);
        tvDocTypes.setThreshold(100);

        objectsAdapter.notifyDataSetChanged();
    }*/



//    private void DownloadLogisticLabel(final String docNo, final String docType) {
//        JsonObject requestObject = ServiceUtils.GoodsReceipt.getGoodsReceiptHdr(docNo, docType);
//
//
//        showProgress(false);
//        App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
//            @Override
//            public void success(GenericRetResponse genericRetResponse, Response response) {
//                dismissProgress();
//                if (genericRetResponse.getErrCode().equals("S")) {
//                    String xmlDoc = genericRetResponse.getXmlDoc();
//                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
//                        showToast(" Error: No data received.");
//                        return;
//                    }
//                    try {
//                        PO_HDR[] po_hdr = new Gson().fromJson(xmlDoc, PO_HDR[].class);
//                        if (po_hdr==null || po_hdr.length<=0) {
//                            showToast(" Error: No data received.");
//                            return;
//                        }
//                        if (po_hdr.length > 0) {
//                            ordType = new DocType();
//                            ordType.CODE = po_hdr[0]. ORTYP;
//                            ordType.NAME = po_hdr[0]. ORTYP;
//                        }
//                        App.getDatabaseClient().getAppDatabase().genericDao().deletePO_HDR(docNo, ordType.CODE);
//                        App.getDatabaseClient().getAppDatabase().genericDao().insertPO_HDR(po_hdr);
//                        DownloadPO_DET(docNo, ordType.CODE);
//                    } catch (Exception e) {
//                        Log.d(App.TAG, e.toString());
//                    }
//                } else {
//                    String msg = genericRetResponse.getErrMessage();
//                    Log.d(App.TAG, msg);
//                    showToast(msg);
//                }
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                dismissProgress();
//                String msg = error.getMessage();
//                Log.d(App.TAG, msg);
//                showToast(msg);
//            }
//        });
//    }
//
//    private void DownloadPO_DET(final String docNo, final String docType) {
//        JsonObject requestObject = ServiceUtils.GoodsReceipt.getGoodsReceiptDet(docNo, docType);
//
//        showProgress(false);
//        App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
//            @Override
//            public void success(GenericRetResponse genericRetResponse, Response response) {
//                dismissProgress();
//                if (genericRetResponse.getErrCode().equals("S")) {
//                    String xmlDoc = genericRetResponse.getXmlDoc();
//                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
//                        showToast(" Error: No data received.");
//                        return;
//                    }
//                    try {
//                        PO_DET[] poDet = new Gson().fromJson(xmlDoc, PO_DET[].class);
//                        App.getDatabaseClient().getAppDatabase().genericDao().deletePO_DET(docNo, docType);
//                        App.getDatabaseClient().getAppDatabase().genericDao().insertPO_DET(poDet);
//                        OpenDocHeader();
//                    } catch (Exception e) {
//                        Log.d(App.TAG, e.toString());
//                    }
//                } else {
//                    String msg = genericRetResponse.getErrMessage();
//                    Log.d(App.TAG, msg);
//                    showToast(msg);
//                }
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                dismissProgress();
//                String msg = error.getMessage();
//                Log.d(App.TAG, msg);
//                showToast(msg);
//            }
//        });
//    }
}
