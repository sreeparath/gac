package com.dcode.gacinventory.ui.view;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.common.StagingValidation;
import com.dcode.gacinventory.common.Utils;
import com.dcode.gacinventory.common.ValidRespSet;
import com.dcode.gacinventory.data.model.INBOUND_HEADER;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SUMBIT;
import com.dcode.gacinventory.data.model.LOT_STATUS;
import com.dcode.gacinventory.data.model.LOT_STATUS_LIST;
import com.dcode.gacinventory.data.model.PUTAWAY_SUMBIT;
import com.dcode.gacinventory.network.model.GenericRetResponse;
import com.dcode.gacinventory.network.service.ServiceUtils;
import com.example.gacinventory.R;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.dcode.gacinventory.common.Utils.ISSUE_DATE_FORMAT3;

public class DirectPutAwayFragment extends BaseFragment {
    private TextView edDocNo,edProdCode;
    private TextInputEditText edLUnit;
    private TextInputEditText edMunit;
    private TextInputEditText edPUnit;
    private TextInputEditText edPl;
    private TextInputEditText edBarcode;
    private TextInputEditText edFillCode;
    private TextInputEditText edFormula;
    private TextView edDisplayQty;
    //private TextInputEditText edLotStatus;
    private TextInputEditText edLotNo;
    private TextView edCountry;
    private TextInputEditText edExp;
    private TextInputEditText edManf;
    private TextView edSkuName;
    private TextView edPickKey;
    private String DocNo;
    private long HDR_ID;
    private List<LOT_STATUS> lot_statuses;
    private ArrayAdapter<LOT_STATUS_LIST> lotStatusArrayAdapter;
    private AutoCompleteTextView edLotStatus;
    //private LOGISTIC_LABEL_SCAN items_prints;
    private LOGISTIC_LABEL_SUMBIT logisticLabelSumbit;
    private View root;
    private LOT_STATUS_LIST lotStatusType;
    //private INBOUND_HEADER inboundHeader;
    private LinearLayout collapsePanel,datepanel;
    private ScrollView scrollView;
    private StagingValidation stagingValidation;
    private TextInputLayout punit,munit,lunit,pl;
    private boolean mResult = false;
    private String StagingMode = "";
    boolean isExpand = false;
    boolean isFromEdit = false;
    private TextView edSite;
    private TextInputEditText edScanLoc;
    private TextView edLocCode;
    private PUTAWAY_SUMBIT putaway_sumbit;
    private Map<String, String> gs1Map;
    private TextInputEditText edBarcode1;
    private TextInputEditText edBarcode2;
    private TextInputEditText edBarcode3;
    private TextInputEditText edBarcode4;
    private TextInputEditText edBarcode5;
    private TextInputEditText edBarcode6;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        //setHasOptionsMenu(true);
        setHasOptionsMenu(true);
        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                //DocNo = bundle.getString(AppConstants.SELECTED_ID);
                //newScanId = bundle.containsKey(AppConstants.SELECTED_ID) ? bundle.getString(AppConstants.MASTER_ID) : "";
                logisticLabelSumbit =bundle.containsKey(AppConstants.SELECTED_OBJECT_NEW) ?  (LOGISTIC_LABEL_SUMBIT) bundle.getSerializable(AppConstants.SELECTED_OBJECT_NEW) : null;
                gs1Map =bundle.containsKey(AppConstants.SELECTED_OBJECT_NEW2) ? (Map<String, String>) bundle.getSerializable(AppConstants.SELECTED_OBJECT_NEW2) : null;
                //inboundHeader = (INBOUND_HEADER) bundle.getSerializable(AppConstants.SELECTED_OBJECT_NEW);
                //isFromEdit = bundle.containsKey(AppConstants.SELECTED_ID) ? bundle.getBoolean(AppConstants.SELECTED_ID) : false;
            }
        }

        root = inflater.inflate(R.layout.fragment_direct_putaway, container, false);
        setupView();
        return root;
    }

    private void setupView(){
        setHeaderToolbar();

        stagingValidation = new StagingValidation();

        edDocNo = root.findViewById(R.id.edScanCode);
        edLUnit = root.findViewById(R.id.edLUnit);
        edMunit= root.findViewById(R.id.edMunit);
        edPUnit= root.findViewById(R.id.edPUnit);
        pl = root.findViewById(R.id.pl);
        edPl= root.findViewById(R.id.edPl);
        edBarcode= root.findViewById(R.id.edBarcode);
        edBarcode1= root.findViewById(R.id.edBarcode1);
        edBarcode2= root.findViewById(R.id.edBarcode2);
        edBarcode3= root.findViewById(R.id.edBarcode3);
        edBarcode4= root.findViewById(R.id.edBarcode4);
        edBarcode5= root.findViewById(R.id.edBarcode5);
        edBarcode6= root.findViewById(R.id.edBarcode6);
        //Not confirmed so commented
//        if(items_prints!=null && items_prints.BARCODE_SCAN_CHECK.length()>0
//        && items_prints.BARCODE_SCAN_CHECK.equalsIgnoreCase("Y") &&
//                items_prints.PALLET_SCAN_CHECK.length()>0
//                && items_prints.PALLET_SCAN_CHECK.equalsIgnoreCase("Y")
//        ){
//            edBarcode.setShowSoftInputOnFocus(false);
//        }
        edFillCode= root.findViewById(R.id.edFillCode);
        edFormula= root.findViewById(R.id.edFormula);
        edLotStatus= root.findViewById(R.id.edLotStatus);
        edLotNo= root.findViewById(R.id.edLotNo);
        edLotNo.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_ENTER){
                if(event.getAction() == KeyEvent.ACTION_DOWN){
                    return onLotNoKeyEvent();
                }else{
                    return false;
                }
            }else{
                return false;
            }
        });
        edLotNo.requestFocus();
//        edLotNo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus) {
//                    showToast("here onFocusChange");
//                }
//            }
//        });
       //edLotNo.requestFocus();
        edSkuName = root.findViewById(R.id.edSkuName);
        edProdCode = root.findViewById(R.id.edProdCode);
        edCountry = root.findViewById(R.id.edCountry);
        edPickKey= root.findViewById(R.id.edPickKey);
        edExp= root.findViewById(R.id.edExp);
        edExp.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    onExpDtKeyEvent();
                }
            }
        });
        edManf= root.findViewById(R.id.edManf);
        edManf.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    onManFNoKeyEvent();
                }
            }
        });

        edManf.setOnClickListener(v -> onDateEntryClick(1));
        edExp.setOnClickListener(v -> onDateEntryClick(2));
        //edJobCode= root.findViewById(R.id.edJobCode);
//        edDocNo = root.findViewById(R.id.edScanCode);
//        edDocNo.setOnKeyListener((v, keyCode, event) -> {
//            if(keyCode == KeyEvent.KEYCODE_ENTER){
//                if(event.getAction() == KeyEvent.ACTION_DOWN){
//                    return onScanCodeKeyEvent();
//                }else{
//                    return false;
//                }
//            }else{
//                return false;
//            }
//        });

        edDisplayQty = root.findViewById(R.id.edDisplayQty);

        MaterialButton btPhoto = root.findViewById(R.id.btnBack);
        btPhoto.setOnClickListener(v -> {
            OnBackClick();
        });

        MaterialButton btnPrint = root.findViewById(R.id.btnNext);
        btnPrint.setOnClickListener(v -> OnNextClick());

        collapsePanel = root.findViewById(R.id.collapsePanel);
        //collapsePanel.setVisibility(View.GONE);

//        Button expand  = root.findViewById(R.id.expand);
        /*expand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // If the CardView is already expanded, set its visibility
                //  to gone and change the expand less icon to expand more.
                if (collapsePanel.getVisibility() == View.VISIBLE) {

                    // The transition of the hiddenView is carried out
                    //  by the TransitionManager class.
                    // Here we use an object of the AutoTransition
                    // Class to create a default transition.
                    TransitionManager.beginDelayedTransition(collapsePanel,
                            new AutoTransition());
                    collapsePanel.setVisibility(View.GONE);
                    //expand.setImageResource(R.drawable.ic_baseline_expand_more_24);
                }

                // If the CardView is not expanded, set its visibility
                // to visible and change the expand more icon to expand less.
                else {

                    TransitionManager.beginDelayedTransition(collapsePanel,
                            new AutoTransition());
                    collapsePanel.setVisibility(View.VISIBLE);
                    //arrow.setImageResource(R.drawable.ic_baseline_expand_less_24);
                }
            }
        });*/

//        expand.setOnClickListener(v -> {
//            if(isExpand){
//                collapse();
//            }else{
//                expand();
//            }
//        });

        datepanel  = root.findViewById(R.id.datepanel);
        //datepanel.setVisibility(View.GONE);



        scrollView = root.findViewById(R.id.sw_layout);

        punit = root.findViewById(R.id.punit);
        munit = root.findViewById(R.id.munit);
        lunit = root.findViewById(R.id.lunit);
        //UI Validation Starts here
        setDynamicUIValidations();

        edLocCode= root.findViewById(R.id.edLocCode);
        edSite = root.findViewById(R.id.edSite);
        edScanLoc = root.findViewById(R.id.edScanLoc);
//        edScanLoc.setOnKeyListener((v, keyCode, event) -> {
//            if(keyCode == KeyEvent.KEYCODE_ENTER){
//                if(event.getAction() == KeyEvent.ACTION_DOWN){
//                    return onDefaultPutawayUpdate();
//                }else{
//                    return false;
//                }
//            }else{
//                return false;
//            }
//        });
    }


    private void setDynamicUIValidations(){

        try {
            if (App.dynFuncs == null) {
                return;
            }
            StagingValidation stagingValidation = new StagingValidation();
            ValidRespSet validObject = null;

            //pallet_scan_check
//        if(items_prints.PALLET_SCAN_CHECK!=null && items_prints.PALLET_SCAN_CHECK.equals("Y")){
//            edPl.setVisibility(View.GONE);
//        }else{
//            edPl.setVisibility(View.VISIBLE);
//        }

            //FormulaAndFillCodeDisable
            validObject = stagingValidation.FormulaAndFillCodeDisable(App.dynFuncs, logisticLabelSumbit.PRIN_CODE);
            if (validObject.getErrCode().length() > 0 && validObject.getErrCode().equals("S")) {
                edFillCode.setEnabled(false);
                edFormula.setEnabled(false);
            }

            //getExpDate
            validObject = stagingValidation.getExpDate(App.dynFuncs, logisticLabelSumbit.PRIN_CODE, logisticLabelSumbit.MFG_DATE, logisticLabelSumbit.SHELF_LIFE,
                    Utils.ISSUE_DATE_FORMAT2, logisticLabelSumbit.EXPDATE);
            if (validObject.getErrCode().length() > 0 && validObject.getErrCode().equals("R")) {
                edExp.setText(validObject.getTargetValue());
            }


            //getManfDate
            validObject = stagingValidation.getManfDate(App.dynFuncs, logisticLabelSumbit.PRIN_CODE, logisticLabelSumbit.EXPDATE, logisticLabelSumbit.SHELF_LIFE,
                    Utils.ISSUE_DATE_FORMAT2, logisticLabelSumbit.MFG_DATE);
            if (validObject.getErrCode().length() > 0 && validObject.getErrCode().equals("R")) {
                edManf.setText(validObject.getTargetValue());
            }

        }catch (Exception e){
            e.printStackTrace();
            showAlert("setDynamicUIValidations - Error",e.getMessage());
        }

    }

    private boolean onLotNoKeyEvent(){

        try {
            if (App.dynFuncs == null) {
                return false;
            }
            if (logisticLabelSumbit == null) {
                return false;
            }

            //ValidateLotNoOnEntry
            ValidRespSet validObject = stagingValidation.ValidateLotNoOnEntry(App.dynFuncs, logisticLabelSumbit.PRIN_CODE, logisticLabelSumbit.LOT_NO, logisticLabelSumbit.CAL_BATCHNO);
            if (validObject.getErrCode().length() > 0 && validObject.getErrCode().equals("R")) {
                edLotNo.setText(validObject.getTargetValue());
            } else if (validObject.getErrCode().length() > 0 && validObject.getErrCode().equals("E")) {
                edLotNo.setError(validObject.getErrMSG());
                edLotNo.requestFocus();
                return false;
            }

        }catch(Exception e){
            showAlert("onLotNoKeyEvent - Error",e.getMessage());
            return false;
        }
        return true;
    }

    private boolean onManFNoKeyEvent(){
        try{
        if(App.dynFuncs==null){
            return false;
        }
        if(logisticLabelSumbit==null){
            return false;
        }

        //getExpDate
        ValidRespSet validObject = stagingValidation.getExpDate(App.dynFuncs, logisticLabelSumbit.PRIN_CODE,edManf.getText().toString(),logisticLabelSumbit.SHELF_LIFE,
                Utils.ISSUE_DATE_FORMAT2,edExp.getText().toString());
        if(validObject.getErrCode().length()>0 && validObject.getErrCode().equals("R")){
            edExp.setText(validObject.getTargetValue());
        }

    }catch(Exception e){
        showAlert("onManFNoKeyEvent - Error",e.getMessage());
        return false;
    }
        return true;
    }

    private boolean onExpDtKeyEvent(){
        try{
        if(App.dynFuncs==null){
            return false;
        }
        if(logisticLabelSumbit==null){
            return false;
        }

        //getManfDate
        ValidRespSet validObject = stagingValidation.getManfDate(App.dynFuncs, logisticLabelSumbit.PRIN_CODE,edExp.getText().toString(),logisticLabelSumbit.SHELF_LIFE,
                Utils.ISSUE_DATE_FORMAT2,edManf.getText().toString());
        if(validObject.getErrCode().length()>0 && validObject.getErrCode().equals("R")){
            edManf.setText(validObject.getTargetValue());
        }


    }catch(Exception e){
        showAlert("onManFNoKeyEvent - Error",e.getMessage());
        return false;
    }
        return true;
}



    private void expand()
    {
        isExpand = true;
        collapsePanel.setVisibility(View.VISIBLE);
        final int widthSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        final int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        collapsePanel.measure(widthSpec, heightSpec);
        ValueAnimator mAnimator = slideAnimator(0, collapsePanel.getMeasuredHeight());
        mAnimator.start();
    }

    private void collapse() {
        isExpand = false;
        int finalHeight = collapsePanel.getHeight();

        ValueAnimator mAnimator = slideAnimator(finalHeight, 0);

        mAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                //Height=0, but it set visibility to GONE
                collapsePanel.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }

        });
        mAnimator.start();
    }

    private ValueAnimator slideAnimator(int start, int end)
    {

        ValueAnimator animator = ValueAnimator.ofInt(start, end);

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                //Update Height
                int value = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = collapsePanel.getLayoutParams();
                layoutParams.height = value;
                collapsePanel.setLayoutParams(layoutParams);
            }
        });
        return animator;
    }


    private void setHeaderToolbar() {
        if (logisticLabelSumbit != null) {
            Toolbar toolbar = root.findViewById(R.id.toolbar);
            TextView info1 = root.findViewById(R.id.info1);
            info1.setText(App.currentUser.USER_ID);
            TextView info2 = root.findViewById(R.id.info2);
            info2.setText(logisticLabelSumbit.JOB_NO + " - " + logisticLabelSumbit.PRIN_NAME );
        }
    }

    private void onDateEntryClick(int ID) {
        Calendar currentDate = Calendar.getInstance();
        int curYear = currentDate.get(Calendar.YEAR);
        int curMonth = currentDate.get(Calendar.MONTH);
        int curDay = currentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(getContext(), (datePicker, cYear, cMonth, cDay) -> {
            String dateString = String.format(Locale.ENGLISH, "%d-%d-%d", cDay, (cMonth + 1), cYear);
            String selectedDate = Utils.getFormattedDate(dateString, Utils.ISSUE_DATE_FORMAT, Utils.ISSUE_DATE_FORMAT2);

            if (ID == 1) {
                edManf.setText(selectedDate);
                edManf.setError(null);
            } else if (ID == 2) {
                edExp.setText(selectedDate);
                edExp.setError(null);
            }
        }, curYear, curMonth, curDay);
        mDatePicker.setTitle("Select date");
        mDatePicker.show();
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        fillLogisticData(logisticLabelSumbit);

        FillLotStatus();
    }

/*    private boolean onDocNoKeyEvent() {
        DocNo = edDocNo.getText().toString();

        if (DocNo.isEmpty() || DocNo.trim().length() == 0) {
            showToast(String.format("%s required", getString(promptResource)));
            return false;
        }
        return true;
    }*/

    private void confirmDamageReport() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putString(AppConstants.SELECTED_ID, DocNo);
        bundle.putSerializable(AppConstants.SELECTED_CODE, "");
        bundle.putLong(AppConstants.PARENT_ID, HDR_ID);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.action_nav_damage_confirmation, bundle);
    }

    private void OnNextClick()  {
        int mQty = 0;
        int pQty = 0;
        int lQty = 0;
        if(Validate() && DynamicValidationSave()) {

            showToast("reached heeereeee");

            mQty = Integer.parseInt(edMunit.getText().toString());
            pQty = Integer.parseInt(edPUnit.getText().toString());
            lQty = Integer.parseInt(edLUnit.getText().toString());

            //logisticLabelSumbit.MFG_DATE = edManf.getText().toString();
            //logisticLabelSumbit.EXP_DATE = edExp.getText().toString();

            if(edManf.getText().toString().length()>0){
                logisticLabelSumbit.MFG_DATE = Utils.getForamtedDate(edManf.getText().toString(), Utils.ISSUE_DATE_FORMAT2, Utils.DATEONLY_FORMAT);
            }else{
                logisticLabelSumbit.MFG_DATE = "";
            }
            if(edExp.getText().toString().length()>0){
                logisticLabelSumbit.EXP_DATE = Utils.getForamtedDate(edExp.getText().toString(), Utils.ISSUE_DATE_FORMAT2, Utils.DATEONLY_FORMAT);
            }else{
                logisticLabelSumbit.EXP_DATE = "";
            }

            logisticLabelSumbit.LOT_NO = edLotNo.getText().toString();
            logisticLabelSumbit.LOT_STATUS = lotStatusType.NEW_LOT_STATUS;
            logisticLabelSumbit.FORMULA_NO = edFormula.getText().toString();
            logisticLabelSumbit.FIL_CODE = edFillCode.getText().toString();
            logisticLabelSumbit.BARCODE = edBarcode.getText().toString();
            logisticLabelSumbit.INPUT_PQTY = String.valueOf(pQty);
            logisticLabelSumbit.INPUT_MQTY = String.valueOf(mQty);
            logisticLabelSumbit.INPUT_LQTY = String.valueOf(lQty);
            logisticLabelSumbit.LOC_CODE = edScanLoc.getText().toString();
            logisticLabelSumbit.PALLET_ID = edPl.getText().toString();
            logisticLabelSumbit.SCAN_PUT_LOC_CODE = edScanLoc.getText().toString();


            //new validation as per ui dynamic validation
            int ActualStgQty = (pQty * logisticLabelSumbit.UPPP) + (mQty * logisticLabelSumbit.LUPPP) + lQty;
            if(ActualStgQty > 0 &&  (ActualStgQty < logisticLabelSumbit.QUANTITY)){
                boolean confirm = getValidationConfirmation(getContext(),getString(R.string.confirmation),
                        "Qty less than expected. Do you wish to confirm the remaining qty later?");
                if(confirm){
                    StagingMode = "P";
                    //showToast(StagingMode);
                    logisticLabelSumbit.SRNO_ORIGINAL = logisticLabelSumbit.SRNO;
                    uploadGACScanPartilData();
                }else{
                    StagingMode = "S";
                    //showToast(StagingMode);
                    uploadGACScanData();
                }
            }else{
                StagingMode = "S";
                //showToast(StagingMode+" outer");
                uploadGACScanData();
            }



        }

    }

    private boolean DynamicValidationSave(){
        try{
            AlertDialog alertDialog;
            ValidRespSet validObject;
            //ValidateLotNoOnSave
//        validObject = stagingValidation.ValidateLotNoOnSave(App.dynFuncs, inboundHeader.PRIN_CODE,items_prints.LOT_NO,items_prints.CAL_BATCHNO);
//        if(validObject.getErrCode().length()>0 && validObject.getErrCode().equals("R")){
//            edLotNo.setText(validObject.getTargetValue());
//        }else if(validObject.getErrCode().length()>0 && validObject.getErrCode().equals("E")){
//            edLotNo.setError(validObject.getErrMSG());
//            edLotNo.requestFocus();
//            return false;
//        }

            //ValidateLotstatusOnSave
            validObject = stagingValidation.ValidateLotstatusOnSave(App.dynFuncs, logisticLabelSumbit.PRIN_CODE,logisticLabelSumbit.LOT_STATUS,lotStatusType.NEW_LOT_STATUS);
            if(validObject.getErrCode().length()>0 && validObject.getErrCode().equals("E")){
                edLotStatus.setError(validObject.getErrMSG());
                edLotStatus.requestFocus();
                return false;
            }

            //ValidateLotNoEmptyOnSave
            validObject = stagingValidation.ValidateLotNoEmptyOnSave(App.dynFuncs, logisticLabelSumbit.PRIN_CODE,edLotNo.getText().toString(),logisticLabelSumbit.CAL_BATCHNO,logisticLabelSumbit.ISLOTREQUIRED);
            if(validObject.getErrCode().length()>0 && validObject.getErrCode().equals("E")){
                edLotNo.setError(validObject.getErrMSG());
                edLotNo.requestFocus();
                return false;
            }else if(validObject.getErrCode().length()>0 && validObject.getErrCode().equals("C")){
                return getValidationConfirmation(getContext(),getString(R.string.confirmation),validObject.getErrMSG());
            }

            //ValidateMfgDateEmptyOnSave
            validObject = stagingValidation.ValidateMfgDateEmptyOnSave(App.dynFuncs, logisticLabelSumbit.PRIN_CODE,edManf.getText().toString(),logisticLabelSumbit.CAL_BATCHNO,logisticLabelSumbit.ISMANUREQUIRED);
            if(validObject.getErrCode().length()>0 && validObject.getErrCode().equals("E")){
                edManf.setError(validObject.getErrMSG());
                edManf.requestFocus();
                return false;
            }else if(validObject.getErrCode().length()>0 && validObject.getErrCode().equals("C")){
                return getValidationConfirmation(getContext(),getString(R.string.confirmation),validObject.getErrMSG());
            }

            //ValidateExpDateEmptyOnSave
            validObject = stagingValidation.ValidateExpDateEmptyOnSave(App.dynFuncs, logisticLabelSumbit.PRIN_CODE,edExp.getText().toString(),logisticLabelSumbit.CAL_BATCHNO,logisticLabelSumbit.ISEXPDATEREQUIRED);
            if(validObject.getErrCode().length()>0 && validObject.getErrCode().equals("E")){
                edExp.setError(validObject.getErrMSG());
                edExp.requestFocus();
                return false;
            }else if(validObject.getErrCode().length()>0 && validObject.getErrCode().equals("C")){
                return getValidationConfirmation(getContext(),getString(R.string.confirmation),validObject.getErrMSG());
            }

            String mfd = edManf.getText().toString();
            String exp = edExp.getText().toString();
            String errMessage;
            if((mfd!=null && mfd.length()>0) &&  (exp!=null && exp.length()>0)){
                Date mfDate = Utils.convertStringToDate(mfd, Utils.ISSUE_DATE_FORMAT2);
                Date exDate = Utils.convertStringToDate(exp, Utils.ISSUE_DATE_FORMAT2);
                Date today = new Date();
                if (mfDate.compareTo(exDate)==0) {
                    errMessage = "Manf date and Exp date cannot be same day";
                    showToast(errMessage);
                    edManf.setError(errMessage);
                    return false;
                }

                if (mfDate.after(today)) {
                    errMessage = "Invalid manufacture date, date in future";
                    showToast(errMessage);
                    edManf.setError(errMessage);
                    return false;
                }

                Log.d("exDate##",String.valueOf(exDate));
                Log.d("today##",String.valueOf(today));

                if (exDate.before(today)) {
                    errMessage = "Invalid expiry date, already expired";
                    showToast(errMessage);
                    edExp.setError(errMessage);
                    return false;
                }
            }

            //        if(Integer.parseInt(mQty)>Integer.parseInt(items_prints.Mqty)){
//            errMessage = "Munit shouldn't be greater than "+items_prints.Mqty;
//            showToast(errMessage);
//            edMunit.setError(errMessage);
//            return false;
//        }

            String lQty = edLUnit.getText().toString();
            if (lQty.length() == 0) {
                errMessage = "Invalid LUnit";
                showToast(errMessage);
                edLUnit.setError(errMessage);
                return false;
            }

            String pQty = edPUnit.getText().toString();
            if (pQty.length() == 0) {
                errMessage = "Invalid PUnit";
                showToast(errMessage);
                edPUnit.setError(errMessage);
                return false;
            }


            String mQty = edMunit.getText().toString();
            if (mQty.length() == 0) {
                errMessage = "Invalid Munit";
                showToast(errMessage);
                edMunit.setError(errMessage);
                return false;
            }

            int MQty = Integer.parseInt(edMunit.getText().toString());
            int PQty = Integer.parseInt(edPUnit.getText().toString());
            int LQty = Integer.parseInt(edLUnit.getText().toString());

            int ActualStgQty = (PQty * logisticLabelSumbit.UPPP) + (MQty * logisticLabelSumbit.LUPPP) + LQty;

            if(ActualStgQty>logisticLabelSumbit.QUANTITY){
                showAlert("Warning","Qty is greater than expected. Pls enter valid Qty");
                edPUnit.requestFocus();
                return false;
            }

            if((logisticLabelSumbit.PALLET_SCAN_CHECK!=null && logisticLabelSumbit.PALLET_SCAN_CHECK.equals("Y")) &&
                    (logisticLabelSumbit.JOBCLASS!=null && !logisticLabelSumbit.JOBCLASS.equals("003")) &&
                    !(edPl.getText().toString()!=null && edPl.getText().toString().length()>0) &&
                    ActualStgQty==0
            ){
                showAlert("Warning","Please fill up Pallet ID/SSCC before proceeding, principal \" + Principal Name + \" requires Pallet ID/SSCC validation");
                edPl.requestFocus();
                return false;
            }

            //ValidatePalletDifSave
            validObject = stagingValidation.ValidatePalletDifSave(App.dynFuncs, logisticLabelSumbit.PRIN_CODE,logisticLabelSumbit.PALLET_ID,edPl.getText().toString());
            if(validObject.getErrCode().length()>0 && validObject.getErrCode().equals("E")){
                edPl.setError(validObject.getErrMSG());
                edPl.requestFocus();
                return false;
            }else if(validObject.getErrCode().length()>0 && validObject.getErrCode().equals("C")){
                edPl.requestFocus();
                return getValidationConfirmation(getContext(),getString(R.string.confirmation),validObject.getErrMSG());
            }


            //BARCODE_SCAN_CHECK
            Log.d("len##",String.valueOf(edBarcode.getText().toString().trim().length()));
            if((logisticLabelSumbit.BARCODE_SCAN_CHECK!=null && logisticLabelSumbit.BARCODE_SCAN_CHECK.equals("Y")) &&
                    (edBarcode.getText().toString().trim().length()<=0)
            ){
                showAlert("Warning","Please fill up Barcode before proceeding, product " + logisticLabelSumbit.DESCRIPTION + " requires product barcode validation");
                edBarcode.requestFocus();
                return false;
            }
            //BARCODE_SCAN_CHECK
            if((logisticLabelSumbit.BARCODE_SCAN_CHECK!=null && logisticLabelSumbit.BARCODE_SCAN_CHECK.equals("Y")) &&
                    !(edBarcode.getText().toString().trim().equalsIgnoreCase(logisticLabelSumbit.BARCODE) ||
                            edBarcode.getText().toString().trim().equalsIgnoreCase(logisticLabelSumbit.BARCODE2) ||
                            edBarcode.getText().toString().trim().equalsIgnoreCase(logisticLabelSumbit.BARCODE3) ||
                            edBarcode.getText().toString().trim().equalsIgnoreCase(logisticLabelSumbit.BARCODE4) ||
                            edBarcode.getText().toString().trim().equalsIgnoreCase(logisticLabelSumbit.BARCODE5) ||
                            edBarcode.getText().toString().trim().equalsIgnoreCase(logisticLabelSumbit.BARCODE6)
                    )

            ){
                showAlert("Warning","Invalid Product Barcode");
                edBarcode.requestFocus();
                return false;
            }


        }catch(Exception e){
            showAlert("DynamicValidationSave - Error",e.getMessage());
            return false;
        }
        return true;
    }

    private void uploadGACScanPartilData() {
        if(logisticLabelSumbit==null){
            return;
        }
        showToast(getString(R.string.send_data_to_server));
        JsonObject requestObject = ServiceUtils.LogisticLabel.getLOGISTIC_LABEL_SUMBIT_PARTIAL(logisticLabelSumbit);
        showProgress(false);
        App.getNetworkClient().getAPIService().submitWithLinesData(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericSubmissionResponse, Response response) {
                dismissProgress();
                if (!genericSubmissionResponse.getErrCode().equals("E")) {
                    if (!genericSubmissionResponse.getErrCode().equals("E")) {
                        String msg = genericSubmissionResponse.getErrMessage();

                        resetUI();
                        //showAlert("Success",msg);

                        if(msg==null && msg.length()==0){
                            showAlert("Error", "No ID received.");
                            return;
                        }
                        DownloadLogistic_Label(msg);


                        //items_prints =null;
                        //logistic_label_sumbit = null;
//                        if (msg.length > 1) {
//                            ReceiptNo = msg[2];
//                        }
//                        try {
//                            OnConfirmationClick(AppConstants.DAMAGE_KEY, AppConstants.DAMAGE_ID, -1,getString(R.string.damage_conf),R.string.no,R.string.yes,logistic_label_sumbit);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
                    } else {
                        showAlert("Error",genericSubmissionResponse.getErrMessage());
                        //showToast(genericSubmissionResponse.getErrMessage());
                    }
                } else {
                    Log.d(App.TAG, genericSubmissionResponse.getErrMessage());
                    showAlert("Error",genericSubmissionResponse.getErrMessage());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(App.TAG, error.getMessage());
                showToast(error.getMessage());
                dismissProgress();
            }
        });
    }

    private void DownloadLogistic_Label(String newLabel) {
        if (newLabel.length() <= 0 || logisticLabelSumbit == null )  {
            return;
        }

        JsonObject requestObject = ServiceUtils.LogisticLabel.getLogisticLabel("labelNo",newLabel,logisticLabelSumbit.SKU
                ,logisticLabelSumbit.PRIN_CODE,"P",logisticLabelSumbit.JOB_NO,"");

        showProgress(false);
        App.getNetworkClient().getAPIService().getRetGeneric(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (!genericRetResponse.getErrCode().equals("E")) {
                    String xmlDoc = genericRetResponse.getJsonString();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showAlert("Error", "No data received.");
                        return;
                    }
                    try {
                        LOGISTIC_LABEL_SUMBIT[] tempLogisticLabelScan = new Gson().fromJson(xmlDoc, LOGISTIC_LABEL_SUMBIT[].class);
                        if(tempLogisticLabelScan==null){
                            showAlert("Error", "No data received.");
                            return;
                        }
                        LOGISTIC_LABEL_SUMBIT logisticLabelSumbit = tempLogisticLabelScan[0];
                        if(logisticLabelSumbit.SRNO==null){
                            showAlert("Error", "No Serial No Found!.");
                            return;
                        }
                        INBOUND_HEADER inboundHeader  = new INBOUND_HEADER();
                        inboundHeader.JOB_NO = logisticLabelSumbit.JOB_NO;
                        inboundHeader.PRIN_NAME = logisticLabelSumbit.PRIN_NAME;
                        OpenPrintLabel(newLabel,inboundHeader,logisticLabelSumbit);

                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                        showAlert("Error", e.getMessage());
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showAlert("Error", msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showAlert("Error", msg);
            }
        });
    }

    private void OpenPrintLabel(String newId,INBOUND_HEADER inboundHeader,LOGISTIC_LABEL_SUMBIT logisticLabelSumbit) {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putString(AppConstants.SELECTED_ID, newId);
        bundle.putLong(AppConstants.SOURCE_ID, AppConstants.DIRECT_PUT);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, inboundHeader);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT_NEW2, logisticLabelSumbit);
        ((MainActivity) requireActivity()).NavigateFullBackStack();
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_label_print, bundle);
    }

    private void uploadGACScanData() {
        if(logisticLabelSumbit==null){
            return;
        }
        showToast(getString(R.string.send_data_to_server));
        JsonObject requestObject = ServiceUtils.LogisticLabel.getLOGISTIC_LABEL_SUMBIT_FULL(logisticLabelSumbit,"Y");
        showProgress(false);
        App.getNetworkClient().getAPIService().submitWithLinesData(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericSubmissionResponse, Response response) {
                dismissProgress();
                if (genericSubmissionResponse.getErrCode().equals("S")) {
                    if (genericSubmissionResponse.getErrCode().equals("S")) {
                        String msg = genericSubmissionResponse.getErrMessage();
                        resetUI();
                        showAlert("Success",msg);
                        showAlert("Success",msg);
                        OpenDirectPutAwayHeaderFragment();
                    } else {
                        showAlert("Error",genericSubmissionResponse.getErrMessage());
                        //showToast(genericSubmissionResponse.getErrMessage());
                    }
                } else {
                    Log.d(App.TAG, genericSubmissionResponse.getErrMessage());
                    showAlert("Error",genericSubmissionResponse.getErrMessage());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(App.TAG, error.getMessage());
                showToast(error.getMessage());
                dismissProgress();
            }
        });
    }

    private void uploadPutAwayData(boolean varianceFlag) {
        if(putaway_sumbit==null){
            return;
        }
        showToast(getString(R.string.send_data_to_server));
        JsonObject requestObject = ServiceUtils.LogisticLabel.getPUTAWAY_SUMBIT(putaway_sumbit);
        showProgress(false);
        App.getNetworkClient().getAPIService().submitWithLinesData(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericSubmissionResponse, Response response) {
                dismissProgress();
                if (genericSubmissionResponse.getErrCode().equals("S")) {
                    if (genericSubmissionResponse.getErrCode().equals("S")) {
                        String msg = genericSubmissionResponse.getErrMessage();
                        resetUI();
                        showAlert("Success",msg);
                        OpenDirectPutAwayHeaderFragment();
                    } else {
                        showAlert("Error",genericSubmissionResponse.getErrMessage());
                        //showToast(genericSubmissionResponse.getErrMessage());
                    }
                } else {
                    Log.d(App.TAG, genericSubmissionResponse.getErrMessage());
                    showAlert("Error",genericSubmissionResponse.getErrMessage());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(App.TAG, error.getMessage());
                showToast(error.getMessage());
                dismissProgress();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (resultCode != Activity.RESULT_OK) {
            Log.d("resultCode##",String.valueOf(requestCode));
            try {
                if (requestCode == AppConstants.PUT_ID) {
                    //OpenLogisticLabel();
                    return;
                }
                OnConfirmationClick(AppConstants.PUT_AWAY_KEY, AppConstants.PUT_ID, -1,getString(R.string.put_away_conf),R.string.no,R.string.yes,logisticLabelSumbit);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return;
        }

        if (requestCode == AppConstants.DAMAGE_ID) {
            int yesNo = intent.hasExtra(AppConstants.SELECTED_ID) ?  intent.getIntExtra(AppConstants.SELECTED_ID,-1) : -1;
            if(yesNo==1){
                //OpenDamageFragment();
            }
            return;
        }

        if (requestCode == AppConstants.PUT_ID) {
            int yesNo = intent.hasExtra(AppConstants.SELECTED_ID) ?  intent.getIntExtra(AppConstants.SELECTED_ID,-1) : -1;
            if(yesNo==1){
               // OpenPutAwayFragment();
            }
            return;
        }
    }






    public boolean getValidationConfirmation(Context context,String title,String message) {
        mResult = false;
        final Handler handler = new Handler(Looper.getMainLooper()) {

            @Override
            public void handleMessage(Message mesg) {
                throw new RuntimeException();
            }
        };

        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(title);
        alert.setMessage(message);
        alert.setIcon(R.drawable.logo);
        alert.setPositiveButton("Yes", new
                DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mResult= true;
                        handler.sendMessage(handler.obtainMessage());
                    }
                });
        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                mResult= false;
                handler.sendMessage(handler.obtainMessage());
            }
        });
        alert.show();

        try {
            Looper.loop();
        } catch (RuntimeException e) {
        }

        return mResult;
    }


    public void confrimDynamicValidation(Context context, String title, String message,
                                         final DialogSingleButtonListener dialogSingleButtonListener) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setTitle(getString(R.string.confirmation));
        dialogBuilder.setMessage(message);
        dialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                    dialogSingleButtonListener.onButtonClicked(dialog);
            }
        });
        dialogBuilder.setPositiveButton("Yes", (dialog, which) -> {
            dialog.dismiss();
                    });
        AlertDialog dialog = dialogBuilder.create();
        dialog.show();
    }

    public interface DialogSingleButtonListener {
        public abstract boolean onButtonClicked(DialogInterface dialog);
    }

    private boolean Validate()  {
        String errMessage;
        if (logisticLabelSumbit.SRNO.length() == 0) {
            errMessage = String.format("%s required", "GACScanId");
            showToast(errMessage);
            edDocNo.setError(errMessage);
            return false;
        }




        if(lotStatusType==null){
            errMessage = "Invalid Lot Status";
            showToast(errMessage);
            edLotStatus.setError(errMessage);
            return false;
        }


        String scanLoc = edScanLoc.getText().toString();
        if (scanLoc.length() == 0) {
            errMessage = String.format("%s required", "Scan Location");
            showToast(errMessage);
            edScanLoc.setError(errMessage);
            return false;
        }

        return true;

    }




    private void OnBackClick() {
        getActivity().onBackPressed();
    }



    private void OpenDirectPutAwayHeaderFragment() {
            Bundle bundle = new Bundle();
            bundle.putString(AppConstants.SELECTED_ID, "P");
            ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_direct_put_away_header, bundle);
    }



    private void fillLogisticData(LOGISTIC_LABEL_SUMBIT logistic_label_submit){
        //Log.d("DocNo##",DocNo);

        try {

            punit.setHint(punit.getHint()+":"+logistic_label_submit.PUOM);
            munit.setHint(munit.getHint()+":"+logistic_label_submit.MUOM);
            lunit.setHint(lunit.getHint()+":"+logistic_label_submit.LUOM);

            edDocNo.setText(getString(R.string.gac_scan_id)+" : "+logistic_label_submit.SRNO);
            edLUnit.setText(String.valueOf(logistic_label_submit.Lqty));
            edMunit.setText(String.valueOf(logistic_label_submit.Mqty));
            edPUnit.setText(String.valueOf(logistic_label_submit.Pqty));
            if(logistic_label_submit.Lqty==logistic_label_submit.Mqty){
                edMunit.setEnabled(false);
            }
            //edBarcode.setText(logistic_label_submit.BARCODE);
            edBarcode1.setText(logistic_label_submit.BARCODE);
            edBarcode2.setText(logistic_label_submit.BARCODE2);
            edBarcode3.setText(logistic_label_submit.BARCODE3);
            edBarcode4.setText(logistic_label_submit.BARCODE4);
            edBarcode5.setText(logistic_label_submit.BARCODE5);
            edBarcode6.setText(logistic_label_submit.BARCODE6);

            edFillCode.setText(logistic_label_submit.FIL_CODE);
            edFormula.setText(logistic_label_submit.FORMULA_NO);
            edPl.setText(logistic_label_submit.PALLET_ID);
            //pallet_scan_check
            if(logistic_label_submit.PALLET_SCAN_CHECK!=null && logistic_label_submit.PALLET_SCAN_CHECK.equals("Y")){
                edPl.setVisibility(View.VISIBLE);
                pl.setVisibility(View.VISIBLE);
            }else{
                edPl.setVisibility(View.GONE);
                pl.setVisibility(View.GONE);
            }
            edLotNo.setText(logistic_label_submit.LOT_NO);
            //logistic_label_scan.LOT_STATUS = "I";
            if(logistic_label_submit.LOT_STATUS!=null && logistic_label_submit.LOT_STATUS.length()>0){

                LOT_STATUS_LIST lotStatusList  = App.getDatabaseClient().getAppDatabase().genericDao().getLotStatusFromKey(logistic_label_submit.LOT_STATUS);
                if(lotStatusList!=null){
                    edLotStatus.setText(lotStatusList.DESCRIPTION);
                    lotStatusType = lotStatusList;
                }
            }
            edPickKey.setText(logistic_label_submit.PACK_KEY);
            edSkuName.setText(logistic_label_submit.DESCRIPTION);
            edCountry.setText(logistic_label_submit.COUNTRYNAME);
            edProdCode.setText(getString(R.string.sku)+" : "+logistic_label_submit.SKU);
            edLocCode.setText("Sugg Loc : "+logistic_label_submit.LOC_CODE);
            edSite.setText("Site : "+logistic_label_submit.SITE);
            if (logistic_label_submit.MFG_DATE != null && logistic_label_submit.MFG_DATE.trim().length()>0) {
                edManf.setText(Utils.getForamtedDate(logistic_label_submit.MFG_DATE, ISSUE_DATE_FORMAT3, Utils.ISSUE_DATE_FORMAT2));
            }
            if (logistic_label_submit.EXPDATE != null&& logistic_label_submit.EXPDATE.trim().length()>0) {
                edExp.setText(Utils.getForamtedDate(logistic_label_submit.EXPDATE, ISSUE_DATE_FORMAT3, Utils.ISSUE_DATE_FORMAT2));
            }

            edDisplayQty.setText(logistic_label_submit.PUT_QTY+"/"+(Integer.parseInt(String.valueOf(logistic_label_submit.TOTAL_QTY))-Integer.parseInt(String.valueOf(logistic_label_submit.PUT_QTY))));

        }catch(Exception e){
            showAlert("Error",e.getMessage());
            e.printStackTrace();
        }
    }

    private void resetUI(){
        edDocNo.setText("");
        edLUnit.setText("");
        edMunit.setText("");
        edPUnit.setText("");
        edPl.setText("");
        edBarcode.setText("");
        edFillCode.setText("");
        edFormula.setText("");
        edLotStatus.setText("");
        edLotNo.setText("");
        //edLocCode.setText("");
        edExp.setText("");
        edManf.setText("");
        lotStatusType = new LOT_STATUS_LIST();
        //edJobCode.setText("");
        //items_prints = null;
    }








    private void FillLotStatus() {
        List<LOT_STATUS_LIST> lot_status_lists = App.getDatabaseClient().getAppDatabase().genericDao().getLotStatus();
        if(lot_status_lists!=null&& lot_status_lists.size()>0){
            lotStatusArrayAdapter = new ArrayAdapter<>(getContext(), R.layout.dropdown_menu_popup_item, lot_status_lists);
            edLotStatus.setAdapter(lotStatusArrayAdapter);
            edLotStatus.setThreshold(100);
            edLotStatus.setOnItemClickListener((parent, view, position, id) -> {
                if (position >= 0) {
                    lotStatusType = lotStatusArrayAdapter.getItem(position);
                } else {
                    lotStatusType = null;
                }

            });
        }


    }

    @Override
    public void onCreateOptionsMenu(@NonNull @NotNull Menu menu, @NonNull @NotNull MenuInflater inflater) {
        menu.clear();

        // Add the new menu items
        inflater.inflate(R.menu.staging_menu, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.actionImages:
                openImages();
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openImages(){
//        // String jobNo = edJobNo.getText().toString();
//        if (items_prints==null) {
//            showToast("Enter Job Id");
//            return;
//        }
//
//        Bundle bundle = new Bundle();
//        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
//        //bundle.putString(AppConstants.SELECTED_ID, inbound_header.JOB_NO);
//        //bundle.putSerializable(AppConstants.SELECTED_OBJECT, inbound_header);
//        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_confirmation_list_tablayout, bundle);

    }

}
