package com.dcode.gacinventory.ui.view;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Vibrator;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.network.service.ImageUploadService;
import com.dcode.gacinventory.ui.dialog.CustomProgressDialog;
import com.example.gacinventory.R;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;


public abstract class BaseActivity extends AppCompatActivity {

    protected int selectedID;
    protected String selectedCode;
    protected String selectedName;
    private CustomProgressDialog progressDialog;
    private AlertDialog alertDialog;
    private boolean confirmation = false;

    protected abstract int getLayoutResourceId();

    protected abstract void onViewReady(Bundle savedInstanceState);

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResourceId());
        onViewReady(savedInstanceState);

        //startService(new Intent(this, ImageUploadService.class));

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void finishAndRemoveTask(){
        super.finishAndRemoveTask();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (intent == null) return;

        if (resultCode != Activity.RESULT_OK) return;

        selectedID = intent.hasExtra(AppConstants.SELECTED_ID) ? intent.getIntExtra(AppConstants.SELECTED_ID, -1) : -1;
        selectedCode = intent.hasExtra(AppConstants.SELECTED_CODE) ? intent.getStringExtra(AppConstants.SELECTED_CODE) : "";
        selectedName = intent.hasExtra(AppConstants.SELECTED_NAME) ? intent.getStringExtra(AppConstants.SELECTED_NAME) : "";
    }

/*    protected void setupToolBar(String screenTitle) {
        Toolbar toolbar = findViewById(R.id.toolbar);
        ((TextView) toolbar.findViewById(R.id.tv_toolbar)).setText(screenTitle);
        setSupportActionBar(toolbar);
    }*/

    protected void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    protected void showAlert(final String title, final String message) {
        if (alertDialog != null && alertDialog.isShowing())
            return;

//        LayoutInflater factory = LayoutInflater.from(getContext());
//        final View view = factory.inflate(R.layout.layout_app_validation, null);
//        final EditText edGuid = (EditText) view.findViewById(R.id.edDeviceID);
//        final EditText edEnGuid = (EditText) view.findViewById(R.id.edLicense);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        alertDialog = builder
                .setTitle(title)
                //.setView(view)
                .setIcon(R.drawable.logo)
                .setMessage(message)
                .setPositiveButton("OK", (dialog, which) -> {
                })
                .create();
        alertDialog.show();
        shortVibration();
    }

    protected void showAlert(String title, String message, final boolean isFinish) {
        if (alertDialog != null && alertDialog.isShowing())
            return;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        alertDialog = builder.setTitle(title).setMessage(message).setPositiveButton("OK", (dialog, which) -> {
            if (isFinish) {
                finish();
            }
        }).create();
        alertDialog.setCancelable(isFinish);
        alertDialog.show();
        shortVibration();
    }


    protected void showProgress(boolean isCancelable) {
        if (progressDialog != null && progressDialog.isShowing())
            return;

        progressDialog = new CustomProgressDialog(this, isCancelable);
        progressDialog.show();
    }

    protected void updateProgressMessage(String message) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.updateMessage(message);
        }
    }

    protected void dismissProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    protected void shortVibration() {
        Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        if (vibe != null && vibe.hasVibrator()) vibe.vibrate(10);
    }

//    protected void OnLookupClick(int master_id, int parent_id, @Nullable int[] array) {
//        Intent intent = new Intent(this, searchFragment.class);
//        intent.putExtra(AppConstants.MASTER_ID, master_id);
//        intent.putExtra(AppConstants.PARENT_ID, parent_id);
//
//        if (array != null) {
//            intent.putExtra(AppConstants.FILTER_KEY_INT_ARRAY, array);
//        }
//        startActivityForResult(intent, master_id);
//    }



    public String getVersionName(Context context) {
        PackageInfo packageInfo = null;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), 0);
            return String.valueOf(packageInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
