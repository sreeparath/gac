package com.dcode.gacinventory.ui.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;


import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppVariables;
import com.dcode.gacinventory.data.model.MENU;
import com.dcode.gacinventory.data.model.VIEW_IMAGE;
import com.example.gacinventory.R;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Authenticator;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;

public class ImageGlideAdapter
        extends RecyclerView.Adapter<ImageGlideAdapter.RecyclerViewHolder>
        implements Filterable {

    private Context context;
    private View.OnClickListener shortClickListener;
    private List<VIEW_IMAGE> dataList;
    private ArrayList<VIEW_IMAGE> dataListFull;

    private Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<VIEW_IMAGE> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(dataListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (VIEW_IMAGE item : dataListFull) {
//                    if (item.MENU_NAME.toLowerCase().contains(filterPattern.toLowerCase()) ||
//                            item.MENU_DESC.toLowerCase().contains(filterPattern.toLowerCase())) {
//                        filteredList.add(item);
//                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            dataList.clear();
            dataList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public ImageGlideAdapter(Context context, List<VIEW_IMAGE> objects, View.OnClickListener shortClickListener) {
        this.context = context;
        this.shortClickListener = shortClickListener;
        this.dataList = objects;
        this.dataListFull = new ArrayList<>(objects);
        setHasStableIds(true);
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_image_viewer, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        final VIEW_IMAGE objectItem = this.dataList.get(position);
try {
    //holder.menuText.setText(objectItem.MENU_NAME);
    //holder.menuDesc.setText(objectItem.MENU_DESC);
//        if(objectItem.MENU_ICON>0){
//            holder.icon.setImageDrawable(context.getResources().getDrawable(objectItem.MENU_ICON));
//        }

    if (objectItem.API_DOC_ID.length() > 0) {
        //String url = "https://picsum.photos/200/300" ;

//        String url = "http://api-test.gacgroup.net/internal/documents/dev/ad-v1/api/documents/download?module=job&documentId=2aa14184-7070-4284-b93a-37a3e5e9576c";
//        GlideUrl glideUrl = new GlideUrl(url,
//                new LazyHeaders.Builder()
//                        .addHeader("Authorization", AppVariables.getAccessToken())
//                        .addHeader("Accept", "*/*")
//                        .addHeader("Ocp-Apim-Subscription-Key", App.appSettings().getSUBSCRIPTION_KEY())
//                        .build());
//
//        Log.d("glideUrl#",glideUrl.toString());
//
//        Glide.with(context)
//                .asBitmap()
//                .load(url)
//                .listener(requestListener)
//                .error(R.drawable.circle)
//                .into(holder.icon);


        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .authenticator(new Authenticator()
                {
                    @Override
                    public Request authenticate(Route route, Response response) throws IOException
                    {
                        return response.request().newBuilder()
                                .header("Authorization", AppVariables.getAccessToken())
                                .header("Accept", "*/*")
                                .header("Ocp-Apim-Subscription-Key","8bb005d79dc9445baf46e93ea9129c3f")
                                .build();
                    }
                }).build();

        Picasso picasso = new Picasso.Builder(context)
                .downloader(new OkHttp3Downloader(okHttpClient))
                .build();
        picasso.load("http://api-test.gacgroup.net/internal/documents/dev/ad-v1/api/documents/download?module=job&documentId=2aa14184-7070-4284-b93a-37a3e5e9576c").into(holder.icon);
    }

    holder.itemView.setTag(objectItem);
    holder.itemView.setOnClickListener(shortClickListener);
}catch (Exception e){
    e.printStackTrace();
}

        //Random rnd = new Random();
        //int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));

//        int color= ((int)(Math.random()*16777215)) | (0xFF << 24);
//        LayerDrawable layerDrawable = (LayerDrawable) context.getDrawable(R.drawable.border);
//        GradientDrawable gradientDrawable = (GradientDrawable) layerDrawable
//                .findDrawableByLayerId(R.id.borcol);
//        gradientDrawable.setColor(color);


    }

//    private RequestListener<Bitmap> requestListener = new RequestListener<Bitmap>() {
//        @Override
//        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
//            // todo log exception to central service or something like that
//Log.d("onLoadFailed##",e.getMessage());
//            // important to return false so the error placeholder can be placed
//            return false;
//        }
//
//        @Override
//        public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
//            // everything worked out, so probably nothing to do
//            Log.d("onResourceReady##","onResourceReady");
//            return false;
//        }
//    };

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public void addItems(List<VIEW_IMAGE> list) {
        this.dataList = list;
        this.dataListFull = new ArrayList<>(list);

        notifyDataSetChanged();
    }

    public List<VIEW_IMAGE> getItems() {
        return this.dataList;
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView menuText;
        TextView menuDesc;
        LinearLayout borderPanel;
        ImageView icon;

        RecyclerViewHolder(View view) {
            super(view);
            menuText = view.findViewById(R.id.menuText);
            menuDesc = view.findViewById(R.id.menuDesc);
            icon = view.findViewById(R.id.appImage);
            //borderPanel = view.findViewById(R.id.borderPanel);
        }
    }

}
