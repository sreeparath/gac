package com.dcode.gacinventory.ui.view;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;


import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.common.GACDynFuncs;
import com.dcode.gacinventory.data.model.USERS;
import com.dcode.gacinventory.data.model.WAREHOUSE;
import com.dcode.gacinventory.network.model.GenericRetResponse;
import com.dcode.gacinventory.network.service.ServiceUtils;
import com.example.gacinventory.R;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.Arrays;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class LoginActivity
        extends BaseActivity {
    private ImageButton imgSettings;
    private Button btSignIn;
    private TextInputEditText edUserName;
    private TextInputEditText edPassword;
    private TextInputEditText edWorkSites;
    //    private ArrayAdapter<LOCATIONS> locationsAdapter;
    private ArrayAdapter<WAREHOUSE> warehouseAdapter;
    private List<WAREHOUSE> warehouseList;
    private AutoCompleteTextView edWare;
    private boolean isSignClicked = false;
    private boolean isUserValid = false;
    private TextView version,edAppMode;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_login;
    }


    @Override
    protected void onViewReady(Bundle savedInstanceState) {
        imgSettings = findViewById(R.id.imgSettings);
        imgSettings.setOnClickListener(v -> onSettingsClick());
        edUserName = findViewById(R.id.edUserName);
        edPassword = findViewById(R.id.edPassword);

        edUserName.setText("andr01");
        edPassword.setText("123");

        version  = findViewById(R.id.version);
        version.setText("Gacware "+getVersionName(getApplicationContext()));
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) { // API 21
//            edUserName.setShowSoftInputOnFocus(false);
//        } else { // API 11-20
//            edUserName.setTextIsSelectable(true);
//        }

//        edUserName.setOnKeyListener((v, keyCode, event) -> {
//                return true;
//        });

        btSignIn = findViewById(R.id.btSignIn);
        btSignIn.setOnClickListener(v -> OnSignInClick());
        btSignIn.setEnabled(false);
        edWare = findViewById(R.id.edWare);
        getWarehouseOnline();

        edAppMode= findViewById(R.id.edAppMode);
        getAppMode();
       // Log.d("nano # ", String.valueOf(System.currentTimeMillis()- AppConstants.SERIAL_NO_CONSTANT));


    }

    @Override
    public void onBackPressed() {
        finishAndRemoveTask();
        super.onBackPressed();
    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
//        super.onActivityResult(requestCode, resultCode, intent);
//
//        if (resultCode != RESULT_OK) {
//            return;
//        }
//
//        if (requestCode == AppConstants.LOCATION) {
//            LOCATIONS selectedObject = (LOCATIONS) intent.getSerializableExtra(AppConstants.SELECTED_OBJECT);
//            App.currentLocation = selectedObject;
//            if (selectedObject != null && selectedObject.LOCODE.length() > 0) {
//                edWorkSites.setText(selectedObject.LONAME);
//                btSignIn.setText(R.string.next);
//            }
//        }
//    }

    private void OnSignInClick() {
        String errMessage = "";
        edUserName.setError(null);
        edPassword.setError(null);



//        if (App.currentCompany != null && App.currentLocation != null && isSignClicked && isUserValid) {
//            //showToast(App.currentCompany.Code + " ## "+App.currentLocation.LOCODE);
//            GetAppSettings();
//            return;
//        }

        String loginName = edUserName.getText().toString();
        String passWd = edPassword.getText().toString().trim();

        if (loginName.length() < 1) {
            errMessage = String.format("%s required..", getString(R.string.username));
            showToast(errMessage);
            edUserName.setError(errMessage);
            return;
        }

        if (passWd.length() < 1) {
            errMessage = String.format("%s required..", getString(R.string.password));
            showToast(errMessage);
            edPassword.setError(errMessage);
            return;
        }


        if (App.currentWarehouse==null) {
            errMessage = String.format("%s required..", getString(R.string.warehouse));
            showToast(errMessage);
            edWare.setError(errMessage);
            return;
        }

//        USERS curUser = new USERS();
//        curUser.USER_ID = "admin";
//        curUser.USER_NAME = "admin";
//
//        AppVariables.CurrentUser = curUser;

        //GetAppSettings();

/*        if (App.currentLocation == null) {
            showToast(String.format("%s required..", getString(R.string.work_site)));
            return;
        }*/

//        if (App.currentCompany == null) {
//            errMessage = String.format("%s required..", getString(R.string.company));
//            showToast(errMessage);
//            acCompany.setError(errMessage);
//            return;
//        }


         showProgress(false);
         ValidateUserOnline(loginName, passWd, passWd.length());
    }


    private void ValidateUserOnline(String loginName, String passWd, int passWdLen) {

        JsonObject requestObject = ServiceUtils.UserValidation.getUserLoginDetails(loginName,passWd);

        showProgress(false);
        App.getNetworkClient().getAPIService().getRetGeneric(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("S")) {
                    String xmlDoc = genericRetResponse.getJsonString();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showAlert("Error", "Invalid credentials.");
                        return;
                    }
                    try {
                        USERS[] user = new Gson().fromJson(xmlDoc, USERS[].class);
                        if(user.length==0){
                            showAlert("Error", "Invalid credentials.");
                            return;
                        }
                        if (user[0].USER_ID == null || user[0].USER_ID.length() <= 0 || user[0].USER_ID.equals("-1")) {
                            String errMessage = "Invalid credentials. " + user[0].FULL_NAME;
                            showToast(errMessage);
                            edUserName.setError(errMessage);
                            edPassword.setError(errMessage);
                            isUserValid = false;
                            return;
                        } else {
                            App.getDatabaseClient().getAppDatabase().genericDao().deleteUser(user[0].USER_ID);
                            App.getDatabaseClient().getAppDatabase().genericDao().insertUser(user[0]);
                            App.currentUser = user[0];
                            doAfterLoginUpdate(user[0]);
                            App.appSettings().destroyInstance();
                            isUserValid = true;
                            Log.d("currentUser#",App.currentUser.USER_ID);

                        }

                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                        showAlert("Error", e.getMessage());
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showAlert("Error", msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showAlert("Error", msg);
            }
        });
    }

    private void doAfterLoginUpdate(USERS user) {

        JsonObject requestObject = ServiceUtils.UserValidation.setAfterLoginProcess(user);

        showProgress(false);
        App.getNetworkClient().getAPIService().getRetGeneric(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("S")) {
                    String xmlDoc = genericRetResponse.getJsonString();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showAlert("Error", "Update Failed.");
                        return;
                    }
                    showToast(genericRetResponse.getErrMessage());
                    downloadAllMasters();
                    doDynamicValidationFetch();
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showAlert("Error", msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showAlert("Error", msg);
            }
        });
    }

    private void doDynamicValidationFetch() {

        JsonObject requestObject = ServiceUtils.UserValidation.setDynamicValidationData();
        showProgress(false);
        //updateProgressMessage("Downloading Dynamic Validations...");
        App.getNetworkClient().getAPIService().getRetGeneric(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                //dismissProgress();
                if (!genericRetResponse.getErrCode().equals("E")) {
                    String xmlDoc = genericRetResponse.getJsonString();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showAlert("Error", "Update Failed.");
                        return;
                    }
                    try {
                        GACDynFuncs[] validationFuncs = new Gson().fromJson(xmlDoc, GACDynFuncs[].class);
                        if(validationFuncs.length==0){
                            showAlert("Error", "Invalid GACDynFuncs.");
                            dismissProgress();
                            return;
                        }
                        App.dynFuncs = Arrays.asList(validationFuncs);
                        GetAppSettings();
                       //updateProgressMessage("Downloading Dynamic Validations Completed.");
                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                        showAlert("Error", e.getMessage());
                    }
                    dismissProgress();
                    showToast(genericRetResponse.getErrMessage());

                } else {
                    dismissProgress();
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showAlert("Error", msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showAlert("Error", msg);

            }
        });
    }

    private void downloadAllMasters(){
        try{
            ServiceUtils.SyncActivity syncActivity = new ServiceUtils.SyncActivity(this, false);
            //syncActivity.DownloadPrinterList(AppConstants.DatabaseEntities.PRINTER_LIST_SERVICE);
           // syncActivity.DownloadPrintFormats(AppConstants.DatabaseEntities.PRINT_FORMATS);
            syncActivity.DownloadLotStatus(AppConstants.DatabaseEntities.LOT_STATUS);
            syncActivity.DownloadUserPrincipleInfo(AppConstants.DatabaseEntities.USER_PRIN_INFO);
        }catch (Exception e){
            e.printStackTrace();
            showToast(e.getMessage());
        }
    }

//    private void ValidateUserOnline(String loginName, String passWd, int passWdLen) {
//        LoginParams loginParams = new LoginParams();
//        loginParams.LoginName = loginName;
//        loginParams.LoginPassword = passWd;
////        loginParams.LoginPassword = CryptoSecurity.encrypt(passWd);
//        loginParams.PasswordLength = passWdLen;
//        loginParams.SPName = "PDT.USER_VALIDATE_SPR";
//        loginParams.PasswordEncrypted = 0;
//        loginParams.EncryptToDB = 0;
//        loginParams.DBName = App.currentCompany.DbName;
//        loginParams.DeviceIP = App.DeviceIP;
//
//        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
//        String json = gson.toJson(loginParams);
//
//        JsonObject jsonObject = gson.fromJson(json, JsonObject.class);
//
//        App.getNetworkClient().getAPIService().ValidateUser(jsonObject, new Callback<ValidUserResponse>() {
//            @Override
//            public void success(ValidUserResponse validUserResponse, Response response) {
//                try {
//                    String errMessage;
//
//                    USERS users = new USERS();
//                    users.USER_ID = validUserResponse.getUserID();
//                    users.FULL_NAME = validUserResponse.getFullName();
//                    users.USER_NAME = validUserResponse.getLoginName();
//                    users.USER_PWD = validUserResponse.getLoginPassword();
//                    users.TEAM_ID = validUserResponse.getTeamID();
//
//                    LOCATIONS locations = new LOCATIONS();
//                    locations.LOCODE = validUserResponse.getLoCode();
//                    locations.LONAME = validUserResponse.getLoName();
//                    /*should not happen*/
//                    locations.LONAME = locations.LONAME == null || locations.LONAME.isEmpty() ? "No Text" : locations.LONAME;
//
//                    if (locations.LOCODE == null || locations.LOCODE.length() <= 0 || locations.LOCODE.equals("-1")) {
//                        errMessage = "Invalid Location code.";
//                        showToast(errMessage);
//                        edWorkSites.setError(errMessage);
//                        isSignClicked = false;
//                    } else {
//                        edWorkSites.setText(locations.LONAME);
//                        btSignIn.setText(R.string.next);
//                        isSignClicked = true;
//                    }
//
//                    if (users.USER_ID == null || users.USER_ID.length() <= 0 || users.USER_ID.equals("-1")) {
//                        errMessage = "Invalid credentials. " + users.FULL_NAME;
//                        showToast(errMessage);
//                        edUserName.setError(errMessage);
//                        edPassword.setError(errMessage);
//                        isUserValid = false;
//                        return;
//                    } else {
//                        App.getDatabaseClient().getAppDatabase().genericDao().deleteUser(users.USER_ID);
//                        App.getDatabaseClient().getAppDatabase().genericDao().insertUser(users);
//                        App.appSettings().destroyInstance();
//                        isUserValid = true;
//                    }
//
//                    App.currentLocation = locations;
//                    App.currentUser = users;
////                    GetLocations();
//                } catch (Exception e) {
//                    Log.d(App.TAG, e.getStackTrace().toString());
//                    showToast(e.getMessage());
//                } finally {
//                    dismissProgress();
//                }
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                dismissProgress();
//                Log.d(App.TAG, error.getMessage());
//                showToast("User login: Network call failure");
//            }
//        });
//    }


/*    private void GetLocations() {
        if (App.currentCompany == null || App.currentCompany.Code.length() <= 0) {
            showToast("Please sign-in to select location");
        }

        if (Utils.GetRowsCount("LOCATIONS", true, "COCODE", App.currentCompany.Code) == 0) {
            GetLocationsOnline();
        } else {
            Intent intent = new Intent(this, LocationSearchActivity.class);
            startActivityForResult(intent, AppConstants.LOCATION);
        }
    }*/

    private void getWarehouseOnline() {
        // download the locations
        try {
            showProgress(false);
            JsonObject requestObject = ServiceUtils.UserValidation.getWareHouseList();
            Log.d("requestObject##", requestObject.toString());
            App.getNetworkClient().getAPIService().getRetGeneric(requestObject, new Callback<GenericRetResponse>() {
                @Override
                public void success(GenericRetResponse genericRetResponse, Response response) {
                    if (genericRetResponse.getErrCode().equals("S")) {
                        String xmlDoc = genericRetResponse.getJsonString();
                        if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                            showAlert("Error", "No data received.");
                            return;
                        }
                        WAREHOUSE[] warehouseLists = new Gson().fromJson(xmlDoc, WAREHOUSE[].class);
                        GetWarehouses(warehouseLists);
                    } else {
                        String msg = genericRetResponse.getErrMessage();
                        Log.d(App.TAG, msg);
                        showToast(msg);
                        updateProgressMessage("Download request failed.");
                        btSignIn.setEnabled(false);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    String msg = error.getMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                }
            });
        } catch (Exception e) {
            String msg = e.getMessage();
            Log.d(App.TAG, msg);
            showToast(msg);
        } finally {
            dismissProgress();
        }
    }

    private void getAppMode() {
        // download the locations
        try {
            showProgress(false);
            JsonObject requestObject = ServiceUtils.UserValidation.getAppMode();
           // Log.d("requestObject##", requestObject.toString());
            App.getNetworkClient().getAPIService().getAppModeGeneric(requestObject,new Callback<GenericRetResponse>() {
                @Override
                public void success(GenericRetResponse genericRetResponse, Response response) {
                    if (genericRetResponse.getErrCode().equals("S")) {
                        String msg = genericRetResponse.getErrMessage();
                        if (msg == null || msg.trim().length() == 0) {
                            //showAlert("Error", "No data received.");
                            edAppMode.setText("NOT SET");
                            return;
                        }
                        edAppMode.setText(msg);
                    } else {
                        String msg = genericRetResponse.getErrMessage();
                        Log.d(App.TAG, msg);
                        showToast(msg);
                        updateProgressMessage("Download request failed.");
                        btSignIn.setEnabled(false);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    String msg = error.getMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                }
            });
        } catch (Exception e) {
            String msg = e.getMessage();
            Log.d(App.TAG, msg);
            showToast(msg);
        } finally {
            dismissProgress();
        }
    }

    private void GetAppSettings() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void onSettingsClick() {
        Intent intent = new Intent(com.dcode.gacinventory.ui.view.LoginActivity.this, SettingsActivity.class);
        startActivity(intent);
        finish();
    }

    private void GetWarehouses(WAREHOUSE[] warehouseList) {
        warehouseAdapter = new ArrayAdapter<>(LoginActivity.this, R.layout.dropdown_menu_popup_item, warehouseList);
        edWare.setAdapter(warehouseAdapter);
        edWare.setThreshold(100);
        edWare.setOnItemClickListener((parent, view, position, id) -> {
            if (position >= 0) {
                App.currentWarehouse = warehouseAdapter.getItem(position);
            } else {
                App.currentWarehouse = null;
            }

        });

        btSignIn.setEnabled(true);
    }
}