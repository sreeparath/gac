package com.dcode.gacinventory.ui.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.room.ColumnInfo;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.common.StagingValidation;
import com.dcode.gacinventory.data.model.COUNTRY_LIST;
import com.dcode.gacinventory.data.model.INBOUND_HEADER;
import com.dcode.gacinventory.data.model.INVESIGATE_REGISTER;
import com.dcode.gacinventory.data.model.INVEST_REASONS;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SCAN;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SUMBIT;
import com.dcode.gacinventory.data.model.MASTER_LIST;
import com.dcode.gacinventory.data.model.PROD_UPD_INFO;
import com.dcode.gacinventory.network.model.GenericRetResponse;
import com.dcode.gacinventory.network.service.ServiceUtils;
import com.example.gacinventory.R;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.JsonObject;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.dcode.gacinventory.common.AppConstants.INVEST_HEADER;
import static com.dcode.gacinventory.common.AppConstants.INVEST_PUT;
import static com.dcode.gacinventory.common.AppConstants.INVEST_STAG;

public class ProductUpdateInfoFragment extends BaseFragment {
    private TextView edInfoNew ;
//    private AutoCompleteTextView tvDocTypes;
//    private ArrayAdapter<DocType> objectsAdapter;
    private int promptResource;
    private String DocNo;
    private long HDR_ID;
    private LOGISTIC_LABEL_SCAN logisticLabelScan;
    private TextInputEditText edPartSet;
    private TextInputEditText edDeptCode;
    private TextInputEditText edStroke;
    private TextInputEditText edColorCode;
    private TextInputEditText edSize;
    private View root;
    private PROD_UPD_INFO prodUpdInfo;
    //private INBOUND_HEADER inboundHeader;
    private AutoCompleteTextView edCountry,edStorageType;
    private ArrayAdapter<COUNTRY_LIST> countryListArrayAdapter;
    private ArrayAdapter<MASTER_LIST> masterListArrayAdapter;
    private MASTER_LIST masterList;
    private COUNTRY_LIST countryList;

    StagingValidation stagingValidation;
    private boolean mResult = false;
    private long fromModule = -1;

    //private RadioGroup radioGroupImage;
    //private RadioGroup radioGroupDanger;
    private Switch swImageOn,swDangerOn;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

            //ModuleID = this.getArguments().getInt("ModuleID", 0);
            Bundle bundle = this.getArguments();
            if (bundle != null) {
//                if(bundle.containsKey(AppConstants.SELECTED_OBJECT_NEW)){
//                    inboundHeader = (INBOUND_HEADER) bundle.getSerializable(AppConstants.SELECTED_OBJECT_NEW);
//                }
                if(bundle.containsKey(AppConstants.SELECTED_OBJECT)){
                    logisticLabelScan = (LOGISTIC_LABEL_SCAN) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
                }

                fromModule = bundle.containsKey(AppConstants.SELECTED_ID) ? bundle.getLong(AppConstants.SELECTED_ID) : -1;
            }

        root = inflater.inflate(R.layout.fragment_product_update, container, false);

        setupView();
        return root;
    }


    private void setupView(){
        //setHeaderToolbar();
        View headerLayout = (View) root.findViewById(R.id.top_bar);
        headerLayout.setVisibility(View.GONE);
        if(fromModule==AppConstants.PROD_UPDATE_HEADER){
            headerLayout.setVisibility(View.VISIBLE);
            setHeaderToolbar();
        }

        //edJobCode= root.findViewById(R.id.edJobCode);
        ///edInvestNo= root.findViewById(R.id.edInvestNo);
        edPartSet= root.findViewById(R.id.edPartSet);
        edCountry = root.findViewById(R.id.edCountryList);
        edStorageType = root.findViewById(R.id.edStorageType);
        edDeptCode= root.findViewById(R.id.edDeptCode);
        edStroke= root.findViewById(R.id.edStroke);
        edColorCode= root.findViewById(R.id.edColorCode);
        edSize = root.findViewById(R.id.edSize);

        MaterialButton btnBack = root.findViewById(R.id.btnBack);
        btnBack.setOnClickListener(v -> getActivity().onBackPressed());

        MaterialButton btnNext = root.findViewById(R.id.btnNext);
        btnNext.setOnClickListener(v -> OnNextClick());

        //radioGroupImage = root.findViewById(R.id.radioGroupImage);
        //radioGroupDanger = root.findViewById(R.id.radioGroupDanger);

        swImageOn = root.findViewById(R.id.swImageOn);
        swImageOn.setChecked(true);
        swDangerOn = root.findViewById(R.id.swDangerOn);
        swDangerOn.setChecked(true);
    }

    private void setHeaderToolbar() {
        if (logisticLabelScan != null) {
            Toolbar toolbar = root.findViewById(R.id.toolbar);
            TextView info1 = root.findViewById(R.id.info1);
            info1.setText(App.currentUser.USER_ID);
            TextView info2 = root.findViewById(R.id.info2);
            info2.setText(logisticLabelScan.JOB_NO + " - "+ logisticLabelScan.PRIN_NAME);
        }
    }


   @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       FillHeader();
       FillCountry();
       FillStorageType();
    }


    private void FillHeader(){
        if(logisticLabelScan!=null){
            edDeptCode.setText(logisticLabelScan.DEPT_CODE);
            edStroke.setText(logisticLabelScan.STROKE);
            edColorCode.setText(logisticLabelScan.COLOR_CODE);
            edSize.setText(logisticLabelScan.SIZE);
        }
    }

/*    private boolean onDocNoKeyEvent() {
        DocNo = edDocNo.getText().toString();

        if (DocNo.isEmpty() || DocNo.trim().length() == 0) {
            showToast(String.format("%s required", getString(promptResource)));
            return false;
        }
        return true;
    }*/

    public PROD_UPD_INFO getProdUpdInfo(){
        return  this.prodUpdInfo;
    }

    private void OnNextClick() {
        if(Validate()){
            prodUpdInfo = new PROD_UPD_INFO();
            prodUpdInfo.JOB_NO = logisticLabelScan.JOB_NO.trim();
            prodUpdInfo.PROD_CODE = logisticLabelScan.SKU.trim();
            prodUpdInfo.PRIN_CODE = logisticLabelScan.PRIN_CODE.trim();
            if(masterList!=null){
                prodUpdInfo.STORAGE_TYPE = masterList.CODE;
            }else{
                prodUpdInfo.STORAGE_TYPE = "";
            }
            if(countryList!=null){
                prodUpdInfo.COO = countryList.COUNTRY_CODE;
            }else{
                prodUpdInfo.COO = "";
            }
            prodUpdInfo.PART_SET = Integer.parseInt(edPartSet.getText().toString());
            //int radioGroupImagCcheck = radioGroupImage.getCheckedRadioButtonId();
            //RadioButton rdoImage = (RadioButton) radioGroupImage.findViewById(radioGroupImagCcheck);
            //showToast(rdoImage.getText().toString());
//            if((rdoImage.getText().toString().equalsIgnoreCase(getString(R.string.image_on)))){
//                prodUpdInfo.IMAGE_CENSOR = 1;
//            }else{
//                prodUpdInfo.IMAGE_CENSOR = 0;
//            }
            if(swImageOn.isChecked()){
                prodUpdInfo.IMAGE_CENSOR = 1;
            }else{
                prodUpdInfo.IMAGE_CENSOR = 0;
            }

//            int radioGroupDangerCheck = radioGroupDanger.getCheckedRadioButtonId();
//            RadioButton rdoDanger = (RadioButton) radioGroupDanger.findViewById(radioGroupDangerCheck);
//            //showToast(rdoImage.getText().toString());
//            if((rdoDanger.getText().toString().equalsIgnoreCase(getString(R.string.danger_on)))){
//                prodUpdInfo.DANGEROUS_GOODS = 1;
//            }else{
//                prodUpdInfo.DANGEROUS_GOODS = 0;
//            }

            if(swDangerOn.isChecked()){
                prodUpdInfo.DANGEROUS_GOODS = 1;
            }else{
                prodUpdInfo.DANGEROUS_GOODS = 0;
            }

            //invesigateRegister.INVESTIGATION_REASON = investReasons.REASON_DESC;

            uploadProdUpdInfo();
        }
    }

    private void backToHome() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        ((MainActivity) requireActivity()).NavigateFullBackStack();
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_home, bundle);
    }

    private void backToDirectPutaway() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        ((MainActivity) requireActivity()).NavigateFullBackStack();
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_put_away_header, bundle);
    }

    private void backToHeader() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        ((MainActivity) requireActivity()).NavigateFullBackStack();
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_product_update_header, bundle);
    }

    private boolean Validate() {
        String errMessage;
//        //String scanLoc = edInvestNo.getText().toString();
//        if (countryList == null) {
//            errMessage = String.format("%s required", "Country");
//            showToast(errMessage);
//            edCountry.setError(errMessage);
//            return false;
//        }
//        if (masterList == null) {
//            errMessage = String.format("%s required", "StorageType");
//            showToast(errMessage);
//            edStorageType.setError(errMessage);
//            return false;
//        }
//
        String partSet = edPartSet.getText().toString();
        if (partSet.length() == 0) {
            errMessage = String.format("%s required/or enter zero", "PartSet");
            showToast(errMessage);
            edPartSet.setError(errMessage);
            return false;
        }

//        if(inboundHeader==null){
//            showAlert("Error","Header is empty.");
//            return false ;
//        }
        if(logisticLabelScan==null){
            showAlert("Error","Product Information is empty.");
            return false ;
        }

        return true;

    }



    private void OpenGACScan() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putString(AppConstants.SELECTED_ID, DocNo);
        bundle.putSerializable(AppConstants.SELECTED_CODE, "");
        bundle.putLong(AppConstants.PARENT_ID, HDR_ID);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_gac_scan, bundle);
    }


    private void uploadProdUpdInfo() {
        if(prodUpdInfo==null){
            return;
        }

        showToast(getString(R.string.send_data_to_server));
        JsonObject requestObject = ServiceUtils.LogisticLabel.getProdUpdateInfo(prodUpdInfo);
        showProgress(false);
        App.getNetworkClient().getAPIService().submitWithLinesData(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericSubmissionResponse, Response response) {
                dismissProgress();
                if (genericSubmissionResponse.getErrCode().equals("S")) {
                        String msg = genericSubmissionResponse.getErrMessage();
                        //resetUI();
                        showAlert("Success",msg);
                        if(fromModule==AppConstants.PROD_UPDATE_HEADER){
                            backToHeader();
                        }
                } else {
                    Log.d(App.TAG, genericSubmissionResponse.getErrMessage());
                    showAlert("Error",genericSubmissionResponse.getErrMessage());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(App.TAG, error.getMessage());
                showToast(error.getMessage());
                dismissProgress();
            }
        });
    }

    private void FillCountry() {
        List<COUNTRY_LIST> country_lists = App.getDatabaseClient().getAppDatabase().genericDao().getCountryList();
        if(country_lists!=null&& country_lists.size()>0){
            countryListArrayAdapter = new ArrayAdapter<>(getContext(), R.layout.dropdown_menu_popup_item, country_lists);
            edCountry.setAdapter(countryListArrayAdapter);
            edCountry.setThreshold(100);
            edCountry.setOnItemClickListener((parent, view, position, id) -> {
                if (position >= 0) {
                    countryList = countryListArrayAdapter.getItem(position);
                } else {
                    countryList = null;
                }

            });
        }
    }

    private void FillStorageType() {
        List<MASTER_LIST> masterLists = App.getDatabaseClient().getAppDatabase().genericDao().getStorageList();
        if(masterLists!=null&& masterLists.size()>0){
            masterListArrayAdapter = new ArrayAdapter<>(getContext(), R.layout.dropdown_menu_popup_item, masterLists);
            edStorageType.setAdapter(masterListArrayAdapter);
            edStorageType.setThreshold(100);
            edStorageType.setOnItemClickListener((parent, view, position, id) -> {
                if (position >= 0) {
                    masterList = masterListArrayAdapter.getItem(position);
                } else {
                    masterList = null;
                }

            });
        }
    }

    private void backToPutaway() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        ((MainActivity) requireActivity()).NavigateFullBackStack();
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_put_away_header, bundle);
    }

    private void backToLogistic() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        //bundle.putSerializable(AppConstants.SELECTED_OBJECT, inboundHeader);
        ((MainActivity) requireActivity()).NavigateFullBackStack();
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_logistic_label, bundle);
    }

    private void resetUI(){
        countryList = null;
        logisticLabelScan = null;
    }

}
