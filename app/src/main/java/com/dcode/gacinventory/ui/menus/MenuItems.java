package com.dcode.gacinventory.ui.menus;

/**
 * Created by Vijildhas on 05/08/2020.
 */
public class MenuItems {
    private String menuName;
    private boolean isImageShown;
    private int menuImage;
    private int menuID; //ADD, DELETE, EDIT
    private boolean isSelected;

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public boolean isImageShown() {
        return isImageShown;
    }

    public void setImageShown(boolean imageShown) {
        isImageShown = imageShown;
    }

    public int getMenuImage() {
        return menuImage;
    }

    public void setMenuImage(int menuImage) {
        this.menuImage = menuImage;
    }

    public int getMenuID() {
        return menuID;
    }

    public void setMenuID(int menuID) {
        this.menuID = menuID;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
