package com.dcode.gacinventory.ui.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.data.model.INBOUND_HEADER;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SCAN;
import com.dcode.gacinventory.ui.adapter.ConfirmListTabAdapter;
import com.dcode.gacinventory.ui.adapter.ObjectsAdapter;
import com.dcode.gacinventory.ui.adapter.StaggingTabAdapter;
import com.example.gacinventory.R;
import com.google.android.material.tabs.TabLayout;

import java.util.Map;


public class StaggingTabFragment extends BaseFragment implements View.OnClickListener {

    View root;
    ObjectsAdapter detAdapter;
    TabLayout tabLayout;
    ViewPager viewPager;
    INBOUND_HEADER inboundHeader;
    //String jobNo;
    StaggingTabAdapter adapter;
    LOGISTIC_LABEL_SCAN logisticLabelScan;
    Map<String, String> gs1Map;
    private long sourceId = -1;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true);
        if (this.getArguments() != null) {
            Bundle bundle = this.getArguments();
            //parentId = bundle.containsKey(AppConstants.PARENT_ID) ? bundle.getInt(AppConstants.PARENT_ID) : -1;
            //jobNo = bundle.containsKey(AppConstants.SELECTED_ID) ? bundle.getString(AppConstants.SELECTED_ID, "") : "";
            logisticLabelScan  = bundle.containsKey(AppConstants.SELECTED_OBJECT) ? (LOGISTIC_LABEL_SCAN) bundle.getSerializable(AppConstants.SELECTED_OBJECT) : null;
            inboundHeader = bundle.containsKey(AppConstants.SELECTED_OBJECT_NEW) ? (INBOUND_HEADER) bundle.getSerializable(AppConstants.SELECTED_OBJECT_NEW) : null;
            gs1Map = bundle.containsKey(AppConstants.GS1_PARAM) ? (Map<String, String>) bundle.getSerializable(AppConstants.GS1_PARAM) : null;

        }


        root = inflater.inflate(R.layout.fragment_stagging_list_tab, container, false);



        setupView();

        return root;
    }

    @Override
    public void onClick(View view) {

    }

    void setupView(){
        setHeaderToolbar();
        tabLayout=(TabLayout) root.findViewById(R.id.tabLayout);
        viewPager=(ViewPager) root.findViewById(R.id.viewPager);
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.staging_status)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.product_update)));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        adapter = new StaggingTabAdapter(getContext(),getChildFragmentManager(), tabLayout.getTabCount(),this.getArguments());
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(2);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                viewPager.removeViewAt(tab.getPosition());
            }
        });

    }

    private void setHeaderToolbar() {
        if (inboundHeader != null) {
            Toolbar toolbar = root.findViewById(R.id.toolbar);
            TextView info1 = root.findViewById(R.id.info1);
            info1.setText(App.currentUser.USER_ID);
            TextView info2 = root.findViewById(R.id.info2);
            info2.setText(inboundHeader.JOB_NO + " - " + inboundHeader.PRIN_NAME );
        }
    }


}