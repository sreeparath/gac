package com.dcode.gacinventory.ui.view;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.dcode.gacinventory.App;
import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.common.StagingValidation;
import com.dcode.gacinventory.common.Utils;
import com.dcode.gacinventory.common.ValidRespSet;
import com.dcode.gacinventory.data.model.INBOUND_HEADER;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SCAN;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SUMBIT;
import com.dcode.gacinventory.data.model.LOT_STATUS;
import com.dcode.gacinventory.data.model.LOT_STATUS_LIST;
import com.dcode.gacinventory.network.model.GenericRetResponse;
import com.dcode.gacinventory.network.service.ServiceUtils;
import com.example.gacinventory.R;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


import static com.dcode.gacinventory.common.GS1Identifiers.AI_DATE_EXPIRATION;
import static com.dcode.gacinventory.common.GS1Identifiers.AI_LOT_NUMBER;
import static com.dcode.gacinventory.common.GS1Identifiers.AI_QTY;
import static com.dcode.gacinventory.common.Utils.GS_DATE_NUM_FORMAT;

public class GACScanFragment extends BaseFragment {
    private TextView edDocNo,edProdCode;
    private TextInputEditText edLUnit;
    private TextInputEditText edMunit;
    private TextInputEditText edPUnit;
    private TextInputEditText edPl;
    private TextInputEditText edBarcode;
    private TextInputEditText edFillCode;
    private TextInputEditText edFormula;
    private TextView edDisplayQty;
    //private TextInputEditText edLotStatus;
    private TextInputEditText edLotNo;
    private TextView edCountry;
    private TextInputEditText edExp;
    private TextInputEditText edManf;
    private TextView edSkuName;
    private TextView edPickKey;
    private String DocNo,barcode;
    private long HDR_ID;
    private List<LOT_STATUS> lot_statuses;
    private ArrayAdapter<LOT_STATUS_LIST> lotStatusArrayAdapter;
    private AutoCompleteTextView edLotStatus;
    private LOGISTIC_LABEL_SCAN items_prints;
    private LOGISTIC_LABEL_SUMBIT logistic_label_sumbit;
    private View root;
    private LOT_STATUS_LIST lotStatusType;
    private INBOUND_HEADER inboundHeader;
    private LinearLayout collapsePanel,datepanel;
    private ScrollView scrollView;
    StagingValidation stagingValidation;
    private TextInputLayout punit,munit,lunit,pl;
    private boolean mResult = false;
    private String StagingMode = "";
    boolean isExpand = false;
    boolean isFromEdit = false;
    private long sourceId = -1;
    Map<String, String> gs1Map;
    private TextInputEditText edBarcode1;
    private TextInputEditText edBarcode2;
    private TextInputEditText edBarcode3;
    private TextInputEditText edBarcode4;
    private TextInputEditText edBarcode5;
    private TextInputEditText edBarcode6;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true);
        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                //DocNo = bundle.getString(AppConstants.SELECTED_ID);
                //barcode = bundle.getString(AppConstants.MASTER_ID);
                items_prints = (LOGISTIC_LABEL_SCAN) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
                inboundHeader = (INBOUND_HEADER) bundle.getSerializable(AppConstants.SELECTED_OBJECT_NEW);
                isFromEdit = bundle.containsKey(AppConstants.SELECTED_ID) ? bundle.getBoolean(AppConstants.SELECTED_ID) : false;
                sourceId = bundle.containsKey(AppConstants.SOURCE_ID) ? bundle.getLong(AppConstants.SOURCE_ID) : -1;
                gs1Map = bundle.containsKey(AppConstants.GS1_PARAM) ? (Map<String, String>) bundle.getSerializable(AppConstants.GS1_PARAM) : null;
            }
        }

        root = inflater.inflate(R.layout.fragment_gac_scan, container, false);
        setupView();
        return root;
    }

    private void setupView(){
        //setHeaderToolbar();

        stagingValidation = new StagingValidation();

        edDocNo = root.findViewById(R.id.edScanCode);
        edLUnit = root.findViewById(R.id.edLUnit);
        edMunit= root.findViewById(R.id.edMunit);
        edPUnit= root.findViewById(R.id.edPUnit);
        pl = root.findViewById(R.id.pl);
        edPl= root.findViewById(R.id.edPl);
        edBarcode= root.findViewById(R.id.edBarcode);
        edBarcode1= root.findViewById(R.id.edBarcode1);
        edBarcode2= root.findViewById(R.id.edBarcode2);
        edBarcode3= root.findViewById(R.id.edBarcode3);
        edBarcode4= root.findViewById(R.id.edBarcode4);
        edBarcode5= root.findViewById(R.id.edBarcode5);
        edBarcode6= root.findViewById(R.id.edBarcode6);
        edProdCode = root.findViewById(R.id.edProdCode);
        //Not confirmed so commented
//        if(items_prints!=null && items_prints.BARCODE_SCAN_CHECK.length()>0
//        && items_prints.BARCODE_SCAN_CHECK.equalsIgnoreCase("Y") &&
//                items_prints.PALLET_SCAN_CHECK.length()>0
//                && items_prints.PALLET_SCAN_CHECK.equalsIgnoreCase("Y")
//        ){
//            edBarcode.setShowSoftInputOnFocus(false);
//        }
        edFillCode= root.findViewById(R.id.edFillCode);
        edFormula= root.findViewById(R.id.edFormula);
        edLotStatus= root.findViewById(R.id.edLotStatus);
        edLotNo= root.findViewById(R.id.edLotNo);
        edLotNo.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_ENTER){
                if(event.getAction() == KeyEvent.ACTION_DOWN){
                    return onLotNoKeyEvent();
                }else{
                    return false;
                }
            }else{
                return false;
            }
        });
        edLotNo.requestFocus();
//        edLotNo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus) {
//                    showToast("here onFocusChange");
//                }
//            }
//        });
       //edLotNo.requestFocus();
        edSkuName = root.findViewById(R.id.edSkuName);
        edCountry = root.findViewById(R.id.edCountry);
        edPickKey= root.findViewById(R.id.edPickKey);
        edExp= root.findViewById(R.id.edExp);
        edExp.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    onExpDtKeyEvent();
                }
            }
        });
        edManf= root.findViewById(R.id.edManf);
        edManf.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    onManFNoKeyEvent();
                }
            }
        });

        edManf.setOnClickListener(v -> onDateEntryClick(1));
        edExp.setOnClickListener(v -> onDateEntryClick(2));
        //edJobCode= root.findViewById(R.id.edJobCode);
//        edDocNo = root.findViewById(R.id.edScanCode);
//        edDocNo.setOnKeyListener((v, keyCode, event) -> {
//            if(keyCode == KeyEvent.KEYCODE_ENTER){
//                if(event.getAction() == KeyEvent.ACTION_DOWN){
//                    return onScanCodeKeyEvent();
//                }else{
//                    return false;
//                }
//            }else{
//                return false;
//            }
//        });

        edDisplayQty = root.findViewById(R.id.edDisplayQty);

        MaterialButton btPhoto = root.findViewById(R.id.btnBack);
        btPhoto.setOnClickListener(v -> {
            OnBackClick();
        });

        MaterialButton btnPrint = root.findViewById(R.id.btnNext);
        btnPrint.setOnClickListener(v -> OnNextClick());

        collapsePanel = root.findViewById(R.id.collapsePanel);
        //collapsePanel.setVisibility(View.GONE);

//        Button expand  = root.findViewById(R.id.expand);
        /*expand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // If the CardView is already expanded, set its visibility
                //  to gone and change the expand less icon to expand more.
                if (collapsePanel.getVisibility() == View.VISIBLE) {

                    // The transition of the hiddenView is carried out
                    //  by the TransitionManager class.
                    // Here we use an object of the AutoTransition
                    // Class to create a default transition.
                    TransitionManager.beginDelayedTransition(collapsePanel,
                            new AutoTransition());
                    collapsePanel.setVisibility(View.GONE);
                    //expand.setImageResource(R.drawable.ic_baseline_expand_more_24);
                }

                // If the CardView is not expanded, set its visibility
                // to visible and change the expand more icon to expand less.
                else {

                    TransitionManager.beginDelayedTransition(collapsePanel,
                            new AutoTransition());
                    collapsePanel.setVisibility(View.VISIBLE);
                    //arrow.setImageResource(R.drawable.ic_baseline_expand_less_24);
                }
            }
        });*/

//        expand.setOnClickListener(v -> {
//            if(isExpand){
//                collapse();
//            }else{
//                expand();
//            }
//        });

        datepanel  = root.findViewById(R.id.datepanel);
        //datepanel.setVisibility(View.GONE);



        scrollView = root.findViewById(R.id.sw_layout);

        punit = root.findViewById(R.id.punit);
        munit = root.findViewById(R.id.munit);
        lunit = root.findViewById(R.id.lunit);
        //UI Validation Starts here
        setDynamicUIValidations();
    }


    private void setDynamicUIValidations(){

        try {
            if (App.dynFuncs == null) {
                return;
            }
            StagingValidation stagingValidation = new StagingValidation();
            ValidRespSet validObject = null;

            //pallet_scan_check
//        if(items_prints.PALLET_SCAN_CHECK!=null && items_prints.PALLET_SCAN_CHECK.equals("Y")){
//            edPl.setVisibility(View.GONE);
//        }else{
//            edPl.setVisibility(View.VISIBLE);
//        }

            //FormulaAndFillCodeDisable
            validObject = stagingValidation.FormulaAndFillCodeDisable(App.dynFuncs, inboundHeader.PRIN_CODE);
            if (validObject.getErrCode().length() > 0 && validObject.getErrCode().equals("S")) {
                edFillCode.setEnabled(false);
                edFormula.setEnabled(false);
            }

            //getExpDate
            validObject = stagingValidation.getExpDate(App.dynFuncs, inboundHeader.PRIN_CODE, items_prints.MFG_DATE, items_prints.SHELF_LIFE,
                    Utils.ISSUE_DATE_FORMAT2, items_prints.EXPDATE);
            if (validObject.getErrCode().length() > 0 && validObject.getErrCode().equals("R")) {
                edExp.setText(validObject.getTargetValue());
            }

            //getManfDate
            validObject = stagingValidation.getManfDate(App.dynFuncs, inboundHeader.PRIN_CODE, items_prints.EXPDATE, items_prints.SHELF_LIFE,
                    Utils.ISSUE_DATE_FORMAT2, items_prints.MFG_DATE);
            if (validObject.getErrCode().length() > 0 && validObject.getErrCode().equals("R")) {
                edManf.setText(validObject.getTargetValue());
            }

        }catch (Exception e){
            e.printStackTrace();
            showAlert("setDynamicUIValidations - Error",e.getMessage());
        }

    }

    private boolean onLotNoKeyEvent(){

        try {
            if (App.dynFuncs == null) {
                return false;
            }
            if (items_prints == null) {
                return false;
            }

            //ValidateLotNoOnEntry
            ValidRespSet validObject = stagingValidation.ValidateLotNoOnEntry(App.dynFuncs, inboundHeader.PRIN_CODE, items_prints.LOT_NO, items_prints.CAL_BATCHNO);
            if (validObject.getErrCode().length() > 0 && validObject.getErrCode().equals("R")) {
                edLotNo.setText(validObject.getTargetValue());
            } else if (validObject.getErrCode().length() > 0 && validObject.getErrCode().equals("E")) {
                edLotNo.setError(validObject.getErrMSG());
                edLotNo.requestFocus();
                return false;
            }

        }catch(Exception e){
            showAlert("onLotNoKeyEvent - Error",e.getMessage());
            return false;
        }
        return true;
    }

    private boolean onManFNoKeyEvent(){
        try{
        if(App.dynFuncs==null){
            return false;
        }
        if(items_prints==null){
            return false;
        }

        //getExpDate
        ValidRespSet validObject = stagingValidation.getExpDate(App.dynFuncs, inboundHeader.PRIN_CODE,edManf.getText().toString(),items_prints.SHELF_LIFE,
                Utils.ISSUE_DATE_FORMAT2,edExp.getText().toString());
        if(validObject.getErrCode().length()>0 && validObject.getErrCode().equals("R")){
            edExp.setText(validObject.getTargetValue());
        }

    }catch(Exception e){
        showAlert("onManFNoKeyEvent - Error",e.getMessage());
        return false;
    }
        return true;
    }

    private boolean onExpDtKeyEvent(){
        try{
        if(App.dynFuncs==null){
            return false;
        }
        if(items_prints==null){
            return false;
        }

        //getManfDate
        ValidRespSet validObject = stagingValidation.getManfDate(App.dynFuncs, inboundHeader.PRIN_CODE,edExp.getText().toString(),items_prints.SHELF_LIFE,
                Utils.ISSUE_DATE_FORMAT2,edManf.getText().toString());
        if(validObject.getErrCode().length()>0 && validObject.getErrCode().equals("R")){
            edManf.setText(validObject.getTargetValue());
        }


    }catch(Exception e){
        showAlert("onManFNoKeyEvent - Error",e.getMessage());
        return false;
    }
        return true;
}



    private void expand()
    {
        isExpand = true;
        collapsePanel.setVisibility(View.VISIBLE);
        final int widthSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        final int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        collapsePanel.measure(widthSpec, heightSpec);
        ValueAnimator mAnimator = slideAnimator(0, collapsePanel.getMeasuredHeight());
        mAnimator.start();
    }

    private void collapse() {
        isExpand = false;
        int finalHeight = collapsePanel.getHeight();

        ValueAnimator mAnimator = slideAnimator(finalHeight, 0);

        mAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                //Height=0, but it set visibility to GONE
                collapsePanel.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }

        });
        mAnimator.start();
    }

    private ValueAnimator slideAnimator(int start, int end)
    {

        ValueAnimator animator = ValueAnimator.ofInt(start, end);

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                //Update Height
                int value = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = collapsePanel.getLayoutParams();
                layoutParams.height = value;
                collapsePanel.setLayoutParams(layoutParams);
            }
        });
        return animator;
    }


    private void setHeaderToolbar() {
        if (inboundHeader != null) {
            Toolbar toolbar = root.findViewById(R.id.toolbar);
            TextView info1 = root.findViewById(R.id.info1);
            info1.setText(App.currentUser.USER_ID);
            TextView info2 = root.findViewById(R.id.info2);
            info2.setText(inboundHeader.JOB_NO + " - " + inboundHeader.PRIN_NAME );
        }
    }

    private void onDateEntryClick(int ID) {
        Calendar currentDate = Calendar.getInstance();
        int curYear = currentDate.get(Calendar.YEAR);
        int curMonth = currentDate.get(Calendar.MONTH);
        int curDay = currentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(getContext(), (datePicker, cYear, cMonth, cDay) -> {
            String dateString = String.format(Locale.ENGLISH, "%d-%d-%d", cDay, (cMonth + 1), cYear);
            Log.d("dateString##",dateString);
            String selectedDate = Utils.getFormattedDate(dateString, Utils.ISSUE_DATE_FORMAT, Utils.ISSUE_DATE_FORMAT2);
            Log.d("selectedDate##",selectedDate);
            if (ID == 1) {
                edManf.setText(selectedDate);
                edManf.setError(null);
            } else if (ID == 2) {
                edExp.setText(selectedDate);
                edExp.setError(null);
            }
        }, curYear, curMonth, curDay);
        mDatePicker.setTitle("Select date");
        mDatePicker.show();
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        fillLogisticData(items_prints);

        FillLotStatus();
    }

/*    private boolean onDocNoKeyEvent() {
        DocNo = edDocNo.getText().toString();

        if (DocNo.isEmpty() || DocNo.trim().length() == 0) {
            showToast(String.format("%s required", getString(promptResource)));
            return false;
        }
        return true;
    }*/

    private void confirmDamageReport() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putString(AppConstants.SELECTED_ID, DocNo);
        bundle.putSerializable(AppConstants.SELECTED_CODE, "");
        bundle.putLong(AppConstants.PARENT_ID, HDR_ID);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.action_nav_damage_confirmation, bundle);
    }

    private void OnNextClick()  {
        int mQty = 0;
        int pQty = 0;
        int lQty = 0;
        if(Validate() && DynamicValidationSave() && !isFromEdit){

            showToast("reached heeereeee");

            mQty = Integer.parseInt(edMunit.getText().toString());
            pQty = Integer.parseInt(edPUnit.getText().toString());
            lQty = Integer.parseInt(edLUnit.getText().toString());

            logistic_label_sumbit = new LOGISTIC_LABEL_SUMBIT();
            logistic_label_sumbit.SRNO = items_prints.SRNO;
            logistic_label_sumbit.JOB_NO = inboundHeader.JOB_NO;
            logistic_label_sumbit.PRIN_NAME = inboundHeader.PRIN_NAME;
            if(edManf.getText().toString().length()>0){
                logistic_label_sumbit.MFG_DATE = Utils.getForamtedDate(edManf.getText().toString(), Utils.ISSUE_DATE_FORMAT2, Utils.DATEONLY_FORMAT);
            }else{
                logistic_label_sumbit.MFG_DATE = "";
            }
            if(edExp.getText().toString().length()>0){
                logistic_label_sumbit.EXP_DATE = Utils.getForamtedDate(edExp.getText().toString(), Utils.ISSUE_DATE_FORMAT2, Utils.DATEONLY_FORMAT);
            }else{
                logistic_label_sumbit.EXP_DATE = "";
            }
            logistic_label_sumbit.LOT_NO = edLotNo.getText().toString();
            logistic_label_sumbit.LOT_STATUS = lotStatusType.NEW_LOT_STATUS;
            logistic_label_sumbit.FORMULA_NO = edFormula.getText().toString();
            logistic_label_sumbit.FIL_CODE = edFillCode.getText().toString();
            logistic_label_sumbit.BARCODE = edBarcode.getText().toString();
            logistic_label_sumbit.INPUT_PQTY = String.valueOf(pQty);
            logistic_label_sumbit.INPUT_MQTY = String.valueOf(mQty);
            logistic_label_sumbit.INPUT_LQTY = String.valueOf(lQty);
            logistic_label_sumbit.PUOM = items_prints.PUOM;
            logistic_label_sumbit.MUOM = items_prints.MUOM;
            logistic_label_sumbit.LUOM = items_prints.LUOM;
            logistic_label_sumbit.LOC_CODE = items_prints.LOC;
            logistic_label_sumbit.SITE = items_prints.SITE;
            logistic_label_sumbit.UPDATE_FROM = "S";
            logistic_label_sumbit.PALLET_ID = edPl.getText().toString();
            logistic_label_sumbit.SCAN_PUT_LOC_CODE = "";
            logistic_label_sumbit.DESCRIPTION = items_prints.DESCRIPTION;
            logistic_label_sumbit.STG_QTY = items_prints.STG_QTY;
            logistic_label_sumbit.TOTAL_QTY = items_prints.TOTAL_QTY;
            logistic_label_sumbit.PUT_QTY =  items_prints.PUT_QTY;
            logistic_label_sumbit.UPPP = items_prints.UPPP;
            logistic_label_sumbit.LUPPP = items_prints.LUPPP;
            logistic_label_sumbit.Pqty = String.valueOf(pQty);
            logistic_label_sumbit.Mqty = String.valueOf(mQty);
            logistic_label_sumbit.Lqty = String.valueOf(lQty);
            logistic_label_sumbit.TOTAL_PO_QTY = items_prints.TOTAL_PO_QTY;


            //Old validation flow
            /* if(mQty< Integer.parseInt(items_prints.Mqty) ||
                    pQty< Integer.parseInt(items_prints.Pqty)||
                    lQty< Integer.parseInt(items_prints.Lqty) ){
                //OpenPrintLabel("123456");
                items_prints.Mqty = String.valueOf(mQty);
                items_prints.Pqty = String.valueOf(pQty);
                items_prints.Lqty= String.valueOf(lQty);
                logistic_label_sumbit.SRNO_ORIGINAL = logistic_label_sumbit.SRNO;

                final AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                alert.setTitle("Is Partial/Damage ?");
                alert.setPositiveButton("Yes", (dialog, whichButton) -> {
                    //uploadGACScanPartilData();
                });
                alert.setNegativeButton("No", (dialog, whichButton) -> {
                    //uploadGACScanData();
                });
                alert.show();

            }else{
               // uploadGACScanData();
            }*/

            //new validation as per ui dynamic validation
            int ActualStgQty = (pQty * items_prints.UPPP) + (mQty * items_prints.LUPPP) + lQty;
            if(ActualStgQty > 0 &&  (ActualStgQty < items_prints.QUANTITY)){
                boolean confirm = getValidationConfirmation(getContext(),getString(R.string.confirmation),
                        "Qty less than expected. Do you wish to confirm the remaining qty later?");
                if(confirm){
                    StagingMode = "P";
                    //showToast(StagingMode);
                    logistic_label_sumbit.SRNO_ORIGINAL = logistic_label_sumbit.SRNO;
                    uploadGACScanPartilData();
                }else{
                    StagingMode = "S";
                    //showToast(StagingMode);
                    uploadGACScanData();
                }
            }else{
                StagingMode = "S";
                //showToast(StagingMode+" outer");
                uploadGACScanData();
            }
        }else{
            showToast("isFromEdit");
        }

    }


    private void OpenPrintLabel(String newId) {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putString(AppConstants.SELECTED_ID, newId);
        bundle.putInt(AppConstants.SELECTED_CODE, -1);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, inboundHeader);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT_NEW, items_prints);
        bundle.putLong(AppConstants.SOURCE_ID, AppConstants.LOGISTIC_LABEL);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_label_print, bundle);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (resultCode != Activity.RESULT_OK) {
            Log.d("resultCode##",String.valueOf(requestCode));
            try {
                if (requestCode == AppConstants.PUT_ID) {
                    OpenLogisticLabel();
                    return;
                }
                OnConfirmationClick(AppConstants.PUT_AWAY_KEY, AppConstants.PUT_ID, -1,getString(R.string.put_away_conf),R.string.no,R.string.yes,logistic_label_sumbit);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return;
        }

        if (requestCode == AppConstants.DAMAGE_ID) {
            int yesNo = intent.hasExtra(AppConstants.SELECTED_ID) ?  intent.getIntExtra(AppConstants.SELECTED_ID,-1) : -1;
            if(yesNo==1){
                OpenDamageFragment();
            }
            return;
        }

        if (requestCode == AppConstants.PUT_ID) {
            int yesNo = intent.hasExtra(AppConstants.SELECTED_ID) ?  intent.getIntExtra(AppConstants.SELECTED_ID,-1) : -1;
            if(yesNo==1){
                OpenPutAwayFragment();
            }
            return;
        }
    }

    private void OpenLogisticLabel() {
        if(items_prints==null){
            showAlert("Warning","Data not found!");
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putString(AppConstants.SELECTED_ID, DocNo);
        //bundle.putSerializable(AppConstants.SELECTED_OBJECT,  items_prints);
        bundle.putLong(AppConstants.PARENT_ID, HDR_ID);
        ((MainActivity) requireActivity()).NavigateBackStack(null, false);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_inbound, bundle);

    }

    private boolean DynamicValidationSave(){
        try{
        AlertDialog alertDialog;
        ValidRespSet validObject;
        //ValidateLotNoOnSave
//        validObject = stagingValidation.ValidateLotNoOnSave(App.dynFuncs, inboundHeader.PRIN_CODE,items_prints.LOT_NO,items_prints.CAL_BATCHNO);
//        if(validObject.getErrCode().length()>0 && validObject.getErrCode().equals("R")){
//            edLotNo.setText(validObject.getTargetValue());
//        }else if(validObject.getErrCode().length()>0 && validObject.getErrCode().equals("E")){
//            edLotNo.setError(validObject.getErrMSG());
//            edLotNo.requestFocus();
//            return false;
//        }

        //ValidateLotstatusOnSave
        validObject = stagingValidation.ValidateLotstatusOnSave(App.dynFuncs, inboundHeader.PRIN_CODE,items_prints.LOT_STATUS,lotStatusType.NEW_LOT_STATUS);
        if(validObject.getErrCode().length()>0 && validObject.getErrCode().equals("E")){
            edLotStatus.setError(validObject.getErrMSG());
            edLotStatus.requestFocus();
            return false;
        }

        //ValidateLotNoEmptyOnSave
        validObject = stagingValidation.ValidateLotNoEmptyOnSave(App.dynFuncs, inboundHeader.PRIN_CODE,edLotNo.getText().toString(),items_prints.CAL_BATCHNO,items_prints.ISLOTREQUIRED);
        if(validObject.getErrCode().length()>0 && validObject.getErrCode().equals("E")){
            edLotNo.setError(validObject.getErrMSG());
            edLotNo.requestFocus();
            return false;
        }else if(validObject.getErrCode().length()>0 && validObject.getErrCode().equals("C")){
            return getValidationConfirmation(getContext(),getString(R.string.confirmation),validObject.getErrMSG());
        }

        //ValidateMfgDateEmptyOnSave
        validObject = stagingValidation.ValidateMfgDateEmptyOnSave(App.dynFuncs, inboundHeader.PRIN_CODE,edManf.getText().toString(),items_prints.CAL_BATCHNO,items_prints.ISMANUREQUIRED);
        if(validObject.getErrCode().length()>0 && validObject.getErrCode().equals("E")){
            edManf.setError(validObject.getErrMSG());
            edManf.requestFocus();
            return false;
        }else if(validObject.getErrCode().length()>0 && validObject.getErrCode().equals("C")){
            return getValidationConfirmation(getContext(),getString(R.string.confirmation),validObject.getErrMSG());
        }

        //ValidateExpDateEmptyOnSave
        validObject = stagingValidation.ValidateExpDateEmptyOnSave(App.dynFuncs, inboundHeader.PRIN_CODE,edExp.getText().toString(),items_prints.CAL_BATCHNO,items_prints.ISEXPDATEREQUIRED);
        if(validObject.getErrCode().length()>0 && validObject.getErrCode().equals("E")){
            edExp.setError(validObject.getErrMSG());
            edExp.requestFocus();
            return false;
        }else if(validObject.getErrCode().length()>0 && validObject.getErrCode().equals("C")){
            return getValidationConfirmation(getContext(),getString(R.string.confirmation),validObject.getErrMSG());
        }

        String mfd = edManf.getText().toString();
        String exp = edExp.getText().toString();
        String errMessage;
        if((mfd!=null && mfd.length()>0) &&  (exp!=null && exp.length()>0)){
            Date mfDate = Utils.convertStringToDate(mfd, Utils.ISSUE_DATE_FORMAT2);
            Date exDate = Utils.convertStringToDate(exp, Utils.ISSUE_DATE_FORMAT2);
            Date today = new Date();
            if (mfDate.compareTo(exDate)==0) {
                errMessage = "Manf date and Exp date cannot be same day";
                showToast(errMessage);
                edManf.setError(errMessage);
                return false;
            }

            if (mfDate.after(today)) {
                errMessage = "Invalid manufacture date, date in future";
                showToast(errMessage);
                edManf.setError(errMessage);
                return false;
            }

            Log.d("exDate##",String.valueOf(exDate));
            Log.d("today##",String.valueOf(today));

            if (exDate.before(today)) {
                errMessage = "Invalid expiry date, already expired";
                showToast(errMessage);
                edExp.setError(errMessage);
                return false;
            }
        }

        //        if(Integer.parseInt(mQty)>Integer.parseInt(items_prints.Mqty)){
//            errMessage = "Munit shouldn't be greater than "+items_prints.Mqty;
//            showToast(errMessage);
//            edMunit.setError(errMessage);
//            return false;
//        }

        String lQty = edLUnit.getText().toString();
        if (lQty.length() == 0) {
            errMessage = "Invalid LUnit";
            showToast(errMessage);
            edLUnit.setError(errMessage);
            return false;
        }

        String pQty = edPUnit.getText().toString();
        if (pQty.length() == 0) {
            errMessage = "Invalid PUnit";
            showToast(errMessage);
            edPUnit.setError(errMessage);
            return false;
        }


        String mQty = edMunit.getText().toString();
        if (mQty.length() == 0) {
            errMessage = "Invalid Munit";
            showToast(errMessage);
            edMunit.setError(errMessage);
            return false;
        }

        int MQty = Integer.parseInt(edMunit.getText().toString());
        int PQty = Integer.parseInt(edPUnit.getText().toString());
        int LQty = Integer.parseInt(edLUnit.getText().toString());

        int ActualStgQty = (PQty * items_prints.UPPP) + (MQty * items_prints.LUPPP) + LQty;

        if(ActualStgQty>items_prints.QUANTITY){
            showAlert("Warning","Qty is greater than expected. Pls enter valid Qty");
            edPUnit.requestFocus();
            return false;
        }

        if((items_prints.PALLET_SCAN_CHECK!=null && items_prints.PALLET_SCAN_CHECK.equals("Y")) &&
                (items_prints.JOBCLASS!=null && !items_prints.JOBCLASS.equals("003")) &&
                !(edPl.getText().toString()!=null && edPl.getText().toString().length()>0) &&
                ActualStgQty==0
        ){
            showAlert("Warning","Please fill up Pallet ID/SSCC before proceeding, principal \" + Principal Name + \" requires Pallet ID/SSCC validation");
            edPl.requestFocus();
            return false;
        }

        //ValidatePalletDifSave
        validObject = stagingValidation.ValidatePalletDifSave(App.dynFuncs, inboundHeader.PRIN_CODE,items_prints.PALLET_ID,edPl.getText().toString());
        if(validObject.getErrCode().length()>0 && validObject.getErrCode().equals("E")){
            edPl.setError(validObject.getErrMSG());
            edPl.requestFocus();
            return false;
        }else if(validObject.getErrCode().length()>0 && validObject.getErrCode().equals("C")){
            edPl.requestFocus();
            return getValidationConfirmation(getContext(),getString(R.string.confirmation),validObject.getErrMSG());
        }

        //BARCODE_SCAN_CHECK
            Log.d("len##",String.valueOf(edBarcode.getText().toString().trim().length()));
        if((items_prints.BARCODE_SCAN_CHECK!=null && items_prints.BARCODE_SCAN_CHECK.equals("Y")) &&
                (edBarcode.getText().toString().trim().length()<=0)
        ){
            showAlert("Warning","Please fill up Barcode before proceeding, product " + items_prints.DESCRIPTION + " requires product barcode validation");
            edBarcode.requestFocus();
            return false;
        }
        //BARCODE_SCAN_CHECK
        if((items_prints.BARCODE_SCAN_CHECK!=null && items_prints.BARCODE_SCAN_CHECK.equals("Y")) &&
                !(edBarcode.getText().toString().trim().equalsIgnoreCase(items_prints.BARCODE) ||
                        edBarcode.getText().toString().trim().equalsIgnoreCase(items_prints.BARCODE2) ||
                        edBarcode.getText().toString().trim().equalsIgnoreCase(items_prints.BARCODE3) ||
                        edBarcode.getText().toString().trim().equalsIgnoreCase(items_prints.BARCODE4) ||
                        edBarcode.getText().toString().trim().equalsIgnoreCase(items_prints.BARCODE5) ||
                        edBarcode.getText().toString().trim().equalsIgnoreCase(items_prints.BARCODE6)
                        )

        ){
            showAlert("Warning","Invalid Product Barcode");
            edBarcode.requestFocus();
            return false;
        }

    }catch(Exception e){
        showAlert("DynamicValidationSave - Error",e.getMessage());
        return false;
    }
        return true;
}


    public boolean getValidationConfirmation(Context context,String title,String message) {
        mResult = false;
        final Handler handler = new Handler(Looper.getMainLooper()) {

            @Override
            public void handleMessage(Message mesg) {
                throw new RuntimeException();
            }
        };

        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(title);
        alert.setMessage(message);
        alert.setIcon(R.drawable.logo);
        alert.setPositiveButton("Yes", new
                DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mResult= true;
                        handler.sendMessage(handler.obtainMessage());
                    }
                });
        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                mResult= false;
                handler.sendMessage(handler.obtainMessage());
            }
        });
        alert.show();

        try {
            Looper.loop();
        } catch (RuntimeException e) {
        }

        return mResult;
    }


    public void confrimDynamicValidation(Context context, String title, String message,
                                         final DialogSingleButtonListener dialogSingleButtonListener) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setTitle(getString(R.string.confirmation));
        dialogBuilder.setMessage(message);
        dialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                    dialogSingleButtonListener.onButtonClicked(dialog);
            }
        });
        dialogBuilder.setPositiveButton("Yes", (dialog, which) -> {
            dialog.dismiss();
                    });
        AlertDialog dialog = dialogBuilder.create();
        dialog.show();
    }

    public interface DialogSingleButtonListener {
        public abstract boolean onButtonClicked(DialogInterface dialog);
    }

    private boolean Validate()  {
        String errMessage;
        if (items_prints.SRNO.length() == 0) {
            errMessage = String.format("%s required", "GACScanId");
            showToast(errMessage);
            edDocNo.setError(errMessage);
            return false;
        }




        if(lotStatusType==null){
            errMessage = "Invalid Lot Status";
            showToast(errMessage);
            edLotStatus.setError(errMessage);
            return false;
        }



        return true;

    }




    private void OnBackClick() {
        getActivity().onBackPressed();
    }



    private void OpenDamageFragment() {
        if(items_prints==null){
            showAlert("Warning","Data not found!");
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putString(AppConstants.SELECTED_ID, DocNo);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, logistic_label_sumbit);
        bundle.putLong(AppConstants.PARENT_ID, HDR_ID);
        ((MainActivity) requireActivity()).NavigateBackStack(null, false);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_damage_confirmation, bundle);

    }

    private void OpenPutAwayFragment() {
        if(items_prints==null){
            showAlert("Warning","Data not found!");
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        bundle.putString(AppConstants.SELECTED_ID, DocNo);
        bundle.putSerializable(AppConstants.SELECTED_OBJECT,  logistic_label_sumbit);
        bundle.putLong(AppConstants.PARENT_ID, HDR_ID);
        ((MainActivity) requireActivity()).NavigateBackStack(null, false);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_put_away, bundle);
    }



    private void fillLogisticData(LOGISTIC_LABEL_SCAN logistic_label_scan){
        //Log.d("DocNo##",DocNo);

        try {

            punit.setHint(punit.getHint()+":"+logistic_label_scan.PUOM);
            munit.setHint(munit.getHint()+":"+logistic_label_scan.MUOM);
            lunit.setHint(lunit.getHint()+":"+logistic_label_scan.LUOM);

            edDocNo.setText(getString(R.string.gac_scan_id)+" : "+logistic_label_scan.SRNO);
            edLUnit.setText(String.valueOf(logistic_label_scan.Lqty));
            edMunit.setText(String.valueOf(logistic_label_scan.Mqty));
            edPUnit.setText(String.valueOf(logistic_label_scan.Pqty));
            if(logistic_label_scan.Lqty==logistic_label_scan.Mqty){
                edMunit.setEnabled(false);
            }
            //edBarcode.setText("");
            edBarcode1.setText(logistic_label_scan.BARCODE);
            edBarcode2.setText(logistic_label_scan.BARCODE2);
            edBarcode3.setText(logistic_label_scan.BARCODE3);
            edBarcode4.setText(logistic_label_scan.BARCODE4);
            edBarcode5.setText(logistic_label_scan.BARCODE5);
            edBarcode6.setText(logistic_label_scan.BARCODE6);

            edFillCode.setText(logistic_label_scan.FIL_CODE);
            edFormula.setText(logistic_label_scan.FORMULA_NO);
            edPl.setText(logistic_label_scan.PALLET_ID);
            //pallet_scan_check
            if(logistic_label_scan.PALLET_SCAN_CHECK!=null && logistic_label_scan.PALLET_SCAN_CHECK.equals("Y")){
                edPl.setVisibility(View.VISIBLE);
                pl.setVisibility(View.VISIBLE);
            }else{
                edPl.setVisibility(View.GONE);
                pl.setVisibility(View.GONE);
            }
            edLotNo.setText(logistic_label_scan.LOT_NO);
            //logistic_label_scan.LOT_STATUS = "I";
            if(logistic_label_scan.LOT_STATUS!=null && logistic_label_scan.LOT_STATUS.length()>0){

                LOT_STATUS_LIST lotStatusList  = App.getDatabaseClient().getAppDatabase().genericDao().getLotStatusFromKey(logistic_label_scan.LOT_STATUS);
                if(lotStatusList!=null){
                    edLotStatus.setText(lotStatusList.DESCRIPTION);
                    lotStatusType = lotStatusList;
                }
            }
            edPickKey.setText(logistic_label_scan.PACK_KEY);
            edSkuName.setText(logistic_label_scan.DESCRIPTION);
            edCountry.setText(logistic_label_scan.COUNTRYNAME);
            edProdCode.setText(getString(R.string.sku)+" : "+logistic_label_scan.SKU);
            edDisplayQty.setText(logistic_label_scan.STG_QTY+"/"+logistic_label_scan.TOTAL_QTY);
            if (logistic_label_scan.MFG_DATE != null && logistic_label_scan.MFG_DATE.trim().length()>0) {
                edManf.setText(Utils.getForamtedDate(logistic_label_scan.MFG_DATE, Utils.ISSUE_DATE_FORMAT3, Utils.ISSUE_DATE_FORMAT2));
            }
            if (logistic_label_scan.EXPDATE != null&& logistic_label_scan.EXPDATE.trim().length()>0) {
                edExp.setText(Utils.getForamtedDate(logistic_label_scan.EXPDATE, Utils.ISSUE_DATE_FORMAT3, Utils.ISSUE_DATE_FORMAT2));
            }

            //gs1 modification
            if(gs1Map!=null && gs1Map.size()>0){
                if((gs1Map.containsKey(AI_LOT_NUMBER) )&& (gs1Map.get(AI_LOT_NUMBER)!=null && gs1Map.get(AI_LOT_NUMBER).length()>0)){
                    edLotNo.setText(gs1Map.get(AI_LOT_NUMBER));
                }
                if((gs1Map.containsKey(AI_QTY) )&& (gs1Map.get(AI_QTY)!=null && gs1Map.get(AI_QTY).length()>0)){
                    edPUnit.setText(gs1Map.get(AI_QTY));
                }
                if((gs1Map.containsKey(AI_DATE_EXPIRATION) )&& (gs1Map.get(AI_DATE_EXPIRATION)!=null && gs1Map.get(AI_DATE_EXPIRATION).length()>0)){
                    edExp.setText(Utils.getForamtedDate(gs1Map.get(AI_DATE_EXPIRATION), GS_DATE_NUM_FORMAT, Utils.ISSUE_DATE_FORMAT2));
                }

            }

        }catch(Exception e){
            showAlert("Error",e.getMessage());
            e.printStackTrace();
        }
    }

    private void resetUI(){
        edDocNo.setText("");
        edLUnit.setText("");
        edMunit.setText("");
        edPUnit.setText("");
        edPl.setText("");
        edBarcode.setText("");
        edFillCode.setText("");
        edFormula.setText("");
        edLotStatus.setText("");
        edLotNo.setText("");
        //edLocCode.setText("");
        edExp.setText("");
        edManf.setText("");
        lotStatusType = new LOT_STATUS_LIST();
        //edJobCode.setText("");
        //items_prints = null;
    }


    private void uploadGACScanPartilData() {
        if(logistic_label_sumbit==null){
            return;
        }
        showToast(getString(R.string.send_data_to_server));
        JsonObject requestObject = ServiceUtils.LogisticLabel.getLOGISTIC_LABEL_SUMBIT_PARTIAL(logistic_label_sumbit);
        showProgress(false);
        App.getNetworkClient().getAPIService().submitWithLinesData(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericSubmissionResponse, Response response) {
                dismissProgress();
                if (!genericSubmissionResponse.getErrCode().equals("E")) {
                    if (!genericSubmissionResponse.getErrCode().equals("E")) {
                        String msg = genericSubmissionResponse.getErrMessage();

                        resetUI();
                        //showAlert("Success",msg);

                        if(msg==null && msg.length()==0){
                            showAlert("Error", "No ID received.");
                            return;
                        }
                        DownloadLogistic_Label(msg);


                        //items_prints =null;
                        //logistic_label_sumbit = null;
//                        if (msg.length > 1) {
//                            ReceiptNo = msg[2];
//                        }
//                        try {
//                            OnConfirmationClick(AppConstants.DAMAGE_KEY, AppConstants.DAMAGE_ID, -1,getString(R.string.damage_conf),R.string.no,R.string.yes,logistic_label_sumbit);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
                    } else {
                        showAlert("Error",genericSubmissionResponse.getErrMessage());
                        //showToast(genericSubmissionResponse.getErrMessage());
                    }
                } else {
                    Log.d(App.TAG, genericSubmissionResponse.getErrMessage());
                    showAlert("Error",genericSubmissionResponse.getErrMessage());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(App.TAG, error.getMessage());
                showToast(error.getMessage());
                dismissProgress();
            }
        });
    }

    private void DownloadLogistic_Label(String newLabel) {
        if (newLabel.length() <= 0 || inboundHeader == null || items_prints == null)  {
            return;
        }

        JsonObject requestObject = ServiceUtils.LogisticLabel.getLogisticLabel("labelNo",newLabel,items_prints.SKU
                ,inboundHeader.PRIN_CODE,"P",inboundHeader.JOB_NO,"");

        showProgress(false);
        App.getNetworkClient().getAPIService().getRetGeneric(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (!genericRetResponse.getErrCode().equals("E")) {
                    String xmlDoc = genericRetResponse.getJsonString();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showAlert("Error", "No data received.");
                        return;
                    }
                    try {
                        LOGISTIC_LABEL_SCAN[] tempLogisticLabelScan = new Gson().fromJson(xmlDoc, LOGISTIC_LABEL_SCAN[].class);
                        if(tempLogisticLabelScan==null){
                            showAlert("Error", "No data received.");
                            return;
                        }
                        items_prints = tempLogisticLabelScan[0];
                        if(items_prints.SRNO==null){
                            showAlert("Error", "No Serial No Found!.");
                            return;
                        }
                        OpenPrintLabel(newLabel);

                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                        showAlert("Error", e.getMessage());
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showAlert("Error", msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showAlert("Error", msg);
            }
        });
    }

    private void uploadGACScanData() {
        if(logistic_label_sumbit==null){
            return;
        }
        showToast(getString(R.string.send_data_to_server));
        JsonObject requestObject = ServiceUtils.LogisticLabel.getLOGISTIC_LABEL_SUMBIT_FULL(logistic_label_sumbit,"N");
        showProgress(false);
        App.getNetworkClient().getAPIService().submitWithLinesData(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericSubmissionResponse, Response response) {
                dismissProgress();
                if (genericSubmissionResponse.getErrCode().equals("S")) {
                    if (genericSubmissionResponse.getErrCode().equals("S")) {
                        String msg = genericSubmissionResponse.getErrMessage();
                        resetUI();
                        showAlert("Success",msg);
                        if(sourceId == AppConstants.DIRECT_STAG){
                            NavigateToStagging(R.id.nav_stagging);
                        }else {
                            Bundle bundle = new Bundle();
                            bundle.putInt(AppConstants.MODULE_ID, ModuleID);
                            bundle.putString(AppConstants.SELECTED_ID, DocNo);
                            bundle.putSerializable(AppConstants.SELECTED_OBJECT_NEW, logistic_label_sumbit);
                            bundle.putSerializable(AppConstants.SELECTED_OBJECT, items_prints);
                            bundle.putSerializable(AppConstants.SELECTED_OBJECT_NEW2, inboundHeader);
                            bundle.putLong(AppConstants.PARENT_ID, HDR_ID);
                            ((MainActivity) requireActivity()).NavigateFullBackStack();
                            ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_menu_fragment, bundle);
                        }
                        items_prints =null;
                        logistic_label_sumbit = null;
                    } else {
                        showAlert("Error",genericSubmissionResponse.getErrMessage());
                        //showToast(genericSubmissionResponse.getErrMessage());
                    }
                } else {
                    Log.d(App.TAG, genericSubmissionResponse.getErrMessage());
                    showAlert("Error",genericSubmissionResponse.getErrMessage());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(App.TAG, error.getMessage());
                showToast(error.getMessage());
                dismissProgress();
            }
        });
    }

    private void NavigateToStagging(int fragmaneID) {
        Bundle bundle = new Bundle();
        //bundle.putString(AppConstants.SELECTED_ID, "P");
        ((MainActivity) requireActivity()).NavigateFullBackStack();
        ((MainActivity) requireActivity()).NavigateToFragment(fragmaneID, bundle);
    }

    private void generateNewGACIds(int lastId,int count){
        String keyType = "I";
        for(int i=0;i<count;i++){
            Log.d("generateNewGACIds##",keyType+(lastId-i));
            //OpenPrintLabel();
        }
    }

    private void FillLotStatus() {
        List<LOT_STATUS_LIST> lot_status_lists = App.getDatabaseClient().getAppDatabase().genericDao().getLotStatus();
        if(lot_status_lists!=null&& lot_status_lists.size()>0){
            lotStatusArrayAdapter = new ArrayAdapter<>(getContext(), R.layout.dropdown_menu_popup_item, lot_status_lists);
            edLotStatus.setAdapter(lotStatusArrayAdapter);
            edLotStatus.setThreshold(100);
            edLotStatus.setOnItemClickListener((parent, view, position, id) -> {
                if (position >= 0) {
                    lotStatusType = lotStatusArrayAdapter.getItem(position);
                } else {
                    lotStatusType = null;
                }

            });
        }


    }

    @Override
    public void onCreateOptionsMenu(@NonNull @NotNull Menu menu, @NonNull @NotNull MenuInflater inflater) {
        menu.clear();

        // Add the new menu items
        inflater.inflate(R.menu.staging_menu, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.actionImages:
                openImages();
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openImages(){
        // String jobNo = edJobNo.getText().toString();
        if (items_prints==null) {
            showToast("Enter Job Id");
            return;
        }

        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.MODULE_ID, ModuleID);
        //bundle.putString(AppConstants.SELECTED_ID, inbound_header.JOB_NO);
        //bundle.putSerializable(AppConstants.SELECTED_OBJECT, inbound_header);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_confirmation_list_tablayout, bundle);

    }

}
