package com.dcode.gacinventory.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.gacinventory.data.model.MENU;
import com.dcode.gacinventory.data.model.SECTION;
import com.example.gacinventory.R;

import java.util.ArrayList;
import java.util.List;

public class ObjectsSectionAdapter
        extends RecyclerView.Adapter<ObjectsSectionAdapter.RecyclerViewHolder>
        implements Filterable {

    private Context context;
    private View.OnClickListener shortClickListener;
    private List<SECTION> dataList;
    private ArrayList<SECTION> dataListFull;

    private Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<SECTION> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(dataListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (SECTION item : dataListFull) {
                    if (item.SECTION_MENU_NAME.toLowerCase().contains(filterPattern.toLowerCase()) ||
                            item.SECTION_MENU_DESC.toLowerCase().contains(filterPattern.toLowerCase())) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            dataList.clear();
            dataList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public ObjectsSectionAdapter(Context context, List<SECTION> objects, View.OnClickListener shortClickListener) {
        this.context = context;
        this.shortClickListener = shortClickListener;
        this.dataList = objects;
        this.dataListFull = new ArrayList<>(objects);
        setHasStableIds(true);
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_card_menu, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        final SECTION objectItem = this.dataList.get(position);

        holder.menuText.setText(objectItem.SECTION_MENU_NAME);
        holder.menuDesc.setText(objectItem.SECTION_MENU_DESC);

        if(objectItem.SECTION_ICON>0){
            holder.icon.setImageDrawable(context.getResources().getDrawable(objectItem.SECTION_ICON));
        }

        holder.itemView.setTag(objectItem);
        holder.itemView.setOnClickListener(shortClickListener);

        //Random rnd = new Random();
        //int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));

//        int color= ((int)(Math.random()*16777215)) | (0xFF << 24);
//        LayerDrawable layerDrawable = (LayerDrawable) context.getDrawable(R.drawable.border);
//        GradientDrawable gradientDrawable = (GradientDrawable) layerDrawable
//                .findDrawableByLayerId(R.id.borcol);
//        gradientDrawable.setColor(color);


    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void addItems(List<SECTION> list) {
        this.dataList = list;
        this.dataListFull = new ArrayList<>(list);

        notifyDataSetChanged();
    }

    public List<SECTION> getItems() {
        return this.dataList;
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView menuText;
        TextView menuDesc;
        LinearLayout borderPanel;
        ImageView icon;

        RecyclerViewHolder(View view) {
            super(view);
            menuText = view.findViewById(R.id.menuText);
            menuDesc = view.findViewById(R.id.menuDesc);
            icon = view.findViewById(R.id.appImage);
            //borderPanel = view.findViewById(R.id.borderPanel);
        }
    }

}
