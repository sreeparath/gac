package com.dcode.gacinventory.ui.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.gacinventory.data.model.CONFIRMATION_LIST;
import com.dcode.gacinventory.data.model.JOB_CLOSURE_INFO;
import com.dcode.gacinventory.data.model.LABEL_ITEM_LIST;
import com.dcode.gacinventory.ui.view.GACScanFragment;
import com.example.gacinventory.R;
import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.List;

public class ClosureAdapter
        extends RecyclerView.Adapter<ClosureAdapter.RecyclerViewHolder> implements Filterable {

    private JOB_CLOSURE_INFO jobClosureInfo;
    private List<JOB_CLOSURE_INFO> jobClosureInfoList;
    private List<JOB_CLOSURE_INFO> jobClosureInfoListTemp;
    private View.OnClickListener shortClickListener;
    private boolean isSelectedAll = false;

    public Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<JOB_CLOSURE_INFO> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(jobClosureInfoListTemp);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (JOB_CLOSURE_INFO item : jobClosureInfoListTemp) {
                    if (item.JOB_NO.contains(filterPattern) ||  item.PRIN_CODE.contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }

            }
            Log.d("filteredList##",String.valueOf(filteredList.size()));

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            jobClosureInfoList = new ArrayList<>();
            if (results != null && results.values != null) {
                jobClosureInfoList.addAll((List) results.values);
            }
            notifyDataSetChanged();
        }
    };


    public ClosureAdapter(List<JOB_CLOSURE_INFO> itemsPrints,
                          View.OnClickListener shortClickListener) {
        this.jobClosureInfoList = itemsPrints;
        this.shortClickListener = shortClickListener;
        setHasStableIds(true);
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_closure_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        final JOB_CLOSURE_INFO items_print = jobClosureInfoList.get(position);

        holder.tvDesc.setText(items_print.JOB_NO);
        holder.tvSrNo.setText(items_print.PRIN_CODE);
        holder.tvSku.setText(String.valueOf(items_print.PRIN_NAME));
        holder.tvPrnCode.setText(String.valueOf(items_print.JOB_DATE));
        //holder.tvtStgQty.setText("Pending");
        //holder.chkSelect.setChecked(items_print.IS_SELECT ? true : false);
        holder.chkSelect.setChecked(items_print.IS_SELECT ? true : false);
        if(items_print.UPDATE_STATUS == 1){
            holder.tvtStgQty.setText("Closed");
            holder.tvtStgQty.setBackgroundColor(Color.GREEN);
        }else if(items_print.UPDATE_STATUS == 0){
            holder.tvtStgQty.setText("Pending");
            holder.tvtStgQty.setBackgroundColor(Color.BLUE);
        }else if(items_print.UPDATE_STATUS == 2){
            holder.tvtStgQty.setText("Error");
            //holder.tvtStgQty.setTextColor(Color.WHITE);
            holder.tvtStgQty.setBackgroundColor(Color.RED);
        }
        holder.tvtStgQty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    showAlert(view.getContext(), holder.tvtStgQty.getText().toString(),items_print.STATUS_MSG);
                } catch (ClassCastException e) {
                    // do something
                }
            }
        });
        //holder.tvtStgQty.setText(items_print.UPDATE_STATUS ? true : false);
        holder.chkSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    items_print.IS_SELECT = true;
                }else{
                    items_print.IS_SELECT = false;
                }
           }
        });




       // holder.tvTotQty.setText(String.valueOf(items_print.TOTAL_QTY));
        //holder.tvtStgQty.setText(String.valueOf(items_print.STG_QTY));
        //holder.itemView.setTag(items_print);
//        holder.btnShort.setTag(items_print);
//        holder.btnShort.setOnClickListener(shortClickListener);
//        if(items_print.IS_STAGED.equalsIgnoreCase("Y")){
//            holder.btnShort.setVisibility(View.GONE);
//        }else{
//            holder.btnShort.setVisibility(View.VISIBLE);
//        }

//        holder.btnShort.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                try {
//                    items_print.IS_CONFIRMED = "1";
//                    notifyDataSetChanged();
//                } catch (ClassCastException e) {
//                    // do something
//                }
//            }
//        });


        //in some cases, it will prevent unwanted situations
        //holder.chkSelect.setOnCheckedChangeListener(null);

        //if true, your checkbox will be selected, else unselected
        //items_print.IS_SELECT = Boolean.TRUE;
        //holder.chkSelect.setChecked(true);

//        if (!isSelectedAll) {
//            holder.chkSelect.setChecked(false);
//        }
//        else{
//            holder.chkSelect.setChecked(true);
//        }

//        holder.chkSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                //set your object's last status
////                if (isChecked){
////                    holder.chkSelect.setChecked(false);
////                    items_print.IS_SELECT = false;
////                }else{
////                    holder.chkSelect.setChecked(true);
////                    items_print.IS_SELECT = true;
////                }
//                if(items_print.IS_SELECT){
//                    items_print.IS_SELECT = false ;
//                }else{
//                    items_print.IS_SELECT = true;
//                }
//
//            }
//        });
        holder.itemView.setTag(items_print);
        //holder.itemView.setOnClickListener(shortClickListener);
        jobClosureInfo = items_print;
    }

    public void showAlert(Context context, String title, String message) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setTitle(title);
        dialogBuilder.setMessage(message);
        dialogBuilder.setPositiveButton("Yes", (dialog, which) -> {
            dialog.dismiss();
        });
        AlertDialog dialog = dialogBuilder.create();
        dialog.show();
    }

    public void selectAll(boolean select){
        Log.e("onClickSelectAll",String.valueOf(select));
        isSelectedAll=select;
        notifyDataSetChanged();
    }

    public void selectAllItem(boolean isSelectedAll) {
        try {

            if (jobClosureInfoList != null) {
                for (int i = 0; i < jobClosureInfoList.size(); i++) {
                    jobClosureInfoList.get(i).IS_SELECT = isSelectedAll;

                   Log.d("isSelectedAll",String.valueOf(jobClosureInfoList.get(i).IS_SELECT));
                }
            }
            notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        if (jobClosureInfoList == null) {
            return 0;
        }
        return jobClosureInfoList.size();
    }

    public ArrayList<JOB_CLOSURE_INFO> getSelected() {
        ArrayList<JOB_CLOSURE_INFO> selected = new ArrayList<>();
        for (int i = 0; i < jobClosureInfoList.size(); i++) {
            //Log.d("getSelected##",String.valueOf(jobClosureInfoList.get(i).IS_SELECT));
            if (jobClosureInfoList.get(i).IS_SELECT) {
                selected.add(jobClosureInfoList.get(i));
            }
        }
        return selected;
    }

    public void updateStatus(String jobNo,int status,String errMsg) {
        ArrayList<JOB_CLOSURE_INFO> selected = new ArrayList<>();
        for (int i = 0; i < jobClosureInfoList.size(); i++) {
            //Log.d("getSelected##",String.valueOf(jobClosureInfoList.get(i).IS_SELECT));
            if (jobClosureInfoList.get(i).JOB_NO.equalsIgnoreCase(jobNo)) {
                jobClosureInfoList.get(i).UPDATE_STATUS = status;
                jobClosureInfoList.get(i).STATUS_MSG = errMsg;
            }
        }
        notifyDataSetChanged();
    }


    public List<JOB_CLOSURE_INFO> getItems() {
        return this.jobClosureInfoList;
    }

    public void addItems(List<JOB_CLOSURE_INFO> detList) {
        this.jobClosureInfoList = detList;
        this.jobClosureInfoListTemp = detList;
        notifyDataSetChanged();
    }

    public void addItems(JOB_CLOSURE_INFO items_print) {
        this.jobClosureInfoList.add(items_print);
        notifyDataSetChanged();
    }

    public JOB_CLOSURE_INFO getItemByPosition(int position) {
        return this.jobClosureInfoList.get(position);
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView tvDesc;
        private TextView tvSrNo;
        private TextView tvSku;
        private TextView tvPrnCode;
        private TextView tvTotQty;
        private TextView tvtStgQty;
        private CheckBox chkSelect;
        //private MaterialButton btnShort;

        RecyclerViewHolder(View view) {
            super(view);
            tvDesc = view.findViewById(R.id.tvDesc);
            tvSrNo = view.findViewById(R.id.tvSrNo);
            tvSku = view.findViewById(R.id.tvSku);
            tvPrnCode = view.findViewById(R.id.tvPrnCode);
            tvTotQty = view.findViewById(R.id.tvTotQty);
            tvtStgQty = view.findViewById(R.id.tvtStgQty);
            //btnShort = view.findViewById(R.id.btnShort);
            chkSelect= view.findViewById(R.id.chkSelect);
        }
    }

    public interface ConfirmAdapterListener {

        void confirmShortClick(View v, int position);
    }
}
