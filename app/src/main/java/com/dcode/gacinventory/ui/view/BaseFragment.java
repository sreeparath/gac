package com.dcode.gacinventory.ui.view;

import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.dcode.gacinventory.common.AppConstants;
import com.dcode.gacinventory.data.model.BARCODE_FRMT;
import com.dcode.gacinventory.data.model.INBOUND_HEADER;
import com.dcode.gacinventory.data.model.LOGISTIC_LABEL_SUMBIT;
import com.dcode.gacinventory.data.model.PRN_CODE_LIST;
import com.dcode.gacinventory.ui.dialog.CustomProgressDialog;
import com.example.gacinventory.R;

import org.jetbrains.annotations.NotNull;


public abstract class BaseFragment extends Fragment{
    protected int ModuleID;
    //    protected int selectedID;
//    protected String selectedCode;
//    protected String selectedName;
    private CustomProgressDialog progressDialog;
    private AlertDialog alertDialog;
    private boolean confirmation = false;



    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull @NotNull Menu menu, @NonNull @NotNull MenuInflater inflater) {
        menu.clear();

        // Add the new menu items
        inflater.inflate(R.menu.main, menu);
        menu.setGroupVisible(R.id.menu_group,false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                OpenHome();
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }



    private void OpenHome() {
        Bundle bundle = new Bundle();
        ((MainActivity) requireActivity()).NavigateFullBackStack();
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_home, bundle);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);



//        if (resultCode != Activity.RESULT_OK) return;
//
//        selectedID = intent.hasExtra(AppConstants.SELECTED_ID) ? intent.getIntExtra(AppConstants.SELECTED_ID, -1) : -1;
//        selectedCode = intent.hasExtra(AppConstants.SELECTED_CODE) ? intent.getStringExtra(AppConstants.SELECTED_CODE) : "";
//        selectedName = intent.hasExtra(AppConstants.SELECTED_NAME) ? intent.getStringExtra(AppConstants.SELECTED_NAME) : "";

    }

    protected void showToast(final String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
        //shortVibration();
    }

    protected void showToastLong(final String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    protected void showAlert(final String title, final String message) {
        if (alertDialog != null && alertDialog.isShowing())
            return;

//        LayoutInflater factory = LayoutInflater.from(getContext());
//        final View view = factory.inflate(R.layout.layout_app_validation, null);
//        final EditText edGuid = (EditText) view.findViewById(R.id.edDeviceID);
//        final EditText edEnGuid = (EditText) view.findViewById(R.id.edLicense);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        alertDialog = builder
                .setTitle(title)
                //.setView(view)
                .setIcon(R.drawable.logo)
                .setMessage(message)
                .setPositiveButton("OK", (dialog, which) -> {
                })
                .create();
        alertDialog.show();
        shortVibration();
        ringtone();
    }

//    protected void OnLookupClick(int master_id, int parent_id, @Nullable int[] array) {
//        Intent intent = new Intent(getActivity(), searchFragment.class);
//        intent.putExtra(AppConstants.MASTER_ID, master_id);
//        intent.putExtra(AppConstants.PARENT_ID, parent_id);
//
//        if (array != null) {
//            intent.putExtra(AppConstants.FILTER_KEY_INT_ARRAY, array);
//        }
//        startActivityForResult(intent, master_id);
//    }

    protected void showProgress(boolean isCancelable) {
        if (progressDialog != null && progressDialog.isShowing())
            return;

        progressDialog = new CustomProgressDialog(getContext(), isCancelable);
        progressDialog.show();
    }

    public void dismissProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
    }


    protected void shortVibration() {
        Vibrator vibe = (Vibrator) this.getActivity().getSystemService(Context.VIBRATOR_SERVICE);
        if (vibe != null && vibe.hasVibrator()) vibe.vibrate(10);
    }


    protected void OnConfirmationClick(String title, int master_id, int parent_id, String message, int left, int right, LOGISTIC_LABEL_SUMBIT logistic_label_sumbit) {
        Intent intent = new Intent(getActivity(), ConfirmationActivity.class);
        intent.putExtra(AppConstants.TITLE, title);
        intent.putExtra(AppConstants.MESSAGE, message);
        intent.putExtra(AppConstants.LEFT, left);
        intent.putExtra(AppConstants.RIGHT, right);
        intent.putExtra(AppConstants.MASTER_ID, master_id);
        intent.putExtra(AppConstants.PARENT_ID, parent_id);
        intent.putExtra(AppConstants.SELECTED_OBJECT, logistic_label_sumbit);
        startActivityForResult(intent, master_id);
//        ((BaseActivity) requireActivity()).OnLookupClick(master_id, parent_id, null, title);
    }

    protected void OnJobNoLookupClick(String key, int master_id, int parent_id, INBOUND_HEADER[] inbound_header) {
        Intent intent = new Intent(getActivity(), SearchActivity.class);
        intent.putExtra(String.valueOf(AppConstants.TITLE), key);
        intent.putExtra(AppConstants.MASTER_ID, master_id);
        intent.putExtra(AppConstants.PARENT_ID, parent_id);
        intent.putExtra(AppConstants.SELECTED_OBJECT, inbound_header);
        startActivityForResult(intent, master_id);
//        ((BaseActivity) requireActivity()).OnLookupClick(master_id, parent_id, null, title);
    }

    protected void OnPRNLookupClick(String key, int master_id, int parent_id) {
        Intent intent = new Intent(getActivity(), SearchPRNActivity.class);
        intent.putExtra(String.valueOf(AppConstants.TITLE), key);
        intent.putExtra(AppConstants.MASTER_ID, master_id);
        intent.putExtra(AppConstants.PARENT_ID, parent_id);
        startActivityForResult(intent, master_id);
//        ((BaseActivity) requireActivity()).OnLookupClick(master_id, parent_id, null, title);
    }

    protected void OnJOBLookupClick(String key, int master_id, int parent_id, BARCODE_FRMT prnCodeList) {
        Intent intent = new Intent(getActivity(), SearchJOBActivity.class);
        intent.putExtra(String.valueOf(AppConstants.TITLE), key);
        intent.putExtra(AppConstants.MASTER_ID, master_id);
        intent.putExtra(AppConstants.PARENT_ID, parent_id);
        intent.putExtra(AppConstants.SELECTED_OBJECT, prnCodeList);
        startActivityForResult(intent, master_id);
//        ((BaseActivity) requireActivity()).OnLookupClick(master_id, parent_id, null, title);
    }

    public void ringtone(){
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getContext(), notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
