package com.dcode.gacinventory;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;

import com.dcode.gacinventory.common.AppPreferences;
import com.dcode.gacinventory.common.AppVariables;
import com.dcode.gacinventory.common.GACDynFuncs;
import com.dcode.gacinventory.common.TypefaceUtil;
import com.dcode.gacinventory.data.DatabaseClient;
import com.dcode.gacinventory.data.MySettings;
import com.dcode.gacinventory.data.model.COMPANY;
import com.dcode.gacinventory.data.model.LOCATIONS;
import com.dcode.gacinventory.data.model.USERS;
import com.dcode.gacinventory.data.model.WAREHOUSE;
import com.dcode.gacinventory.network.NetworkClient;
import com.dcode.gacinventory.network.NetworkClientOAuth;
import com.dcode.gacinventory.network.NetworkClientUploadPhoto;
import com.example.gacinventory.R;

import java.util.List;


//import io.github.inflationx.calligraphy3.CalligraphyConfig;
//import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
//import io.github.inflationx.viewpump.ViewPump;

//import com.facebook.stetho.Stetho;

public class App extends Application {
    public static String TAG = "ERR_TAG";
    public static String DeviceID = "12345";
    public static String DeviceIP = "10.0.2.1";
    public static String PrinterMAcId = "";  //00:06:66:65:18:12
    public static String base64CompanyLogoPng = "";
    public static String PrinterCode = "PB51";
    public static String jsonCmdAttribStr = "";
    public static USERS currentUser;
    public static LOCATIONS currentLocation;
    public static COMPANY currentCompany;
    public static WAREHOUSE currentWarehouse;
    public static long gr_hdr_id = -1;
    public static long ls_hdr_id = -1;
    public static long pl_hdr_id = -1;
    public static long sr_hdr_id = -1;
    public static String BarCodeSeparator = ":";
    public static String BarCodeSeparatorDollor = "\\$";
    public static List<GACDynFuncs> dynFuncs;
    public static String SSBarCodeSeparator = ":";
    public static String SSBarCodelINESeparator = ",";
    public static boolean putawayMixtypeKey = false;
    public static String putawayMixtypeJob = "";

    private static Context context;

    public static NetworkClient getNetworkClient() {
        return NetworkClient.getInstance();
    }

    public static NetworkClientUploadPhoto getNetworkClientUploadPhoto() {
        return NetworkClientUploadPhoto.getInstance();
    }

    public static NetworkClientOAuth getNetworkClientOAuth() {
        return NetworkClientOAuth.getInstance();
    }
//
    public static DatabaseClient getDatabaseClient() {
        return DatabaseClient.getInstance(context);
    }
//
    public static MySettings appSettings() {
        return MySettings.getInstance();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;

        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "font/Lato-Regular.ttf"); // font from assets: "assets/fonts/Roboto-Regular.ttf

//        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
//                .setDefaultFontPath("font/Lato-Regular.ttf")
//                .build()
//        );

//        ViewPump.init(ViewPump.builder()
//                .addInterceptor(new CalligraphyInterceptor(
//                        new CalligraphyConfig.Builder()
//                                .setDefaultFontPath("font/dubai-regular.ttf")
//                                .setFontAttrId(R.attr.fontPath)
//                                .build()))
//                .build());

//        DeviceIP = DeviceNetworkInfo.getIPAddress(true);
//        DeviceID = AppPreferences.getValue(context, AppPreferences.DEV_UNIQUE_ID, true);
//        PrinterMAcId = AppPreferences.getValue(context, AppPreferences.PRINTER_MAC_ID, false);
//



        //Stetho.initializeWithDefaults(context);
        new AppVariables(context);

        String ServiceURL = AppPreferences.getValue(context, AppPreferences.SERVICE_URL, false);
        if (ServiceURL == null || ServiceURL.length() <= 0) {
            appSettings().setBaseUrl(getString(R.string.default_url));
        } else {
            appSettings().setBaseUrl(ServiceURL);
        }

        appSettings().setFileUploadUrl(getString(R.string.file_upload_url));
        appSettings().setSUBSCRIPTION_KEY(getString(R.string.subscription_key));

        String defaultPutaway = AppVariables.getDefaultPutaway();
        if (defaultPutaway == null || defaultPutaway.length() <= 0) {
            AppPreferences.SetValue(context, AppPreferences.DEFAULT_PUTAWAY, "true");
        }
    }


    public static void resetApp(){
        com.dcode.gacinventory.App.currentLocation = null;
        com.dcode.gacinventory.App.currentCompany = null;
        com.dcode.gacinventory.App.currentUser = null;
        com.dcode.gacinventory.App.currentWarehouse= null;
    }




}
